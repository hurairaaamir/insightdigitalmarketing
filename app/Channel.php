<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Instagram;
use App\Fakebook;

class Channel extends Model
{
    protected $fillable = ['name', 'user_id', 'channeltype_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function channeltype()
    {
        return $this->belongsTo(Channeltype::class);
    }

    public function instagram()
    {
        return $this->hasOne(Instagram::class);
    }

    public function facebook()
    {
        return $this->hasOne(Fakebook::class);
    }

    public function youtube()
    {
        return $this->hasOne(Youtube::class);
    }
}
