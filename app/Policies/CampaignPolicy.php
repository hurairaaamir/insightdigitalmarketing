<?php

namespace App\Policies;

use App\User;
use App\Campaign;
use Illuminate\Auth\Access\HandlesAuthorization;

class CampaignPolicy
{
    use HandlesAuthorization;
    


    /**
     * Determine whether the user can update the campaign.
     *
     * @param  \App\User  $user
     * @param  \App\Campaign  $campaign
     * @return mixed
     */
    public function Influencer(User $user, Campaign $campaign)
    {
        
    }

    public function brand(User $user, Campaign $campaign)
    {
        return $user->id  == $campaign->user_id;
    }
}
