<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'company_name', 'website', 'address_supplement', 'first_name', 'last_name', 'gender', 'telephone',
        'country', 'city', 'street_house', 'zip', 'profile_image', 'brand_id', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
