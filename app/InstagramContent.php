<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramContent extends Model
{
    protected $fillable=['content_url','campaign_name','offer_id','user_id','campaign_id'];
}
