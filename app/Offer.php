<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Profile;


class Offer extends Model
{
    protected $fillable=['user_id','campaign_id','details','budget','fee','status'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }

    public function offer_channels(){
        return $this->hasMany(OfferChannel::class);
    }

    public function youtube_content(){
        return $this->hasMany(YoutubeContent::class);
    }

    public function facebook_content(){
        return $this->hasMany(FacebookContent::class);
    }

    public function instagram_content(){
        return $this->hasMany(InstagramContent::class);
    }
}
