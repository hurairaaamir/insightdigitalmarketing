<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrossPromotion extends Model
{
    protected $fillable =["title",'details','date_range','subscribers','user_id'];

    public function channels(){
        return $this->hasMany(CrossPromotionChannel::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

}