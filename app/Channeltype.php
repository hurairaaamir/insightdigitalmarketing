<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channeltype extends Model
{
    protected $fillable = ['name'];

    public function profiles()
    {
        return $this->belongsToMany(Profile::class);
    }

    public function channel()
    {
        return $this->hasMany(Channel::class);
    }
}
