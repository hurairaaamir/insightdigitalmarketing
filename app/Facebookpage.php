<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facebookpage extends Model
{
    protected $fillable=['name','page_id','category','permalink','fan_count','fakebook_id','posts'];
}

