<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrossPromotionChannel extends Model
{
    protected $fillable =['name','cross_promotion_id'];

    public function cross_promotions(){
        return $this->belongsTo(CrossPromotion::class);
    }
}
