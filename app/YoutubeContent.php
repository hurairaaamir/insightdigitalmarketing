<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YoutubeContent extends Model
{
    protected $fillable=['video_url','campaign_name','offer_id','user_id','campaign_id'];
}
