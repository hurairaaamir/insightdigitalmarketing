<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SocialCategory;
use App\Campaignimage;
use App\Offer;

class Campaign extends Model
{
    protected $fillable=['image','title','upper_cost','lower_cost','user_id','description','daterange'];

    public function social_categories(){
        return $this->belongsToMany(SocialCategory::class);
    }

    public function otherImg(){
        return $this->hasMany(Campaignimage::class);
    }

    public function offers(){
        return $this->hasMany(Offer::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function youtubeContents(){
        return $this->hasMany(YoutubeContent::class);
    }
    public function facebookContents(){
        return $this->hasMany(FacebookContent::class);
    }
    public function instagramContents(){
        return $this->hasMany(InstagramContent::class);
    }
    
}
