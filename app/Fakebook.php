<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fakebook extends Model
{
    protected $fillable=['facebook_id','name','image','user_id','channel_id'];

    public function pages(){
        return $this->hasMany(Facebookpage::class);
    }
}
