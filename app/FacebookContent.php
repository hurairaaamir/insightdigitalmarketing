<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookContent extends Model
{
    protected $fillable=['content_url','campaign_name','offer_id','user_id','campaign_id'];

}
