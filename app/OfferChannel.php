<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferChannel extends Model
{
    protected $fillable=['channel','budget','offer_id'];
}
