<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    protected $fillable = [
        'email', 'mobile', 'company_name', 'cost_center', 'fax', 'address_supplement', 'first_name', 'last_name', 'gender', 'telephone',
        'country', 'city', 'street_house', 'zip', 'vat_number', 'brand_id', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
