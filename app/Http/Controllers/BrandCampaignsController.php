<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Campaign;
use App\SocialCategory;
use App\Campaignimage;
use App\Offer;

class BrandCampaignsController extends Controller
{
    public function index()
    {
        $campaigns=Campaign::where('user_id',auth()->id())->get();

        return view('brandProfile.campaigns.index',[
            "campaigns" => $campaigns
        ]);
    }

    // $socials
    public function create()
    {
        // $socials=SocialCategory::all();
        $socials=session('socials');

        return view('brandProfile.campaigns.create',[
            "socials"=>$socials
        ]);
    }

    public function select_social_store(Request $request)
    {
        $messages = [
            'required' => 'Select atleast one Social Media before continuing',
        ];
        $rules=[
            "socials"=>'required'
        ];

        $validator=Validator::make($request->all(), $rules, $messages)->validate();
        
        $socials=explode(',',$request->socials);
   

        $socials=SocialCategory::whereIn('name',$socials)->get();

        session(['socials' => $socials]);

        return redirect('/brand/campaigns/create');

        return route('create_campaign', ['socials' => $socialS]);

    }


    public function select_social(Request $request){

        return view('brandProfile.campaigns.select-social');

    }

    public function store(Request $request)
    {
        $request->validate([
            "title"=>'required|min:3|max:50',
            "description"=>'required|min:40',
            "cost_range"=>"required",
            "image1"=>'required|image|mimes:jpg,png,jpeg|max:4000',
            "image2"=>'image|mimes:jpg,png,jpeg|max:4000',
            "image3"=>'image|mimes:jpg,png,jpeg|max:4000',
            "socials"=>'required',
            'daterange'=>'required'
        ]);

        $cost_range=explode(';',$request->cost_range);
        $lower_cost=$cost_range[0];
        $upper_cost=$cost_range[1];
        
        $image1=$this->storeImage($request->image1);

        if($request->image2){
            $image2=$this->storeImage($request->image2);
        }

        if($request->image3){
            $image3=$this->storeImage($request->image3);
        }

        $campaign=Campaign::create([
            'user_id'=>auth()->id(),
            "title"=> $request->title,
            "description"=> $request->description,
            "lower_cost"=> $lower_cost,
            "upper_cost"=> $upper_cost,
            "image"=> $image1,
            "daterange"=>$request->daterange
        ]);

        if($request->image2){
            CampaignImage::create([
                "campaign_id"=>$campaign->id,
                "image"=>$image2
            ]);
        }
        if($request->image3){
            CampaignImage::create([
                "campaign_id"=>$campaign->id,
                "image"=>$image3
            ]);
        }
        
        $campaign->social_categories()->attach($request->socials);
        
        return redirect('/brand/campaigns/success');
    }
    protected function storeImage($image){
        
        $image_name = uniqid().'.'. $image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/compaigns/';
        $image->move($destinationPath, $image_name);
        $path1='/images/compaigns/'.$image_name;

        return $path1;
    }

    public function show($id){

        $campaign = Campaign::findOrFail($id);
        
        $this->authorize('brand', $campaign);

        return view('brandProfile.campaigns.show',[
            "campaign"=>$campaign
        ]);
    }

    public function update(Request $request){

    }

    public function edit($id){
        $campaign = Campaign::findOrFail($id);
        
        $this->authorize('brand', $campaign);

        $socials=SocialCategory::all();
        
        return view('brandProfile.campaign.edit',[
            "campaign"=>$campaign,
            "socials"=>$socials
        ]);
    }

    public function destroy($id){

    }


    protected function getImage($social)
    {
        switch($social){
            case 'facebook':
                return '/images/socials/facebook.jpg';
            break;
            case 'instagram':
                return '/images/socials/instagram.jpg';
            break;
            case 'youtube':
                return '/images/socials/youtube.png';
            break;
        }
    }

    public function success(){
        return view('brandProfile.flashPages.campaign_success'); 
    }

    public function payment($offer_id){
        $offer=Offer::findOrFail($offer_id);

        $payment=$offer->budget + $offer->fee;
        $campaign_id=$offer->campaign->id;
        $offer_id=$offer->id;
        
        return view('brandProfile.flashPages.payment',[
            "payment"=>$payment,
            "campaign_id"=>$campaign_id,
            "offer_id"=>$offer_id
        ]); 
    }
}
