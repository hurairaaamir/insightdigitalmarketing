<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use App\OfferChannel;
class OfferController extends Controller
{
    
    public function index()
    {
        $offers=Offer::where('user_id',auth()->id())->get();

        return view('influencerProfile.offers',[
            "offers"=>$offers
        ]);
    }
    public function store(Request $request)
    {
        $request->validate([
            "campaign_id"=>'required',
            'budget'=>'required',
            'fee'=>'required',
            'details'=>'required|min:200'
        ]);
        $status='In Process';
        $offer=Offer::create([
            "campaign_id"=>$request->campaign_id,
            'budget'=>$request->budget,
            'details'=>$request->details,
            'fee'=>$request->fee,
            'user_id'=>auth()->id(),
            'status'=>$status
        ]);

        return response()->json([
            "data"=>$offer
        ]);

    }
    public function update($id,Request $request)
    {
        dd($id);
    }

    public function updateOffer($id,$status)
    {
        $offer=Offer::findOrFail($id);

        $offer->update([
            "status"=>$status
        ]);

        return response()->json([
            "data"=>"status is update"
        ]);
    }

    public function channel(Request $request)
    {
        $data=OfferChannel::create([
            "channel"=>$request->channel,
            "budget"=>$request->budget,
            'offer_id'=>$request->offer_id
        ]);

        return response()->json([
            "data"=>$data
        ]);
    }

    public function show($id)
    {
        $selected_offer=Offer::findOrFail($id);
        $data=view('cardData.offerCard',[
            "selected_offer"=>$selected_offer
        ])->render();
        return response()->json([
            "data"=>$data
        ]);
    }

    public function campaign_offer($id)
    {
        $selected_offer=Offer::findOrFail($id);
        $data=view('cardData.completeProfile',[
            "selected_offer"=>$selected_offer
        ])->render();
        return response()->json([
            "data"=>$data
        ]);
    }

    public function all_campaign_offer($id){
        
        $offers=Offer::where('campaign_id','=',$id)->get();

        $data=view('cardData.allOffers',[
            "offers"=>$offers
        ])->render();

        return response()->json([
            "data"=>$data
        ]);
    }

    public function calendar(){    
        
        $offers=Offer::where('user_id',auth()->id())->get();
        
        $datas=array();
        $current;
        foreach($offers as $key=>$offer){

            if($offer->status=='Accepted'){
                $date=explode('-',$offer->campaign->daterange);
                $date=explode('/',$date[1]);
                $date=trim($date[2]).'-'.trim($date[0]).'-'.trim($date[1]);

                $item=[
                'title'=>substr($offer->campaign->title,0,13).'...',
                'start'=>$date,
                'textColor'=>'white'
                ];
                array_push($datas,$item);
                if($key==0){
                    $current=$date;
                }
            }
        }
        if(!isset($current)){
            dd('');
        }

        return response()->json([
            "data"=>$datas,
            "current"=>$current
        ]);
    
    }
}
