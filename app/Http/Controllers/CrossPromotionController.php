<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CrossPromotion;
use App\CrossPromotionChannel;
use App\SocialCategory;
use App\Channel;

class CrossPromotionController extends Controller
{
    public function index(){
        $socials=SocialCategory::all();

        $promotions=CrossPromotion::where('id','>',0)->limit(6)->get();

        $channels=Channel::where('user_id','=',auth()->id())->pluck('name')->toArray();

        return view('influencerProfile.crossPromotion.index',[
            "socials"=>$socials,
            'promotions'=>$promotions,
            'channels'=>$channels
        ]);
    }

    public function create(){
        return view('influencerProfile.crossPromotion.create');
    }

    public function store(Request $request){
        
        $socials=explode(',',$request->socials);

        request()->validate([
            'subscribers' => 'required',
            'date_range' => 'required',
            'title' => 'required',
            'details' => 'required',
        ]);

        $cross_promotion=CrossPromotion::create([
            'subscribers' => $request->subscribers,
            'date_range' => $request->date_range,
            'title' => $request->title,
            'details' => $request->details,
            'user_id'=>auth()->id()
        ]); 

        foreach($socials as $social){
            CrossPromotionChannel::create([
                "name"=>$social,
                "cross_promotion_id"=>$cross_promotion->id
            ]);
        }

        return redirect()->back();
    }

    public function show($id){
        $selected_promotion=CrossPromotion::findOrFail($id);
        // dd($selected_promotion);
        $view = view('cardData.completeCrossPromotion',[
            "selected_promotion"=>$selected_promotion
        ])->render();

        return response()->json([
            "data"=>$view
        ]);
    }

    public function loadmore(Request $request){
        $social=$request->social;

        if(($social=='all'|| $social==null))
        {
            $promotions = CrossPromotion::where('id','>',0)
            ->orderBy('id','desc')
            ->paginate(6);

        }else if($social!='all' || $social != null){
            $promotions = CrossPromotion::whereHas('channels', function($query) use($social) {
                $query->where('cross_promotion_channels.name', $social);
            })->paginate(6);
        }
        
        $channels=Channel::where('user_id','=',auth()->id())->pluck('name')->toArray();

        $data=view('cardData.CrossPromotion',[
            "channels"=>$channels,
            'promotions'=>$promotions
        ])->render();
        

        return response()->json([
            "data"=>$data
        ]);

    }
}
