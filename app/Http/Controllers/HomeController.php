<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use App\SocialCategory;
use App\Channel;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function Influencer(){
        $offers=Offer::where('user_id',auth()->id())->get();
        $inProgress=Offer::where('user_id',auth()->id())->where('status','In Process')->count();
        $completed=Offer::where('user_id',auth()->id())->where('status','Completed')->count();
        $rejected=Offer::where('user_id',auth()->id())->where('status','Rejected')->count();
        $Accepted=Offer::where('user_id',auth()->id())->where('status','Accepted')->count();
        $socials=SocialCategory::all();
        $channels=Channel::where('user_id','=',auth()->id())->select('name')->get();
        $array=[];
        foreach($channels as $channel){
            array_push($array,$channel->name);
        }
        // dd(in_array('facebook',$array));
        // dd($array);
        // array_push()
        
        return view('influencerProfile.dashboard',[
            "offers"=> $offers,
            'rejected'=>$rejected,
            'Accepted'=>$Accepted,
            'completed'=>$completed,
            'inProgress'=>$inProgress,
            'socials'=>$socials,
            'channels'=>$array
        ]);

    }
}
