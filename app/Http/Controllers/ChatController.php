<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;

class ChatController extends Controller
{
    public function index(){
        $chats=Chat::all();

        return view('admin.chat.index',[
            "chats"=>$chats
        ]);
    }
    public function store(Request $request){
        $request->validate([
            "email"=>'required|email',
            "message"=>'required'
        ]);

        if(isset($request->user_id)){
            $chat=Chat::create([
                "message"=>$request->message,
                "email"=>$request->email,
                "user_id"=>auth()->id()
            ]);
            $chat->user;
            $chat->user->profile;
        }else{
            $chat=Chat::create([
                "message"=>$request->message,
                "email"=>$request->email,
            ]);
        }
        return response()->json([
            "data"=>$chat
        ]);
    }
    public function destroy($id){
        $chat=Chat::findOrFail($id);

        $chat->delete();

        return redirect()->back()->with('Chat Deleted!');
    }

    public function mass_destroy(){
        Chat::whereIn('id', request('ids'))->delete();

        return redirect()->back()->with('Chat Deleted!');
    }
}
