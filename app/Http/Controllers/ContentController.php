<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offer;
use App\YoutubeContent;
use App\InstagramContent;
use App\FacebookContent;
use App\Campaign;
use App\Fakebook;
use App\Facebookpage;
use Socialite;
use GuzzleHttp\Client;
use Facebook\Facebook;

class ContentController extends Controller
{
    public function index($type){
        $offers=Offer::where('status','Accepted')->where('user_id',auth()->id())->get();
        
        switch($type){
            case 'facebook':
                return view('influencerProfile.content.facebook',[
                    "offers"=>$offers,
                    "type"=>"facebook"
                ]);
            break;
            case 'instagram':
                return view('influencerProfile.content.instagram',[
                    "offers"=>$offers,
                    "type"=>"instagram"
                ]);
            break;
            case 'youtube':
                return view('influencerProfile.content.youtube',[
                    "offers"=>$offers,
                    "type"=>"youtube"
                ]);
            break;
            default:
                return view('influencerProfile.content.youtube',[
                    "offers"=>$offers,
                    "type"=>"youtube"
                ]);
            break;
        }
    }

    public function youtube(Request $request){
        // dd($request->all());
        $videos=$request->selectedVideos;
        
        $offer=Offer::findOrFail($request->offer_id);
        $data=[];
        foreach($videos as $video){
            $tube=YoutubeContent::create([
                "offer_id"=>$offer->id,
                "campaign_name"=>$offer->campaign->title,
                "video_url"=>$video,
                "user_id"=>auth()->id(),
                'campaign_id'=>$offer->campaign->id
            ]);
            array_push($data,$tube);  
        }
        return response()->json([
            "data"=>$tube
        ]);

    }

    public function campaign($id,$type){  
        $offers=Offer::where('campaign_id',$id)->where('user_id',auth()->id())->where('status','Accepted')->get();
        
        $view= view('cardData.content.influencerContent',[
            "offers"=>$offers,
            "type"=>$type
        ])->render();

        return response()->json([
            "data"=>$view
        ]);
    }

    public function instagram(Facebook $fb,Request $request){

        $access_token= $request->access_token;
        // dump($access_token);
        $fb->setDefaultAccessToken($access_token);

        $contents = $fb->get('me/accounts');
        $contents=$contents->getBody();
        $contents=json_decode($contents);
        $contents=$contents->data;
        $insta_id='';

        // dump($contents);
        foreach($contents as $content){
            // dump($content);
            $page= $fb->get($content->id.'?fields=instagram_business_account');
            $page=$page->getBody();
            $page=json_decode($page);

            if($page->instagram_business_account->id){
                $insta_id=$page->instagram_business_account->id;
                break;
            }
        }
        $medias;

        if($insta_id){
            // dump($insta_id);
            // $profile=$fb->get($insta_id.'?fields=biography,media_count,username,profile_picture_url,followers_count,website');
            $profile=$fb->get($insta_id.'/media');

            $profile=$profile->getBody();

            $media=json_decode($profile);

            $medias=$media->data;
            // dd($medias);
            
        }

        $all_data=[];
        foreach($medias as $media){
        // dump($medias);
            // dump($media);
            $profile=$fb->get($media->id.'?fields=id,media_type,media_url,username,timestamp,permalink,thumbnail_url,caption');    
            
            $profile=$profile->getBody();

            $data=json_decode($profile);

            array_push($all_data,$data);
        }
        // $view=view('cardData.content.instagramContent',[
        //     "instagrams"=>$all_data
        // ])->render();

        return response()->json([
            "data"=>$all_data
        ]);
    }

    public function instagram_redirect(Request $request){
        $data=$request->data;
        $id=$request->id;
        $offer_id=$request->offer_id;
        $offer=Offer::findOrFail($offer_id);
        $offers=Offer::where('campaign_id',$offer->id)->where('user_id',auth()->id())->where('status','Accepted')->get();



        return view('influencerProfile.content.instagramRedirect',[
            "instagrams"=>$data,
            "offer"=>$offer,
            "type"=>"instagram",
            "offers"=>$offers
        ]);
    }

    public function instagram_store(Request $request){

        $offer=Offer::findOrFail($request->offer_id);

        $contents=$request->contents;

        foreach($contents as $content){
            InstagramContent::create([
                'content_url'=>$content,
                'campaign_name'=>$offer->campaign->title,
                'offer_id'=>$offer->id,
                'user_id'=>auth()->id(),
                'campaign_id'=>$offer->campaign->id
            ]);
        }

        return response()->json([
            "data"=>"stored"
        ]);
    }

    public function facebook(Facebook $fb,Request $request)
    {
        $access_token= $request->access_token;
        $fb->setDefaultAccessToken($access_token);

        
        $page_id=$request->page_id;
        $page = $fb->get($page_id.'?fields=posts.limit(20){picture,permalink_url},id,link,fan_count,name,new_like_count');
        $page = $page->getBody();
        $page = json_decode($page);
        $posts=$page->posts->data;

        
        return response()->json([
            "data"=>$posts
        ]);
         
    }
    public function facebook_redirect(Request $request){
        $data=$request->data;

        $offer_id=$request->offer_id;
        $offer=Offer::findOrFail($offer_id);
        $offers=Offer::where('campaign_id',$offer->id)->where('user_id',auth()->id())->where('status','Accepted')->get();



        return view('influencerProfile.content.facebookRedirect',[
            "posts"=>$data,
            "offer"=>$offer,
            "type"=>"facebook",
            "offers"=>$offers
        ]);
    }
    public function facebook_store(Request $request){
        $offer=Offer::findOrFail($request->offer_id);

        $contents=$request->contents;

        foreach($contents as $content){
            FacebookContent::create([
                'content_url'=>$content,
                'campaign_name'=>$offer->campaign->title,
                'offer_id'=>$offer->id,
                'user_id'=>auth()->id(),
                'campaign_id'=>$offer->campaign->id
            ]);
        }

        return response()->json([
            "data"=>"stored"
        ]);
    }
}
