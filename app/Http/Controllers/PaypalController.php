<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paypal;
use App\User;
use App\Notifications\AdminPayment;
use App\Notifications\CompanyPayment;
use App\Campaign;
use App\Offer;


class PaypalController extends Controller
{
    public function store(Request $request){
        $data=$request->details;
        $campaign_id=$request->campaign_id;
        $offer_id=$request->offer_id;

        $paypal=Paypal::create([
            "trans_id"=>$data['id'],
            "status"=>$data['status'],
            "payer_id"=>$data['payer']['payer_id'],
            "payer_email"=>$data['payer']['email_address'],
            "payer_address"=>$data["payer"]['address']['country_code'],
            "payer_firstname"=>$data['payer']['name']['given_name'],
            "payer_surname"=>$data['payer']['name']['surname'],
            "amount"=>$data['purchase_units'][0]['amount']['value'],
            "currency_code"=>$data['purchase_units'][0]['amount']['currency_code'],
            "campaign_id"=>$campaign_id,
            "offer_id"=>$offer_id
        ]);
        
        $this->send_notification($campaign_id,$offer_id);
        $this->update_offer($offer_id);
        return response()->json([
            "data"=>"Amount sent successfully"
        ]);

    }
    private function update_offer($offer_id){
        $offer=Offer::findOrFail($offer_id);

        $offer->update([
            "status"=>"Accepted"
        ]);

    }
    
    public function send_notification($campaign_id,$offer_id){
        $campaign=Campaign::findOrFail($campaign_id);
        $offer=Offer::findOrFail($offer_id);

        
        $admin=User::where('role','admin')->first();
        $company=$campaign->user;
        $influencer=$offer->user;

        $title="From Admin";
        $image=$admin->profile->image;
        $description="payment has been sent successfully and you will recieve notication and mails about the progress of the Campaign";
        $link="";
        $company->notify(new CompanyPayment($title,$description,$image,$link));

        $title="From Company";
        $image=$company->profile->image;
        $description="A Company name ".$company->name." has accepted the offer by influecer ".$influencer->name." on campaign titled ".$campaign->title;
        $link="";

        $admin->notify(new CompanyPayment($title,$description,$image,$link));

        $title="From Campaign";
        $image=$company->profile->image;
        $description="A Company name ".$company->name." has accepted your offer you will recieve the payment as soon as you complete your campaign content";
        $link="";

        $admin->notify(new CompanyPayment($title,$description,$image,$link));
    }

    public function index(){
        $paypals=Paypal::all();

        return view('admin.paypal.payment',[
            "paypals"=>$paypals
        ]);
    }
    public function destroy($id){
        $paypal=findOrFail($id);
        $paypal->delete();
        return redirect()->back();
    }



    
}
