<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Profile;
use App\SocialCategory;
use App\Channeltype;

class BrandInfluencerController extends Controller
{
    public function index()
    {
        $users = User::where('role', 'influencer')->limit(9)->get();
        $types = Channeltype::all();
        $categories = SocialCategory::all();
        return view('brandProfile.influencer.influencer', [
            'users' => $users,
            'categories' => $categories,
            'types' => $types
        ]);
    }

    public function load_more_data($id)
    {
        $users = User::where('role', 'influencer')->where('id', '>', $id)->limit(6)->get();

        $view = view('cardData.influencerCard', [
            "users" => $users,
        ])->render();

        return response()->json([
            'data' => $view,
        ]);
    }

    public function execute_search($social, Request $request)
    {
        // if ($request->ajax()) {
        //     return response()->json([
        //         'data' => $request->all()
        //     ]);
        // }
        $type = $request->type;
        $min = (int)$request->min;
        $max = (int)$request->max;
        if ($min == null) $min = 0;
        if ($max == null) $max = 100000000000;

        $gender = $request->gender;

        if ($social == 'all' && $min == 0 && $gender == '' && $max == 100000000000 && $type == '') {
            $users = $this->byNone();
        } else if ($social != 'all' && $min == 0 && $gender == '' && $max == 100000000000 && $type == '') {
            $users = $this->bySocial($social);
        }
        else if ($social == 'all' && $min == 0 && $gender != '' && $max == 100000000000 && $type == '') {
            $users = $this->byGender($gender);
        }
        else if ($min != 0 && $gender == '' && $max == 100000000000 && $type == '') {
            $users = $this->byFollowers($social, $min, $max);
        }
        else if ($min == 0 && $gender == '' && $max != 100000000000 && $type == '') {
            $users = $this->byFollowers($social, $min, $max);
        }
        else if ($min != 0 && $gender == '' && $max != 100000000000 && $type == '') {
            $users = $this->byFollowers($social, $min, $max);
        }
        else if ($social == 'all' && $min == 0 && $gender == '' && $max == 100000000000 && $type != '') {
            $users = $this->byChannelType($type);
        }
        else if ($min != 0 && $gender != '' && $max != 100000000000 && $type != '') {
            $users = $this->byAll($social, $min, $max, $gender, $type);
        }
        else {
            $users = $this->byNone();
        }

        // if ($request->ajax()) {
        //     return response()->json([
        //         'users' => $users
        //     ]);
        // }


        if (count($users) == 0) {
            return response()->json([
                'data' => '<div class="center-out no-data card" >
                            <i class="fas fa-search"></i>
                            <h3>No Data Found For this Search</h3>
                        </div>',
            ]);
        }
        $view = view('cardData.influencerCard', [
            "users" => $users,
        ])->render();

        return response()->json([
            'data' => $view,
        ]);
    }
    protected function byNone()
    {
        $users = User::where('role', 'influencer')->limit(9)->get();

        return $users;
    }
    protected function bySocial($social)
    {
        $users = User::where('role', 'influencer')->whereHas('channels', function ($query) use ($social) {
            $query->where('channels.name', $social);
        })->limit(9)->get();

        return $users;
    }
    protected function byGender($gender)
    {
        $users = User::where('role', 'influencer')->whereHas('profile', function ($query) use ($gender) {
            $query->where('profiles.gender', $gender);
        })->limit(9)->get();

        return $users;
    }

    protected function byFollowers($social, $min, $max)
    {


        if ($social == 'instagram') {
            $users = User::where('role', 'influencer')->whereHas('instagram', function ($query) use ($min, $max) {
                $query->where('instagrams.followers_count', '>=', $min)->where('instagrams.followers_count', '<=', $max);
            })->limit(9)->get();
        } else if ($social == 'youtube') {
            $users = User::where('role', 'influencer')->whereHas('youtube', function ($query) use ($min, $max) {
                $query->where('youtubes.subscribers', '>=', $min)->where('youtubes.subscribers', '<=', $max);
            })->limit(9)->get();
        }
        else { //If $social == 'all' we assume the followers as of facebook.

            $userIds = DB::table('users')
                ->join('fakebooks', 'users.id', '=', 'fakebooks.user_id')
                ->join('facebookpages', 'fakebooks.id', '=', 'facebookpages.fakebook_id')
                ->where('users.role', '=', 'influencer')
                ->where('facebookpages.fan_count', '>=', $min)
                ->where('facebookpages.fan_count', '<=', $max)
                ->select('fakebooks.user_id')
                ->limit(9)
                ->get();
            $users = array();
            for ($i = 0; $i < $userIds->count(); $i++) {
                $user = User::where('id', '=', $userIds[$i]->user_id)->first();
                array_push($users, $user);
            }
        }




        return $users;
    }

    protected function byChannelType($type)
    {
        $channeltype = Channeltype::where('name', $type)->first();
        $channeltype_id = $channeltype['id'];

        $users = User::where('role', 'influencer')->whereHas('channels', function ($query) use ($channeltype_id) {
            $query->where('channels.channeltype_id', $channeltype_id);
        })->limit(9)->get();

        return $users;
    }

    protected function byAll($social, $min, $max, $gender, $type)
    {
        $channeltype = Channeltype::where('name', $type)->first();
        $channeltype_id = $channeltype['id'];
        if ($social == 'youtube') {
            $userIds = DB::table('users')
                ->join('youtubes', 'users.id', '=', 'youtubes.user_id')
                ->join('channels', 'users.id', '=', 'channels.user_id')
                ->join('profiles', 'users.id', '=', 'profiles.user_id')
                ->where('users.role', '=', 'influencer')
                ->where('youtubes.subscribers', '>=', $min)
                ->where('youtubes.subscribers', '<=', $max)
                ->where('channels.channeltype_id', '=', $channeltype_id)
                ->where('profiles.gender', '=', $gender)
                ->select('youtubes.user_id')
                ->limit(9)
                ->get();
            $users = array();
            for ($i = 0; $i < $userIds->count(); $i++) {
                $user = User::where('id', '=', $userIds[$i]->user_id)->first();
                array_push($users, $user);
            }
        } else if ($social == 'instagram') {
            $userIds = DB::table('users')
                ->join('instagrams', 'users.id', '=', 'instagrams.user_id')
                ->join('channels', 'users.id', '=', 'channels.user_id')
                ->join('profiles', 'users.id', '=', 'profiles.user_id')
                ->where('users.role', '=', 'influencer')
                ->where('instagrams.followers_count', '>=', $min)
                ->where('instagrams.followers_count', '<=', $max)
                ->where('channels.channeltype_id', '=', $channeltype_id)
                ->where('profiles.gender', '=', $gender)
                ->select('instagrams.user_id')
                ->limit(9)
                ->get();
            $users = array();
            for ($i = 0; $i < $userIds->count(); $i++) {
                $user = User::where('id', '=', $userIds[$i]->user_id)->first();
                array_push($users, $user);
            }
        }
        else {
            $userIds = DB::table('users')
                ->join('fakebooks', 'users.id', '=', 'fakebooks.user_id')
                ->join('facebookpages', 'fakebooks.id', '=', 'facebookpages.fakebook_id')
                ->join('channels', 'users.id', '=', 'channels.user_id')
                ->join('profiles', 'users.id', '=', 'profiles.user_id')
                ->where('users.role', '=', 'influencer')
                ->where('facebookpages.fan_count', '>=', $min)
                ->where('facebookpages.fan_count', '<=', $max)
                ->where('channels.channeltype_id', '=', $channeltype_id)
                ->where('profiles.gender', '=', $gender)
                ->select('fakebooks.user_id')
                ->limit(9)
                ->get();
            $users = array();
            for ($i = 0; $i < $userIds->count(); $i++) {
                $user = User::where('id', '=', $userIds[$i]->user_id)->first();
                array_push($users, $user);
            }
        }


        return $users;
    }
}
