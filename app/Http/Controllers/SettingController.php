<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Setting;
use App\EmailNotificationsSetting;

use App\BillingAddress;
use App\User;
use App\Brand;


class SettingController extends Controller
{
    public function getSettingsAndCountries()
    {
        // Find active Brand
        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();

        // Get Settings For Active Brand
        $settings = Setting::where('user_id', auth()->id())->where('brand_id', $active_brand['id'])->with('user')->first();

        $countries = DB::table('countries')->get();

        // dd($settings);

        return response()->json([
            "settings" => $settings,
            "countries" => $countries
        ]);
    }

    public function postGeneral(Request $request)
    {
        // Find active Brand
        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();

        // Get Settings For Active Brand
        $settings = Setting::where('user_id', auth()->id())->where('brand_id', $active_brand['id'])->first();

        $settings->update([
            "first_name" => $request->first_name,
            "company_name" => $request->company_name,
            "website" => $request->website,
            "address_supplement" => $request->address_supplement,
            "gender" => $request->gender,
            "telephone" => $request->telephone,
            "last_name" => $request->last_name,
            "street_house" => $request->street_house,
            "zip" => $request->zip,
            "city" => $request->city,
            "country" => $request->country
        ]);

        return response()->json([
            "msg" => "Settings Saved Successfully",
        ]);
    }

    public function getBillingAddressSettingsDataAndCountries()
    {
        // Find active Brand
        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();

        // Get Billing Address Settings For Active Brand
        $billingAddressSettings = BillingAddress::where('user_id', auth()->id())->where('brand_id', $active_brand['id'])->first();

        $countries = DB::table('countries')->get();

        // dd($billingAddress);

        return response()->json([
            "billingAddressSettings" => $billingAddressSettings,
            "countries" => $countries
        ]);
    }

    public function postBillingAddress(Request $request)
    {
        // Find active Brand
        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();

        // Get Billing Address Settings For Active Brand
        $billingAddress = BillingAddress::where('user_id', auth()->id())->where('brand_id', $active_brand['id'])->first();

        $billingAddress->update([
            "first_name" => $request->first_name,
            "email" => $request->email,

            "company_name" => $request->company_name,
            "cost_center" => $request->cost_center,
            "fax" => $request->fax,
            "address_supplement" => $request->address_supplement,
            "gender" => $request->gender,
            "telephone" => $request->telephone,
            "mobile" => $request->mobile,
            "last_name" => $request->last_name,
            "street_house" => $request->street_house,
            "zip" => $request->zip,
            "vat_number" => $request->vat_number,
            "city" => $request->city,
            "country" => $request->country
        ]);

        return redirect()->back();
    }

    public function getProfileImage()
    {
        // Find active Brand
        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();

        // Get Settings For Active Brand
        $settings = Setting::where('user_id', auth()->id())->where('brand_id', $active_brand['id'])->first();
        return response()->json(['settings' => $settings]);
    }

    public function postProfile(Request $request)
    {
        $imageName = time() . '.' . $request->file->getClientOriginalExtension();
        $request->file->move(public_path('images/brand/profile/'), $imageName);

        // Find active Brand
        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();
        $active_brand->update([
            "profile_image" => $imageName
        ]);

        // Get Settings For Active Brand
        $settings = Setting::where('user_id', auth()->id())->where('brand_id', $active_brand['id'])->first();
        $settings->update([
            "profile_image" => $imageName
        ]);

        return response()->json(['success' => 'You have successfully upload file.']);
    }

    public function changePassword(Request $request)
    {
        $user = User::where('id', '=', auth()->id())->first();


        $check = Hash::check($request->currentPassword['_value'], $user->password);

        if (!$check) {
            return response()->json([
                'icon' => 'error',
                'title' => 'Invalid form entry. Please correct: Current password'
            ]);
        }

        $user->update([
            'password' => Hash::make($request->newPassword['_value'])
        ]);
        return response()->json([
            'icon' => 'success',
            'title' => 'Password has just been successfully changed'
        ]);
    }

    public function viewGeneralSettings()
    {
        $brands = Brand::where('user_id', auth()->id())->get();
        if (count($brands) >= 1) {
            $no_brand_found = false;
        } else {
            $no_brand_found = true;
        }
        return view('brandProfile.settings.index', [
            "no_brand_found" => $no_brand_found
        ]);
    }

    public function viewProfileImage()
    {
        $brands = Brand::where('user_id', auth()->id())->get();
        if (count($brands) >= 1) {
            $no_brand_found = false;
        } else {
            $no_brand_found = true;
        }
        return view('brandProfile.settings.profile', [
            "no_brand_found" => $no_brand_found
        ]);
    }

    public function viewBillingAddress()
    {
        $brands = Brand::where('user_id', auth()->id())->get();
        if (count($brands) >= 1) {
            $no_brand_found = false;
        } else {
            $no_brand_found = true;
        }
        return view('brandProfile.settings.billingAddress', [
            "no_brand_found" => $no_brand_found
        ]);
    }

    public function viewEmailNotificationsSettings()
    {
        $brands = Brand::where('user_id', auth()->id())->get();
        if (count($brands) >= 1) {
            $no_brand_found = false;
        } else {
            $no_brand_found = true;
        }
        return view('brandProfile.settings.emailNotifications', [
            "no_brand_found" => $no_brand_found
        ]);
    }

    public function getEmailNotificationsSettings()
    {
        // Find active Brand
        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();

        // Get Email Notifications Settings For Active Brand
        $emailNotificationsSettings = EmailNotificationsSetting::where('user_id', auth()->id())->where('brand_id', $active_brand['id'])->with('user')->first();

        return response()->json(['emailNotificationsSettings' => $emailNotificationsSettings]);
    }

    public function postEmailNotificationsSettings(Request $request)
    {
        // Find active Brand
        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();

        // Get Email Notifications Settings For Active Brand
        $emailNotificationsSettings = EmailNotificationsSetting::where('user_id', auth()->id())->where('brand_id', $active_brand['id'])->with('user')->first();

        $emailNotificationsSettings->update([
            "summary" => $request->summary,
            "newProposalForPlacementCampaign" => $request->newProposalForPlacementCampaign,
            "newParticipantForPerformanceCampaign" => $request->newParticipantForPerformanceCampaign,
            "newRequestForGiveawayCampaign" => $request->newRequestForGiveawayCampaign,
            "campaignContentPublished" => $request->campaignContentPublished,
            "newMessage" => $request->newMessage
        ]);

        return response()->json([
            "msg" => "Settings Saved Successfully",
        ]);
    }
}
