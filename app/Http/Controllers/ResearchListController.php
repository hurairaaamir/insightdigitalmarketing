<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ResearchList;

class ResearchListController extends Controller
{
    public function getAllLists(Request $request)
    {
        $lists = ResearchList::where('user_id', auth()->id())->get();

        if ($request->expectsJson()) {
            return response()->json([
                "lists" => $lists
            ]);
        }
    }

    public function createList(Request $request)
    {


        $request->validate([
            "listName" => 'required|min:3|max:50'
        ]);
        ResearchList::create([
            'name' => $request->listName,
            'user_id' => auth()->id()
        ]);

        return $request->listName;
    }

    public function updateList(Request $request)
    {

        $researchList = ResearchList::where('user_id', auth()->id())->where('id', $request->id)->first();

        $request->validate([
            "listName" => 'required|min:3|max:50'
        ]);

        $researchList->update([
            'name' => $request->listName
        ]);

        return $request->listName;
    }

    public function addInfluencerToList(Request $request)
    {

        $researchList = ResearchList::where('user_id', auth()->id())->where('id', $request->listId)->first();

        $researchList->update([
            'influencer_ids' => $researchList['influencer_ids'] . $request->influencerId . ','
        ]);

        return response()->json([
            "success" => "Influencer is Successfully added to the list"
        ]);
    }

    public function deleteList(Request $request, $id)
    {

        $researchList = ResearchList::where('user_id', auth()->id())->where('id', $id)->first();



        $researchList->delete();

        return $id;
    }

    public function showSingleList(Request $request, $id)
    {
        $list = ResearchList::where('user_id', auth()->id())->where('id', $id)->first();
        return view('brandProfile.influencer.list.single-list', [
            "list" => $list
        ]);
    }
}
