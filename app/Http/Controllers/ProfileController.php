<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Channeltype;
use App\User;
use App\Profile;
use App\Campaignimage;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function info()
    {
        $countries = DB::table('countries')->get();

        $channelType = Channeltype::all();

        return view('auth.info', [
            "countries" => $countries,
            "channelType" => $channelType,
        ]);
    }

    public function settings_general()
    {
        if (!auth()->user()->profile) {
            Profile::create([
                "user_id" => auth()->id()
            ]);
        }
        $profile = Profile::where('user_id', auth()->id())->first();


        if (!isset($profile->birthday)) {
            $profile->birthday = "1:1: ";
        }

        return view('influencerProfile.settings.general', [
            "profile" => $profile,
        ]);
    }

    public function  settings_addresses()
    {
        $profile = Profile::where('user_id', auth()->id())->first();

        $countries = DB::table('countries')->get();

        return view('influencerProfile.settings.addresses', [
            "profile" => $profile,
            "countries" => $countries
        ]);
    }

    public function addresses_store(Request $request)
    {

        $profile = Profile::where('user_id', auth()->id())->first();

        $profile->update([
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "street_house" => $request->street_house,
            "zip" => $request->zip,
            "city" => $request->city,
            "country" => $request->country
        ]);

        return redirect()->back();
    }



    public function settings_profile()
    {
        // dd('here');

        $profile = Profile::where('user_id', auth()->id())->first();
        $types = Channeltype::all();

        $checks = $profile->channeltypes;
        $checked = [];
        foreach ($checks as $check) {
            array_push($checked, $check->name);
        }
        return view('influencerProfile.settings.profile', [
            "profile" => $profile,
            "types" => $types,
            'checked' => $checked
        ]);
    }
    public function profile_store(Request $request)
    {

        $profile = Profile::where('user_id', auth()->id())->first();

        $profile->channeltypes()->detach();

        $profile->update([
            "multichannel" => $request->multichannel,
            "about_channel" => $request->about_channel,
        ]);

        $profile->channeltypes()->attach($request->channel_types);

        return redirect()->back();
    }

    public function payout_history(Request $request)
    {
        $profile = Profile::where('user_id', auth()->id())->first();
        // dd($profile);
        if (request()->path() == "brand/settings/notifications") {
            return view('brandProfile.settings.notifications', [
                "profile" => $profile
            ]);
        } else {
            return view('influencerProfile.settings.payout_history', [
                "profile" => $profile
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            "gender" => 'required',
            "country" => 'required',
            'channel_type' => 'required',
            'city' => 'required',
            "birthday" => '::'
        ]);

        Profile::create([
            "gender" => $request->gender,
            "country" => $request->country,
            "channel_type" => $request->channel_type,
            "city" => $request->city,
            "user_id" => auth()->id()
        ]);

        return redirect('/');
    }

    public function general_store(Request $request)
    {
        $birthday = $request->day . ':' . $request->month . ':' . $request->year;

        $profile = Profile::where('user_id', auth()->id())->first();

        $profile->update([
            "gender" => $request->gender,
            "birthday" => $birthday,
            "telephone" => $request->telephone,
        ]);

        return redirect()->back();
    }

    public function image()
    {
        $profile = Profile::where('user_id', auth()->id())->first();
    }

    public function uploadImage(Request $request)
    {

        $profile = Profile::where('user_id', auth()->id())->first();

        $old = $profile->image;
        $parts = explode('/', $old);

        if ($parts[3] != 'default.jpg') {
            $path = public_path($profile->image);
            @unlink($path);
        }

        $image = $request->image;
        $image_name = uniqid() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path() . '/images/profiles/';
        $image->move($destinationPath, $image_name);
        $path1 = '/images/profiles/' . $image_name;


        $profile->update([
            "image" => $path1,
        ]);

        return response()->json([
            "data" => $profile->image
        ]);
    }

    public function channels()
    {
        return view('influencerProfile.settings.lower_section.channels');
    }
    public function interests()
    {
        return view('influencerProfile.settings.lower_section.interests');
    }
    public function payouts()
    {
        $profile=Profile::where("user_id",auth()->id())->first();
        return view('influencerProfile.settings.lower_section.payouts',[
            "profile"=>$profile
        ]);
    }
    public function password()
    {
        return view('influencerProfile.settings.lower_section.password');
    }
    public function terminate_account()
    {
        return view('influencerProfile.settings.lower_section.terminate_account');
    }

    public function payment_method(Request $request){
        
        $profile=Profile::where('user_id',auth()->id())->first();

        if($request->payment_type == 'bank_transfer'){
            $request->validate([
                'Account_holder'=>"required",
                'IBAN'=>"required",
                'BIC'=>"required",
            ]);

            $profile->update([
                'Account_holder'=>$request->Account_holder,
                'IBAN'=>$request->IBAN,
                'BIC'=>$request->BIC,
                'paypal_email'=>"",
                'payment_type'=>$request->payment_type,
            ]);
        }else if($request->payment_type == 'paypal'){
            $request->validate([
                'paypal_email'=>"required|email"
            ]);
            $profile->update([
                'Account_holder'=>"",
                'IBAN'=>"",
                'BIC'=>"",
                'payment_type'=>$request->payment_type,
                'paypal_email'=>$request->paypal_email
            ]);
        }else{
            $profile->update([
                'Account_holder'=>$request->Account_holder,
                'IBAN'=>$request->IBAN,
                'BIC'=>$request->BIC,
                'paypal_email'=>$request->paypal_email,
                'payment_type'=>$request->payment_type,
            ]);
        }

        return redirect()->back();

    }

    public function change_password(Request $request){
        // dd($request->all());

        $request->validate([
            'current_password'=>["required"],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user=User::findOrFail(auth()->id());
        // dd(Hash::check($request->current_password,$user->password));
        if(Hash::check($request->current_password,$user->password)){
            $user->update([
                'password' => Hash::make($request->password)
            ]);
        }else{
        
            return redirect()->back()->with("error",[
                "current password is incorrect"
            ]);
        }
        return redirect()->back()->with("success",[
            "Password Updated Successfully!"
        ]);
    }
}
