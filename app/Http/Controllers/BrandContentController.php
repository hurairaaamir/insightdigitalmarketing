<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;

class BrandContentController extends Controller
{
    public function index($type,$campaign_id=null){
        $campaigns=Campaign::where('user_id',auth()->id())->get();
        // $selected_campaign=[];
        // if(isset($campaign_id)){
            $selected_campaign=Campaign::find($campaign_id);
            // if(!$selected_campaign){
            //     $selected_campaign=[];
            // }
        // }

        switch($type){
            case 'facebook':
                // if(isset($campaign_id)){
                    return view('brandProfile.content.facebook',[
                        "campaigns"=>$campaigns,
                        "selected_campaign"=>$selected_campaign,
                        "type"=>"facebook"
                    ]);
                // }else{
                //     return view('brandProfile.content.facebook',[
                //         "campaigns"=>$campaigns,
                //         "type"=>"facebook"
                //     ]);
                // }
            break;
            case 'instagram':
                // if(isset($campaign_id)){
                    return view('brandProfile.content.instagram',[
                        "campaigns"=>$campaigns,
                        "selected_campaign"=>$selected_campaign,
                        "type"=>"instagram"
                    ]);
                // }else{
                //     return view('brandProfile.content.instagram',[
                //         "campaigns"=>$campaigns,
                //         "type"=>"instagram"
                //     ]);
                // }
            break;
            case 'youtube':
                // if(isset($campaign_id)){
                    return view('brandProfile.content.youtube',[
                        "campaigns"=>$campaigns,
                        "selected_campaign"=>$selected_campaign,
                        "type"=>"youtube"
                    ]);
                // }else{
                //     return view('brandProfile.content.youtube',[
                //         "campaigns"=>$campaigns,
                //         "type"=>"youtube"
                //     ]);
                // }
            break;
            default:
                // if(isset($campaign_id)){
                    return view('brandProfile.content.youtube',[
                        "campaigns"=>$campaigns,
                        "selected_campaign"=>$selected_campaign,
                        "type"=>"youtube"
                    ]);
                // }else{
                //     return view('brandProfile.content.youtube',[
                //         "offers"=>$offers,
                //         "type"=>"youtube"
                //     ]);
                // }
            break;
        }

    }
    
}
