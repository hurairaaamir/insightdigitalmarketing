<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;
use App\Campaignimage;
use App\CampaignsSocials;
use App\Offer;
use App\SocialCategory;
use App\Http\Resources\CampaignsResource;
use Illuminate\Support\Facades\DB;

class CampaignController extends Controller
{
    public function index(Request $request)
    {
        $social=$request->social;
        $type=$request->type;
        $budget=$request->budget;

        if(($social=='all'|| $social==null)&&($budget == '1'||$budget == null)&&($type == 'all'||$type == null))
        {
            $campaigns = Campaign::where('id','>',0)
            ->orderBy('campaigns.created_at','desc')
            ->paginate(6);

        }
        else if(($social!='all')&&($budget == '1')&&($type == 'all'))
        {
            $campaigns=$this->bySocial($social);
        }
        else if(($social=='all')&&($budget != '1')&&($type == 'all'))
        {
            $campaigns=$this->byBudget($budget);
        }
        else if(($social=='all')&&($budget == '1')&&($type != 'all'))
        {
            $campaigns=$this->byType($type);
        }
        else if(($social!='all')&&($budget != '1')&&($type == 'all'))
        {
            $campaigns=$this->bySocialAndBudget($social,$budget);
        }

    	if ($request->ajax()){
    		$view = view('cardData.data',[
                "campaigns"=>$campaigns,
            ])->render();
            return response()->json(['campaigns'=>$view,
            ]);
        }

        $socials=SocialCategory::all();

        return view('influencerProfile.campaigns',[
            "socials"=>$socials,
            "campaigns"=>$campaigns,
        ]);
    }
    // SocialCategory
    // social_categories
    protected function bySocial($social){
        // $campaigns= Campaign::with('social_categories')->where('social_categories.name',$social)->paginate(6);
        // $campaigns= Campaign::all();
        $campaigns = Campaign::whereHas('social_categories', function($query) use($social) {
            $query->where('social_categories.name', $social);
        })->paginate(6);
    
       
        return $campaigns;
    }
    protected function byType(){

    }
    protected function byBudget($budget){
        $campaigns = Campaign::whereHas('social_categories', function($query) use($budget) {
            $query->where('campaigns.lower_cost','>=', $budget);
        })->paginate(6);

        // $campaigns = Campaign::where('campaigns.lower_cost','>=',$budget)
        //     ->paginate(6);

        return $campaigns;
    }
    protected function bySocialAndType(){

    }
    protected function bySocialAndBudget($social,$budget){

        $campaigns = Campaign::whereHas('social_categories', function($query) use($budget,$social) {
            $query->where('campaigns.lower_cost','>=', $budget)->where('social_categories.name',$social);
        })->paginate(6);

        return $campaigns;
    }
    protected function byTypeAndBudget(){

    }
    protected function byTypeBudgetSocial(){

    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function admin_index(){
        $campaigns = Campaign::where('id','>',0)
            ->orderBy('campaigns.created_at','desc')
            ->paginate(4);
        $total=Campaign::where('id','>',0)->count();
        // $latest=Campaign::where('created_at','')

        return view('admin.campaign.index',[
            "campaigns"=>$campaigns,
            'total'=>$total
        ]);
    }
    
    public function show($id)
    {
        // $campaign = Campaigns::join('campaigns_socials','campaigns_socials.campaign_id','=','campaigns.id')
        //     ->where('campaigns.id',$id)
        //     ->orderBy('campaigns.id','desc')
        //     ->select('campaigns.*')
        //     ->first();

        // $campaign=Campaigns::where('id','=',$id)->first();
        // CampaignsResource::collection()

        $campaign=Campaign::findOrFail($id);
        $campaign->social_categories;
        $campaign->otherImg;
        $user=auth()->user();
        $auth_channels=$user->channels;
        

        return response()->json([
            'campaign'=>$campaign,
            'channels'=>$auth_channels
        ]);
    }
    public function admin_loadmore(Request $request){
        $type=$request->type;
        if(!isset($type)){
            $campaigns = Campaign::where('id','>',0)
                ->orderBy('campaigns.created_at','desc')
                ->paginate(4);
        }else if($type=='latest'){
            $campaigns = Campaign::where('id','>',0)
                ->orderBy('campaigns.created_at','desc')
                ->paginate(4);
        }else if($type=='dead'){
            $campaigns = Campaign::where('campaigns.daterange','<',now())
                ->paginate(4);
        }else if($type=='old'){
            $campaigns = Campaign::where('id','>',0)
                ->orderBy('campaigns.created_at','asc')
                ->paginate(4);
        }
        $view = view('cardData.adminCampaign',[
            "campaigns"=>$campaigns,
        ])->render();
        return response()->json([
            'campaigns'=>$view,
        ]);
    }

    public function readmore($id){
        $campaign=Campaign::findOrFail($id);

        return response()->json([
            "description"=>$campaign->description
        ]);
    }

    public function destroy($id){
        $campaign=Campaign::findOrFail($id);
        $images=Campaignimage::where('campaign_id',$id)->get();

        $path=public_path($campaign->image);            
        @unlink($path);

        foreach($images as $image){
            $path=public_path($image);            
            @unlink($path);
        }

        $campaign->delete();

        return response()->json([
            "data"=>'campaign deleted'
        ]);
    }

    

}


//  05/16/2020 - 05/16/2020   
// public function getCampaignData(Request $request){


//     if($request->type!='all'){

//         $campaigns=CampaignsResource::collection(Campaigns::join('campaigns_socials','campaigns_socials.campaign_id','=','campaigns.id')
//         ->where('campaigns_socials.name',$request->type)
//         ->select('campaigns.id','campaigns.title','campaigns.cost_range','campaigns.description','campaigns.image')
//         ->orderBy('campaigns.id','DESC')
//         ->paginate(6));

//     }else{
//         $campaigns=CampaignsResource::collection(Campaigns::all(6));
//     }
    
//     return response()->json([
//         "data"=>$campaigns
//     ]);

// }






// if($request->type!='all'){

            // $campaigns=CampaignsResource::collection(Campaigns::join('campaigns_socials','campaigns_socials.campaign_id','=','campaigns.id')
            // ->where('campaigns_socials.name',$request->type)
            // ->select('campaigns.id','campaigns.title','campaigns.cost_range','campaigns.description','campaigns.image')
            // ->orderBy('campaigns.id','DESC')
            // ->limit(9)
            // ->get());

        // }else{
        //     $campaigns=CampaignsResource::collection(Campaigns::all());
        // }
        
        // return response()->json([
        //     "data"=>$campaigns
        // ]);



































         // $campaigns->diff()
        // $campaigns=CampaignsResource::collection($ca);
        // $campaigns->social_categories()->where('name',$social)->paginate(6);
        // echo $campaigns;

        // dd($campaigns);
        // $users = User::with('podcasts')->get();
        
        // $campaigns->social_categories()->where('social_categories.name',$social)->paginate(6);
        // dd($campaigns);
        // $campaigns = Campaign::join('social_categories','social_categories.campaign_id','=','campaigns.id')
        //     ->where('social_categories.name',$social)
        //     ->orderBy('campaigns.created_at','desc')
        //     ->select('campaigns.*')
        //     ->paginate(6);













        // $campaigns = Campaign::join('social_categories','social_categories.campaign_id','=','campaigns.id')
        //     ->where('social_categories.name',$social)
        //     ->where('campaigns.lower_cost','>=',$budget)
        //     ->orderBy('campaigns.created_at','desc')
        //     ->select('campaigns.*')
        //     ->paginate(6);