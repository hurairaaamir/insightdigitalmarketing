<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Channel;
use App\Instagram;
use App\Youtube;
use App\Fakebook;
use App\Facebookpage;
use Socialite;
use GuzzleHttp\Client;
use Facebook\Facebook;

class ChannelController extends Controller
{
    public function index()
    {
        $channels=Channel::where('user_id','=',auth()->id())->get();

        return view('influencerProfile.channel',[
            "channels"=>$channels
        ]);
    }
    
    public function facebook_login(Facebook $fb,Request $request)
    {
        $access_token= $request->access_token;
        $fb->setDefaultAccessToken($access_token);

        $fields = "id,cover,name,first_name,last_name,age_range,link,gender,locale,picture,timezone,updated_time,verified";
        
        $fb_user = $fb->get('/me?fields='.$fields)->getGraphUser();

        $data=json_decode($fb_user);

        $facebook=Fakebook::where('user_id',auth()->id())->first();

        if($facebook){
            Facebookpage::where('fakebook_id',$facebook->id)->delete();
            $facebook->delete();
        }else{
            $channel=Channel::create([
                "name"=>'facebook',
                'user_id'=>auth()->id()
            ]);
        }
        $channel=Channel::where('user_id',auth()->id())->first();
        $facebook=Fakebook::create([
            "facebook_id"=>$data->id,
            "name"=>$data->name,
            "image"=>$data->picture->url,
            "user_id"=>auth()->id(),
            'channel_id'=>$channel->id
        ]);

        // dump($facebook);

        $contents = $fb->get('me/accounts');


        $contents=$contents->getBody();
        $contents=json_decode($contents);
        $pages=$contents->data;

        foreach($pages as $page){

            $category=$page->category;
            $page = $fb->get($page->id.'?fields=posts.limit(250){picture,permalink_url},id,link,fan_count,name,new_like_count');
            $page = $page->getBody();
            $page = json_decode($page);

            $page=Facebookpage::create([
                'name'=>$page->name,
                'page_id'=>$page->id,
                'category'=>$category,
                'permalink'=>$page->link,
                'fan_count'=>$page->fan_count,
                'fakebook_id'=>$facebook->id,
                'posts'=>count($page->posts->data)
            ]);
            dump($page);
        }
        
        return response()->json([
            "data"=>$facebook
        ]);
         
    }

    public function instagram_login(Facebook $fb,Request $request)
    {

        $access_token= $request->access_token;
    
        
        $fb->setDefaultAccessToken($access_token);

        $contents = $fb->get('me/accounts');
        $contents=$contents->getBody();
        $contents=json_decode($contents);
        $contents=$contents->data;
        $insta_id='';

        // dump($contents);
        foreach($contents as $content){
            // dump($content);
            $page= $fb->get($content->id.'?fields=instagram_business_account');
            $page=$page->getBody();
            $page=json_decode($page);

            if($page->instagram_business_account->id){
                $insta_id=$page->instagram_business_account->id;
                break;
            }
        }
        // dump($insta_id);
        if($insta_id){
            $profile=$fb->get($insta_id.'?fields=biography,media_count,username,profile_picture_url,followers_count,website');
            $profile=$profile->getBody();
            $profile=json_decode($profile);

            $instagram=Instagram::find(auth()->id());
            if($instagram){
                $instagram->update([
                    "biography"=>$profile->biography,
                    "media_count"=>$profile->media_count,
                    "username"=>$profile->username,
                    "profile_picture_url"=>$profile->profile_picture_url,
                    "followers_count"=>$profile->followers_count,
                    "instagram_id"=>$profile->id,
                    "permalink"=>"https://www.instagram.com/".$profile->username,
                ]);
            }else{
                $channel=Channel::create([
                    "name"=>'instagram',
                    'user_id'=>auth()->id()
                ]);

                $instagram=Instagram::create([
                    "biography"=>$profile->biography,
                    "media_count"=>$profile->media_count,
                    "username"=>$profile->username,
                    "profile_picture_url"=>$profile->profile_picture_url,
                    "followers_count"=>$profile->followers_count,
                    "instagram_id"=>$profile->id,
                    "permalink"=>"https://www.instagram.com/".$profile->username,
                    'channel_id'=>$channel->id,
                    'user_id'=>auth()->id()
                ]);
            }
        }else{
            return response()->json([
                "error"=>'you have facebook page linked with your instagram'
            ]);
        }
   

        return response()->json([
            "data"=> $instagram
        ]);

    } 

    
    public function instagram()
    {
    
        $url='https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=263157344872115&height=200&width=200&ext=1593109272&hash=AeSDZDVqfzTCkTod';
        $url=explode('asid=',$url);
    
        $url=explode('&',$url[1]);
    
        $url=$url[0];
    
        dd($url);
        // Channel::create([
        //     "name"=>'instagram',
        //     'user_id'=>1
        // ]);
    
        // Instagram::create([
        //     "biography" => "this is come bio c019",
        //     "media_count" => 3,
        //     "username" => "cosmos09090",
        //     "profile_picture_url" => "https://scontent-frx5-1.xx.fbcdn.net/v/t51.2885-15/100951084_250632922687171_3114862900427096064_n.jpg?_nc_cat=100&_nc_sid=86c713&_nc_eui2=AeF3gV4KcSFug7glcNirb3ix1LNx55FsikDUs3HnkWyKQMPA_-62YpXscZ5yzYBGXrgq6mNzShJG8auD88qwfAEg&_nc_ohc=pckS1ml0j48AX-zwf1F&_nc_ht=scontent-frx5-1.xx&oh=d0ae5e91ed69397cc69f6187d94b7d1c&oe=5EF31028",
        //     "followers_count" => 0,
        //     "instagram_id" => "17841434789241030",
        //     "permalink" => "https://www.instagram.com/cosmos09090",
        //     "channel_id" => 1,
        //     "updated_at" => "2020-05-26 05:27:16",
        //     "created_at" => "2020-05-26 05:27:16",
        //     "id" => 1,
        // ]);
    
        
    }
    public function facebook_pages(){
        $facebook=Fakebook::where('user_id',auth()->id())->first();

        return view('influencerProfile.facebook_pages',[
            "facebook"=>$facebook
        ]);
    }

    public function youtube_login(Request $request){
        
        $youtube=YouTube::find(auth()->id());
        $url="https://www.youtube.com/channel/".$request->youtube_channel_id;

        $channel=Channel::create([
            "name"=>'youtube',
            'user_id'=>auth()->id()
        ]);

        if($youtube){
            $youtube->update([
                'title'=>$request->title,
                'youtube_channel_id'=>$request->youtube_channel_id,
                'subscribers'=>$request->subscribers,
                'views'=>$request->views,
                'videos'=>$request->videos,
                'comments'=>$request->comments,
                'channel_url'=>$url,
                'image_url'=>$request->image_url,
                'user_id'=>auth()->id()
            ]);
        }else{
            $channel=Channel::create([
                "name"=>'youtube',
                'user_id'=>auth()->id()
            ]);
            $youtube=YouTube::create([
                'title'=>$request->title,
                'youtube_channel_id'=>$request->youtube_channel_id,
                'subscribers'=>$request->subscribers,
                'views'=>$request->views,
                'videos'=>$request->videos,
                'comments'=>$request->comments,
                'channel_url'=>$url,
                'image_url'=>$request->image_url,
                'channel_id'=>$channel->id,
                'user_id'=>auth()->id()
            ]);

        }
        
        return response()->json([
            "data"=>$youtube
        ]);
    }

}


// <blockquote class="embedly-card"><h4><a href="https://www.instagram.com/p/CAo0mrqFC6r/?utm_source=ig_web_copy_link">null</a></h4><p>null</p></blockquote>
// <script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>



// https://www.instagram.com/p/CAo0mrqFC6r/?utm_source=ig_web_copy_link




























// +"posts": {#690
//     +"data": array:6 [
//       0 => {#611
//         +"picture": "https://external-frt3-2.xx.fbcdn.net/safe_image.php?d=AQA54y0LTQ5b7Fp3&w=130&h=130&url=https%3A%2F%2Fmedia1.tenor.co%2Fimages%2F150b8ac1bb5e4261e56625795015ff93%2Ftenor.gif%3Fitemid%3D5469684&cfs=1&_nc_eui2=AeGxAJBqo_SqAWViOSe5PDxpkYUUml-B0lqRhRSaX4HSWp-KEk8p0cUtawO5lGNPMw00cDjmRxjIqpQoKVNvSKAw&_nc_hash=AQBGupgJTKvDGLRF"
//         +"permalink_url": "https://www.facebook.com/106788421053782/posts/106823611050263/"
//         +"id": "106788421053782_106823611050263"
//       }
//       1 => {#622
//         +"picture": "https://scontent-frt3-1.xx.fbcdn.net/v/t1.0-0/s130x130/99150382_106821701050454_1287245468622913536_n.jpg?_nc_cat=102&_nc_sid=110474&_nc_eui2=AeFYythMNBT8NnpGQ_zZ2vz91P-glR_xPPXU_6CVH_E89STjFtpCpRCBWxkwag5N2MJEBDrNjJtPe3PnOqOFztBl&_nc_ohc=aVRHrWdpOZoAX9d3uor&_nc_ht=scontent-frt3-1.xx&_nc_tp=7&oh=b5acbc766dd6f71719d928d6f763639f&oe=5F2CDFA8"
//         +"permalink_url": "https://www.facebook.com/106788421053782/posts/106821727717118/"
//         +"id": "106788421053782_106821727717118"
//       }
//       2 => {#626
//         +"permalink_url": "https://www.facebook.com/106788421053782/posts/106819551050669/"
//         +"id": "106788421053782_106819551050669"
//       }
//       3 => {#687
//         +"picture": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-0/s130x130/100732188_106816827717608_5509414907201716224_n.jpg?_nc_cat=105&_nc_sid=110474&_nc_eui2=AeEBPfbcxd7XT767vjSsiPYyuZ6D8OhyGqC5noPw6HIaoCCUWW8U7-DjhTrRWH0szu4wD5YkVI8OVAKC2JlRM9kv&_nc_ohc=xLet-1RrTTkAX9YiOID&_nc_ht=scontent-frx5-1.xx&_nc_tp=7&oh=393dfe81eebcdcbfa907c38f4d32ab59&oe=5F2D6769"
//         +"permalink_url": "https://www.facebook.com/106788421053782/posts/106816914384266/"
//         +"id": "106788421053782_106816914384266"
//       }
//       4 => {#688
//         +"picture": "https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-0/s130x130/100062080_106808321051792_5917669913966673920_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_eui2=AeE59OiWpchF_PEfDzF_ccrAj7bfz7tynbGPtt_Pu3KdsRLU8wv4X2A8FPuC1u_n9QPrHpPB35VQJCwLt1AR1vGk&_nc_ohc=nQfoo-xDzTcAX_wkK1c&_nc_ht=scontent-frx5-1.xx&_nc_tp=7&oh=aeaa86b555dbee7459bfb0f4e6c2840a&oe=5F2F934F"
//         +"permalink_url": "https://www.facebook.com/106788421053782/posts/106808421051782/?substory_index=0"
//         +"id": "106788421053782_106808421051782"
//       }
//       5 => {#689
//         +"picture": "https://scontent-frt3-1.xx.fbcdn.net/v/t1.0-0/s130x130/100487209_106798024386155_8691435535471738880_n.jpg?_nc_cat=108&_nc_sid=110474&_nc_eui2=AeEEcI-tkj5PqkYNDMaq2BVdzi80LFOZfJHOLzQsU5l8kbIFV_vMXGkM79mogXUQtyaKJAoHNSHU1ngUHUbUqNGz&_nc_ohc=SglccJk_Zb8AX8nZLth&_nc_ht=scontent-frt3-1.xx&_nc_tp=7&oh=83bd77a4f9ecd499464c96660ab5db0c&oe=5F2DE7AC"
//         +"permalink_url": "https://www.facebook.com/106788421053782/posts/106798064386151/"
//         +"id": "106788421053782_106798064386151"
//       }
//     ]
//     +"paging": {#692
//       +"cursors": {#691
//         +"before": "QVFIUlVabW1WbUxQZAWtjTmtkZA3BPb3d6RTljREF5RWVhbzZAqQVQ3YWNUM2JSQ1JCbm1UWU5yZAkFsUDliMTVVREJoUlBBaGVtVnZAULVRzcklJSXBsaGVjMFQyMlBDcmJla2VHUHZAOMGNwSFFNRENhRW5iS2l6SEZAyV09YZAWVDTlJFRlVp"
//         +"after": "QVFIUkN2SENqSFNQMmcyXzdYV2FrUEFOcmQ5dmR0OEdpNWwxLWp4Mnk5NG5ta3c1X09wMW9XR0JMMHdWVjV0TlRiQmZAMUE40QTFkVlBKek9UTzkyUHFadUZAIdURlNXJYcnNERWllamdjUGdvVUdWSmZA2SlMwM1BPUjJzaGN5SUJ4RkJm"
//       }
//     }
//   }
//   +"id": "106788421053782"
//   +"link": "https://www.facebook.com/106788421053782"
//   +"fan_count": 0
//   +"name": "TEST page"
//   +"new_like_count": 0
// }






































// public function index_instagram(){
//     $Instagrams=Instagram::all();
    
//     return view('influencerProfile.instagram',[
//         "instagrams"=>$Instagrams
//     ]);
// }







// $table->bigIncrements('id');

// $table->text('biography');
// $table->string('media_count');
// $table->string('username');
// $table->string('profile_picture_url');
// $table->string('followers_count');
// $table->string('instagram_id');
// $table->string('permalink');

// $table->unsignedBigInteger('channel_id');







// Manage your business

// Manage your Pages



// Show a list of the Pages you manage

// Access your Page and App insights

// 17923583668392806

// public function FacebookRedirectToProvider()
// {
//     return Socialite::driver('facebook')->scopes([
//         "business_management, pages_read_user_content", "read_insights"])->redirect();           
// }



// public function handleProviderCallback()
// {
//     $user = Socialite::driver('facebook')->user();

//     dd($user->token);
// }


































// public function redirectToInstagramProvider()
// {
//     $appId = '1341920255998393';
//     $redirectUri = 'https://reachhero.dev.rigrex.com/login/instagram/callback';
//     return redirect()->to("https://api.instagram.com/oauth/authorize?app_id={$appId}&redirect_uri={$redirectUri}&scope=user_profile,user_media&response_type=code");
// }

// public function instagramProviderCallback(Request $request)
// {
//     $code = $request->code;
//     if (empty($code)) return redirect()->back()->with('error', 'Failed to login with Instagram.');

//     $appId = '1341920255998393';
//     $secret = '74fb2cb21c77c5b983f7ecfe8185d6d1';
//     $redirectUri = 'https://reachhero.dev.rigrex.com/login/instagram/callback';

//     $client = new Client();

//     // Get access token
//     $response = $client->request('POST', 'https://api.instagram.com/oauth/access_token', [
//         'form_params' => [
//             'app_id' => $appId,
//             'app_secret' => $secret,
//             'grant_type' => 'authorization_code',
//             'redirect_uri' => $redirectUri,
//             'code' => $code,
//         ]
//     ]);

//     if ($response->getStatusCode() != 200) {
//         return redirect()->back()->with('error', 'Unauthorized login to Instagram.');
//     }

//     $content = $response->getBody()->getContents();
//     $content = json_decode($content);

//     $accessToken = $content->access_token;

//     $userId = $content->user_id;
//     // Get user info
//     dd($accessToken);
//     $response = $client->request('GET', "https://api.instagram.com/v1/users/media/self/?access_token={$accessToken}");
    
//     // $response = $client->request('GET', "https://graph.instagram.com/me/media?fields=id,caption&access_token={$accessToken}");

//     $content = $response->getBody()->getContents();

//     $medias = json_decode($content);

//     dd($medias);

//     $data=[];

//     foreach($medias->data as $media){

//         $response = $client->request('GET', "https://graph.instagram.com/{$media->id}?fields=id,media_type,media_url,username,timestamp,caption,permalink,thumbnail_url&access_token={$accessToken}");

//         $content = $response->getBody()->getContents();

//         $content=json_decode($content);

//         array_push($data,$content);

//         // print_r($content->media_url);

//         // echo $content;
//     }
    
//     foreach($data as $d){
//             Instagram::create([
//                 "instagram_id"=>$d->id,
//                 "media_type"=>$d->media_type,
//                 "media_url"=>$d->media_url,
//                 "username"=>$d->username,
//                 "timestamp"=>$d->timestamp,
//                 "permalink"=>$d->permalink,
//                 "channel_id"=>1,            
//             ]);
//     }

// }