<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function mark_as_read($id){
        $user=User::findOrFail($id);
        $user->unreadNotifications->markAsRead();

        return response()->json([
            "data"=>"Mark as Read"
        ]);
    }
    public function admin_notification_all(){
        return view('admin.notifications.all');
    }
    public function admin_destroy($id){
        auth()->user()->notifications()->where('id', $id)->get()->first()->delete();
        
        return redirect()->back();
    }
}
