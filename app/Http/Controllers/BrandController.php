<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use App\EmailNotificationsSetting;

use App\BillingAddress;
use App\User;
use App\Brand;

class BrandController extends Controller
{
    public function index()
    {
        return view('brandProfile.dashboard');
    }

    public function view()
    {
        $brands = Brand::where('user_id', auth()->id())->get();
        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();


        // dd($brands);

        return view('brandProfile.settings.createdBrands', [
            "brands" => $brands,
            "active_brand" => $active_brand
        ]);
    }

    public function getAllowedUsers($id)
    {
        $brand = Brand::where('user_id', auth()->id())->where('id', $id)->first();
        $allowed_users = explode(',', $brand['allowed_users']);
        // echo auth()->user()->email;
        // dd($brand['allowed_users']);
        return response()->json([
            "allowedUsers" => $allowed_users,
            "loginUserEmail" => auth()->user()->email
        ]);
    }

    public function addAllowedUser(Request $request, $id)
    {
        $brand = Brand::where('user_id', auth()->id())->where('id', $id)->first();

        // echo auth()->user()->email;
        // dd($brand['allowed_users']);
        $brand->update([
            "allowed_users" => $brand['allowed_users'] . $request->newUserEmail . ','
        ]);
        return response()->json([
            "success" => "User Successfully Added"
        ]);
    }

    public function deleteAllowedUser(Request $request, $id)
    {
        $brand = Brand::where('user_id', auth()->id())->where('id', $id)->first();

        // echo auth()->user()->email;
        // dd($brand['allowed_users']);
        $brand->update([
            "allowed_users" => implode(',', $request->newAllowedUsers) . ','
        ]);
        return response()->json([
            "success" => "User Successfully Added"
        ]);
    }



    public function create(Request $request)
    {
        $brands = Brand::where('user_id', auth()->id())->get();
        // dd(count($brands));
        // $active = 1;
        if (count($brands) == 0) {
            $active = 1;
        } else {
            $active = 0;
        }

        // dd($active);
        $brand_id = Brand::create([
            "first_name" => $request->first_name,
            "email" => $request->email,
            "website" => $request->website,
            "company_name" => $request->company_name,
            "telephone" => $request->telephone,
            "active"  => $active,
            "allowed_users" => auth()->user()->email . ',',
            "last_name" => $request->last_name,
            "user_id" => auth()->id(),

        ]);


        Setting::create([
            "first_name" => $request->first_name,
            "email" => $request->email,
            "website" => $request->website,
            "company_name" => $request->company_name,
            "telephone" => $request->telephone,
            "last_name" => $request->last_name,
            "user_id" => auth()->id(),
            "brand_id" => $brand_id['id']
        ]);

        BillingAddress::create([
            "user_id" => auth()->id(),
            "brand_id" => $brand_id['id']
        ]);
        EmailNotificationsSetting::create([
            "user_id" => auth()->id(),
            "brand_id" => $brand_id['id']
        ]);

        return redirect('/brand/settings');
    }

    public function activateBrand($id)
    {

        $active_brand = Brand::where('user_id', auth()->id())->where('active', 1)->first();
        $active_brand->update([
            'active' => 0
        ]);

        $activate_brand = Brand::where('user_id', auth()->id())->where('id', $id)->first();
        $activate_brand->update([
            'active' => 1
        ]);
        return redirect('/brand/dashboard');
    }
}
