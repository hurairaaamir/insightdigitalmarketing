<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InfluencerReward;
class InfluencerRewardController extends Controller
{
    public function index(){
        $rewards=InfluencerReward::all();
        return view('influencerProfile.reward.index',[
            'rewards'=>$rewards
        ]);

    }
    public function show($id){
        $selected_reward=InfluencerReward::findOrFail($id);

        $data=view('cardData.reward',[
            "selected_reward"=>$selected_reward
        ])->render();

        return response()->json([
            "data"=>$data
        ]);

    }

    public function admin(){
        $rewards=InfluencerReward::where('id','>',0)->orderby('id','desc')->get();

        return view('admin.reward.index',[
            'rewards'=>$rewards
        ]);
    }

    public function store(Request $request){

        $request->validate([
            "title"=>"required",
            "description"=>"required",
            'points'=>'required'
        ]);

        if(isset($request->id)){
            $reward=InfluencerReward::findOrFail($request->id);

            if(isset($request->image)){
                $path=public_path($reward->image);            
                @unlink($path);

                $image=$this->storeImage($request->image);

                $reward->update([
                    "image"=>$image,
                ]);
            } 
            $reward->update([
                "title"=>$request->title,
                "description"=>$request->description,
                'points'=>$request->points
            ]);

            return redirect('admin/reward/index');
        }

        $request->validate([
            "image"=>'required|image|mimes:jpg,png,jpeg|max:2000',
        ]);

        $image=$this->storeImage($request->image);

        $reward=InfluencerReward::create([
            "title"=>$request->title,
            "description"=>$request->description,
            "image"=>$image,
            'points'=>$request->points
        ]);

        return redirect()->back();
    }

    protected function storeImage($image){
        
        $image_name = uniqid().'.'. $image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/rewards/';
        $image->move($destinationPath, $image_name);
        $path='/images/rewards/'.$image_name;

        return $path;
    }

    public function edit($id){
        $edit_reward=InfluencerReward::findOrFail($id);

        $rewards=InfluencerReward::where('id','>',0)->orderby('id','desc')->get();

        return view('admin.reward.index',[

            'rewards'=>$rewards,

            'edit_reward'=>$edit_reward
        ]);
        
    }

    public function destroy(Request $request){
        $reward=InfluencerReward::findOrFail($request->id);

        $path=public_path($reward->image);                    

        @unlink($path);

        $reward->delete();

        return redirect('admin/reward/index');
    }

}
