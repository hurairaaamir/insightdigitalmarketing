<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class CampaignsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "image"=>$this->image,
            "title"=>$this->title,
            "lower_cost"=>$this->lower_cost,
            "upper_cost"=>$this->upper_cost,
            "description"=>$this->description,
            "socials"=>$this->CampaignsSocials
        ];
    }
}
