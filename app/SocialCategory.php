<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;

class SocialCategory extends Model
{
    protected $fillable=['name','image'];

    public function campaigns(){
        return $this->belongsToMany(Campaign::class);
    }
}
