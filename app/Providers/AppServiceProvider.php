<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Facebook\Facebook;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);


        $this->app->singleton(Facebook::class, function ($app) {
            return new Facebook([
                'app_id' => '228877818409800',
                'app_secret' => 'cebac13bd8d1fd5715ca61b09532f26a',
                'default_graph_version' => 'v7.0',
            ]);
        });
        // config('facebook_config')
    }
}
