<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Channeltype;

class Profile extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'gender', 'channel_type', 'telephone', 'birthday',
        'country', 'city', 'street_house', 'zip', 'multichannel', 'about_channel', 'image', 'user_id',
        'Account_holder','IBAN','BIC','payment_type','paypal_email'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function channeltypes()
    {
        return $this->belongsToMany(Channeltype::class);
    }
}
