<?php

namespace App;

use Carbon\Carbon;
use Hash;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Profile;
use App\Setting;
use App\BillingAddress;
use App\Campaign;
use App\Offer;
use App\Channel;
use App\ResearchList;

class User extends Authenticatable
{
    use SoftDeletes, Notifiable;

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
        'email_verified_at',
    ];

    protected $fillable = [
        'role',
        'name',
        'email',
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
        'remember_token',
        'email_verified_at',
    ];

    public function getEmailVerifiedAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setEmailVerifiedAtAttribute($value)
    {
        $this->attributes['email_verified_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function setting()
    {
        return $this->hasOne(Setting::class);
    }

    public function emailNotificationsSetting()
    {
        return $this->hasOne(EmailNotificationsSetting::class);
    }

    public function billingAddress()
    {
        return $this->hasOne(BillingAddress::class);
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }

    public function brand()
    {
        return $this->hasMany(Brand::class);
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function channels()
    {
        return $this->hasMany(Channel::class);
    }
    public function researchlists()
    {
        return $this->hasMany(ResearchList::class);
    }

    public function instagram()
    {
        return $this->hasOne(Instagram::class);
    }

    public function facebook()
    {
        return $this->hasOne(Fakebook::class);
    }

    public function youtube()
    {
        return $this->hasOne(Fakebook::class);
    }

    public function youtubeContent()
    {
        return $this->hasMany(YoutubeContent::class);
    }
    public function facebookContent()
    {
        return $this->hasMany(FacebookContent::class);
    }
    public function instagramContent()
    {
        return $this->hasMany(InstagramContent::class);
    }
}
