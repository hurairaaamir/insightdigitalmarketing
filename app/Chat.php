<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Chat extends Model
{
    protected $fillable =['email','message','user_id'];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
