<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Campaignimage extends Model
{
    protected $fillable=['image','campaign_id'];

}
