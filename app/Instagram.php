<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{
    protected $fillable=['biography',
                        'media_count',
                        'username',
                        'profile_picture_url',
                        'followers_count',
                        'instagram_id',
                        "permalink",
                        "channel_id",
                        "user_id",
                    ];

    public function channel(){
        return $this->belongsTo(Channel::class);
    }
    
}
