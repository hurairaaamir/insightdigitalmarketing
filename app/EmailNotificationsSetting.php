<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailNotificationsSetting extends Model
{
    protected $fillable = ["summary", "newProposalForPlacementCampaign", "newParticipantForPerformanceCampaign", "newRequestForGiveawayCampaign", "campaignContentPublished", "newMessage", "brand_id", "user_id"];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
