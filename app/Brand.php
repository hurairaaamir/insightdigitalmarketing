<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['company_name', 'active', 'allowed_users', 'email', 'website', 'first_name', 'last_name', 'telephone', 'user_id', 'profile_image'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setting()
    {
        return $this->hasOne(Setting::class);
    }

    public function emailNotificationsSetting()
    {
        return $this->hasOne(EmailNotificationsSetting::class);
    }

    public function billingAddress()
    {
        return $this->hasOne(BillingAddress::class);
    }
}
