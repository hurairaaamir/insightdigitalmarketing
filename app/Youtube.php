<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Youtube extends Model
{
    protected $fillable=['title','youtube_channel_id','subscribers','views','videos','comments','channel_url','image_url','user_id','channel_id'];
}
