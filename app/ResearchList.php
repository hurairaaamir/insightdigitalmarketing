<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ResearchList extends Model
{
    protected $fillable = ['name', 'influencer_ids', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
