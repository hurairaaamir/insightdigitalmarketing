<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfluencerReward extends Model
{
    protected $fillable=['title','points','description','image'];
}
