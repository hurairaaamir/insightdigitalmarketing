/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
import VueSweetalert2 from "vue-sweetalert2";

// If you don't need the styles, do not connect
import "sweetalert2/dist/sweetalert2.min.css";

Vue.use(VueSweetalert2);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component(
    "add-to-list-button",
    require("./components/AddToListButton.vue").default
);

Vue.component(
    "brand-profile",
    require("./components/brandProfile/Settings/Profile.vue").default
);
Vue.component(
    "brand-settings-index",
    require("./components/brandProfile/Settings/BrandSettingsIndex.vue").default
);
Vue.component(
    "billing-address",
    require("./components/brandProfile/Settings/BillingAddress.vue").default
);
Vue.component(
    "brand-change-password",
    require("./components/brandProfile/Settings/Password.vue").default
);
Vue.component(
    "brand-email-notifications-settings",
    require("./components/brandProfile/Settings/EmailNotifications.vue").default
);
Vue.component(
    "manage-user-access",
    require("./components/brandProfile/Settings/ManageUserAccess.vue").default
);

Vue.component(
    "influencer-lists",
    require("./components/brandProfile/InfluencerResearch/InfluencerLists.vue")
        .default
);

Vue.component(
    "show-all",
    require("./components/brandProfile/InfluencerResearch/ShowAll.vue").default
);

Vue.component(
    "single-list",
    require("./components/brandProfile/InfluencerResearch/SingleList.vue")
        .default
);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app"
});
