@extends('layouts.admin')
@section('styles')
<style>
    .cursor-pointer {
    cursor: pointer !important;
}
.reward-container{
    display:flex;
    justify-content: flex-start;
    align-items: flex-start;
    padding:10px;
    flex-wrap:wrap;
}
@media(max-width:780px){
    .reward-container{
        justify-content: center;
        align-items: center;
    }
}
.reward-container .card{
    flex:0 1 342px;
    margin:10px;
}
.reward-container .card .card-body {
    display:flex;
    flex-direction: column;
}

.reward-container .card .card-body .reward-img{
    width:100%;
    height: 100%;
}
.reward-container .card .card-body .reward-img img{
    width:100%; 
    max-height:500px;
}
.reward-container .card .card-body .upper{
    margin:15px 0px;
}
.reward-container .card .card-body .upper h6{
    font-weight: 700;
    font-size:20px;
}
.reward-container .card .card-body .upper .points{
    color:#009465;
    font-size:16px;
}
.center_out{
    display:flex;
    justify-content: center;
    align-items: center;
    text-align:center;
}
.center-loader{
    display: flex;
    width: 100%;
    justify-content: flex-start;
    flex-direction: column;
}
.body-height{
    min-width:700px;
    min-height:400px;
    display:flex;
    overflow-y:scroll;
    flex-direction: row;
    flex-wrap:wrap;
    padding:10px 5px;
}
@media(max-width:780px){
    .body-height{
        min-width:300px;
    }
}
.body-height .child1{
    flex:0 1 346px;
    display:flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
}
.body-height .child1 .img-container{
    width:300px;
    height: auto;
}
.body-height .child1 .img-container img{
    width:100%;
    height: 100%;
}

.body-height .child2{
    flex:0 1 400px;
    display:flex;
    padding:10px;
    flex-direction: column;
}
.body-height .child2 h6{
    font-weight: 600;
}
.body-height .child2 .points{
    color:#009465;
    font-size:23px;
}
.body-height .child2 span{
    font-size:13px;
    margin-top:10px;

}
.preview-container{
    width:250px;
    height:auto;
    border:1px solid gray;
    opacity:7;
}
.preview-container img{
    width:100%;
    height:100%;
}
.center_out{
    display:flex;
    flex-direction: column;
    justify-content: center;
    align-items:center;
    margin-bottom:50px;
}
.preview-container:hover{
    opacity:1;
    cursor:pointer;
}
.red{
    color:red;
}
</style>

@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal" action="{{url('admin/reward/store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="center_out">

                            <input type="file" accept="image/*" onchange="preview_image(event)" id="image-feild" style="display:none;" name="image">
                            <div class="preview-container" onclick="ItIsClick()">
                                <img id="output_image" src="{{isset($edit_reward->image)? $edit_reward->image:'/images/general/Preview-icon.png'}}"  />
                            </div>
                            @if($errors->has('image'))
                                <p class="red">{{$errors->get('image')[0]}}</p>
                            @endif
                        </div>

                        <input type="hidden" name="id" value="{{isset($edit_reward->id)? $edit_reward->id:''}}">
                        
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">Title</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputName" placeholder="Title" name="title" value="{{isset($edit_reward->title)? $edit_reward->title:'' }}">
                                @if($errors->has('title'))
                                    <p class="red">{{$errors->get('title')[0]}}</p>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Points" class="col-sm-2 col-form-label">Points</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="Points" placeholder="Points" name="points" value="{{isset($edit_reward->points)? $edit_reward->points:''}}">
                                @if($errors->has('points'))
                                    <p class="red">{{$errors->get('points')[0]}}</p>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="inputExperience" class="col-sm-2 col-form-label">Details</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="inputExperience" placeholder="Details" style="margin-top: 0px; margin-bottom: 0px; height: 192px;" name="description">{{isset($edit_reward->description)? $edit_reward->description:''}}</textarea>
                                @if($errors->has('description'))
                                    <p class="red">{{$errors->get('description')[0]}}</p>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-primary mt-2">{{isset($edit_reward->image)?'Edit':'Submit'}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <h1>Rewards</h1>
            <div class="reward-container">
                
                @foreach($rewards as $reward)
                @php
                    $selected_reward=$reward;
                @endphp
                <div class="card">
                    <div class="card-body light">
                        <div>
                            <a href="{{url('/admin/reward/edit/'.$selected_reward->id)}}" class="btn btn-secondary btn-sm">edit</a>
                            <button class="btn btn-danger btn-sm m-2" onclick="destroy_reward('{{$selected_reward->id}}')">delete</button>
                        </div>
                        <form action='{{url("/admin/reward/destroy/".$selected_reward->id)}}' method="POST" id="reward_destroy_{{$selected_reward->id}}">
                            @csrf
                            <input type="hidden" value="{{$selected_reward->id}}" name="id">
                        </form>
                        <div class="reward-img">
                            <img src="{{$reward->image}}" />
                        </div>
                        <div class="upper">
                            <h6>{{$reward->title}}</h6>
                            <div class="points">Points: {{$reward->points}}</div>
                        </div>
                        @php
                            $description=substr($reward->description,0,150);
                        @endphp
                        <p>{{$description}}</p>
                        <div class="center_out">
                            <button type="button" data-toggle="modal" data-target=".bd-example-modal-lg" class="btn-secondary btn-sm btn" onclick="getcompleteReward('{{$reward->id}}')">Details</button>

                            {{-- <button type="button" data-toggle="modal" data-target=".bd-example-modal-lg" class="btn-secondary btn-sm btn" onclick="getcompleteReward('{{$reward->id}}')">Details</button>

                            <button type="button" data-toggle="modal" data-target=".bd-example-modal-lg" class="btn-secondary btn-sm btn" onclick="getcompleteReward('{{$reward->id}}')">Details</button>                             --}}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>  
        </div>
        
    </div>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body body-height" id="reward-id">
                    
                    
                </div>
                <div class="modal-footer">
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script src="{{asset('js/ajax/admin_campaigns.js')}}"></script>
    <script>
        function getcompleteReward(id){
        document.getElementById('reward-id').innerHTML='<div class="center-loader"><img src="/images/gifs/bluePreloader.svg">';
        $.ajax({
            type: "GET",
            url: '/influencer/reward/show/'+id,
            success: function(response){
                document.getElementById('reward-id').innerHTML=response.data;
            }
        });
    }

    function ItIsClick(){
        $("#image-feild").click();
    }

    function preview_image(event){
        var reader = new FileReader();
        reader.onload = function(){
            var output = document.getElementById('output_image');
            
            output.src = reader.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }

    function destroy_reward(id){
        if(confirm('Are You sure.....')){
            document.getElementById('reward_destroy_'+id).submit();
        }
    }
    </script>
@endsection

