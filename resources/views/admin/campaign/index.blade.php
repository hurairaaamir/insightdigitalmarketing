@extends('layouts.admin')
@section('styles')
<style>
.loaderhider{
    display:none!important;
}
.centerOut{
    display:flex;
    justify-self: center;
    align-items: center;
    flex-direction:column;
}
.youtube{
    background-color: #BC1C0E;
    color:white;
}
.instagram{
    background-color: #E66A5E;
    color:white;
}
.twitter{
    background-color: #70B8F2;
    color:white;
}
.facebook{
    background-color: #4966A2;
    color:white;
}
.blog{
    background-color: #EF7438;
    color:white;
}
.social-icon{
    justify-content:flex-start;
    display:flex;
    align-items:center;
}
.social-icon div{
    font-size:18px;
    display:flex;
    justify-content: center;
    align-items: center;
    border-radius:100%;
    margin-right:5px;
    width:25px;
    height: 25px;
}
.sm-svg img{
    width:80px;
    height: 20px;
}
</style>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">

            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item" onclick="getCampaigns('latest')"><a class="nav-link active" href="#activity" data-toggle="tab">Latest</a></li>
                        <li class="nav-item" onclick="getCampaigns('old')"><a class="nav-link" href="#timeline" data-toggle="tab">Oldests</a></li>
                        <li class="nav-item" onclick="getCampaigns('dead')"><a class="nav-link" href="#settings" data-toggle="tab">Dead</a></li>
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">

        {{-- ------------------------------------------------------------------------------------------------------------------------- --}}
        <!------------------------------------------------------ /.tab-pane ------------------------------------------------------------>
        {{-- ------------------------------------------------------------------------------------------------------------------------- --}}
                        @if(count($campaigns) !=0)
                            <div class="active tab-pane" id="activity">
                                @foreach($campaigns as $campaign)
                                <div class="post" id="campaign_{{$campaign->id}}">
                                    <div class="user-block">
                                        <img style="width:100px;height:auto;margin-right:10px;" src="{{$campaign->image}}" alt="user image">
                                        <span class="username">
                                            <a href="#">{{$campaign->title}}</a>
                                            <a  class="float-right btn btn-danger btn-xs m-3" id="del_btn_{{$campaign->id}}" style="color:white" onclick="deleteCampaign('{{$campaign->id}}')"> delete  <i class="fas fa-trash-alt"></i></a>
                                        </span>
                                        @php
                                            $description=substr($campaign->description,0,200).'.......'
                                        @endphp
                                        <span class="" id="des_{{$campaign->id}}">
                                            <span>{!!$description!!}<span>
                                            <div class=" centerOut sm-svg loaderhider" id="sm_blue_{{$campaign->id}}">
                                                <img src="/images/gifs/bluePreloader.svg">
                                            </div>
                                            <button class="btn btn-secondary btn-xs m-2" id="del_btn_{{$campaign->id}}" onclick="readmore('{{$campaign->id}}')">read more</button>
                                        </span>
                                    </div>
                                    <span class="social-icon">
                                        @foreach($campaign->social_categories as $social)
                                            {!!$social->image!!}
                                        @endforeach
                                    </span>
                                    <!-- /.user-block -->
                                    <span >
                                        
                                    </span>

                                    <p>
                                        <a href="#" class="link-black text-sm mr-2">{{$campaign->lower_cost}} - {{$campaign->upper_cost}} <i class="fas fa-euro-sign"></i></a>
                                        <span class="float-right">
                                            <a class="text-md mt-2" style="cursor:pointer" onclick="getAllOffers('{{$campaign->offers()->count() != 0 ? $campaign->id : ''}}')">
                                                <i class="fas fa-caret-down"></i> <i class="far fa-comments mr-1"></i>view all offers {{$campaign->offers()->count()}}
                                            </a>  
                                        </span>
                                        <span style="display:block;" class="mt-2">
                                            <a href="#" class="link-black text-sm mr-2"><i class="fas fa-angle-double-right"></i> published at : {{$campaign->created_at}}</a>
                                        </span>
                                    </p>
                                </div>
                                <div class="ml-5" id="offer_data_{{$campaign->id}}">
                                    <div class="centerOut mt-2 mb-2 loaderhider" id="blue_sm_loader_{{$campaign->id}}">
                                        <img src="/images/gifs/bluePreloader.svg">
                                    </div>
                                </div>
                                @endforeach
                                <!-- /.post -->
                            </div>
                        @else
                        <div class="active tab-pane" id="activity">
                            <p>
                                No Result Found
                            </p>
                        </div>
                        @endif
                    </div>
                    <!-- /.tab-content -->
                    <div class="centerOut mt-2 mb-2" id="blue-loader">
                        <img src="/images/gifs/bluePreloader.svg">
                    </div>
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        
        {{-- <div class="col-md-3">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Campaigns</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b><i class="fas fa-address-book"></i> Total Campaigns:</b> <a class="float-right text-muted">{{$total}}</a>
                        </li>
                        <li class="list-group-item">
                            <b><i class="fas fa-user"></i> Latest Campaigns:</b> <a class="float-right text-muted"></a>
                        </li>
                        <li class="list-group-item">
                            <b><i class="fas fa-venus-mars"></i> Old Campaigns:</b> <a class="float-right text-muted"></a>
                        </li>
                    </ul>       

                </div>
                <!-- /.card-body -->
            </div>
        </div> --}}

    </div>
</div>
@endsection
@section('scripts')
    <script src="{{asset('js/ajax/admin_campaigns.js')}}"></script>
@endsection

