@extends('layouts.admin')
@section('styles')
<style>
.youtube{
    background-color: #BC1C0E;
    color:white;
}
.instagram{
    background-color: #E66A5E;
    color:white;
}
.twitter{
    background-color: #70B8F2;
    color:white;
}
.facebook{
    background-color: #4966A2;
    color:white;
}
.blog{
    background-color: #EF7438;
    color:white;
}
.social-icon{
    justify-content:flex-start;
    display:flex;
    align-items:center;
}
.social-icon div{
    font-size:18px;
    display:flex;
    justify-content: center;
    align-items: center;
    border-radius:100%;
    margin-right:5px;
    width:25px;
    height: 25px;
}

</style>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" style="width:100px;height:100px;" src="{{$user->profile ? $user->profile->image :'/images/general/default.jpg'}}" alt="User profile picture">
                    </div>

                    <h3 class="profile-username text-center">{{$user->name}}</h3>

                    <p class="text-muted text-center">{{$user->email}}</p>

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Role:</b> <a class="float-right">{{$user->role}}</a>
                        </li>
                    </ul>
                    {{-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> --}}
                </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">About Me</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b><i class="fas fa-address-book"></i> First Name:</b> <a class="float-right text-muted">{{$user->profile ? $user->profile->first_name:'not provided'}}</a>
                        </li>
                        <li class="list-group-item">
                            <b><i class="fas fa-user"></i> Last Name:</b> <a class="float-right text-muted">{{$user->profile ? $user->profile->last_name:'not provided'}}</a>
                        </li>
                        <li class="list-group-item">
                            <b><i class="fas fa-venus-mars"></i> Gender:</b> <a class="float-right text-muted">{{$user->profile ? $user->profile->gender:'not provided'}}</a>
                        </li>
                        <li class="list-group-item">
                            <b><i class="fas fa-map-marker-alt"></i> location:</b> <a class="float-right text-muted">{{$user->profile ? $user->profile->street_house:''}} {{$user->profile ? $user->profile->city:''}} {{$user->profile ? $user->profile->country:'not provided'}}</a>
                        </li>
                        <li class="list-group-item">
                            <b><i class="fas fa-phone"></i> Telephone no:</b> <a class="float-right text-muted">{{$user->profile ? $user->profile->telephone:'not provided'}}</a>
                        </li>
                        <li class="list-group-item">
                            <b><i class="fas fa-birthday-cake"></i> Birthday:</b> <a class="float-right text-muted">{{$user->profile ? $user->profile->birthday:'not provided'}}</a>
                        </li>
                        {{-- <li class="list-group-item">
                            <b><i class="fas fa-book mr-1"></i> location:</b> <a class="float-right text-muted"></a>
                        </li>
                        <li class="list-group-item">
                            <b><i class="fas fa-book mr-1"></i> location:</b> <a class="float-right text-muted"></a>
                        </li> --}}
                    </ul>       

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>


























        <div class="col-md-9">



            <div class="card">
                <div class="card-header p-2">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li>
                        <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline</a></li>
                        <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                    </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                    <div class="tab-content">

{{-- ------------------------------------------------------------------------------------------------------------------------- --}}
<!------------------------------------------------------ /.tab-pane ------------------------------------------------------------>
{{-- ------------------------------------------------------------------------------------------------------------------------- --}}
                        @if(count($user->campaigns) !=0)
                            <div class="active tab-pane" id="activity">
                                @foreach($user->campaigns as $campaign)
                                <div class="post">
                                    <div class="user-block">
                                        <img style="width:100px;height:auto;margin-right:10px;" src="{{$campaign->image}}" alt="user image">
                                        <span class="username">
                                            <a href="#">{{$campaign->title}}</a>
                                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                        </span>
                                        <span class="description">{!!$campaign->description!!}</span>
                                    </div>
                                    <span class="social-icon">
                                        @foreach($campaign->social_categories as $social)
                                            {!!$social->image!!}
                                        @endforeach
                                    </span>
                                    <!-- /.user-block -->
                                    <span >
                                        
                                    </span>

                                    <p>
                                        <a href="#" class="link-black text-sm mr-2">{{$campaign->lower_cost}} - {{$campaign->upper_cost}} <i class="fas fa-euro-sign"></i></a>
                                        <span class="float-right">
                                            <a href="#" class="link-black text-sm mt-2">
                                                <i class="far fa-comments mr-1"></i> offers {{$campaign->offers()->count()}}
                                            </a>    
                                        </span>
                                        <span style="display:block;" class="mt-2">
                                            <a href="#" class="link-black text-sm mr-2"><i class="fas fa-angle-double-right"></i> published at : {{$campaign->created_at}}</a>
                                        </span>
                                    </p>
                                </div>
                                @endforeach
                                <!-- /.post -->
                            </div>
                        @elseif(count($user->offers)!=0)
                            @foreach($user->offers as $offer)
                            <div class="active tab-pane" id="activity">
                                <div class="post">
                                    <div class="user-block">
                                        <img style="width:100px;height:auto;margin-right:10px;" src="{{$offer->campaign->image}}" alt="user image">
                                        <span class="username">
                                            <a href="#">{{$offer->campaign->title}}</a>
                                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                        </span>
                                        <span class="description">{!!$offer->details!!}</span>
                                    </div>
                                    <span class="social-icon">
                                        @foreach($offer->campaign->social_categories as $social)
                                            {!!$social->image!!}
                                        @endforeach
                                    </span>
                                    <!-- /.user-block -->
                                    <span >
                                        
                                    </span>

                                    <p>
                                        <a href="#" class="link-black text-sm mr-2">{{$offer->budget}} <i class="fas fa-euro-sign"></i></a>
                                        <span class="float-right">
                                            <a href="#" class="link-black text-sm">
                                                <i class="far fa-comments mr-1"></i> status {{$offer->status}}
                                            </a>    
                                        </span>
                                    </p>
                                </div>
                            </div>
                            @endforeach
                                <!-- /.post -->
                        @else
                        <div class="active tab-pane" id="activity">
                            <div class="post">
                                <p>
                                    No Record is According for this User
                                </p>
                            </div>
                        </div>
                        @endif

{{-- ------------------------------------------------------------------------------------------------------------------------- --}}
<!------------------------------------------------------ /.tab-pane ------------------------------------------------------------>
{{-- ------------------------------------------------------------------------------------------------------------------------- --}}
                        

                        {{-- <div class="tab-pane" id="timeline">
                            <div class="timeline timeline-inverse">
                            <div class="time-label">
                                <span class="bg-danger">
                                10 Feb. 2014
                                </span>
                            </div>
                            <div>
                                <i class="fas fa-envelope bg-primary"></i>

                                <div class="timeline-item">
                                <span class="time"><i class="far fa-clock"></i> 12:05</span>

                                <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                                <div class="timeline-body">
                                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                    weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                    jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                    quora plaxo ideeli hulu weebly balihoo...
                                </div>
                                <div class="timeline-footer">
                                    <a href="#" class="btn btn-primary btn-sm">Read more</a>
                                    <a href="#" class="btn btn-danger btn-sm">Delete</a>
                                </div>
                                </div>
                            </div>
                            <div>
                                <i class="fas fa-user bg-info"></i>

                                <div class="timeline-item">
                                <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>

                                <h3 class="timeline-header border-0"><a href="#">Sarah Young</a> accepted your friend request
                                </h3>
                                </div>
                            </div>
                            <div>
                                <i class="fas fa-comments bg-warning"></i>

                                <div class="timeline-item">
                                <span class="time"><i class="far fa-clock"></i> 27 mins ago</span>

                                <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                                <div class="timeline-body">
                                    Take me to your leader!
                                    Switzerland is small and neutral!
                                    We are more like Germany, ambitious and misunderstood!
                                </div>
                                <div class="timeline-footer">
                                    <a href="#" class="btn btn-warning btn-flat btn-sm">View comment</a>
                                </div>
                                </div>
                            </div>
                            <div class="time-label">
                                <span class="bg-success">
                                3 Jan. 2014
                                </span>
                            </div>
                            <div>
                                <i class="fas fa-camera bg-purple"></i>

                                <div class="timeline-item">
                                <span class="time"><i class="far fa-clock"></i> 2 days ago</span>

                                <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                                <div class="timeline-body">
                                    <img src="http://placehold.it/150x100" alt="...">
                                    <img src="http://placehold.it/150x100" alt="...">
                                    <img src="http://placehold.it/150x100" alt="...">
                                    <img src="http://placehold.it/150x100" alt="...">
                                </div>
                                </div>
                            </div>
                            <div>
                                <i class="far fa-clock bg-gray"></i>
                            </div>
                            </div>
                        </div> --}}

{{-- ------------------------------------------------------------------------------------------------------------------------- --}}
<!------------------------------------------------------ /.tab-pane ------------------------------------------------------------>
{{-- ------------------------------------------------------------------------------------------------------------------------- --}}


                        {{-- <div class="tab-pane" id="settings">
                            <form class="form-horizontal">
                            <div class="form-group row">
                                <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                <input type="email" class="form-control" id="inputName" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputName2" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputExperience" class="col-sm-2 col-form-label">Experience</label>
                                <div class="col-sm-10">
                                <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                    <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                    </label>
                                </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-danger">Submit</button>
                                </div>
                            </div>
                            </form>
                        </div> --}}
                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>
</div>

@endsection