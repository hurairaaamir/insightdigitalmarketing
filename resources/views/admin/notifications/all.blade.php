@extends('layouts.admin')
@section('styles')
<style>
    .red-notification{
            margin-bottom:4px;
            background-color:#F1F1F1!important;
        }
        .red-notification:hover{
            background-color:#F1F1F1!important;
        }
</style>
@endsection
@section('content')

<div class="card">
    <div class="card-header">
        Notifications
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th style="min-width:140px;">
                           Title 
                        </th>
                        <th>
                            Description
                        </th>
                        <th>
                            TimeStamp
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(auth()->user()->unreadNotifications as $key => $notification)
                        <tr>
                            <td>

                            </td>
                            <td>
                                <img src="{{$notification->data["image"]}}" style="width:40px;height:40px;border-radius:100%;">
                                {{ $notification->data["title"] ?? '' }}
                            </td>
                            <td>
                                {{ $notification->data["description"] ?? '' }}
                            </td>
                            <td>
                                {{ $notification->created_at ?? '' }}
                            </td>
                            
                            <td>
                                <form action="{{url('/notification/admin/delete/')}}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>                                
                            </td>

                        </tr>
                    @endforeach
                    @foreach(auth()->user()->readNotifications as $key => $notification)
                        <tr class="red-notification">
                            <td>

                            </td>
                            <td>
                                <img src="{{$notification->data["image"]}}" style="width:40px;height:40px;border-radius:100%;">
                                {{ $notification->data["title"] ?? '' }}
                            </td>
                            <td>
                                {{ $notification->data["description"] ?? '' }}
                            </td>
                            <td>
                                {{ $notification->created_at ?? '' }}
                            </td>
                            
                            <td>
                                <form action="{{url('/notification/admin/delete/')}}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                </form>                                
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('user_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection