<footer id="footer">

    <div id="copyrights">
    
        <div class="container ">
    
            <div class="row">
                <div class="col-sm-6">
                    <div class="copyright-links">
                        <a href="">Contact</a>
                        / <a href="">Imprint</a>
                        / <a href="">FAQ</a>
                        / <a href="">Blog</a>
                        / <a href="">Lexicon</a>
                        / <a href="">Jobs</a>
                        / <a href="">Press</a>
                        / <a href=" ">Terms and Conditions</a>
                        / <a href="">Data Protection</a>
                    </div>
                    <a href="#" class="changeLanguage" data-lang="de"><i class="flag-icon flag-icon-de"></i></a>&nbsp;
                    <a href="#" class="changeLanguage" data-lang="en"><i class="flag-icon flag-icon-gb"></i></a>&nbsp;
                    <a href="#" class="changeLanguage" data-lang="fr"><i class="flag-icon flag-icon-fr"></i></a>
                </div>
                <div class="col-sm-6 sm-hidden">
                    <div class="footer-social">
                        <a href="https://www.facebook.com/ReachHero-374608429363836" target="_blank" >
                            <span><i class="fab fa-facebook-f"></i></span>
                            <span><i class="fab fa-facebook-f"></i></span>
                        </a>
    
                        <a href="https://www.instagram.com/reachhero_de" target="_blank" >
                            <span><i class="fab fa-instagram"></i></span>
                            <span><i class="fab fa-instagram"></i></span>
                        </a>
    
                        <a href="https://twitter.com/reachhero" target="_blank" >
                            <span><i class="fab fa-twitter"></i></span>
                            <span><i class="fab fa-twitter"></i></span>
                        </a>
    
                        <a href="https://www.snapchat.com/add/ReachHero" target="_blank">
                            <span><i class="fab fa-snapchat-ghost"></i></span>
                            <span><i class="fab fa-snapchat-ghost"></i></span>
                        </a>
    
                    </div>
    
                    <div class="contact-line"><i class="fa fa-envelope"></i>contact@reachhero.de <span class="middot"><i class="fa fa-mobile-alt"></i> 030.2084989.82 </span> </div>
            </div>
    
    
    
        </div>
    
    </div>
    
    </footer>