<header>
    <nav class="navbar navbar-expand-lg nav-large">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="#">
            <img class="logo-black" src="/FrontEndImages/logo-black.png" alt="">
            <img class="logo-white" src="/FrontEndImages/logo-small.png" alt="">

        </a>


        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="../influencer">For Influencers </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../company">For Brands + Agencies</a>
                </li>
                @if(auth()->id())
                    @if(auth()->user()->role == 'influencer' || auth()->user()->role==='admin')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{url('/influencer/dashboard')}}">Influencers Dashboard</a>
                        </li>
                    @endif
                    @if(auth()->user()->role == 'brand'|| auth()->user()->role==='admin')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{url('/brand/dashboard')}}">Campanies Dashboard</a>
                        </li>
                    @endif
                {{-- @if(auth()->user()) --}}
                    @if(auth()->user()->role==='admin')
                        <li class="nav-item active">
                            <a class="nav-link" href="{{url('/admin')}}">Admin</a>
                        </li>
                    @endif
                {{-- @endif --}}
                <li class="nav-item">
                    <a class="nav-link " href="{{route('logout')}}"  >LogOut</a>
                </li>
                @else
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Register
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('register')}}"><i class="fas fa-user"></i>Influencer</a>
                            <a class="dropdown-item" href="{{url('/brand/register')}}"><i class="fas fa-university"></i>Brand</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{route('login')}}"  >login</a>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
</header>