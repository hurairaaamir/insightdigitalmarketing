<div class="chat-button" onclick="chat_show()" id="chat-btn-id">
    <img src="https://assetscdn-wchat.freshchat.com/static/assets/images/freshchat-line-1cc53925eae96a19142ee7b807093272.svg" alt="Freshchat">
</div>
<div class="chat" >
    <div class="cross chat-hider" onclick="chat_show()" id="chat-x">X</div>
    <div class="chat-container chat-hider" id="chat-id">
        
        <div class="chat-child1">
            <div class="chat-logo"><img class="animated zoomIn faster" src="https://assetscdn-wchat.freshchat.com/static/assets/images/freshchat-line-1cc53925eae96a19142ee7b807093272.svg" alt="Freshchat"></div>
            <p>INBOX</p>
            <span><img src="https://assetscdn-wchat.freshchat.com/static/assets/images/ic_offline-2ba96002b738a49d340002ab84587a3a.svg">offline</span>
        </div>
        <div class="chat-child2">
            <div class="chat-inner chatscroll">
                <div id="chat_inner_id">
                    <div class="text-section">
                        <div><span>ReachHero GmbH</span></div>
                        <div class="text-inner">
                            <div class="part1">
                                <img src="https://276857890134288.webpush.freshchat.com/b1219644b63f5393840135d1a65ee555f87f9db3a34c2c923106c7d355019733/f_hlimage/u_aa1dd7ccfa1fb406279672c1ecf0373c14c9817ad65b0ff6774b3f57e1cfc9a8/img_1570347700481.png" height="24px" width="24px">
                            </div>
                            <div class="part2 text-radius1">
                                <p>Hallo 😀! Schau Dich um! Wenn Du Fragen hast, setze Dich mit uns in Verbindung.</p>
                            </div>
                        </div>
                    </div>
                    <div class="text-section">
                        <div><span>ReachHero GmbH</span></div>
                        <div class="text-inner">
                            <div class="part1">
                                <img src="https://276857890134288.webpush.freshchat.com/b1219644b63f5393840135d1a65ee555f87f9db3a34c2c923106c7d355019733/f_hlimage/u_aa1dd7ccfa1fb406279672c1ecf0373c14c9817ad65b0ff6774b3f57e1cfc9a8/img_1570347700481.png" height="24px" width="24px">
                            </div>
                            <div class="part2 text-radius1">
                                <p>Hi! Du kontaktierst uns außerhalb unserer Geschäftszeiten. Gerne kannst du uns aber eine Nachricht hinterlassen und wir melden uns schnellstmöglich bei dir, sobald wir wieder da sind.</p>
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="chat-form">
                    <form id="chat_submit">
                        <input type="text" class="form-control" placeholder="Your email" name="email" required>
                        <textarea class="form-control" placeholder="How can we help you?" name="message" required></textarea>
                        <button class="btn-danger btn-block mt-1 btn mb-1">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>    
</div>
