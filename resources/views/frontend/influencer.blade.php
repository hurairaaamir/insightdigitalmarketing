@extends('layouts.front')

@section('content')
<div class="hero">
    <div class="gradient"></div>
    <div class="overlay">

        <h2 >Influencer marketing</h2>
        <h1 >Product placements make you more with your social channels.</h1>

        <div class="btn-area">
            <a href="#"><i class="fas fa-paper-plane"></i> view product placement</a>
        </div>

    </div>
    <div class="video">
        <video poster="/video/frontend/header.jpg" preload="auto" loop="" autoplay="" muted="" >
            <source src="/video/frontend/header.mp4" type="video/mp4">
            <source src="/video/frontend/header.webm" type="video/webm">
        </video>
    </div>
</div>

<!-- Hero Section Ends-->
<!--influncer social-->
<div class="influncer-social">
    <div class="heading-center">

    <h2>You decide which product placements you would like to advertise on your social channels. Because this enables you to live faster from your social media channels.</h2>
    </div>
    <div class="social">
        <a href=""> <i class="fab fa-youtube"></i></a>
        <a href=""> <i class="fab fa-snapchat-ghost"></i></a>
        <a href=""> <i class="fab fa-instagram"></i></a>
        <a href=""> <i class="fab fa-facebook-f"></i></a>
        <a href=""> <i class="fab fa-twitter"></i></a>
        <a href=""> <i class="fab fa-tumblr"></i></a>
    </div>
</div>
<!--/   influncer social-->

<!--blocks-->
<section class="influncer-blocks">
    <div class="container ">
        <div class="row">
            <div class="col-md-4">
                <div class="feature-box ">
                    <div class="fbox-icon">
                        <i class="far fa-times-circle"></i>
                    </div>
                    <h3>No contract</h3>
                    <p> At ReachHero, every influencer can participate from 1000 subscribers. There is no catch! You retain full rights to your channel and can cancel your Reachhero account at any time.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-box ">
                    <div class="fbox-icon">
                        <i class="far fa-money-bill-alt"></i>
                    </div>
                    <h3>Easily earn money</h3>
                    <p> With product placements on your social media channels, youmore and can live faster from your channels. And best of all: Reachhero is completely free for you!</span>
                    </p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="feature-box ">
                    <div class="fbox-icon">
                        <i class="fas fa-chart-line"></i>
                    </div>
                    <h3>Grow faster</h3>
                    <p> By taking additional product placements, you can professionalize your social media channels faster and buy the equipment you really need.</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ blocks-->
<!--screens-->
<section class="container screens">

    <div class=" center ">

        <h2>This is how it works as an influencer</h2>

        <div class="heading-block  center" >
            <h2>CHOOSE PRODUCT PLACEMENTS</h2>
            <div class="d-flex justify-content-center">
                <p >After registering with Reachhero, you can browse through the unique product placements and be inspired by the products and services.</p>
            </div>
            <img src="./FrontEndImages/screens/selectclient-small.png">
        </div>

        <div class="heading-block  center" data-animate="fadeIn">
            <h2>APPLY DIRECTLY TO PRODUCT PLACEMENT</h2>
            <div class="d-flex justify-content-center">
                <p>Reachhero allows you to send offers to companies where you like the product placement. With your offer, say what kind of post you want for the product placement and what budget you want for it. Reachhero will then tell you directly which company has accepted your offer.</p>
            </div>
            <img src="./FrontEndImages/screens/selectclient-small.png">
        </div>
        <div class="heading-block  center" data-animate="fadeIn">
            <h2>PRODUCED AND SHARED - RECEIVED MONEY</h2>
            <div class="d-flex justify-content-center">
                <p>After sharing your finished product placement post, tweet or video with your subscribers on your channel, you will receive the agreed price. It is really that easy!</p>
            </div>
            <img src="./FrontEndImages/screens/selectclient-small.png">
        </div>
    </div>

</section>
<!--/screens-->
<!--count-->
<div class="count-influncer">
    <h4>Over 80,000+ influencers have already registered</h4>
</div>
<!--/ count-->
<!--influncers -->
<section>
    <div class="influncers">
        <h2>INFLUENCERS WHO USE REACHHERO</h2>
        <p class="brace">(to show just a few)</p>
        <div class="container">
            <div class="row">
                <div class="col-6 col-sm-3">
                    <img src="./FrontEndImages/youtubers/youtuber-3.jpg" class="img-responsive">
                </div>
                <div class="col-6 col-sm-3">
                    <img src="./FrontEndImages/youtubers/youtuber-3.jpg" class="img-responsive">
                </div>
                <div class="col-6 col-sm-3">
                    <img src="./FrontEndImages/youtubers/youtuber-3.jpg" class="img-responsive">
                </div>
                <div class="col-6 col-sm-3">
                    <img src="./FrontEndImages/youtubers/youtuber-3.jpg" class="img-responsive">
                </div>
                <div class="col-6 col-sm-3">
                    <img src="./FrontEndImages/youtubers/youtuber-3.jpg" class="img-responsive">
                </div>
                <div class="col-6 col-sm-3">
                    <img src="./FrontEndImages/youtubers/youtuber-3.jpg" class="img-responsive">
                </div>
                <div class="col-6 col-sm-3">
                    <img src="./FrontEndImages/youtubers/youtuber-3.jpg" class="img-responsive">
                </div>
                <div class="col-6 col-sm-3">
                    <img src="./FrontEndImages/youtubers/youtuber-3.jpg" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</section>
<!--influncers -->

<!--placement-->
<section class="placement">
    <div class="heading-center">
        <h2>Jetzt Product Placements finden & Geld verdienen</h2>
        <p class="lead">
            Find unique product placements quickly and easily. You’re just a few clicks away.
        </p>
    </div>
    <div class="btn-area">
        <a href="#"><i class="fas fa-paper-plane"></i> view product placement</a>
    </div>
    <p>You have further questions or need help? <a href="">To the FAQ page</a></p>
</section>

@endsection

@section('scripts')

@endsection