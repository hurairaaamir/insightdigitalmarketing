@extends('layouts.front')

@section('content')
<div class="hero">
    <div class="gradient"></div>
    <div class="overlay">

        <h2 >ARE YOU LOOKING FOR <br> MORE <br><span>CUSTOMERS</span></h2>
        <h1 >We help you to grow your brand through influencer <br> marketing on social channels.</h1>

        <div class="btn-area">
            <a href="#"> Find influencer now</a>
            <a href="#"> What is reach hero?</a>
        </div>

    </div>
    <div class="video">
        <video poster="/video/frontend/header.jpg" preload="auto" loop="" autoplay="" muted="" >
            <source src="/video/frontend/header.mp4" type="video/mp4">
            <source src="/video/frontend/header.webm" type="video/webm">
        </video>
    </div>
</div>
<!-- Hero Section Ends-->

<!--customer trust -->

<section class="customer-trust">
    <div class="heading-center">
        <h2>Customers who trust us</h2>
    </div>
    <div class="slider ">
        <div class="customer-trust-carousel ">
            <div class="customer-trust-slide">
                <img src="./FrontEndImages/customer-trust/aeg-logo.png" alt="">
            </div>
            <div class="customer-trust-slide">
                <img src="./FrontEndImages/customer-trust/aeg-logo.png" alt="">
            </div>
            <div class="customer-trust-slide">
                <img src="./FrontEndImages/customer-trust/aeg-logo.png" alt="">
            </div>
            <div class="customer-trust-slide">
                <img src="./FrontEndImages/customer-trust/aeg-logo.png" alt="">
            </div>
            <div class="customer-trust-slide">
                <img src="./FrontEndImages/customer-trust/essence-logo.png" alt="">
            </div>
            <div class="customer-trust-slide">
                <img src="./FrontEndImages/customer-trust/essence-logo.png" alt="">
            </div>
            <div class="customer-trust-slide">
                <img src="./FrontEndImages/customer-trust/essence-logo.png" alt="">
            </div>
            <div class="customer-trust-slide">
                <img src="./FrontEndImages/customer-trust/essence-logo.png" alt="">
            </div>
        </div>
    </div>
</section>
<!--/ customer trust -->

<!--video-->
<section class="video-section">
    <div class="heading-center">
        <h2>
            WHAT IS REACHHERO?
        </h2>
    </div>
    <div class="video-di">
        <div class="video-content">
            <div class="video-banner">
                <img src="./FrontEndImages/youtube-video-preview.jpg" alt="" >
                <span><i class="fa fa-play"></i></span>
            </div>
            <iframe style="max-width: 768px; width: 758px; height: 388px" src="https://www.youtube.com/embed/7JVNjwilPMU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
    <div class="btn-area">
        <a href=""><i class="fas fa-arrow-right"></i>Find Influencer Now</a>
    </div>
</section>
<!--/video-->

<!--Influencer Marketing-->
<section class="influencer-marketing">
    <div class="heading-center">
        <h2>INFLUENCER MARKETING WITH REACHHERO</h2>
        <p class="lead">ReachHero offers you a 360 ° package in influencer marketing. From self-service with our automated influencer marketplace to managed service with us as your influencer marketing agency , we support you in every area.
        </p>
    </div>
    <div class="container">
        <div class="row justify-content-center"><img src="./FrontEndImages/influncers/macbook-platform.png" class="img-responsive" alt=""></div>
        <!--blocks-->
        <section class="influncer-blocks">
            <div class="container ">
                <div class="row">
                    <div class="col-md-4">
                        <div class="feature-box ">
                            <div class="fbox-icon">
                                <i class="far fa-times-circle"></i>
                            </div>
                            <h3>SELF-SERVICE</h3>
                            <p> Start your influencer campaigns through our automated marketplace
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box ">
                            <div class="fbox-icon">
                                <i class="far fa-money-bill-alt"></i>
                            </div>
                            <h3>AGENCY SERVICE
                            </h3>
                            <p>We take over the entire campaign process for you. From the right influencer selection to detailed reporting.
                            </p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="feature-box ">
                            <div class="fbox-icon">
                                <i class="fas fa-chart-line"></i>
                            </div>
                            <h3>INFLUENCER RELATIONSHIP MANAGEMENT SOFTWARE (IRM)</h3>
                            <p>Manage your influencer relationships, manage your influencer contacts and control your campaigns
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/ blocks-->
    </div>
</section>
<!--/ Influencer Marketing-->

<!--self service-->
<section class="self-service">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="./FrontEndImages/influncers/rh-marktplatz-angebote.png" alt="">
            </div>
            <div class="col-md-8">
                <div class="heading">
                    <h2><a href="#" role="button" data-toggle="modal" data-target=".bd-example-modal-xl" class="self-service-trig">Self-service - marketplace</a></h2>
                    <span>Start your influencer campaigns through our automated marketplace</span>
                </div>
                <p class="text">With self-service, you conduct professional influencer marketing independently by searching for and finding suitable influencers from our influencer database or by independently influencing your campaigns with ideas and budgets. You manage your campaigns via our fully automated marketplace including automatic influencer billing of all your cooperations .</p>
                <div class="row grids">
                    <div class="col-sm-4">
                        <span>1.</span>
                        <h3>START YOUR CAMPAIGN</h3>
                        <p>Whether product placement, giveaway or performance campaign. Create your campaign and put it online on the ReachHero marketplace</p>
                    </div>
                    <div class="col-sm-4">
                        <span>2.</span>
                        <h3>START YOUR CAMPAIGN</h3>
                        <p>Whether product placement, giveaway or performance campaign. Create your campaign and put it online on the ReachHero marketplace</p>
                    </div>
                    <div class="col-sm-4">
                    <span>3.</span>
                        <h3>START YOUR CAMPAIGN</h3>
                        <p>Whether product placement, giveaway or performance campaign. Create your campaign and put it online on the ReachHero marketplace</p>

                    </div>
                </div>
            </div>

        </div>
        <div class="row justify-content-center">
            <a href="#" role="button" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-red-text">Read More</a>
        </div>
    </div>
</section>
<!--self service-->
<!--Self service modal-->
    <div class="modal fade bd-example-modal-xl self-service-modal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-body">
                   <div class="row justify-content-center">
                       <div class="feature-box ">
                           <div class="fbox-icon">
                               <i class="far fa-times-circle"></i>
                           </div>
                           <h3>SELF-SERVICE</h3>
                           <p> Start your influencer campaigns through our automated marketplace
                           </p>
                       </div>
                   </div>

                    <div class="row justify-content-center">
                      <div class="col-md-8">
                          <!-- tab pane-->
                          <ul class="nav nav-tabs" id="myTab" role="tablist">
                              <li class="nav-item">
                                  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                                      <i class="far fa-times-circle"></i>
                                  </a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                                      <i class="far fa-times-circle"></i>
                                  </a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                                      <i class="far fa-times-circle"></i>
                                  </a>
                              </li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                  <h2>DATA-CONTROLLED INFLUENCER SELECTION</h2>
                                  <ul>
                                      <li>Influencer search with quantitative and qualitative filter criteria</li>
                                      <li>Influencer search with quantitative and qualitative filter criteria</li>
                                      <li>Influencer search with quantitative and qualitative filter criteria</li>
                                  </ul>
                              </div>
                              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                   <h2>DATA-CONTROLLED INFLUENCER SELECTION</h2>
                                  <ul>
                                      <li>Influencer search with quantitative and qualitative filter criteria</li>
                                      <li>Influencer search with quantitative and qualitative filter criteria</li>
                                      <li>Influencer search with quantitative and qualitative filter criteria</li>
                                  </ul>
                              </div>
                              <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                   <h2>DATA-CONTROLLED INFLUENCER SELECTION</h2>
                                  <ul>
                                      <li>Influencer search with quantitative and qualitative filter criteria</li>
                                      <li>Influencer search with quantitative and qualitative filter criteria</li>
                                      <li>Influencer search with quantitative and qualitative filter criteria</li>
                                  </ul>
                              </div>
                          </div>
                          <!-- tab pane-->
                          <div class="btn-area">
                              <a href=""><i class="fas fa-arrow-right" aria-hidden="true"></i>Find Influencer Now</a>
                          </div>
                      </div>
                    </div>
                    <br>
                    <hr>
                    <br>
                    <div class="row justify-content-center">
                        <a href="#" role="button" type="button" class="btn-red-text" data-dismiss="modal">Shut Down</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--Self service modal-->

<!--AGENCY SERVICE-->
<section class="self-service">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="heading">
                    <h2><a href="#" role="button" data-toggle="modal" data-target=".agency-service-modal" class="self-service-trig">Self-service - marketplace</a></h2>
                    <span>Start your influencer campaigns through our automated marketplace</span>
                </div>
                <p class="text">With self-service, you conduct professional influencer marketing independently by searching for and finding suitable influencers from our influencer database or by independently influencing your campaigns with ideas and budgets. You manage your campaigns via our fully automated marketplace including automatic influencer billing of all your cooperations .</p>
                <div class="row grids">
                    <div class="col-sm-4">
                        <span class="icon-self"><i class="fa fa-rocket"></i></span>
                        <h3>START YOUR CAMPAIGN</h3>
                        <p>Whether product placement, giveaway or performance campaign. Create your campaign and put it online on the ReachHero marketplace</p>
                    </div>
                    <div class="col-sm-4">
                        <span class="icon-self"><i class="fa fa-rocket"></i></span>
                        <h3>START YOUR CAMPAIGN</h3>
                        <p>Whether product placement, giveaway or performance campaign. Create your campaign and put it online on the ReachHero marketplace</p>
                    </div>
                    <div class="col-sm-4">
                        <span class="icon-self"><i class="fa fa-rocket"></i></span>
                        <h3>START YOUR CAMPAIGN</h3>
                        <p>Whether product placement, giveaway or performance campaign. Create your campaign and put it online on the ReachHero marketplace</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <img src="./FrontEndImages/influncers/rh-marktplatz-angebote.png" alt="">
            </div>

        </div>
        <div class="row justify-content-center">
            <a href="#" role="button" data-toggle="modal" data-target=".agency-service-modal" class="btn-red-text">Read More</a>
        </div>
    </div>
</section>
<!--AGENCY SERVICE-->
<!--AGENCY SERVICE modal-->
<div class="modal fade agency-service-modal self-service-modal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="feature-box ">
                        <div class="fbox-icon">
                            <i class="far fa-times-circle"></i>
                        </div>
                        <h3>SELF-SERVICE</h3>
                        <p> Start your influencer campaigns through our automated marketplace
                        </p>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <!-- tab pane-->
                        <ul class="nav nav-tabs"  role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active"  data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">
                                    <i class="far fa-times-circle"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#profile-1" role="tab" aria-controls="profile-1" aria-selected="false">
                                    <i class="far fa-times-circle"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#contact-1" role="tab" aria-controls="contact-1" aria-selected="false">
                                    <i class="far fa-times-circle"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" i>
                            <div class="tab-pane fade show active" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                                <h2>DATA-CONTROLLED INFLUENCER SELECTION</h2>
                                <ul>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab">
                                <h2>DATA-CONTROLLED INFLUENCER SELECTION</h2>
                                <ul>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="contact-1" role="tabpanel" aria-labelledby="contact-tab">
                                <h2>DATA-CONTROLLED INFLUENCER SELECTION</h2>
                                <ul>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                </ul>
                            </div>
                        </div>
                        <!-- tab pane-->
                        <div class="btn-area">
                            <a href=""><i class="fas fa-arrow-right" aria-hidden="true"></i>Find Influencer Now</a>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <br>
                <div class="row justify-content-center">
                    <a href="#" role="button" type="button" class="btn-red-text" data-dismiss="modal">Shut Down</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--AGENCY SERVICE modal-->


<!--Relation Management-->
<section class="self-service relation-management">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="./FrontEndImages/influncers/rh-marktplatz-angebote.png" alt="">
            </div>

            <div class="col-md-8">
                <div class="heading">
                    <h2><a href="#" role="button" data-toggle="modal" data-target=".agency-service-modal" class="self-service-trig">Self-service - marketplace</a></h2>
                    <span>Start your influencer campaigns through our automated marketplace</span>
                </div>
                <p class="text">With self-service, you conduct professional influencer marketing independently by searching for and finding suitable influencers from our influencer database or by independently influencing your campaigns with ideas and budgets. You manage your campaigns via our fully automated marketplace including automatic influencer billing of all your cooperations .</p>
                <div class="row grids">
                    <div class="col-sm-4">
                        <span class="icon-self"><i class="fa fa-rocket"></i></span>
                        <h3>START YOUR CAMPAIGN</h3>
                        <p>Whether product placement, giveaway or performance campaign. Create your campaign and put it online on the ReachHero marketplace</p>
                    </div>
                    <div class="col-sm-4">
                        <span class="icon-self"><i class="fa fa-rocket"></i></span>
                        <h3>START YOUR CAMPAIGN</h3>
                        <p>Whether product placement, giveaway or performance campaign. Create your campaign and put it online on the ReachHero marketplace</p>
                    </div>
                    <div class="col-sm-4">
                        <span class="icon-self"><i class="fa fa-rocket"></i></span>
                        <h3>START YOUR CAMPAIGN</h3>
                        <p>Whether product placement, giveaway or performance campaign. Create your campaign and put it online on the ReachHero marketplace</p>

                    </div>
                </div>
            </div>

        </div>
        <div class="row justify-content-center">
            <a href="#" role="button" data-toggle="modal" data-target=".agency-service-modal" class="btn-red-text">Read More</a>
        </div>
    </div>
</section>
<!--Relation Management SERVICE-->
<!--Relation Management modal-->
<div class="modal fade agency-service-modal self-service-modal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row justify-content-center">
                    <div class="feature-box ">
                        <div class="fbox-icon">
                            <i class="far fa-times-circle"></i>
                        </div>
                        <h3>SELF-SERVICE</h3>
                        <p> Start your influencer campaigns through our automated marketplace
                        </p>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <!-- tab pane-->
                        <ul class="nav nav-tabs"  role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active"  data-toggle="tab" href="#home-2" role="tab" aria-controls="home-2" aria-selected="true">
                                    <i class="far fa-times-circle"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#profile-2" role="tab" aria-controls="profile-2" aria-selected="false">
                                    <i class="far fa-times-circle"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  data-toggle="tab" href="#contact-2" role="tab" aria-controls="contact-2" aria-selected="false">
                                    <i class="far fa-times-circle"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" i>
                            <div class="tab-pane fade show active" id="home-2" role="tabpanel" aria-labelledby="home-tab">
                                <h2>DATA-CONTROLLED INFLUENCER SELECTION</h2>
                                <ul>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="profile-2" role="tabpanel" aria-labelledby="profile-tab">
                                <h2>DATA-CONTROLLED INFLUENCER SELECTION</h2>
                                <ul>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="contact-2" role="tabpanel" aria-labelledby="contact-tab">
                                <h2>DATA-CONTROLLED INFLUENCER SELECTION</h2>
                                <ul>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                    <li>Influencer search with quantitative and qualitative filter criteria</li>
                                </ul>
                            </div>
                        </div>
                        <!-- tab pane-->
                        <div class="btn-area">
                            <a href=""><i class="fas fa-arrow-right" aria-hidden="true"></i>Find Influencer Now</a>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <br>
                <div class="row justify-content-center">
                    <a href="#" role="button" type="button" class="btn-red-text" data-dismiss="modal">Shut Down</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Relation Management modal-->
<!--successful blocks-->
<section class="successful-blocks">
    <h2>SUCCESSFUL INFLUENCER MARKETING WITH REACHHERO</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="successful-icons"><i class="fa fa-check"></i></span>
                <h3>EXTENSIVE <span>PLATFORM</span></h3>
                <hr>
                <p>Our platform brings together more than 3,000 companies and over 70,000 linked influencer accounts - making us one of the leading platforms for successful influencer marketing.</p>
            </div>
            <div class="col-md-4">
                <span class="successful-icons"><i class="fa fa-check"></i></span>
                <h3>EXTENSIVE <span>PLATFORM</span></h3>
                <hr>
                <p>Our platform brings together more than 3,000 companies and over 70,000 linked influencer accounts - making us one of the leading platforms for successful influencer marketing.</p>
            </div>
            <div class="col-md-4">
                <span class="successful-icons"><i class="fa fa-check"></i></span>
                <h3>EXTENSIVE <span>PLATFORM</span></h3>
                <hr>
                <p>Our platform brings together more than 3,000 companies and over 70,000 linked influencer accounts - making us one of the leading platforms for successful influencer marketing.</p>
            </div>
        </div>
    </div>
</section>
<!--/successful blocks-->
<!--youtube videos-->
<div class="youtube-videos">
    <div class="heading-center">
        <h2>PRODUCT PLACEMENT EXAMPLES IN YOUTUBE VIDEOS</h2>
        <div class="lead">This is how influencers present your products and reach thousands of viewers.</div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="video-card">
                    <div class="video-di">
                        <div class="video-content">
                            <div class="video-banner">
                                <img src="./FrontEndImages/youtube-video-preview.jpg" alt="">
                                <span><i class="fa fa-play" aria-hidden="true"></i></span>
                            </div>
                            <iframe style="width: 100%; height: auto" src="https://www.youtube.com/embed/7JVNjwilPMU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="video-custom-info">
                        <h4>COOL DRUG NEWS ...</h4>
                        <div class="video-custom-info-username">Shirin Gosh</div>
                       <div class="youtube-counts">
                           <div class="video-custom-info-half">
                               <i class="fa fa-eye"></i>
                               <span>13.094</span>
                           </div>
                           <div class="video-custom-info-half">
                               <i class="fas fa-thumbs-up"></i>
                               <span>175</span>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="video-card">
                    <div class="video-di">
                        <div class="video-content">
                            <div class="video-banner">
                                <img src="./FrontEndImages/youtube-video-preview.jpg" alt="">
                                <span><i class="fa fa-play" aria-hidden="true"></i></span>
                            </div>
                            <iframe style="width: 100%; height: auto" src="https://www.youtube.com/embed/7JVNjwilPMU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="video-custom-info">
                        <h4>COOL DRUG NEWS ...</h4>
                        <div class="video-custom-info-username">Shirin Gosh</div>
                       <div class="youtube-counts">
                           <div class="video-custom-info-half">
                               <i class="fa fa-eye"></i>
                               <span>13.094</span>
                           </div>
                           <div class="video-custom-info-half">
                               <i class="fas fa-thumbs-up"></i>
                               <span>175</span>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="video-card">
                    <div class="video-di">
                        <div class="video-content">
                            <div class="video-banner">
                                <img src="./FrontEndImages/youtube-video-preview.jpg" alt="">
                                <span><i class="fa fa-play" aria-hidden="true"></i></span>
                            </div>
                            <iframe style="width: 100%; height: auto" src="https://www.youtube.com/embed/7JVNjwilPMU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="video-custom-info">
                        <h4>COOL DRUG NEWS ...</h4>
                        <div class="video-custom-info-username">Shirin Gosh</div>
                       <div class="youtube-counts">
                           <div class="video-custom-info-half">
                               <i class="fa fa-eye"></i>
                               <span>13.094</span>
                           </div>
                           <div class="video-custom-info-half">
                               <i class="fas fa-thumbs-up"></i>
                               <span>175</span>
                           </div>
                       </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<div class="video-section">
    <div class="btn-area">
        <a href="#" ><i class="fas fa-arrow-right"></i>Find Influencer Now</a>
    </div>
</div>

<!-- Influencer Marketing-->
<section class="influencer-marketing">
    <div class="heading-center">
        <h2>INFLUENCER MARKETING HANDOUT AS PDF</h2>
        <p class="lead center">Subscribe to our newsletter now and receive our influencer marketing handout with all information for free!</p>
    </div>
    <div class="container">
        <div class="row pb-3">
            <div class="col-sm-7 d-flex justify-content-center align-items-center flex-column">
                <form >
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="Organization">
                                <i class="fas fa-briefcase"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control" placeholder="Organization" aria-label="Organization" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="email">
                                <i class="fas fa-briefcase"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control" placeholder="Email" aria-label="email" aria-describedby="basic-addon1">
                    </div>
                </form>
                <div class="btn-area row">
                    <a href="#" role="button">Send Handout</a>
                </div>
            </div>
            <div class="col-sm-5 d-flex justify-content-center">
                <img src="./FrontEndImages/company/marketing-handout-v2.png" class="img-responsive" alt="">
            </div>
        </div>
    </div>
</section>
<!-- Influencer Marketing-->
<!--Find Influencer Now-->
<section class="find-influencer">
    <div class="heading-center">
        <h2>START INFLUENCER MARKETING NOW</h2>
        <p class="lead center">
            Now use our groundbreaking tool to connect you with the right influencers to increase the awareness and reach of your brand.
        </p>
    </div>
    <div class="video-section">
        <div class="btn-area">
            <a href="#"><i class="fas fa-arrow-right"></i>Find Influencer Now</a>
        </div>
    </div>
        <div class="py-2">
            <h4 class="center">Not Sure yet?</h4>
            <p class="lead center">Simply register on <a href="" class="btn-red-text">030 208498982</a> and speak to us!</p>
            <p class="lead center">
                You are also welcome to chat with our support team, write us an <a class="btn-red-text" href="">email</a> or read our <a
                    class="btn-red-text" href="">FAQs</a> .

            </p>
        </div>
</section>



@endsection
@section('scripts')
<script>
    /*==========================*/
    /*    Company page carousel*/
    /*==========================*/
    $(".customer-trust-carousel").owlCarousel({
        loop: true,
        dots: false,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 4000,
        // autoWidth: true,
        items:7 ,
        responsiveClass:true,
        responsive:{
            0:{
                items:2,
            },
            600:{
                items:3,
            },
            1100:{
                items:7,
            }
        }

    } );
</script>
@endsection