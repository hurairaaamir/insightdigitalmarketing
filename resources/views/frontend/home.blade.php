@extends('layouts.front')

@section('content')

<!-- Hero Section Start-->
<div class="hero">
<div class="gradient"></div>
<div class="overlay">

        <h2 >Influencer marketing</h2>
        <h1 >We are marketplace. Database. Agency. <br> All from a single source.</h1>
    <div class="social">
        <i class="fab fa-youtube"></i>
        <i class="fab fa-snapchat-ghost"></i>
        <i class="fab fa-instagram"></i>
        <i class="fab fa-facebook-f"></i>
        <i class="fab fa-twitter"></i>
        <i class="fab fa-tumblr"></i>
    </div>
    <div class="btn-area">
        <a href="../company"> i am a company</a>
        <a href="../influencer"> i am an influencer</a>
    </div>

</div>
<div class="video">
    <video poster="/video/frontend/header.jpg" preload="auto" loop="" autoplay="" muted="" >
        <source src="/video/frontend/header.mp4" type="video/mp4">
        <source src="/video/frontend/header.webm" type="video/webm">
    </video>
</div>
</div>
<!-- Hero Section Ends-->
<!-- Customers who trust us-->

<section class="trust-section">
<div class="heading-center">
<h2>Customers who trust us</h2>
<div class="container">
    <div class="row">
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/aeg-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/essence-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/universal-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/nintendo-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/lidl-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/sony-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/the-body-shop-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/rewe-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/lidl-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/sony-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/the-body-shop-logo.jpg" class="img-responsive"></div>
        <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/rewe-logo.jpg" class="img-responsive"></div>
    </div>
</div>
</div>
</section>
<!--/ Customers who trust us-->
<!--    What is reach-hero-->
<section class="reach-hero">
<div class="container ">
    <div class="row ">

        <div class="col-lg-5">
            <div class="heading-block ">
                <h2>What is ReachHero?</h2>
            </div>
            <p class="lead">ReachHero is the <strong>one-stop source for influencer marketing</strong> Whether as a self-service with our or with full service as an  agency . We bring you as a company together with authentic influencers and bring your influencer marketing campaigns to success.</p>
        </div>

        <div class="col-lg-7">
            <div class="images-rh">
                <img src="./FrontEndImages/reach-hero/rh-browser.png" alt="">
                <img src="./FrontEndImages/reach-hero/rh-mobile.png" alt="">
            </div>
        </div>

    </div>
</div>
</section>
<!--/   What is reach-hero-->

<!-- Press about reach hero-->

<section class="press-section">
<div class="heading-center">
    <h2>The press about ReachHero</h2>
    <div class="container">

        <div class="row">
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/aeg-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/essence-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/universal-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/nintendo-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/lidl-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/sony-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/the-body-shop-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/rewe-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/lidl-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/sony-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/the-body-shop-logo.jpg" class="img-responsive"></div>
            <div class="col-6 col-sm-4 col-md-2" ><img src="/FrontEndImages/logos/rewe-logo.jpg" class="img-responsive"></div>

        </div>
    </div>
</div>
</section>
<!--/ Press about reach hero-->

<!--    More Information-->
<section class="more-info">
<div class="container">
    <div class="heading-center">
        <h2>Do you need more information?</h2>
        <p class="lead">Simply choose whether you want to use ReachHero as a company or an influencer.</p>
    </div>
    <div class="btn-area">
        <a href="#"> i am a company</a>
        <a href="#"> i am an influencer</a>
    </div>
</div>
</section>
<!--/   More Information-->

<!--    Footer Start-->


@endsection
