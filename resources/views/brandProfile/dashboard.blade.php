@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <div class="portlet light">
                <div class="row">
                    <div class="col-md-10 offset-md-1 col-sm-12">
                        <p class="lead">Welcome Bargello parfume!</p>
                        <hr>
                        <p>
                            It's great to have you here. We are happy to see you.<br>
                            For a <b>uncomplicated and correct start with ReachHero</b>, we have prepared a few steps for you, which you should read in advance:
                        </p>
                        <div class="tabbable-custom">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Step 1: <b>How ReachHero works</b></a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Step 2: Examples "<b>Advertising integration</b>".</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Step 3: <b>How To-Video</b></a>
                                    <a class="nav-item nav-link" id="nav-create-campaign-tab" data-toggle="tab" href="#nav-create-campaign" role="tab" aria-controls="nav-create-campaign" aria-selected="false">Step 4: <b>Create campaign for free</b></a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <!-- FIRST TAB -->
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <p></p>
                                    <div class="alert alert-primary" role="alert">
                                        The most important thing at the beginning: <b>the tendering of campaigns on ReachHero is completely free!</b>
                                    </div>
                                    <p></p>
                                    <p>No matter if you advertise a product placement, giveaway or performance campaign on ReachHero: putting a campaign online on ReachHero and getting influencer offers on your campaign <b>is completely free.</b> You have to pay <b>first fees if you want to work with an influencer.</b> And which influencer you want to work with, you decide!</p>
                                    <div style="text-align: center;">
                                        <h2>How ReachHero works</h2>
                                    </div>
                                    <div class="row mt-20">
                                        <div class="offset-sm-2 col-sm-8 col-xs-12">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" frameborder="0" src="//www.youtube.com/embed/nPEzGFEkFag?rel=0&showinfo=0" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- END FIRST TAB -->


                                <!-- SECOND TAB -->
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <p></p>
                                    <div class="alert alert-primary" role="alert">
                                        Here you can find <b>examples of advertising integrations</b> (Product Placements), which were commissioned via the <b>ReachHero Marketplace.</b>
                                    </div>
                                    <p></p>
                                    <h3>YouTube Advertising Integrations</h3>
                                    <div class="row mt-20">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" frameborder="0" src="//www.youtube.com/embed/BK-AmnMvR1Q" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" frameborder="0" src="//www.youtube.com/embed/H_o0wd1drIc" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-20">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" frameborder="0" src="//www.youtube.com/embed/eZLGiFJAAUU" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" frameborder="0" src="//www.youtube.com/embed/2dHTbUNv4VA" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>

                                    <p><br></p>
                                    <h3>Instagram Advertising Integrations</h3>
                                    <p></p>
                                    <p></p>
                                    <div class="row mt-20">
                                        <div class="col-sm-6 col-xs-12">
                                            <iframe class="instagram-media instagram-media-rendered" id="instagram-embed-0" src="https://www.instagram.com/p/Bn5-uNtBc8V/embed/captioned/?cr=1&amp;v=12&amp;rd=https%3A%2F%2Fwww.reachhero.de&amp;rp=%2Fdashboard#%7B%22ci%22%3A0%2C%22os%22%3A5785.5899999999565%2C%22ls%22%3A4081.055000000106%2C%22le%22%3A4118.725000000268%7D" allowtransparency="true" allowfullscreen="true" frameborder="0" height="922" data-instgrm-payload-id="instagram-media-payload-0" scrolling="no" style="background: white; max-width: 540px; width: calc(100% - 2px); border-radius: 3px; border: 1px solid rgb(219, 219, 219); box-shadow: none; display: block; margin: 0px 0px 12px; min-width: 326px; padding: 0px;"></iframe>
                                            <script async="" defer="" src="//www.instagram.com/embed.js"></script>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <iframe class="instagram-media instagram-media-rendered" id="instagram-embed-1" src="https://www.instagram.com/p/BipldPdlH0-/embed/captioned/?cr=1&amp;v=12&amp;rd=https%3A%2F%2Fwww.reachhero.de&amp;rp=%2Fdashboard#%7B%22ci%22%3A1%2C%22os%22%3A5789.0150000002905%2C%22ls%22%3A4081.055000000106%2C%22le%22%3A4118.725000000268%7D" allowtransparency="true" allowfullscreen="true" frameborder="0" height="1336" data-instgrm-payload-id="instagram-media-payload-1" scrolling="no" style="background: white; max-width: 540px; width: calc(100% - 2px); border-radius: 3px; border: 1px solid rgb(219, 219, 219); box-shadow: none; display: block; margin: 0px 0px 12px; min-width: 326px; padding: 0px;"></iframe>
                                            <script async="" defer="" src="//www.instagram.com/embed.js"></script>
                                        </div>
                                    </div>
                                    <div class="row mt-20">
                                        <div class="col-sm-6 col-xs-12">
                                            <iframe class="instagram-media instagram-media-rendered" id="instagram-embed-2" src="https://www.instagram.com/p/Bn0Q3qiAdE4/embed/captioned/?cr=1&amp;v=12&amp;rd=https%3A%2F%2Fwww.reachhero.de&amp;rp=%2Fdashboard#%7B%22ci%22%3A2%2C%22os%22%3A5798.369999999977%2C%22ls%22%3A4081.055000000106%2C%22le%22%3A4118.725000000268%7D" allowtransparency="true" allowfullscreen="true" frameborder="0" height="1120" data-instgrm-payload-id="instagram-media-payload-2" scrolling="no" style="background: white; max-width: 540px; width: calc(100% - 2px); border-radius: 3px; border: 1px solid rgb(219, 219, 219); box-shadow: none; display: block; margin: 0px 0px 12px; min-width: 326px; padding: 0px;"></iframe>
                                            <script async="" defer="" src="//www.instagram.com/embed.js"></script>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <iframe class="instagram-media instagram-media-rendered" id="instagram-embed-3" src="https://www.instagram.com/p/BiACnXUAKZj/embed/captioned/?cr=1&amp;v=12&amp;rd=https%3A%2F%2Fwww.reachhero.de&amp;rp=%2Fdashboard#%7B%22ci%22%3A3%2C%22os%22%3A6773.580000000038%2C%22ls%22%3A4081.055000000106%2C%22le%22%3A4118.725000000268%7D" allowtransparency="true" allowfullscreen="true" frameborder="0" height="1003" data-instgrm-payload-id="instagram-media-payload-3" scrolling="no" style="background: white; max-width: 540px; width: calc(100% - 2px); border-radius: 3px; border: 1px solid rgb(219, 219, 219); box-shadow: none; display: block; margin: 0px 0px 12px; min-width: 326px; padding: 0px;"></iframe>
                                            <script async="" defer="" src="//www.instagram.com/embed.js"></script>
                                        </div>
                                    </div>
                                </div> <!-- END SECOND TAB -->


                                <!-- THIRD TAB -->
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <p></p>
                                    <div class="alert alert-primary" role="alert">
                                        The following video will show you how to create your first product placement campaign. <b>Remember:</b> Advertising campaigns is free on ReachHero <b>free. first</b> if you cooperate with influencers, you will be charged (more on this under <b>step 1: prices)</b>
                                    </div>
                                    <p></p>
                                    <div class="mt-20 mb-25 text-center">
                                        <video width="640" height="360" style="width: 100%; height: 100%; max-width: 800px;" src="/css/video/placement.mp4" type="video/mp4" id="player1" poster="/images/brand/howto/placement.jpg" controls="controls" preload="none" data-vscid="2cdpt6uiv"></video>
                                    </div>
                                </div> <!-- END THIRD TAB -->


                                <!-- FOURTH TAB -->
                                <div class="tab-pane fade" id="nav-create-campaign" role="tabpanel" aria-labelledby="nav-create-campaign-tab">
                                    <p></p>
                                    <div class="alert alert-primary" role="alert">
                                        Start now and create your first product placement campaign. <b>It only takes a few minutes and your campaign on ReachHero is online! <br> Remember:</b> Advertising campaigns is free on ReachHero . <b>First</b> if you cooperate with influencers, you will be charged (more on this under <b>Step 1: Prices</b>)
                                    </div>
                                    <p></p>
                                    <div class="row mb-25">
                                        <div class="col-xs-12 col-sm-4 col-lg-4 channel noselect" style="max-width: 600px; text-align:center;">
                                            <div class="type" data-href="/app/campaign/create?type=placement" style="height: 380px;">
                                                <div class="activeSlope">
                                                    <i class="fa fa-check" style=""></i>
                                                </div>
                                                <div>
                                                    <i class="fa fa-users font-red-sunglo"></i>
                                                </div>
                                                <div class="uppercase headline">
                                                    Product Placement                                    </div>
                                                <div class="description">
                                                    Influencers send you proposals for the creation of the product placement. Whether on YouTube, Facebook, Instagram, Twitter or in the Blog, you decide the next step.                                    </div>
                                            </div>
                                            <div class="info">
                                                <a href="#" class="product-placement-faq">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>More info (FAQ)                                    </a>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-4 col-lg-4 channel noselect" style="max-width: 600px; text-align:center">
                                            <div class="type" data-href="/app/campaign/create?type=performance" style="height: 380px;">
                                                <div style="width: 140px;height: 140px;background: #e5656c;position: absolute;-webkit-transform: rotate(-45deg);transform: rotate(-45deg);-webkit-transform-origin: 45% 0;transform-origin: 45% 0;left: -118px;top: -50px;">
                                                    <span style="bottom: 15px; left:50px; color:white; font-weight: bold; text-transform: uppercase; position: absolute">Beta</span>
                                                </div>
                                                <div class="activeSlope">
                                                    <i class="fa fa-check" style=""></i>
                                                </div>
                                                <div>
                                                    <i class="fa fa-mobile font-red-sunglo"></i>
                                                </div>
                                                <div class="uppercase headline">
                                                    Instagram Shoutouts                                    </div>
                                                <div class="description">
                                                    Influencers publish your given link in their Instagram Story using the Swipe-Up function. The billing is on a "cost per Swipe-Up" basis.                                    </div>
                                            </div>
                                            <div class="info">
                                                <a href="/downloads/Reachhero_Leitfaden_Unternehmen.pdf" target="_blank">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>More info (PDF)                                    </a>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-4 col-lg-4 channel noselect" style="max-width: 600px; text-align:center">
                                            <div class="type" data-href="/app/campaign/create?type=giveaway" style="height: 380px;">
                                                <div class="activeSlope">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                </div>
                                                <div>
                                                    <i class="fa fa-gift font-red-sunglo" aria-hidden="true"></i>
                                                </div>
                                                <div class="uppercase headline">
                                                    GiveAway
                                                </div>
                                                <div class="description">
                                                    You will get requests from influencers, who want to promote your product. Once you select an influencer, you have to send your product to him.                                    </div>
                                            </div>
                                            <div class="info">
                                                <a href="#" class="giveaway-campaigns-faq">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>More info (FAQ)
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- END FOURTH TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
