<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Brand</title>

    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" />

    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/adminlte/OverlayScrollbars.min.css') }}" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/brandMenu.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/influencer.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/contact.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/individualProposal.css') }}" rel="stylesheet" />
</head>
<body>
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="page-content" style="min-height: 391px; margin-left: 0px;">
                <h3>
                    We would love to show you, how our marketplace works
                </h3>

                <p>
                    Did you already read our <b>ReachHero Handout for brands</b>? It explains how ReachHero works step by step.
                </p>

                <p class="text-center">
                    <a href="/downloads/Reachhero_Leitfaden_Unternehmen.pdf" target="_blank" class="btn btn-danger">Download handout now (PDF)</a>
                </p>

    <br>
    <h3>
        Need a personal demo?
    </h3>
    <p>
        If you want to get a personal demo with one of our customer success manager, don't hesitate and click on the following button to request a demo:
    </p>
    <p class="text-center">
        <a href="#" target="_blank" class="btn btn-danger ladda-button" data-style="zoom-in" id="personalDemo">
            <span class="ladda-label">Request demo now</span>
            <span class="ladda-spinner"></span>
        </a>
    </p>

    <br><br>

    <script>
        $(document).ready(function () {
            Ladda.bind('.ladda-button');

            $(document).on("click", "a#personalDemo", function(e) {
                e.preventDefault();

                $.ajax({
                    url: "/brand/requestDemoProcess",
                    data: {},
                    type: "POST",
                    dataType: 'json',
                    success: function (json) {
                        if (json.result == 1) {
                            parent.individualProposalSentSuccessfully();
                            parent.$('.modal').modal('hide');
                        } else {
                            notification.error('An error occurred. Please log out and in again or contact kontakt@reachhero.de');
                        }

                        Ladda.stopAll();
                    },
                    error: function (xhr, status, error) {
                        Ladda.stopAll();

                        notification.error('An error occurred. Please log out and in again or contact kontakt@reachhero.de');
                    },
                    async: true
                });
            });
        });
    </script>            		</div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("div.page-content").css("margin-left", 0);
        })

    </script>
</body>

</html>
