@extends('layouts.brand')

@section('styles')
    <link href="{{asset('css/brandcampaign.css')}}" rel="stylesheet">    
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />   
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" rel="stylesheet">    
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    {{-- DATE RANGE PICKER--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    {{-- SLIDER --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    
@endsection

@section('content')   
<section >
<div class="container">
    <h3>CAMPAIGNS:</h3>
    <form method="post" action="{{url('/brand/campaigns/store')}}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">PRODUCT PHOTOS</h3>
                            <div class="card-tools">
                            </div>
                        </div>
                        <div class="card-body">

                            <label for="exampleInputFile">File Input</label>
                            <div class="product-photos">
                                <div class="single-photo">
                                    <input type="file" accept="image/*" onchange="preview_image1(event)" id="image-feild1" style="display:none;" name="image1">
                                    <div class="preview-container" onclick="ItIsClick1()">
                                        <img id="output_image1" src="{{asset('/images/general/Preview-icon.png')}}"  />
                                    </div>
                                </div>
                                <div class="single-photo">
                                    <input type="file" accept="image/*" onchange="preview_image2(event)" id="image-feild2" style="display:none;" name="image2">
                                    <div class="preview-container" onclick="ItIsClick2()">
                                        <img id="output_image2" src="{{asset('/images/general/Preview-icon.png')}}"  />
                                    </div>
                                </div>
                                <div class="single-photo">
                                    <input type="file" accept="image/*" onchange="preview_image3(event)" id="image-feild3" style="display:none;" name="image3">
                                    <div class="preview-container" onclick="ItIsClick3()">
                                        <img id="output_image3" src="{{asset('/images/general/Preview-icon.png')}}"  />
                                    </div>
                                </div>
                                @if($errors->has('image1'))
                                    <p class="red">{{$errors->get('image1')[0]}}</p>
                                @endif
                                @if($errors->has('image2'))
                                    <p class="red">{{$errors->get('image2')[0]}}</p>
                                @endif
                                @if($errors->has('image3'))
                                    <p class="red">{{$errors->get('image3')[0]}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">CAMPAIGN DETAILS</h3>
                            <div class="card-tools">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label >Title</label>
                                <input name="title" class="form-control" id="campaign_title" placeholder="Enter Title" value="{{old('title')}}" onkeydown="previewTitle()">
                                @if($errors->has('title'))
                                    <p class="red">{{$errors->get('title')[0]}}</p>
                                @endif
                            </div>
                            <label>Description</label>
                            <textarea id="summernote" name="description" value="{{old('description')}}" onkeydown='previewDescription()'></textarea>
                            @if($errors->has('description'))
                                <p class="red">{{$errors->get('description')[0]}}</p>
                            @endif
                            <select name="socials[]" style="display:none;" multiple>  
                                @foreach($socials as $social)
                                            <option value="{{$social->id}}" selected>{{$social->name}}</option>
                                @endforeach
                            <select>
                            <label>Social Media</label>
                            <div class="form-group" data-select2-id="45">
                                <select class="js-example-responsive" multiple="multiple" style="width: 75%" disabled>
                                    @foreach($socials as $social)
                                            <option value="{{$social->name}}" selected>{{$social->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('socials'))
                                    <p class="red">{{$errors->get('socials')[0]}}</p>
                                @endif
                            </div>
                            <label>Submission deadline:</label>
                            <div class="form-group">
                                <input type="text" name="daterange" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">BUDGET RANGE</h3>
                            <div class="card-tools">
                            </div>   
                        </div>
                        <div class="card-body">
                            <div>
                                <input type="text" class="js-range-slider" name="cost_range" value="" />
                                @if($errors->has('cost_range'))
                                    <p class="red">{{$errors->get('cost_range')[0]}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            @include('brandProfile.campaigns.preview')
        </div>
    </div>
        <button class="btn btn-primary mt-2 mb-3" >Submit</button>
    </form>
</div>
</section>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>
    {{-- SLIDER --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
    
    <script>
        
            $('#summernote').summernote({
                placeholder: 'Description',
                disableDragAndDrop: true,
                tabsize: 3,
                height: 250,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear','fontname']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });

            // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.js-example-responsive').select2({
                placeholder: 'Select an option',

            });

            $(".js-range-slider").ionRangeSlider({
                skin: "round",
                type: "double",
                grid: true,
                min: 100,
                max: 20000,
                from: 4000,
                to: 12000,
                step:50,
                prefix: " ",
                postfix: ` <i class="fas fa-euro-sign"></i>`,
                onFinish: function (data) {
                    setCost(data);
                },
            });
            
        });   
    </script>
    {{-- DATE RANGE PICKER --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    
    <script>
        $('input[name="daterange"]').daterangepicker(

        );
    </script>
    <script src="{{asset('js/brandCampaign.js')}}"></script>
    
@endsection














