@extends('layouts.brand')

@section('styles')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.css" rel="stylesheet">    
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

    <style>
        .cost-range{
            display:flex;

        }
        .red{
            color:red;
        }
        .preview-container{
            width:200px;
            height:auto;
            border:1px solid gray;
            opacity:7;
        }
        .preview-container:hover{
            opacity:1;
            cursor:pointer;
        }
        .preview-container img{
            width:100%;
            height:100%;
        }
        li{
            color:black!important;
        }
    </style>
@endsection

@section('content')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{url('/brand/campaigns/index')}}">Campaigns</a></li>
        <li class="breadcrumb-item active"><a href="#">Edit</a></li>
    </ol>
    <div class="container">
        <h3>CAMPAIGNS:</h3>
        <form method="post" action="{{url('/brand/campaigns/store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label >Title</label>
                <input name="title" class="form-control" id="exampleInputEmail1" placeholder="Enter Title" value="{{$campaign->title}}">
                @if($errors->has('title'))
                    <p class="red">{{$errors->get('title')[0]}}</p>
                @endif
            </div>

            <label>Description</label>
            <textarea id="summernote" name="description">{{$campaign->description}}</textarea>
            @if($errors->has('description'))
                <p class="red">{{$errors->get('description')[0]}}</p>
            @endif
            <div class="form-group">
                <label for="exampleInputFile">File Input</label>
                <div class="input-group">
                    <input type="file" accept="image/*" onchange="preview_image(event)" id="image-feild" style="display:none;" name="image">
                    <div class="preview-container" onclick="ItIsClick()">
                        <img id="output_image" src="{{$campaign->image}}"  />
                    </div>
                </div>
                @if($errors->has('image'))
                    <p class="red">{{$errors->get('image')[0]}}</p>
                @endif
            </div>
            <label>Social Media</label>
            <div class="form-group" data-select2-id="45">
                  <select name="socials[]" class="js-example-responsive" multiple="multiple" style="width: 75%">
                  @foreach($campaign->social_categories as $selected)
                    @foreach($socials as $social)
                        <option value="{{$social->name}}" {{$selected->name==$social->name?'selected':''}}>{{$social->name}}</option>
                    @endforeach
                @endforeach
                  </select>
                  @if($errors->has('socials'))
                    <p class="red">{{$errors->get('socials')[0]}}</p>
                @endif
            </div>
            <label>Budget Range</label>
            <div class="cost-range">
                <div>
                    <input type="number" class="form-control mp-3 mr-3" id="exampleInputEmail1" placeholder="lower range" name="lower_cost" value="{{$campaign->lower_cost}}">
                    @if($errors->has('lower_cost'))
                        <p class="red">{{$errors->get('lower_cost')[0]}}></p>
                    @endif
                </div>
                <div class="ml-3">
                    <input type="number" class="form-control mp-3" id="exampleInputEmail1" placeholder="upper range" name="upper_cost" value="{{$campaign->upper_cost}}">
                    @if($errors->has('upper_cost'))
                        <p  class="red">{{$errors->get('upper_cost')[0]}}</p>
                    @endif
                </div>
            </div>
            <button class="btn btn-primary mt-3" >Submit</button>
        </form>
    </div>


    

@endsection


@section('scripts')
    <script>
        function ItIsClick(){
                $("#image-feild").click();
            }
        function preview_image(event){
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output_image');
                output.src = reader.result;
            }
            reader.readAsDataURL(event.target.files[0]);
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote-bs4.min.js"></script>

    <script>
            $('#summernote').summernote({
                placeholder: 'Description',
                tabsize: 3,
                height: 250,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear','fontname']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
            // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.js-example-responsive').select2({
                placeholder: 'Select an option'
            });
        });   
    </script>
    
@endsection
