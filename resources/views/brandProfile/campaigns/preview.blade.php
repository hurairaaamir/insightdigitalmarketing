<div class="card card-size anim" >
    <div class="card-image">

        <div class="img-container">
            <div class="card-price cost-badge">
                <span id="lower-cost">4000</span>-<span id="upper-cost">1100</span> <i class="fas fa-euro-sign"></i>
            </div>
            <img src="/images/general/Preview-icon.png" id="preview-image">
        </div>
        
    </div>
    
    <div class="card-body m-1">
        <h2 id="preview-title">Title</h2>
        <div class="social-container m-1">
            @foreach($socials as $social)
                <div class="social-image">
                    {!!$social->image!!}
                </div>
            @endforeach
        </div>
        
        <p class="mt-3" id="preview-description"> 
            Your Description will go here.....
        </p>
    </div>
</div>
    