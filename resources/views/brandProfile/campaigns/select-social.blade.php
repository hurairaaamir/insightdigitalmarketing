@extends('layouts.brand')

@section('styles')
    <link href="{{asset('css/brandcampaign.css')}}" rel="stylesheet">    
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />   
    
@endsection


@section('content')   
<div class="socials-circle">
    <div>
        <div class="lg round youtube" onclick="active_social('youtube')" id="youtube">
            <i class="fab fa-youtube"></i>
        </div>
        <span class="ft">youtube</span>
    </div>
    <div>
        <div class="sm round instagram" onclick="active_social('instagram')" id="instagram">
            <i class="fab fa-instagram"></i>
        </div>
        <span class="ft">instagram</span>
    </div>
    <div>
        <div class="md round facebook" onclick="active_social('facebook')" id="facebook">
            <i class="fab fa-facebook-f"></i>
        </div>
        <span class="ft">facebook</span>
    </div>
    <div>
        <div class="sm round twitter" onclick="active_social('twitter')" id="twitter">
            <i class="fab fa-twitter"></i>
        </div>
        <span class="ft">twitter</span>
    </div>
    <div>
        <div class="sm round blog" onclick="active_social('blog')" id='blog'>
            <i class="fas fa-rss"></i>
        </div>
        <span class="ft">Blog</span>
    </div>
</div>
<form action="{{url('/brand/campaigns/select-social')}}" method="POST" >
    @csrf
    @if($errors->has('socials'))
        <p class="red">{{$errors->get('socials')[0]}}</p>
    @endif
    <input type="hidden" value="" name='socials' id="socials-feild">
    <button type="submit" style="display:none;" id="social-button">submit</button>
</form>
<div class="social-form">
    <p id="social-error" class="red"></p>
    <button class="btn-social" onclick="submitSocial()">continue</button>
</div>

{{-- <div>
    <div class="youtube">
        <i class="fab fa-youtube"></i>
    </div>
    <div class="instagram">
        <i class="fab fa-instagram"></i>
    </div>
    <div class="facebook">
        <i class="fab fa-facebook-f"></i>
    </div>
    <div class="twitter">
        <i class="fab fa-twitter"></i>
    </div>
    <div class="blog">
        <i class="fas fa-rss"></i>
    </div>
</div> --}}
@endsection

@section('scripts')
    
    <script src="{{asset('js/brandCampaign.js')}}"></script>
@endsection














