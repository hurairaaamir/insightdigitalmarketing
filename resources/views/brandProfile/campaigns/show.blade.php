@extends('layouts.brand')

@section('styles')
    <link href="{{asset('css/campaign.css')}}" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
    <link href="{{asset('css/offer.css')}}" rel="stylesheet" />
@endsection

@section('content')

<div class="container" id='page-wrap'>
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{url('/brand/campaigns/index')}}">Campaigns</a></li>
                <li class="breadcrumb-item active"><a href="#">Details</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 card">
            <div class="row">
                <div class="col-md-5 p-3">
                    <div class="card child1 p-2 m-2">
                        {{-- <div class="card-header"> --}}
                            <h2>{{$campaign->title}}</h2>
                        {{-- </div> --}}
                        <div class="cost-badge mb-3">
                            <i class="far fa-money-bill-alt"></i> Budget {{$campaign->lower_cost}}-{{$campaign->upper_cost}} <i class="fas fa-euro-sign"></i>
                        </div>
                        <div class="card-image">
                            <div class="owl-carousel owl-theme" id="show-owl-carousel">
                                <div class="full-container">    
                                    <img src="{{$campaign->image}}">
                                </div>
                                @foreach($campaign->otherImg as $image)
                                    <div class="full-container">    
                                        <img src="{{$image->image}}" >
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="social-container m-1">
                        @foreach($campaign->social_categories as $social)
                            <div class="social-image">
                                {!!$social->image!!}
                            </div>
                        @endforeach
                    </div>
                    @php
                        $date=$campaign->created_at;
                        $year=substr($date,0,4);
                        $month=substr($date,5,2);
                        $day=substr($date,8,2);
                    @endphp
                    <div class="mt-3 badge-date">
                        <h4><span class="badge badge-secondary">Publish Date : {{$day.'/'.$month.'/'.$year}}</span></h4>
                        <h4><span class="badge badge-secondary">Deadline Date : {{$campaign->daterange}}</span></h4>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class=" child2">
                        <div class="card-header">
                            <h2>Description</h2>
                        </div>
                        <div class="card-body">
                            <p class="mt-3"> 
                                {!!$campaign->description!!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="campaign-buttons">
            <a class="btn btn-primary mr-2" href="{{url('/brand/campaigns/edit/'.$campaign->id)}}">Edit</a>
            <a class="btn btn-danger " href="{{url('/brand/campaigns/destroy/'.$campaign->id)}}">Delete</a>
    </div>     --}}
    <div class="row">
        <div class="col-md-12">
            
            <div class="card">
                <div class="container mt-3 table-style">
                    
                    @php
                        $counter=0;
                    @endphp
                    <table class="table table-bordered table-striped ">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Influencer</th>
                                <th scope="col">budget</th>
                                <th >details</th>
                                <th scope="col">status</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($campaign->offers)!=0)        
                            @foreach($campaign->offers as $offer)
                                @php
                                    $details=substr($offer->details,0,60).'...';
                                    // $selected_offer=$offer;
                                @endphp
                                <tr>
                                    <th style="min-width:50px;" scope="row">{{$counter}}</th>
                                
                                    <td ><img style="width:40px;height:40px;border-radius:100%;" src="{{$offer->user->profile->image}}"> <span class="mt-2">{{$offer->user->name}}</span></td>
                                    <td><span class="badge badge-secondary">{{$offer->budget}} <i class="fas fa-euro-sign"></i></span></td>
                                    <td style="min-width:300px;">{{$details}}</td>
                                    <td><span id='status_{{$offer->id}}' class="{{$offer->status=='In Process' ? 'badge badge-secondary':''}} {{$offer->status=='Accepted' ? ' badge badge-primary':''}} {{$offer->status=='completed' ? ' badge badge-success':''}} {{$offer->status=='Rejected' ? 'badge badge-danger':''}}">{{$offer->status}}</span></td>
                                    <td>
                                        {{-- <button class="btn btn-xs btn-danger" onclick="OfferUpdate('{{$offer->id}}','Reject')">Reject</button> 
                                        <button class="btn btn-xs btn-info" onclick="OfferUpdate('{{$offer->id}}','Accept')">Accept</button> --}}
                                        <button class="btn btn-xs btn-info" onclick="showModel('{{$offer->id}}')">See Details</button>
                                    </td>
                                </tr>
                                @php
                                    $counter++;
                                @endphp
                            @endforeach
                        @else
                        <tr>
                            <th colspan="5">
                            <p style="padding:10px;color:gray;display:flex;justify-content:center">No offers are made yet.</p>
                        <th>
                        <tr>
                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
        
<div class="model-outer">
    <div class="com-model">
        <div class="model-body">
            <div class="model-cross1" onclick="showModel()"><i class="far fa-times-circle"></i></div>
            <div class="card profile-card">
                {{-- @include('cardData.completeProfile') --}}
            </div>
        </div>
    </div>
</div>
        
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="{{asset('js/ajax/offers.js')}}"></script>

@endsection
