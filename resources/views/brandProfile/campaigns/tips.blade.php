@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->

    <div class="page-content-wrapper">
        <h1 class="page-title">Tips</h1>
        <div class="page-content" style="min-height: 889px;">
            <div class="portlet light">
                <div class="portlet-body">
                    <div>
                        <center>

                            <div class="mt-40">
                                <div style="font-size: 1.6em;">Video-Tutorial: Create product placement campaign</div>
                                <div style="font-size: 1.2em;">So you will get your first proposals from influencers, to promote your brand</div>
                            </div>

                            <div class="mt-20">
                                <video width="640" height="360" style="width: 100%; height: 100%; max-width: 800px;" src="/css/video/placement.mp4" type="video/mp4" id="player1" poster="/images/brand/howto/placement.jpg" controls="controls" preload="none" data-vscid="2cdpt6uiv"></video>
                            </div>
                        </center>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-6">
                            <h4>5 Tips for a Successful Campaign</h4>
                            <p>Whatever your target audience, there’s a suitable influencer out there who perfectly fits you and your product! In fact, every social network has its groups and niches with loyal fans who trust the recommendations of their influencers. With ReachHero you can find them! Here are some tips to help bring you to their attention:</p>
                            <ol>
                                <li>
                                    <h4>Tip – Campaign Title</h4>
                                    The title should be short and catchy, and contain your name.<br><br>Example: <br>Save money on your online shopping with Quidco<br>New Year’s resolution campaign with FitTea<br>                    </li>
                                    <li>
                                        <h4>Tip – Campaign Description</h4>
                                        Above all it’s important that the YouTuber gets a good impression of you and your idea. Briefly introduce yourself (2-3 sentences), then describe what you’re looking for. Which target audience should the influencer have, how old should they be, and what kind of content do you envision? Should it be a YouTube video, an Instagram post or Snapchat video, or all of the above? Should the product be tested, or ordered in your shop? <br> Keep it brief, then...                    </li>
                                <li>
                                    <h4>Tip - A picture is worth a thousand words</h4>
                                    Show your product, your shop, or whatever you want to advertise. Not only does this give the influencer a first impression of your product, it also makes your campaign more appealing.                    </li>
                                <li>
                                    <h4>Tip - Giveaways</h4>
                                    At the very least, the influencer should receive a product from you which they can then present. If your budget allows it, it’s great if you can also provide a prize that your influencer can give away to a lucky fan. A discount code for your online store is also a good way to increase the conversion rate.                    </li>
                                <li>
                                    <h4>Tip - Trust your Instincts</h4>
                                    If you’ve followed the first four tips you should quickly receive several applications for your campaign. As you have to decide on only one or a handful of applicants, now it’s just about choosing one. For this, you should go with your gut instinct. As well as the statistics about the influencer that we provide you, you should also have a look at a few of their videos, their Facebook account or their Instagram account. After that you should have a good idea of which influencer is most suitable for your target audience and your campaign. If you’re having any difficulty choosing an influencer, the ReachHero team is of course happy to help you further!                    </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
