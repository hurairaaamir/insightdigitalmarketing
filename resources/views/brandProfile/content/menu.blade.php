<div class="row width-100-percent light-border-bottom ml-0">

    <ul class="nav nav-tabs margin-bottom-0 border-bottom-none float-left margin-top-5">
        <li class="tab-resize {{ $type=='youtube' ?'active' : ''}}">
            <a href="{{url('/brand/content/index/youtube')}}">
                <div class="show-large">
                    <img class="height-15 shift-up-2 margin-right-5" src="/images/svgs/YouTube_icon_light.svg">
                    <!---->YouTube
                </div>
                <div class="show-small">
                    <img class="height-20 shift-up-2" src="/images/svgs/YouTube_icon_light.svg">
                </div>
            </a>
        </li>
        <li class="tab-resize {{ $type=='facebook' ?'active' : ''}}">
            <a href="{{url('/brand/content/index/facebook')}}">
                <div class="show-large">
                    <i class="fab fa-facebook margin-right-5"></i>
                    Facebook
                </div>
                <div class="show-small">
                    <i class="fab fa-facebook font-xl"></i>
                </div>
            </a>
        </li>
        <li class="tab-resize {{ $type=='instagram' ?'active' : ''}}">
            <a href="{{url('/brand/content/index/instagram')}}">
                <div class="show-large">
                    <i class="fab fa-instagram margin-right-5"></i>
                    Instagram Posts
                </div>
                <div class="show-small">
                    <i class="fab fa-instagram font-xl"></i>
                    <span class="shift-up-3">
                            Post                    </span>
                </div>
            </a>
        </li>
        <li class="tab-resize ">
            <a href="/content/placement/instagram-stories">
                <div class="show-large">
                    <i class="fab fa-instagram margin-right-5"></i>
                    Instagram Stories                </div>
                <div class="show-small">
                    <i class="fab fa-instagram font-xl"></i>
                    <span class="shift-up-3">
                        Story                    </span>
                </div>
            </a>
        </li>
        <li class="tab-resize ">
            <a href="/content/placement/instagram-videos">
                <div class="show-large">
                    <i class="fab fa-instagram margin-right-5"></i>
                    Instagram TV Videos                </div>
                <div class="show-small">
                    <i class="fab fa-instagram font-xl"></i>
                    <span class="shift-up-3">TV</span>
                </div>
            </a>
        </li>
        
        {{-- <li class="tab-resize ">
            <a href="/content/placement/blog">
                <div class="show-large">
                    <i class="fa fa-rss margin-right-5"></i>
                    Blog
                </div>
                <div class="show-small">
                    <i class="fa fa-rss font-xl"></i>
                </div>
            </a>
        </li> --}}
    </ul>
    <div class="float-right text-right ">
        <select id="campaignsListing" class="form-control inline-block" onchange="goToByCampaignListing()" style="max-width: 300px;">
            <option value="">-- Select campaign --</option>
                @foreach($campaigns as $campaign)
                    @if(isset($selected_campaign))
                        <option value="{{$campaign->id}}" {!!$selected_campaign->id==$campaign->id ? 'selected' : ''!!}>{{$campaign->title}}</option>
                    @else
                        <option value="{{$campaign->id}}">{{$campaign->title}}</option>
                    @endif
                @endforeach
        </select>
    </div>
</div>



{{-- @foreach($offer->facebook_content as $content)
<div class="container">
    <div class="center_out" >
        <div class="fb-post" 
            data-href="{{$content->permalink_url}}"
            data-width="500">
        </div>
    </div>
</div>
@endforeach    --}}


{{-- @foreach($campaign->facebookContents as $content)
    <div class="center_out" >
        <div class="fb-post" 
            data-href="{{$content->permalink_url}}"
            data-width="500">
        </div>
    </div>
    <br><br>
    @php
        $con=false;
    @endphp
@endforeach --}}