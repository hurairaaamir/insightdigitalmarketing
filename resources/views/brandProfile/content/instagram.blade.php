@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/content.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/brandSettingsMenu.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/emailNotifications.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('/css/influencer/content.css')}}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
    <style>
        html, body, .wrapper {
            min-height: auto;
            overflow-x: inherit;
        }
    </style>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Content</h1>
            <div class="tabbable-line">

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="/content/placement"><i class="fa fa-users font-sunglo"></i> Placement</a>
                    </li>
                    <li>
                        <a href="/content/giveaway"><i class="fa fa-gift font-sunglo"></i> Giveaway</a>
                    </li>
                </ul>    
                <div class="tab-content">
                    <div class="row">
                        <div class="col-md-12 mb-30">
                            @include('brandProfile.content.menu')
                        </div>
                        @php
                            $con=true;
                        @endphp
                        <div class="col-md-12 center_out">
                            <div class="row">
                                <div id="endorsements" style="position: relative; height: 0px;">
                                    <div class="loading-message" style="width: 100%; margin: 0px auto;">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                        <div class="col-md-12">
                                            <div class="portlet light text-center ">
                                                <div class="row ">
                                                    <div class="container ">
                                                        @if(!$selected_campaign)
                                                            @foreach($campaigns as $campaign)
                                                                <div class="center_out insta_center">
                                                                    @foreach($campaign->instagramContents as $content)
                                                                            <div class="center_out">
                                                                                <blockquote class="instagram-media media_outer" data-instgrm-version="2" >
                                                                                    <p> 
                                                                                        <a href="{{$content->content_url}}" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; text-decoration:none;" target="_top"> View on Instagram</a>
                                                                                    </p>
                                                                                </blockquote>
                                                                            </div>
                                                                            <script async defer type="text/javascript" src="//platform.instagram.com/en_US/embeds.js"></script>
                                                                        <br><br>
                                                                        @php
                                                                            $con=false;
                                                                        @endphp
                                                                        
                                                                    @endforeach
                                                                </div>
                                                            @endforeach
                                                            @if($con)
                                                                <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3">
                                                                    <h2>No YouTube Videos</h2>
                                                                    <p>You have not yet received any completed YouTube videos from Influencers. You will receive these when you agree to offers from Influencers for your campaigns.</p>
                                                                    <div class="margin-top-30 margin-bottom-30">
                                                                        <a href="/campaign/create" class="btn btn-danger"><i class="fa fa-paper-plane" aria-hidden="true"></i> Create Campaign                        </a>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @else
                                                            @foreach($selected_campaign->offers as $offer)
                                                                <div class="tab-div">
                                                                    <div class="tab1">
                                                                        <a href="{{url('/influencer/offers')}}"><img src="{{$offer->user->profile->image}}"></a>
                                                                    <span class="badge badge-secondary" >{{$offer->budget}} <i class="fas fa-euro-sign"></i></span>
                                                                    </div>
                                                                    <div class="tab2">
                                                                    <p>{!!substr($offer->details,0,250).'...'!!}</p>
                                                                    <div class="social">
                                                                        @foreach($offer->offer_channels as $social)
                                                                        @if($social->channel=='instagram')
                                                                            <span><div class="instagram"><i class="fab fa-instagram"></i></div></span>
                                                                        @endif
                                                                        @if($social->channel=='facebook')
                                                                            <span><div class="facebook"><i class="fab fa-facebook-f"></i></div></span>
                                                                        @endif
                                                                        @if($social->channel=='youtube')
                                                                            <span><div class="youtube"><i class="fab fa-youtube"></i></div></span>
                                                                        @endif
                                                                        @endforeach
                                                                    </div>
                                                                    <span style="color:rgb(63, 63, 252);float:right;margin:10px 0px;"><i class="fas fa-check"></i> This Campaign Content has been submited , you can review the content and complete offer</span>
                                                                    {{-- <a href="{{url('/influencer/offers')}}" class="btn btn-primary btn-sm mt-4 "></a> --}}
                                                                    </div>
                                                                </div>
                                                                <div class="container">
                                                                    <div class="center_out">
                                                                        @foreach($offer->instagram_content as $content)
                                                                            @php
                                                                                $con=false;
                                                                            @endphp
                                                                            <div class="container">
                                                                                <div class="center_out">
                                                                                    <blockquote class="instagram-media media_outer" data-instgrm-version="2" >
                                                                                        <p> 
                                                                                            <a href="{{$content->content_url}}" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; text-decoration:none;" target="_top"> View on Instagram</a>
                                                                                        </p>
                                                                                    </blockquote>
                                                                                </div>
                                                                            </div>
                                                                            <br><br>
                                                                        @endforeach   
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            @if($con)
                                                                <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3">
                                                                    <h2>Campaign {{$selected_campaign->title}}</h2>
                                                                    <p>You have not yet received any completed Instagram Content from Influencers. You will receive these when you agree to offers from Influencers and the influencer submit there content to you.</p>
                                                                    <div class="margin-top-30 margin-bottom-30">
                                                                        <a href="/campaign/create" class="btn btn-danger"><i class="fa fa-paper-plane" aria-hidden="true"></i> Create Campaign                        </a>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script type="text/javascript">
        function goToByCampaignListing() {
            var campaignsSelect = document.getElementById("campaignsListing");
            var selectedOption = campaignsSelect.options[campaignsSelect.selectedIndex].value;
            console.log(selectedOption);
            if(selectedOption){
                window.location.replace('/brand/content/index/instagram/'+selectedOption);
            }
        }
    </script>
@endsection
