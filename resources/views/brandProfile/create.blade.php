@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/brandSettingsMenu.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/create.css') }}" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Create Brand</h1>
            <div class="portlet light">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="note note-success">Create a new company that you would like to use with your account.</div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <form action="/brand/create" method="POST" autocomplete="off">
                                        @csrf
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>Company name*</td>
                                                <td>
                                                    <input type="text" id="brcompanyname" name="company_name" class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>First name</td>
                                                <td>
                                                    <input type="text" id="brfirstname" name="first_name" class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Last name</td>
                                                <td>
                                                    <input type="text" id="brlastname" name="last_name" class="form-control">
                                                </td>
                                            </tr>
                                            <tr>

                                                <td>Email Address
                                                    <span class="tooltips" title="We do not send e-mails to the e-mail address entered here.">
                                                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                                                    </span>*
                                                </td>
                                                <td>
                                                    <input type="text" id="bremail" name="email" class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Telephone</td>
                                                <td>
                                                    <input type="text" id="brphone" name="telephone" class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Website</td>
                                                <td>
                                                    <input type="text" id="brwebsite" name="website" class="form-control">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p class="font-sm">* required</p>

                                        <input type="submit" value="Save" class="btn btn-success">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
