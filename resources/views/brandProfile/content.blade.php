@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/content.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/brandSettingsMenu.css') }}" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Content</h1>
            <div class="tabbable-line">

                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="/content/placement"><i class="fa fa-users font-sunglo"></i> Placement</a>
                    </li>
                    <li>
                        <a href="/content/giveaway"><i class="fa fa-gift font-sunglo"></i> Giveaway</a>
                    </li>
                </ul>
        <div class="tab-content">
            <div class="row">
                <div class="col-md-12 mb-30">

                    <div class="row width-100-percent light-border-bottom ml-0">

                        <ul class="nav nav-content nav-tabs margin-bottom-0 border-bottom-none float-left margin-top-5">
                            <li class="tab-resize active">
                                <a href="/content/placement">
                                    <div class="show-large">
                                        <img class="height-15 shift-up-2 margin-right-5" src="/images/svgs/YouTube_icon_light.svg">
                                        <!---->YouTube
                                    </div>
                                    <div class="show-small">
                                        <img class="height-20 shift-up-2" src="/images/svgs/YouTube_icon_light.svg">
                                    </div>
                                </a>
                            </li>
                            <li class="tab-resize ">
                                <a href="/content/placement/instagram">
                                    <div class="show-large">
                                        <i class="fab fa-instagram margin-right-5"></i>
                                        Instagram Posts
                                    </div>
                                    <div class="show-small">
                                        <i class="fab fa-instagram font-xl"></i>
                                        <span class="shift-up-3">
                                                Post                    </span>
                                    </div>
                                </a>
                            </li>
                            <li class="tab-resize ">
                                <a href="/content/placement/instagram-stories">
                                    <div class="show-large">
                                        <i class="fab fa-instagram margin-right-5"></i>
                                        Instagram Stories                </div>
                                    <div class="show-small">
                                        <i class="fab fa-instagram font-xl"></i>
                                        <span class="shift-up-3">
                                            Story                    </span>
                                    </div>
                                </a>
                            </li>
                            <li class="tab-resize ">
                                <a href="/content/placement/instagram-videos">
                                    <div class="show-large">
                                        <i class="fab fa-instagram margin-right-5"></i>
                                        Instagram TV Videos                </div>
                                    <div class="show-small">
                                        <i class="fab fa-instagram font-xl"></i>
                                        <span class="shift-up-3">TV</span>
                                    </div>
                                </a>
                            </li>
                            <li class="tab-resize ">
                                <a href="/content/placement/facebook">
                                    <div class="show-large">
                                        <i class="fab fa-facebook margin-right-5"></i>
                                        Facebook
                                    </div>
                                    <div class="show-small">
                                        <i class="fab fa-facebook font-xl"></i>
                                    </div>
                                </a>
                            </li>
                            {{-- <li class="tab-resize ">
                                <a href="/content/placement/blog">
                                    <div class="show-large">
                                        <i class="fa fa-rss margin-right-5"></i>
                                        Blog
                                    </div>
                                    <div class="show-small">
                                        <i class="fa fa-rss font-xl"></i>
                                    </div>
                                </a>
                            </li> --}}
                        </ul>
                        <div class="float-right text-right ">
                            <select id="campaignsListing" class="form-control inline-block" onchange="goToByCampaignListing()" style="max-width: 300px;">
                                <option disabled="disabled" selected="">-- Select campaign --</option>
                                    @foreach($campaigns as $campaign)
                                        <option value="{{$campaign}}">{{$campaign->title}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div id="endorsements" style="position: relative; height: 0px;">
                            <div class="loading-message" style="width: 100%; margin: 0px auto; display: none;">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                                <div class="col-md-12">
                                    <div class="portlet light text-center">
                                        <div class="row">
                                            <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3">
                                                <h2>No YouTube Videos</h2>
                                                <p>You have not yet received any completed YouTube videos from Influencers. You will receive these when you agree to offers from Influencers for your campaigns.</p>
                                                <div class="margin-top-30 margin-bottom-30">
                                                    <a href="/campaign/create" class="btn btn-danger"><i class="fa fa-paper-plane" aria-hidden="true"></i> Create Campaign                        </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <script type="text/javascript">
        function goToByCampaignListing() {
            var campaignsSelect = document.getElementById("campaignsListing");
            var selectedOption = campaignsSelect.options[campaignsSelect.selectedIndex].value;

            alert()
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/influencer/channel/youtube/login',
                type:'POST',
                data:{title,youtube_channel_id,subscribers,views,videos,comments,channel_url,image_url},
            })
            .done(function(response)
            {
                let data=response.data;
                let element=document.getElementById('youtube-head');
                if(element){
                    element.innerHTML=`
                    <a href="${data.channel_url}" target="_blank">
                        <div class="channel-div ml-2">
                            <span>
                                <img src="${data.image_url}" class="ml-1 img-circle">
                            </span>
                            <span>${data.title}</span>
                        </div>
                    </a>`
                }
                console.log(data);
            });


        }
    </script>
@endsection
