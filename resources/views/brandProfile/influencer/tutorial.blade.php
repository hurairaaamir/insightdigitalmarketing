@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Tutorials</h1>
            <div class="portlet light">
                <div class="portlet-body">
                    <div>
                        <center>

                            <div class="mt-40">
                                <h2>Influencer Research</h2>
                                <h3>Use our Influencer Research to quickly and easily find suitable influencers:</h3>
                            </div>

                            <div class="mt-20">
                                <video width="640" height="360" style="width: 100%; height: 100%; max-width: 800px;" src="/video/tutorial/influencer_search_basic.mp4" type="video/mp4" id="player1" poster="/images/tutorial/influencer_search_basic.png" controls="controls" preload="none" data-vscid="hkp2xdoz9"></video>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
