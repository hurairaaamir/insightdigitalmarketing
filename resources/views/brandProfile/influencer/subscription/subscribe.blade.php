@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Buy Lite package</h1>
            <div class="row">
                {{-- <div class="col-xs-12 col-sm-7">
                        </div> --}}

                <div class="col-xs-12 col-sm-7">
                    <div class="portlet light">
                        <div class="portlet-title">
                            <div class="caption font-red">
                                <i class="fa fa-home font-red"></i>
                                <span class="caption-subject bold uppercase">Billing address</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form id="billingAddressForm" method="/" class="form-horizontal">
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 control-label text-left">Email Address            <span class="font-red">*</span></label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email Address" data-label="Email Address" value="testing@testing.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="companyname" class="col-sm-3 control-label text-left">Company name            <span class="font-red">*</span></label>

                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="companyname" name="companyname" placeholder="Company name" data-label="Company name" value="testing Company">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="costcentre" class="col-sm-3 control-label text-left">Cost Centre            <div style="font-size: 10px;">if available</div>
                                    </label>


                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="costcentre" name="costcentre" placeholder="Cost Centre" data-label="Cost Centre" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="salutation" class="col-sm-3 control-label text-left">Title            <span class="font-red">*</span></label>

                                    <div class="col-sm-3">
                                        <select class="form-control" id="salutation" name="salutation" data-label="Title">
                                            <option value="">[please select]</option>
                                            <option value="mr">Mr</option>
                                            <option value="ms">Ms</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="firstname" class="col-sm-3 control-label text-left">
                                        First name<span class="font-red">*</span>
                                    </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="firstname" class="form-control" name="firstname" placeholder="First name" data-label="First name" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lastname" class="col-sm-3 control-label text-left">
                                        Last name<span class="font-red">*</span>
                                    </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="lastname" class="form-control" name="lastname" placeholder="Last name" data-label="Last name" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="adressstreet" class="col-sm-3 control-label text-left">Street Name and Number            <span class="font-red">*</span></label>

                                    <div class="col-sm-9">
                                        <input type="text" name="adressstreet" class="form-control" placeholder="Street Name and Number" data-label="Street Name and Number" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="adressstreetadditional" class="col-sm-3 control-label text-left">Additional Information</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="adressstreetadditional" class="form-control" placeholder="Additional Information" data-label="Additional Information" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="zip" class="col-sm-3 control-label text-left">Zip Code            <span class="font-red">*</span></label>

                                    <div class="col-sm-9">
                                        <input type="text" name="zip" class="form-control" placeholder="Zip Code" data-label="Zip Code" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="city" class="col-sm-3 control-label text-left">City            <span class="font-red">*</span></label>

                                    <div class="col-sm-9">
                                        <input type="text" name="city" class="form-control" placeholder="City" data-label="City" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="country" class="col-sm-3 control-label text-left">Country            <span class="font-red">*</span></label>

                                    <div class="col-sm-9">
                                        <select class="form-control" id="country" name="country" data-label="Land">
                                                    <option value="AF">Afghanistan</option>
                                                    <option value="AX">Aland Islands</option>
                                                    <option value="AL">Albania</option>
                                                    <option value="DZ">Algerien</option>
                                                    <option value="AS">American Samoa</option>
                                                    <option value="AD">Andorra</option>
                                                    <option value="AO">Angola</option>
                                                    <option value="AI">Anguilla</option>
                                                    <option value="AQ">Antarctica</option>
                                                    <option value="AG">Antigua and Barbuda</option>
                                                    <option value="AR">Argentina</option>
                                                    <option value="AM">Armenia</option>
                                                    <option value="AW">Aruba</option>
                                                    <option value="AU">Australien</option>
                                                    <option value="AZ">Azerbaijan</option>
                                                    <option value="BS">Bahamas</option>
                                                    <option value="BH">Bahrain</option>
                                                    <option value="BD">Bangladesh</option>
                                                    <option value="BB">Barbados</option>
                                                    <option value="BY">Belarus</option>
                                                    <option value="BE">Belgien</option>
                                                    <option value="BZ">Belize</option>
                                                    <option value="BJ">Benin</option>
                                                    <option value="BM">Bermuda</option>
                                                    <option value="BT">Bhutan</option>
                                                    <option value="BO">Bolivia</option>
                                                    <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                                    <option value="BA">Bosnia and Herzegovina</option>
                                                    <option value="BW">Botswana</option>
                                                    <option value="BV">Bouvet Island</option>
                                                    <option value="BR">Brazilien</option>
                                                    <option value="IO">British Indian Ocean Territory</option>
                                                    <option value="BN">Brunei</option>
                                                    <option value="BG">Bulgaria</option>
                                                    <option value="BF">Burkina Faso</option>
                                                    <option value="BI">Burundi</option>
                                                    <option value="KH">Cambodia</option>
                                                    <option value="CM">Cameroon</option>
                                                    <option value="CA">Canada</option>
                                                    <option value="CV">Cape Verde</option>
                                                    <option value="KY">Cayman Islands</option>
                                                    <option value="CF">Central African Republic</option>
                                                    <option value="TD">Chad</option>
                                                    <option value="CL">Chile</option>
                                                    <option value="CN">China</option>
                                                    <option value="CX">Christmas Island</option>
                                                    <option value="CC">Cocos (Keeling) Islands</option>
                                                    <option value="CO">Colombia</option>
                                                    <option value="KM">Comoros</option>
                                                    <option value="CG">Congo</option>
                                                    <option value="CK">Cook Islands</option>
                                                    <option value="CR">Costa Rica</option>
                                                    <option value="CI">Cote d'ivoire (Ivory Coast)</option>
                                                    <option value="CU">Cuba</option>
                                                    <option value="CW">Curacao</option>
                                                    <option value="CY">Cyprus</option>
                                                    <option value="CZ">Czechia</option>
                                                    <option value="DK">Dänemark</option>
                                                    <option value="CD">Democratic Republic of the Congo</option>
                                                    <option value="DE" selected="">Deutschland</option>
                                                    <option value="DJ">Djibouti</option>
                                                    <option value="DM">Dominica</option>
                                                    <option value="DO">Dominican Republic</option>
                                                    <option value="EC">Ecuador</option>
                                                    <option value="EG">Egypt</option>
                                                    <option value="SV">El Salvador</option>
                                                    <option value="GQ">Equatorial Guinea</option>
                                                    <option value="ER">Eritrea</option>
                                                    <option value="EE">Estonia</option>
                                                    <option value="ET">Ethiopia</option>
                                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                                    <option value="FO">Faroe Islands</option>
                                                    <option value="FJ">Fiji</option>
                                                    <option value="FI">Finland</option>
                                                    <option value="FR">Frankreich</option>
                                                    <option value="GF">French Guiana</option>
                                                    <option value="PF">French Polynesia</option>
                                                    <option value="TF">French Southern Territories</option>
                                                    <option value="GA">Gabon</option>
                                                    <option value="GM">Gambia</option>
                                                    <option value="GE">Georgia</option>
                                                    <option value="GH">Ghana</option>
                                                    <option value="GI">Gibraltar</option>
                                                    <option value="GL">Greenland</option>
                                                    <option value="GD">Grenada</option>
                                                    <option value="GR">Griechenland</option>
                                                    <option value="GP">Guadaloupe</option>
                                                    <option value="GU">Guam</option>
                                                    <option value="GT">Guatemala</option>
                                                    <option value="GG">Guernsey</option>
                                                    <option value="GN">Guinea</option>
                                                    <option value="GW">Guinea-Bissau</option>
                                                    <option value="GY">Guyana</option>
                                                    <option value="HT">Haiti</option>
                                                    <option value="HM">Heard Island and McDonald Islands</option>
                                                    <option value="HN">Honduras</option>
                                                    <option value="HK">Hong Kong</option>
                                                    <option value="IS">Iceland</option>
                                                    <option value="IN">Indien</option>
                                                    <option value="ID">Indonesien</option>
                                                    <option value="IR">Iran</option>
                                                    <option value="IQ">Iraq</option>
                                                    <option value="IE">Ireland</option>
                                                    <option value="IM">Isle of Man</option>
                                                    <option value="IL">Israel</option>
                                                    <option value="IT">Italien</option>
                                                    <option value="JM">Jamaica</option>
                                                    <option value="JP">Japan</option>
                                                    <option value="JE">Jersey</option>
                                                    <option value="JO">Jordan</option>
                                                    <option value="KZ">Kazakhstan</option>
                                                    <option value="KE">Kenya</option>
                                                    <option value="KI">Kiribati</option>
                                                    <option value="XK">Kosovo</option>
                                                    <option value="HR">Kroatien</option>
                                                    <option value="KW">Kuwait</option>
                                                    <option value="KG">Kyrgyzstan</option>
                                                    <option value="LA">Laos</option>
                                                    <option value="LV">Latvia</option>
                                                    <option value="LB">Lebanon</option>
                                                    <option value="LS">Lesotho</option>
                                                    <option value="LR">Liberia</option>
                                                    <option value="LY">Libya</option>
                                                    <option value="LI">Liechtenstein</option>
                                                    <option value="LT">Lithuania</option>
                                                    <option value="LU">Luxemburg</option>
                                                    <option value="MO">Macao</option>
                                                    <option value="MG">Madagascar</option>
                                                    <option value="MW">Malawi</option>
                                                    <option value="MY">Malaysia</option>
                                                    <option value="MV">Maldives</option>
                                                    <option value="ML">Mali</option>
                                                    <option value="MT">Malta</option>
                                                    <option value="MH">Marshall Islands</option>
                                                    <option value="MQ">Martinique</option>
                                                    <option value="MR">Mauritania</option>
                                                    <option value="MU">Mauritius</option>
                                                    <option value="YT">Mayotte</option>
                                                    <option value="MX">Mexico</option>
                                                    <option value="FM">Micronesia</option>
                                                    <option value="MD">Moldava</option>
                                                    <option value="MC">Monaco</option>
                                                    <option value="MN">Mongolia</option>
                                                    <option value="ME">Montenegro</option>
                                                    <option value="MS">Montserrat</option>
                                                    <option value="MA">Morocco</option>
                                                    <option value="MZ">Mozambique</option>
                                                    <option value="MM">Myanmar (Burma)</option>
                                                    <option value="NA">Namibia</option>
                                                    <option value="NR">Nauru</option>
                                                    <option value="NP">Nepal</option>
                                                    <option value="NC">New Caledonia</option>
                                                    <option value="NZ">New Zealand</option>
                                                    <option value="NI">Nicaragua</option>
                                                    <option value="NL">Niederlande</option>
                                                    <option value="NE">Niger</option>
                                                    <option value="NG">Nigeria</option>
                                                    <option value="NU">Niue</option>
                                                    <option value="NF">Norfolk Island</option>
                                                    <option value="KP">North Korea</option>
                                                    <option value="MK">North Macedonia</option>
                                                    <option value="MP">Northern Mariana Islands</option>
                                                    <option value="NO">Norwegen</option>
                                                    <option value="OM">Oman</option>
                                                    <option value="AT">Österreich</option>
                                                    <option value="PK">Pakistan</option>
                                                    <option value="PW">Palau</option>
                                                    <option value="PS">Palestine</option>
                                                    <option value="PA">Panama</option>
                                                    <option value="PG">Papua New Guinea</option>
                                                    <option value="PY">Paraguay</option>
                                                    <option value="PE">Peru</option>
                                                    <option value="PH">Phillipines</option>
                                                    <option value="PN">Pitcairn</option>
                                                    <option value="PL">Polen</option>
                                                    <option value="PT">Portugal</option>
                                                    <option value="PR">Puerto Rico</option>
                                                    <option value="QA">Qatar</option>
                                                    <option value="RE">Reunion</option>
                                                    <option value="RO">Rumänien</option>
                                                    <option value="RU">Russland</option>
                                                    <option value="RW">Rwanda</option>
                                                    <option value="BL">Saint Barthelemy</option>
                                                    <option value="SH">Saint Helena</option>
                                                    <option value="KN">Saint Kitts and Nevis</option>
                                                    <option value="LC">Saint Lucia</option>
                                                    <option value="MF">Saint Martin</option>
                                                    <option value="PM">Saint Pierre and Miquelon</option>
                                                    <option value="VC">Saint Vincent and the Grenadines</option>
                                                    <option value="WS">Samoa</option>
                                                    <option value="SM">San Marino</option>
                                                    <option value="ST">Sao Tome and Principe</option>
                                                    <option value="SA">Saudi Arabia</option>
                                                    <option value="SE">Schweden</option>
                                                    <option value="CH">Schweiz</option>
                                                    <option value="SN">Senegal</option>
                                                    <option value="RS">Serbia</option>
                                                    <option value="SC">Seychelles</option>
                                                    <option value="SL">Sierra Leone</option>
                                                    <option value="SG">Singapore</option>
                                                    <option value="SX">Sint Maarten</option>
                                                    <option value="SI">Slovenia</option>
                                                    <option value="SK">Slowakei</option>
                                                    <option value="SB">Solomon Islands</option>
                                                    <option value="SO">Somalia</option>
                                                    <option value="ZA">South Africa</option>
                                                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                    <option value="KR">South Korea</option>
                                                    <option value="SS">South Sudan</option>
                                                    <option value="ES">Spanien</option>
                                                    <option value="LK">Sri Lanka</option>
                                                    <option value="SD">Sudan</option>
                                                    <option value="SR">Suriname</option>
                                                    <option value="SJ">Svalbard and Jan Mayen</option>
                                                    <option value="SZ">Swaziland</option>
                                                    <option value="SY">Syria</option>
                                                    <option value="TW">Taiwan</option>
                                                    <option value="TJ">Tajikistan</option>
                                                    <option value="TZ">Tanzania</option>
                                                    <option value="TH">Thailand</option>
                                                    <option value="TL">Timor-Leste (East Timor)</option>
                                                    <option value="TG">Togo</option>
                                                    <option value="TK">Tokelau</option>
                                                    <option value="TO">Tonga</option>
                                                    <option value="TT">Trinidad and Tobago</option>
                                                    <option value="TN">Tunisia</option>
                                                    <option value="TR">Türkei</option>
                                                    <option value="TM">Turkmenistan</option>
                                                    <option value="TC">Turks and Caicos Islands</option>
                                                    <option value="TV">Tuvalu</option>
                                                    <option value="UG">Uganda</option>
                                                    <option value="UA">Ukraine</option>
                                                    <option value="HU">Ungarn</option>
                                                    <option value="GB">United Kingdom</option>
                                                    <option value="US">United States</option>
                                                    <option value="UM">United States Minor Outlying Islands</option>
                                                    <option value="UY">Uruguay</option>
                                                    <option value="UZ">Uzbekistan</option>
                                                    <option value="VU">Vanuatu</option>
                                                    <option value="VA">Vatican City</option>
                                                    <option value="VE">Venezuela</option>
                                                    <option value="AE">Vereinigte Arabische Emirate</option>
                                                    <option value="VN">Vietnam</option>
                                                    <option value="VG">Virgin Islands, British</option>
                                                    <option value="VI">Virgin Islands, US</option>
                                                    <option value="WF">Wallis and Futuna</option>
                                                    <option value="EH">Western Sahara</option>
                                                    <option value="YE">Yemen</option>
                                                    <option value="ZM">Zambia</option>
                                                    <option value="ZW">Zimbabwe</option>
                                            </select>
                                    </div>
                                </div>

                                <div id="vatnumber" class="form-group row">
                                    <label for="vatnumber" class="col-sm-3 control-label text-left">
                                        Vat number<span class="font-red">*</span>
                                    </label>

                                    <div class="col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-12 text-left">
                                                <input type="text" title="Letters and numbers only" class="form-control vatnumber" name="vatnumber" value="dfadfa" style="width: 240px;" data-label="Vat number">

                                                <div style="font-size: 12px; margin-top: 4px;">(without spaces)</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone" class="col-sm-3 control-label text-left">Phone</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="phone" class="form-control" placeholder="Phone" data-label="Phone" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fax" class="col-sm-3 control-label text-left">Fax            <div style="font-size: 10px;">if available</div>
                                    </label>

                                    <div class="col-sm-9">
                                        <input type="text" name="fax" class="form-control" placeholder="Fax" data-label="Fax" value="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="mobile" class="col-sm-3 control-label text-left">Mobile            <div style="font-size: 10px;">if available</div>
                                    </label>

                                    <div class="col-sm-9">
                                        <input type="text" name="mobile" class="form-control" placeholder="Mobile" data-label="Mobile" value="">
                                    </div>
                                </div>


                                <div class="text-center">
                                    <div class="font-red">*- Mandatory</div>
                                    <button type="submit" class="btn btn-primary mt-15" data-style="expand-center" data-size="s">Save changes</button>
                                </div>
                            </form>

                            <script>
                                $(document).ready(function () {

                                    $('input[name=vatcode]').keyup(function (e) {
                                        if (e.which === 32) {
                                            var str = $(this).val();
                                            str = str.replace(/\s/g, '');
                                            $(this).val(str);
                                        }
                                    }).blur(function () {
                                        var str = $(this).val();
                                        str = str.replace(/\s/g, '');
                                        $(this).val(str);
                                    });

                                    $('#billingAddressForm').submit(function (e) {
                                        e.preventDefault();

                                        var click_this = this;

                                        $('#billingAddressForm input,#billingAddressForm select').removeClass("inputError");

                                        var missingInput = false;

                                        var fields = ["email", "salutation", "companyname", "fullname", "emailaddress"];
                                        var missingInputLabels;
                                        var missingInputLabels = [];

                                        fields.forEach(function (entry) {
                                            if ($(click_this).find("input[name=" + entry + "]").val() == "") {

                                                missingInputLabels.push($(click_this).find("input[name=" + entry + "]").data("label"));

                                                $('#billingAddressForm [name=' + entry + ']').addClass("inputError");

                                                missingInput = true;
                                            }
                                        });

                                        if (missingInput) {
                                            notification.error('Entry missing: ' + missingInputLabels.join(", "));

                                            return;
                                        }

                                        var formData = $(this).serialize();
                                        showLoading();

                                        updateBillingAddress(formData);
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 pull-right">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h2 class="panel-title font-size-24">Package Lite</h2>
                        </div>
                        <div class="panel-body">
                            <span class="font-red font-size-18 bold uppercase padding-left-5"> Services</span>
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <td>Daily search limit:</td>
                                    <td>50 searches</td>
                                </tr>
                                <tr>
                                    <td>Monthly profile views:</td>
                                    <td>120 views</td>
                                </tr>
                                <tr>
                                    <td>Display Posting History:</td>
                                    <td>Unlimited</td>
                                </tr>
                                <tr>
                                    <td>Influencer List limit:</td>
                                    <td>30 Items</td>
                                </tr>
                                </tbody>
                            </table>
                            <span class="font-red font-size-18 bold uppercase padding-left-5"> Price</span>
                                            <table class="table table-hover margin-bottom-0">
                                <tbody>
                                <tr>
                                    <td>Billing cycle:</td>
                                    <td>monthly</td>
                                </tr>
                                <tr>
                                    <td>Cancellation due:</td>
                                    <td>monthly</td>
                                </tr>
                                <tr>
                                    <td>Net:</td>
                                    <td>299,00€ per month</td>
                                </tr>
                                                    <tr>
                                    <td>                                Tax (19%):
                                                                </td>
                                    <td>56,81€ per month</td>
                                </tr>
                                <tr class="active">
                                    <td class="bold font-size-18">Total:</td>
                                    <td class="bold font-size-18">355,81€ per month</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
