@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <style>
        .card-header img{
            width:100%;
            height: auto;
        }
        .card-section{
            display:flex;
            justify-content: center;
            align-items:center;
            flex-direction:column;
        }
        .card-social{
            display:flex;
            justify-content: space-evenly;
            align-items: center;
            flex-direction: row;
        }
        .card-social div div{
            display:flex;
            justify-content: center;
            align-items: center;
            width: 35px;
            height: 35px;
            border-radius: 100%;
            font-size:20px;
        }
        .card-social .in-active div{
            background-color:rgb(194, 193, 193);
        }
        .follow-filter{
            display:flex;
            flex-direction: column;
            justify-content: flex-start;
        }
        .follow-filter .child{
            flex:0 1 50px;
            display: flex;
            justify-content: flex-start;
            align-items: flex-start;
            flex-direction: column;
            width:200px;
        }
        .socials{
            display:flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }
        .socials .social{
            display:flex;
            justify-content: center;
            align-items: center;
            flex-direction:row;
            /* margin-righe */
        }
        .socials .social .soc{
            display:flex;
            justify-content: center;
            align-items: center;
            border-radius: 100%;
            width:30px;
            font-size:18px;
            margin:2px;
            height:30px;
        }
        .follow-input{
            display:flex;
            justify-content: space-around;
        }
        .follow-input div{
            flex:1 1 100px;
            margin:5px;
        }
        .influ-btn{
            display:flex;
            justify-content: center;
            align-items: center;
            margin:25px 10px;
        }
        .influ-btn div{
            flex:0 1 100px;
        }
        .social-hover:hover{
            cursor:pointer;
        }
        .my-success{
            background-color:#28A745;
            color:white;
        }
        .center-out{
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
        }
        .fade-in {
            animation: fadeIn ease 1s;
        }
        @keyframes fadeIn {
            0% {opacity:0;}
            100% {opacity:1;}
        }
        .no-data{
            margin:100px 0px;
            padding: 20px;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            background-color:rgb(235, 239, 240);
            font-size:16px;
            text-align: center;
        }
        .no-data i{
            font-size:5.5rem;
            color:rgb(126, 140, 141);
        }
        .no-data h3{
            font-size:2rem;
            color:rgb(126, 140, 141);
            margin:20px 0px;
        }





    </style>
@endsection

@section('content')
<div class="container">
    <h3>Influencers</h3>
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header card-social">
                    <div class="social-hover" onclick="set_category('all')" id="id_all">
                        <div class="my-success"><i class="fas fa-user"></i></div>
                    </div>
                    @foreach($categories as $category)
                        <div class="in-active social-hover" onclick="set_category('{{$category->name}}')" id="id_{{$category->name}}">
                            {!!$category->image!!}
                        </div>
                    @endforeach
                 </div>
                 <div class="card-body follow-filter">
                     <h6>AMOUNT OF FOLLOWER</h6>
                    <form id="execute_search">
                        <div class="follow-input">
                            <div>
                                <input class="form-control" type="number" name="min">
                                <p>Minimum:<span></span></p>
                            </div>
                            <div>
                                <input class="form-control" type="number" name="max">
                                <p>Maximum:<span></span></p>
                            </div>
                        </div>
                        <div class="child mt-4">
                            <span class="mb-2"><i class="fa fa-tag"></i> Category</span>
                            <select class="form-control" name="type">
                                <option value="">--- Not Selected ---</option>
                                @foreach($types as $type)
                                    <option value="{{$type->name}}">{{$type->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="child mt-4">
                            <span class="mb-2"><i class="fas fa-user"></i> Gender</span>
                            <select class="form-control" name="gender">
                                <option value="">--- Not Selected ---</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Without Gender">Without Gender</option>
                            </select>
                        </div>
                        <button type="submit" id="execute-btn" style="display:none;"></button>
                    </form>
                    <div class="influ-btn">
                        <div>
                            <button class="btn btn-info m-1" onclick="execute()">EXECUTE SEARCH</button>
                        </div>
                        <div>
                            <button class="btn btn-secondary m-1" onclick="reset()">RESET</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="app" class="col-md-8">
            <div class="row" id="influencer-cards">
                @include('cardData.influencerCard')
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/brandInfluencer.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
@endsection














