@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Premium</h1>
            <div class="portlet light">
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="caption">
                                <i class="fa fa-info-circle font-green" aria-hidden="true"></i>
                                <span class="caption-subject font-green bold uppercase">Package Information</span>
                            </div>
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
                                            <td>Your package:</td>
                                            <td>Free  </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="caption">
                                <i class="fa fa-check-circle font-green" aria-hidden="true"></i>
                                <span class="caption-subject font-green bold uppercase">Consumption and limits</span>
                            </div>
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
                                            <td>Search queries (daily)</td>
                                            <td>1 / 15</td>
                                        </tr>
                                        <tr>
                                            <td>Profile views (monthly)</td>
                                            <td>3 / 3</td>
                                        </tr>
                                        <tr>
                                            <td>Display Posting History</td>
                                            <td>Unlimited</td>
                                        </tr>
                                        <tr>
                                            <td>Influencer in lists <span class="tooltips" style="font-size: 15px;" data-original-title="You can create lists where you can store influencers in order to find them faster (e.g. fitness influencers). The number here shows you the maximum number of influencers you can save in all lists."><i class="fa fa-question-circle" aria-hidden="true"></i></span></td>
                                            <td>0 / 10</td>
                                        </tr>
                                        <tr>
                                            <td>Audience Reports</td>
                                            <td>0 / 0</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cubes font-blue" aria-hidden="true"></i>
                        <span class="caption-subject font-blue bold uppercase">Packages</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <ul class="plans clearfix">
                        <li class="plans_descriptions">
                            <div class="header_bar"></div>
                            <div class="plan_block">
                                <div class="plan_header description_header">
                                    <div class="plan_price_name">
                                        <div class="plan_name_block">
                                            <div class="plan_name"><span>Wähle</span></div>
                                            <div class="plan_name_second_line"><span>deinen Plan</span></div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="plan_features_descriptions">
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Number of search queries that can be executed per day via the search."><span style="border-bottom: 1px dotted #000; text-decoration: none;">Search queries</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Number of profile views of influencers that can be accessed per month"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Profile views</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="You can create lists where you can store influencers in order to find them faster (e.g. fitness influencers). The number here shows you the maximum number of influencers you can save in all lists."><span style="border-bottom: 1px dotted #000; text-decoration: none;">Influencer in lists</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="For each Instagram profile you can generate an Audience Report. This will show you additional analytics for the audience. The possible additional analytics can be found below."><span style="border-bottom: 1px dotted #000; text-decoration: none;">Audience Reports</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Demography of Audience (Follower)"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Demography</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Geography of Audience (Follower)"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Geography</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Languages of Audience (Follower)"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Languages</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Categorizes the quality of Audience (Follower) in: Real persons, influencers, mass followers and suspicious accounts. For each category, the number of followers in that category is displayed."><span style="border-bottom: 1px dotted #000; text-decoration: none;">Audience quality</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Displays the number of followers that appear authentic in percentage terms"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Audience authenticity</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Whit this option you are able to invite influencers to your campaign."><span style="border-bottom: 1px dotted #000; text-decoration: none;">Invite to campaign</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Shows on which instagram profiles the respective channel has already been mentioned."><span style="border-bottom: 1px dotted #000; text-decoration: none;">Channel mentions</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Displays the percentage of followers who are easily reachable (have less than 1,500 followings)"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Audience reachability</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="The follower growth is analyzed and a result is displayed, how organic and real the follower growth is."><span style="border-bottom: 1px dotted #000; text-decoration: none;">Follower-Growth-Analysis</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Enables you to contact influencers directly on our platform"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Message Influencers</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Shows you the interests of the Audience (Follower) (e.g. Beauty &amp; Cosmetics or Fitness &amp; Yoga)"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Audience interests</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Shows you in which categories the brands advertised on the respective Instagram profile are located (e.g. Beauty &amp; Fashion or Cars &amp; Motorcycles)"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Brand mentions</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Compares the engagement rate of mailings with normal posts and compares them with similar instagram profiles"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Ad performance</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Analyzes the mood in comments (positive, neutral, negative)"><span style="border-bottom: 1px dotted #000; text-decoration: none;">Sentiment Analysis</span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_description tooltips" data-original-title="Monitoring &amp; tracking of hashtags on instagram. This feature is e.g. interesting to get user-generated content (UGC) displayed at a glance."><span style="border-bottom: 1px dotted #000; text-decoration: none;">Hashtag Tracking</span></span>
                                    </li>
                                </ul>
                                <div class="plan_order"></div>
                            </div>
                        </li>
                        <li class="plan">
                            <div class="header_bar"></div>
                            <div class="plan_block">
                                <div class="plan_tip"><span>Best Price</span></div>
                                <div class="plan_header">
                                    <div class="plan_price_name">
                                        <div class="price_block">
                                            <div class="price_amount">
                                                <span class="price_value">€299</span>
                                                <span class="price_period">/mo</span>
                                            </div>
                                        </div>
                                        <div class="plan_name_block">
                                            <div class="plan_name"><span>Lite</span></div>
                                            <div class="plan_name_second_line"><span>small business</span></div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="plan_features">
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="Number of search queries that can be executed per day via the search." style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Search queries:</small></span>
                                        <span class="plan_feature_value">50 daily</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="Number of profile views of influencers that can be accessed per month" style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Profile views:</small></span>
                                        <span class="plan_feature_value">120  monthly</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="You can create lists where you can store influencers in order to find them faster (e.g. fitness influencers). The number here shows you the maximum number of influencers you can save in all lists." style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Influencer in lists:</small></span>
                                        <span class="plan_feature_value">30 Entries</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="For each Instagram profile you can generate an Audience Report. This will show you additional analytics for the audience. The possible additional analytics can be found below." style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Audience Reports:</small></span>
                                        <span class="plan_feature_value">30 monthly</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Demography of Audience (Follower)" style="border-bottom: 1px dotted #000; text-decoration: none;">Demography</span>
                                        <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Geography of Audience (Follower)" style="border-bottom: 1px dotted #000; text-decoration: none;">Geography (lite)</span>
                                        <span class="plan_feature_value"><span class="feature_yes_yellow"></span></span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Languages of Audience (Follower)" style="border-bottom: 1px dotted #000; text-decoration: none;">Languages</span>
                                        <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Categorizes the quality of Audience (Follower) in: Real persons, influencers, mass followers and suspicious accounts. For each category, the number of followers in that category is displayed." style="border-bottom: 1px dotted #000; text-decoration: none;">Audience quality</span>
                                        <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Audience authenticity</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Invite to campaign</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Channel mentions</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Audience reachability</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Follower-Growth-Analysis</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Message Influencers</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Audience interests</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Brand mentions</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Ad performance</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Sentiment Analysis</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Hashtag Tracking</span>
                                        <span class="plan_feature_value"><span class="feature_no"></span></span>
                                    </li>
                                </ul>
                                <div class="plan_order text-center">
                                    <a href="/brand/influencer/subscription/99/subscribe" class="btn green featured-price uppercase">buy now</a>
                                </div>
                            </div>
                        </li>
                        <li class="plan">
                            <div class="header_bar"></div>
                            <div class="plan_block">
                                <div class="plan_tip"><span>★ Most popular ★</span></div>
                                <div class="plan_header">
                                    <div class="plan_price_name">
                                        <div class="price_block">
                                            <div class="price_amount">
                                                <span class="price_value">€499</span>
                                                <span class="price_period">/mo</span>
                                            </div>
                                        </div>
                                        <div class="plan_name_block">
                                            <div class="plan_name"><span>Basic</span></div>
                                            <div class="plan_name_second_line" style="width: 125px"><span>medium business</span></div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="plan_features">
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="Number of search queries that can be executed per day via the search." style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Search queries:</small></span>
                                        <span class="plan_feature_value">150 daily</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="Number of profile views of influencers that can be accessed per month" style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Profile views:</small></span>
                                        <span class="plan_feature_value">360  monthly</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="You can create lists where you can store influencers in order to find them faster (e.g. fitness influencers). The number here shows you the maximum number of influencers you can save in all lists." style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Influencer in lists:</small></span>
                                        <span class="plan_feature_value">150 Entries</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="For each Instagram profile you can generate an Audience Report. This will show you additional analytics for the audience. The possible additional analytics can be found below." style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Audience Reports:</small></span>
                                        <span class="plan_feature_value">50 monthly</span>
                                    </li>
                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Demography of Audience (Follower)" style="border-bottom: 1px dotted #000; text-decoration: none;">Demography</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>
                                    <li>
                                                                                                                            <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Geography of Audience (Follower)" style="border-bottom: 1px dotted #000; text-decoration: none;">Geography</span>
                                                <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                                                                </li>
                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Languages of Audience (Follower)" style="border-bottom: 1px dotted #000; text-decoration: none;">Languages</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                                                            <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Categorizes the quality of Audience (Follower) in: Real persons, influencers, mass followers and suspicious accounts. For each category, the number of followers in that category is displayed." style="border-bottom: 1px dotted #000; text-decoration: none;">Audience quality</span>
                                                <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                                                                </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Displays the number of followers that appear authentic in percentage terms" style="border-bottom: 1px dotted #000; text-decoration: none;">Audience authenticity</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Whit this option you are able to invite influencers to your campaign." style="border-bottom: 1px dotted #000; text-decoration: none;">Invite to campaign</span>
                                        <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                    </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Shows on which instagram profiles the respective channel has already been mentioned." style="border-bottom: 1px dotted #000; text-decoration: none;">Channel mentions</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Displays the percentage of followers who are easily reachable (have less than 1,500 followings)" style="border-bottom: 1px dotted #000; text-decoration: none;">Audience reachability</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="The follower growth is analyzed and a result is displayed, how organic and real the follower growth is." style="border-bottom: 1px dotted #000; text-decoration: none;">Follower-Growth-Analysis</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Enables you to contact influencers directly on our platform" style="border-bottom: 1px dotted #000; text-decoration: none;">Message Influencers</span>
                                        <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                    </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Shows you the interests of the Audience (Follower) (e.g. Beauty &amp; Cosmetics or Fitness &amp; Yoga)" style="border-bottom: 1px dotted #000; text-decoration: none;">Audience interests</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Brand mentions</span>
                                            <span class="plan_feature_value"><span class="feature_no"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Ad performance</span>
                                            <span class="plan_feature_value"><span class="feature_no"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Sentiment Analysis</span>
                                            <span class="plan_feature_value"><span class="feature_no"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up" style="text-decoration: line-through;">Hashtag Tracking</span>
                                            <span class="plan_feature_value"><span class="feature_no"></span></span>
                                                                        </li>
                                </ul>
                                <div class="plan_order text-center">
                                                                        <a href="/brand/influencer/subscription/99/subscribe" class="btn green featured-price uppercase">buy now</a>
                                                                </div>
                            </div>
                        </li>
                        <li class="plan">
                            <div class="header_bar"></div>
                            <div class="plan_block">
                                <div class="plan_tip"><span>Best Value</span></div>
                                <div class="plan_header">
                                    <div class="plan_price_name">
                                        <div class="price_block">
                                            <div class="price_amount">
                                                <span class="price_value">€699</span>
                                                <span class="price_period">/mo</span>
                                            </div>
                                        </div>
                                        <div class="plan_name_block">
                                            <div class="plan_name"><span>Pro</span></div>
                                            <div class="plan_name_second_line"><span>large business</span></div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="plan_features">
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="Number of search queries that can be executed per day via the search." style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Search queries:</small></span>
                                        <span class="plan_feature_value">300 daily</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="Number of profile views of influencers that can be accessed per month" style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Profile views:</small></span>
                                        <span class="plan_feature_value">500  monthly</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="You can create lists where you can store influencers in order to find them faster (e.g. fitness influencers). The number here shows you the maximum number of influencers you can save in all lists." style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Influencer in lists:</small></span>
                                        <span class="plan_feature_value">250 Entries</span>
                                    </li>
                                    <li>
                                        <span class="plan_feature_name tooltips" data-original-title="For each Instagram profile you can generate an Audience Report. This will show you additional analytics for the audience. The possible additional analytics can be found below." style="border-bottom: 1px dotted #000; text-decoration: none;"><small>Audience Reports:</small></span>
                                        <span class="plan_feature_value">100 monthly</span>
                                    </li>
                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Demography of Audience (Follower)" style="border-bottom: 1px dotted #000; text-decoration: none;">Demography</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>
                                    <li>
                                                                                                                            <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Geography of Audience (Follower)" style="border-bottom: 1px dotted #000; text-decoration: none;">Geography</span>
                                                <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                                                                </li>
                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Languages of Audience (Follower)" style="border-bottom: 1px dotted #000; text-decoration: none;">Languages</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                                                            <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Categorizes the quality of Audience (Follower) in: Real persons, influencers, mass followers and suspicious accounts. For each category, the number of followers in that category is displayed." style="border-bottom: 1px dotted #000; text-decoration: none;">Audience quality</span>
                                                <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                                                                </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Displays the number of followers that appear authentic in percentage terms" style="border-bottom: 1px dotted #000; text-decoration: none;">Audience authenticity</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Whit this option you are able to invite influencers to your campaign." style="border-bottom: 1px dotted #000; text-decoration: none;">Invite to campaign</span>
                                        <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                    </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Shows on which instagram profiles the respective channel has already been mentioned." style="border-bottom: 1px dotted #000; text-decoration: none;">Channel mentions</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Displays the percentage of followers who are easily reachable (have less than 1,500 followings)" style="border-bottom: 1px dotted #000; text-decoration: none;">Audience reachability</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="The follower growth is analyzed and a result is displayed, how organic and real the follower growth is." style="border-bottom: 1px dotted #000; text-decoration: none;">Follower-Growth-Analysis</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                        <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Enables you to contact influencers directly on our platform" style="border-bottom: 1px dotted #000; text-decoration: none;">Message Influencers</span>
                                        <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                    </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Shows you the interests of the Audience (Follower) (e.g. Beauty &amp; Cosmetics or Fitness &amp; Yoga)" style="border-bottom: 1px dotted #000; text-decoration: none;">Audience interests</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Shows you in which categories the brands advertised on the respective Instagram profile are located (e.g. Beauty &amp; Fashion or Cars &amp; Motorcycles)" style="border-bottom: 1px dotted #000; text-decoration: none;">Brand mentions</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Compares the engagement rate of mailings with normal posts and compares them with similar instagram profiles" style="border-bottom: 1px dotted #000; text-decoration: none;">Ad performance</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Analyzes the mood in comments (positive, neutral, negative)" style="border-bottom: 1px dotted #000; text-decoration: none;">Sentiment Analysis</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>

                                    <li>
                                                                                <span class="plan_feature_name hidden-sm-up tooltips" data-original-title="Monitoring &amp; tracking of hashtags on instagram. This feature is e.g. interesting to get user-generated content (UGC) displayed at a glance." style="border-bottom: 1px dotted #000; text-decoration: none;">Hashtag Tracking</span>
                                            <span class="plan_feature_value"><span class="feature_yes"></span></span>
                                                                        </li>
                                </ul>
                                <div class="plan_order text-center">
                                                                        <a href="/brand/influencer/subscription/99/subscribe" class="btn green featured-price uppercase">buy now</a>
                                                                </div>
                            </div>
                        </li>
                    </ul>

                    <br><br><br>
                </div>
            </div>
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-question-circle font-blue" aria-hidden="true"></i>
                        <span class="caption-subject font-blue bold uppercase">FAQ</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="faq-page faq-content-1">
                                <div class="faq-content-container">
                                    <div class="faq-section" style="padding-top: 0">
                                        <div class="panel-group accordion faq-content" id="accordion1">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <i class="fa fa-circle"></i>
                                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1" aria-expanded="false">What is the billing period for the packages?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0;">
                                                    <div class="panel-body">
                                                        <p>Each package has a monthly billing period. The billing period begins on the day you purchased the package and is then valid for one month. At the end of a period, it is always automatically extended by one month.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <i class="fa fa-circle"></i>
                                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_2" aria-expanded="false"> What payment methods are available?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_2" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <p>Currently there is the possibility to pay by credit card, SEPA direct debit, Paypal and invoice.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <i class="fa fa-circle"></i>
                                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_3" aria-expanded="false"> Can I upgrade/downgrade my package?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_3" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <p>You can upgrade or downgrade your current package at any time. With an upgrade, your new package will be activated immediately after ordering and any remaining credit of your current package will be charged. If you downgrade, your new package will only become active after the next settlement (except when paying with Paypal).</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <i class="fa fa-circle"></i>
                                                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4" aria-expanded="false"> How can I cancel my package?</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_4" class="panel-collapse collapse" aria-expanded="false">
                                                    <div class="panel-body">
                                                        <p> You can cancel your package at any time towards the end of the period. Just send us an email to brand@reachhero.de</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
