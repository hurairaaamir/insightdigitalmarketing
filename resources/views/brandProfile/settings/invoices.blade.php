@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/brandSettingsMenu.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/invoices.css') }}" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Settings</h1>
            <div class="row">
                <div class="col-sm-4" style="color: #4a4a4a; margin-bottom: 20px;">
                    @include('partials.brandSettingsMenu')
                </div>
                <div class="col-sm-8">
                    <div class="portlet light text-center">
                        <div>
                            <h2 style="padding-top: 0; margin-top: 0;">Invoices</h2>
                        </div>
                        <div>
                            <div>Please find the invoices for the influencer offers you accepted here.</div><div style="font-size: 12px;">(the data under "<a href="/settings">General Information</a>"is used as the invoice address)</div>            </div>

                        <br>
                            <div id="invoices_wrapper" class="dataTables_wrapper no-footer">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="dataTables_length" id="invoices_length">
                                            <label>
                                                <select name="invoices_length" aria-controls="invoices" class="form-control input-sm input-xsmall input-inline">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select> records
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div id="invoices_filter" class="dataTables_filter">
                                            <label>Search:
                                                <input type="search" class="form-control input-sm input-small input-inline" placeholder="" aria-controls="invoices">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered dataTable no-footer" id="invoices" role="grid" aria-describedby="invoices_info" style="width: 690px;">
                                        <thead>
                                        <tr role="row"><th class="sorting_desc" tabindex="0" aria-controls="invoices" rowspan="1" colspan="1" style="width: 64px;" aria-label="Date: activate to sort column ascending" aria-sort="descending">Date</th><th class="sorting" tabindex="0" aria-controls="invoices" rowspan="1" colspan="1" style="width: 173px;" aria-label="Invoice number: activate to sort column ascending">Invoice number</th><th class="sorting" tabindex="0" aria-controls="invoices" rowspan="1" colspan="1" style="width: 67px;" aria-label="Total: activate to sort column ascending">Total</th><th class="sorting" tabindex="0" aria-controls="invoices" rowspan="1" colspan="1" style="width: 80px;" aria-label="Status: activate to sort column ascending">Status</th><th class="sorting" tabindex="0" aria-controls="invoices" rowspan="1" colspan="1" style="width: 62px;" aria-label="Title: activate to sort column ascending">Title</th><th class="sorting_disabled" rowspan="1" colspan="1" style="width: 16px;" aria-label=""></th></tr>
                                        </thead>
                                        <tbody>

                                        <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No invoices found</td></tr></tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-md-5 col-sm-5">
                                        <div class="dataTables_info" id="invoices_info" role="status" aria-live="polite">
                                            Showing 0 to 0 of 0 entries
                                        </div>
                                    </div>
                                    <div class="col-md-7 col-sm-7">
                                        <div class="dataTables_paginate paging_bootstrap_number" id="invoices_paginate">
                                            <ul class="pagination" style="visibility: hidden;">
                                                <li class="prev disabled">
                                                    <a href="#" title="Prev">
                                                        <i class="fa fa-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li class="next disabled">
                                                    <a href="#" title="Next">
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
