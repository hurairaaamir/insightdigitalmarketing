@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/brandSettingsMenu.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/createdBrands.css') }}" rel="stylesheet" />


    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Settings</h1>
            <div class="row">
                <div class="col-sm-4" style="color: #4a4a4a; margin-bottom: 20px;">
                    @include('partials.brandSettingsMenu')
                </div>
                <div id="app" class="col-sm-8">
                    <div class="portlet light text-center">
                        <h3>User Management</h3>
                        @if(count($brands) >= 1)
                            <p>
                                Aktuell verwendete Firma: <b>{{ $active_brand['company_name'] }}</b>

                            </p>
                        @else
                            <p>
                                No Brands Created Yet

                            </p>
                        @endif

                        <table class="table table-striped">
                            <tbody>
                                @foreach($brands as $brand)
                                    @if($brand->active == 1)
                                        <tr>
                                            <td><img src="{{ asset('images/brand/profile/'. $brand->profile_image) }}" height="50" width="50" class="img-rounded"></td>
                                            <td>{{ $brand->company_name }}</td>
                                            <td>{{ $brand->placement_campaigns }} Placement-Kampagnen</td>
                                            <td>{{ $brand->performance_campaigns }} Performance-Kampagnen</td>
                                            <td>
                                                In Verwendung

                                                <br><br>
                                                <manage-user-access :id={{ $brand->id }}></manage-user-access>
                                            </td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td><img src="{{ asset('images/brand/profile/' . $brand->profile_image) }}" height="50" width="50" class="img-rounded"></td>
                                            <td>{{ $brand->company_name }}</td>
                                            <td>{{ $brand->placement_campaigns }} Placement-Kampagnen</td>
                                            <td>{{ $brand->performance_campaigns }} Performance-Kampagnen</td>
                                            <td>
                                                <a class="btn red" href="/brand/settings/activate-brand/{{ $brand->id }}">Verwenden</a>

                                                <br><br>
                                                <manage-user-access :id={{ $brand->id }}></manage-user-access>

                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                            <div class="margin-top-30 margin-bottom-30">
                                <a class="btn blue" href="/brand/create">Create Brand</a>
                            </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END MAIN CONTENT -->






@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    {{-- <script src="{{asset('js/brand/settings/created-brands.js')}}"></script> --}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
