@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/brandSettingsMenu.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <style>
        .dataTables_paginate{
            margin-bottom:20px!important;
        }
        .paginate_button{
            color: #fff!important;
            background-color: #1e7edd!important;
            border-color: #1975d1!important;
            padding: 5px!important;
            font-size: 16px!important;
            border-radius:5px;
            margin: 1px;
        }
        .paginate_button:hover{
            cursor:pointer;

        }

        .table-style {
            width: 100%;
            overflow-x: scroll;
            overflow-y: hidden;
            /* padding-right: 80px; */
        }
    </style>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Settings</h1>
            <div class="row">
                <div class="col-sm-4" style="color: #4a4a4a; margin-bottom: 20px;">
                    @include('partials.brandSettingsMenu')
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="payout-history">
                            <h5>Payout History</h5>
                            <p>Here you can find all of the payout records for your account.</p>
                        </div>
                        <div class="card table-style" style="padding: 10px;" >
                            <table id="example1" class="table table-bordered dataTable " aria-describedby="example1_info">
                                <thead>
                                    <tr>
                                        <th>

                                        </th>
                                        <th style="min-width:170px;">
                                            Title
                                        </th>
                                        <th style="min-width:190px;">
                                            Description
                                        </th>
                                        <th >
                                            TimeStamp
                                        </th>
                                        <th >
                                            &nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count=0;
                                    @endphp
                                    @foreach(auth()->user()->readNotifications as $key => $notification)
                                    @php
                                        $count++;
                                    @endphp
                                        <tr class="red-notification">
                                            <td>
                                                {{$count}}
                                            </td>
                                            <td>
                                                <img src="{{$notification->data["image"]}}" style="width:40px;height:40px;border-radius:100%;">
                                                {{ $notification->data["title"] ?? '' }}
                                            </td>
                                            <td>
                                                {{ $notification->data["description"] ?? '' }}
                                            </td>
                                            <td>
                                                {{ $notification->created_at ?? '' }}
                                            </td>

                                            <td>
                                                <form action="{{url('/notification/admin/delete/'.$notification->id)}}" method="POST" style="display:none;" id="form_notification">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                                <button class="btn btn-xs btn-danger" onclick="NotificationDelete()">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @foreach(auth()->user()->unreadNotifications as $key => $notification)
                                        @php
                                            $count++;
                                        @endphp
                                        <tr>
                                            <td >
                                                {{$count}}
                                            </td>
                                            <td style="min-width:100px;">
                                                <img src="{{$notification->data["image"]}}" style="width:40px;height:40px;border-radius:100%;">
                                                {{ $notification->data["title"] ?? '' }}
                                            </td>
                                            <td style="min-width:150px;">
                                                {{ $notification->data["description"] ?? '' }}
                                            </td>
                                            <td style="min-width:100px;">
                                                {{ $notification->created_at ?? '' }}
                                            </td>

                                            <td style="min-width:100px;">
                                                <form action="{{url('/notification/admin/delete/')}}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>
                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
        $('#example1').DataTable({
            // "scrollX": true,
            "order": [[ 0, "desc" ]],

        } );

        function NotificationDelete(){
            if(confirm('Are you Sure?')){
                document.getElementById('form_notification').submit();
            }
        }
    </script>
@endsection








