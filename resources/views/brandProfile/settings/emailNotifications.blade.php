@extends('layouts.brand')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/brandSettingsMenu.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/emailNotifications.css') }}" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Settings</h1>
            <div class="row">
                <div class="col-sm-4" style="color: #4a4a4a; margin-bottom: 20px;">
                    @include('partials.brandSettingsMenu')
                </div>

                <div id="app" class="col-sm-8">
                    
                    @if($no_brand_found)
                        <div class="portlet light">
                            <div class="margin-top-30 margin-bottom-30">
                                <a class="btn blue" href="/brand/create">Create Brand</a>
                            </div>
                        </div>
                    @else
                        <brand-email-notifications-settings></brand-email-notifications-settings>
                    @endif

                </div>
            </div>

        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script>
        $(function() {
            $(".bootstrap-switch-container").click(function(){
                $(this).toggleClass('change-margin');
            });
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
