@extends('layouts.brand')

@section('styles')
    <style>
        .container .card{
            padding:110px;
        }
        .card .center_out{
            display:flex;
            justify-content: center;
            align-items:center;
            flex-direction:column;
            width:100%;
        }
        .card .center_out .check{
            width:150px;
            height: 150px;
            font-size:80px;
            color:rgb(33, 187, 33);
            display:flex;
            justify-content: center;
            align-items:center;
            flex-direction:column;
            border-radius:100%;
            border:solid 10px rgb(33, 187, 33);
            margin:20px;
        }
        .card .center_out  h3{
            font-weight:600;   
        }
        .btn-social{
            padding:10px 40px;
            font-size: 20px;
            color:red;
            background-color: white;
            border:2px solid red;
            border-radius: 40px;
            transition: all 0.5s ease;
        }
        .btn-social:hover{
            color:white;
            background-color: red;
            cursor:pointer;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="center_out">
                <div class="check"><i class="fas fa-check"></i></div>

                <h3>CAMPAINGN CREATED SUCCESSFULLY</h3>
                <div>Your campaign has been successfully created and is now reviewed by US.</div>
                <div>You will be notified by us as soon as the check is done.</div>
                <a href="{{url('/brand/campaigns/index')}}"><button class="btn-social mt-4" >My Campaigns</button></a>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    
@endsection
