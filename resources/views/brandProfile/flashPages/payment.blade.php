@extends('layouts.brand')

@section('styles')
    <style>
        .container .card{
            padding:110px;
        }
        .card .center_out{
            display:flex;
            justify-content: center;
            align-items:center;
            flex-direction:column;
            text-align:center;
            width:100%;
        }
        .card .center_out .check{
            width:150px;
            height: 150px;
            font-size:120px;
            color:rgb(33, 187, 33);
            display:flex;
            justify-content: center;
            align-items:center;
            flex-direction:column;
            margin:15px;
        }
        .card .center_out  h3{
            font-weight:600;   
        }
        .card .center_out  h4{
            font-weight:600;   
        }
        .btn-social{
            padding:10px 30px;
            font-size: 20px;
            color:red;
            background-color: white;
            border:2px solid red;
            border-radius: 40px;
            transition: all 0.5s ease;
        }
        .btn-social:hover{
            color:white;
            background-color: red;
            cursor:pointer;
        }
        .ShipTo_container_2wZjT {
            display:none!important;
        }
        #paypalflash{
        display:flex;
        align-items:center;
        justify-content:center;
        margin-top:100px;
        }
        #paypalflash #flash{
        display:none;
        padding:10px 20px;
        background-color:rgba(16, 3, 83, 0.808);
        border-radius:5px;
        }
        #paypalflash #flash h5{
        font-weight:bold;
        color:white;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="card">
            @if(isset($payment))
                <div class="center_out">
                    <div class="check"><i class="far fa-smile"></i></div>

                    <h3>PAYPAL PAYMENT</h3>
                    <div>You will have to make the payment of offer before sending the request to the Influencer</div>
                    <div style="font-weight: 800">If the influencer failed to make complete the Campaigns,all your payment will be return</div>
                    <div style="font-weight: 800"> except Paypal transaction fee</div>
                    {{-- <a href="{{url('/brand/campaigns/index')}}"><button class="btn-social mt-4" >PayPal</button></a> --}}
                    <div class="m-3" style="font-weight:800;font-size:20px">Payment : {{$payment}} $</div>

                    <div id="paypal-button-container" class="mt-4"></div>
                </div>
            @endif
        </div>
    </div>
@endsection


@section('scripts')
<script src="https://www.paypal.com/sdk/js?client-id=AQSIHDvNAKgQgA26-859RTZKnVop9N-VieK41zM_qXivcMT_iLkBbJZg5Cgy-DO47Xcy9LrACgxt2bpO&currency=EUR"> </script>

<script>
        var campaign_id= '{{$campaign_id}}';
        var offer_id ='{{$offer_id}}';
        paypal.Buttons({

            createOrder: function(data, actions) {
            // This function sets up the details of the transaction, including the amount and line item details.
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '{{$payment}}'
                    }
                }]
            });
            },

            onApprove: function(data, actions) {
            // This function captures the funds from the transaction.
            return actions.order.capture().then(function(details) {
                // This function shows a transaction success message to your buyer.
                    sendAjax(details);
                    alert('Transaction completed by ' + details.payer.name.given_name);
                    document.getElementById('flash').style='display:block';
                });
            }
            }).render('#paypal-button-container');

            function sendAjax(details){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                    type:'POST',
                    url:'/brand/paypal/store',
                    data:{
                            details:details,
                            campaign_id:campaign_id,
                            offer_id:offer_id
                        },
                    success:function(data) {
                        
                    
                    }
            })
        }
  </script>

@endsection
