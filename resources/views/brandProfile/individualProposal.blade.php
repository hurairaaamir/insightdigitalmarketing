<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Brand</title>

    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" />

    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/adminlte/OverlayScrollbars.min.css') }}" rel="stylesheet" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/brandMenu.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/influencer.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/contact.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/individualProposal.css') }}" rel="stylesheet" />
</head>
<body>
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="page-content" style="min-height: 344px; margin-left: 0px;">
                <div class="mb-25">
                    You would like a consultation for a non-binding, individual offer of your campaign with influencers from 100,000 followers?<br>We will be happy to help you!<br>Please understand that we can only create individual offers <b> from a campaign volume of 10,000€</b>.</div>
                    <form id="individual-proposal-form" method="post" enctype="text/plain" class="form-horizontal">


                        <div class="form-group row">
                            <label for="title" class="col-sm-4 control-label form-iprequestedreach-label">
                                Desired reach            <div class="font-xs">
                                    e.g. number of followers            </div>
                            </label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <select id="iprequestedreach" name="iprequestedreach" class="form-control">
                        <option selected="selected" value="" disabled="">[ please select ]</option>
                        <option value="100.000-250.000">100.000 - 250.000</option>
                        <option value="250.000-500.000">250.000 - 500.000</option>
                        <option value="500.000-1.000.000">500.000 - 1.000.000</option>
                        <option value="1.000.000">over 1.000.000</option>
                    </select>                </div>
                                    <div class="col-sm-6"></div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="title" class="col-sm-4 control-label form-budget-label">
                                Budget (€)
                                <div class="font-xs">
                                    We can only send you individual offers starting at a Campaign volume of 10.000 €.            </div>
                            </label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="number" id="budget" name="budget" class="form-control" min="10000">                </div>
                                    <div class="col-sm-6"></div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="title" class="col-sm-4 control-label form-ipcampaigndetails-label">
                                Campaign Details            <div class="font-xs">
                                    What would you like to promote? Give us a first insight.            </div>
                            </label>
                            <div class="col-sm-8">
                                <textarea id="ipcampaigndetails" name="ipcampaigndetails" class="form-control m-input" rows="12"></textarea>        </div>
                        </div>


                        <div class="form-group row">
                            <label for="datepicker" class="col-sm-4 control-label form-ipcampaignduedate-label">
                                Duedate            <div class="font-xs">
                                    Do you need the content to be completed by a certain date?<br>*optional            </div>
                            </label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" id="ipcampaignduedate" name="ipcampaignduedate" class="form-control">                </div>
                                    <div class="col-sm-6"></div>
                                </div>
                            </div>
                        </div>


                        <div class="text-center">
                            <div class="mb-15">
                                <button id="submit-button" class="btn btn-primary ladda-button ladda" data-style="zoom-in">
                                    <span class="ladda-label">
                                    REQUEST NON-BINDING OFFER
                                    </span>
                                    <span class="ladda-spinner"></span>
                                </button>
                            </div>
                            <a href="#" class="cancel mb-20 font-dark">Cancel</a>
                        </div>
                    </form>

    <script>
        $(document).ready(function () {
            var submitLadda = Ladda.create(document.querySelector('#submit-button'));
            var minimumDate = new Date();
            var formGroups = $('.form-group');
            var form = $('#individual-proposal-form');

            minimumDate.setDate(minimumDate.getDate() + 7);

            var campaignDuedatePicker = new Pikaday({
                field: document.getElementById('ipcampaignduedate'),
                defaultDate: minimumDate,
                minDate: minimumDate,
                i18n: {
                    previousMonth: 'Previous Month',
                    nextMonth: 'Next Month',
                    months: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
                    weekdays: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
                    weekdaysShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
                }, format: 'DD.MM.YYYY'
            });


            $('#iprequestedreach option:first').prop('disabled', true);


            $('.cancel').on("click", function(e) {
                e.preventDefault();

                parent.$('.modal').modal('hide');
            });


            $('#submit-button').on('click', function(e) {
                form.submit();
            });

            form.on('submit', function(e) {
                e.preventDefault();

                var formData = objectifyForm($('#individual-proposal-form').serializeArray());

                formData.ipcampaignduedate = moment(campaignDuedatePicker.toString(), 'DD.MM.YYYY').format('YYYY-MM-DD');

                if (formData.ipcampaignduedate == 'Invalid date') {
                    formData.ipcampaignduedate = null;
                }

                submitLadda.start();

                $.ajax({
                    url: "/brand/individualProposalSend",
                    data: formData,
                    type: "POST",
                    dataType: 'json',
                    success: function (json) {
                        formGroups.removeClass('has-error');

                        if (json.result == 1) {
                            parent.individualProposalSentSuccessfully();
                            parent.$('.modal').modal('hide');
                        } else {
                            submitLadda.stop();

                            formGroups.each(function() {
                                if ($(this).hasClass('has-error')) {
                                    $(this).removeClass('has-class');
                                }
                            });

                            json.data.errors.forEach(function(error) {
                                var field = error['field'];
                                var message = error['message'];
                                var elementGroup = $('.form-' + field + '-label').parents('.form-group');

                                notification.error(message);

                                if (!elementGroup.hasClass('has-error')) {
                                    elementGroup.addClass('has-error');
                                }
                            });
                        }
                    },
                    error: function (xhr, status, error) {
                        submitLadda.stop();
                        notification.error('An error occurred. Please log out and in again or contact kontakt@reachhero.de');
                    },
                    async: true
                });
            });
        });
    </script>            		</div>
        </div>
    </div>
</body>
</html>
