@extends('layouts.front')

@section('content')
<div class="find">
    <div class="width-4">
        <div class="container">
            <div class="row py-5 justify-content-center">
                <img class="logo-pages" src="/FrontEndImages/logo-black.png" alt="">
            </div>
            <div class="register-text">
                <h4 class="center"><strong>Please log in
                </strong></h4>

            </div>
        </div>Regi




        <div class="container">
            <div class="row ">
                <form class="m-login__form m-form" form action="{{ route('login') }}" method="POST" >
                    {{ csrf_field() }}
                    <div class="form-group m-form__group">
                        <input class="form-control m-input @error('email') is-invalid @enderror"" type="text" placeholder="Email address" name="email" autocomplete="off">
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group m-form__group">
                        <input class="form-control m-input m-login__form-input--last kxinveovjmned @error('password') is-invalid @enderror" type="password" placeholder="Password" name="password" autocomplete="off" style="padding-right: 34px;">
                        @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                        @enderror
                    </div>
                    <div class="row m-login__form-sub px-4">
                        <div class="col m--align-left m-login__form-left">
                            <label class="m-checkbox  m-checkbox--focus">
                                <input  type="checkbox" name="remember">
                                Stay logged in                                        <span></span>
                            </label>
                        </div>
                        <div class="col m--align-right m-login__form-right">
                            <a href="{{ route('password.request') }}" id="m_login_forget_password" class="m-link">
                                Forgot Password?                                    </a>
                        </div>
                    </div>
                    <div class="m-login__form-action d-flex justify-content-center mt-4">
                        <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                            LOG IN                                </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="row justify-content-center lang">
            <a href="#" data-lang="de">DE</a>
            <span></span>
            <a href="#" data-lang="en">EN</a>
            <span></span>
            <a href="#" data-lang="fr">FR</a>
        </div>
    </div>
</div>

@endsection
