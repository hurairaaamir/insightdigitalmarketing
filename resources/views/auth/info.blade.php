@extends('layouts.front')


@section('content')
<div  class="find">
    <div class="width-4">
        <div class="container">
            <div class="row py-5 justify-content-center">
                <img class="logo-pages" src="/FrontEndImages/logo-black.png" alt="">
            </div>
            <div class="register-text">
                <h4 class="center">Welcome</h4>
                <h5 class="center">Already <strong>more than 80000 Influencer</strong> registered on ReachHero.</h5>
                <br><br>
                <h5 class="center">And what about <strong>you</strong>?</h5>
                <h5 class="center">Enter your details now and create <strong>for free</strong> your ReachHero account!</h5>
                <br><br>
                <h5 class="center"><strong>You need at least 2,500 followers on your channel to participate in our marketplace.</strong></h5>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <form id="influencerForm2" class="m-login__form m-form influencerForm" method="POST" action="{{ url('/influencer/profile') }}">
                    @csrf                
                    <div class="form-group m-form__group">
                        <select class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" id="gender" name="gender" data-label="Gender" style="padding: 0 0 0 12px;height: 54px;">
                            <option value="">-- Please select your gender --</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="WithOut Gender">WithOut Gender</option>
                        </select>
                        @if($errors->has('gender'))
                            <p class="red">{{$errors->get('gender')[0]}}</p>
                        @endif 
                    </div>
                    <div class="form-group m-form__group">
                        <select class="form-control{{ $errors->has('channel_type') ? ' is-invalid' : '' }}" id="channelCategory" name="channel_type" data-label="Select channel category" style="padding: 0 0 0 12px;height: 54px;">
                            <option value="">-- please select you channel Category --</option>
                            @foreach($channelType as $category)
                                <option value="{{$category->name}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('channel_type'))
                            <p class="red">{{$errors->get('channel_type')[0]}}</p>
                        @endif
                    </div>
                    <div class="form-group m-form__group">
                        <input class="form-control m-input{{ $errors->has('city') ? ' is-invalid' : '' }}" type="text" placeholder="What city do you live in?" data-label="What city do you live in?" name="city">
                        @if($errors->has('city'))
                            <p class="red">{{$errors->get('city')[0]}}</p>
                        @endif
                        <div><small><i class="fa fa-info-circle"></i> Your city is e.g. important for sponsors, if they want to invite you to an event or send you a product.</small></div>
                    </div>
                    
                    <div class="form-group m-form__group">
                        <select class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" id="country" name="country" data-label="Land" style="padding: 0 0 0 12px;height: 54px;">
                            <option value="">-- please select your Country --</option>
                            @foreach($countries as $country)
                                <option value="{{$country->name}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('country'))
                            <p class="red">{{$errors->get('country')[0]}}</p>
                        @endif
                    </div>
                    <div class="m-login__form-action">
                        <button  class="d-flex justify-content-center btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                            Complete registration
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center lang">
            <a href="#" data-lang="de">DE</a>
            <span></span>
            <a href="#" data-lang="en">EN</a>
            <span></span>
            <a href="#" data-lang="fr">FR</a>
        </div>
    </div>
</div>

@endsection
@section('scripts')
 

@endsection





















































                    {{-- <form method="POST" action="{{ url('/influencer/profile') }}">
                        @csrf --}}
                        {{-- <select name="gender" class="form-control m-3">
                            <option value="">please select your Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="WithOut Gender">WithOut Gender</option>
                        </select>
                        @if($errors->has('gender'))
                            <p class="red">{{$errors->get('gender')[0]}}</p>
                        @endif --}}

                        {{-- <label>Country Name</label>
                        <select name="country" class="form-control m-3"  style="width: 75%">
                            <option value="">please select your Country</option>
                            @foreach($countries as $country)
                                <option value="{{$country->name}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('country'))
                            <p class="red">{{$errors->get('country')[0]}}</p>
                        @endif --}}
                    {{-- <label>Channel Category</label>
                        <select class="form-control m-3" name="channel_type">
                            <option value="">please select you channel Category</option>
                            @foreach($channelType as $category)
                                <option value="{{$category->name}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('channel_type'))
                            <p class="red">{{$errors->get('channel_type')[0]}}</p>
                        @endif --}}
                        {{-- <label>City name</label> --}}

                        {{-- <input type="text" class="form-control m-3" placeholder="City Name" name="city">
                        @if($errors->has('city'))
                            <p class="red">{{$errors->get('city')[0]}}</p>
                        @endif --}}
                        {{-- <button class="btn btn-info">submit</button>
                    </form> --}}