@extends('layouts.front')

@section('content')
<div class="find">
    <div class="width-4">
        <div class="container">
            <div class="row py-5 justify-content-center">
                <img class="logo-pages" src="/FrontEndImages/logo-black.png" alt="">
            </div>
            <div class="register-text">
                <h4 class="center" style="font-size:"><b>Register for free as a <b>company</b></b></h4>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <form id="brandForm" class="m-login__form m-form" method="POST" action="{{ url('/brand/register') }}">
                    @csrf
                    
                    <div class="form-group m-form__group">
                        <input class="form-control m-input {{ $errors->has('name') ? ' is-invalid' : '' }}" type="text" placeholder="Company name" data-label="name" name="name" value="{{ old('name') }}" id="name">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <input type="hidden" name="role" value="brand">

                    <div class="form-group m-form__group">
                        <input class="form-control m-input {{ $errors->has('email') ? ' is-invalid' : '' }}" type="text" placeholder="Email address" data-label="Email address" name="email" value="{{ old('email') }}" autocomplete="off">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group m-form__group">
                        <input class="form-control m-input lgbijrsrbxgvj{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" placeholder="Password" data-label="Password" name="password" autocomplete="off" style="padding-right: 34px;">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group m-form__group">
                        <input  class="form-control m-input lgbijrsrbxgvj" id="password-confirm" type="password" placeholder="Password" data-label="Password" name="password_confirmation" autocomplete="off" style="padding-right: 34px;">
                    </div>

                    <div class="mt-3 form-group m-form__group m-login__form-sub">
                        <div class="col m--align-left">
                            <label class="m-checkbox m-checkbox--focus">
                                <input type="checkbox" name="agb">
                                I accept the                                        <a href="/terms" target="_blank" class="m-link m-link--focus">
                                <b>Conditions</b>
                            </a>
                                and the                                        <a href="/privacy" target="_blank" class="m-link m-link--focus">
                                <b>privacy</b>
                            </a>
                                .
                                <span></span>
                            </label>
                            <span class="m-form__help"></span>
                        </div>
                    </div>
                    <div class=" form-group m-form__group m-login__form-sub">
                        <div class="col m--align-left">
                            <label class="m-checkbox m-checkbox--focus">
                                <input type="checkbox" name="newsletter" value="1">
                                <b>Subscribe to newsletter</b>                                        <div style="font-size: 0.8em">
                                I agree that ReachHero GmbH will inform me by e-mail about new offers and campaigns as well as interesting news about ReachHero. My data will be used exclusively for this purpose. The data will only be passed on to third parties if this is necessary for the fulfilment of this purpose. I can revoke my consent at any time by e-mail to kontakt@reachhero.de or by using the unsubscribe link contained in the e-mails. Further information can be found in the data protection information.                                        </div>
                                <span></span>
                            </label>
                            <span class="m-form__help"></span>
                        </div>
                    </div>
                    <div class="m-login__form-action">
                        <button id="m_login_brand_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn" type="submit">
                            Register for free                                </button>
                        &nbsp;&nbsp;
                        <button class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom m_login_cancel m-login__btn">
                            Cancel                                </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="row justify-content-center lang">
            <a href="#" data-lang="de">DE</a>
            <span></span>
            <a href="#" data-lang="en">EN</a>
            <span></span>
            <a href="#" data-lang="fr">FR</a>
        </div>
    </div>
</div>

@endsection