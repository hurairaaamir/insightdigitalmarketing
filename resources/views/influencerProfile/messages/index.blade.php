@extends('layouts.influencer')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/messages.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/subscription.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/brandSettingsMenu.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/settings/emailNotifications.css') }}" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <h1 class="page-title">Messages</h1>
            <div class="inbox">
                <div class="row">
                    <div class="col-md-2">

                        <div class="inbox-sidebar">
                            <div class="btn-group width-100-percent">

                                    <ul class="inbox-nav" style="margin: 0;">
                                        <li class="active">
                                            <a href="/messages" data-type="inbox" data-title="Inbox"> All                                            </a>
                                        </li>
                                    </ul>
                                    </div>

                            <hr>

                            <p>Status <i class="fa fa-question-circle tooltips" data-original-title="Status of placement and giveaway proposals" aria-hidden="true"></i></p>

                            <ul class="inbox-nav" style="margin: 0;">
                                <li>
                                    <a href="/messages?status=open"><i class="fa fa-circle font-red"></i> Open</a>
                                </li>
                                <li>
                                    <a href="/messages?status=accepted"><i class="fa fa-circle font-blue-sharp"></i> Accepted</a>
                                </li>
                                <li>
                                    <a href="/messages?status=approval"><i class="fa fa-circle font-yellow-lemon"></i> Content Approval</a>
                                </li>
                                <li>
                                    <a href="/messages?status=completed"><i class="fa fa-circle font-green"></i> Completed</a>
                                </li>
                            </ul>

                        </div>

                        <style>
                            .select2-dropdown {
                                width: 400px !important;
                            }
                        </style>

                        <script>
                            $(document).ready(function() {
                                $('#sidebar #sidebar-messages').addClass("active");


                                function formatState (state) {
                                    if (!state.id) {
                                        return state.text;
                                    }

                                    let unreadCountHtml = '';

                                    if (state.id === 'all') {
                                        unreadCountHtml = '<span class="badge badge-danger margin-right-4 pull-right">0</span>';
                                    } else if (state.unreadCount > 0) {
                                        unreadCountHtml = '<span class="badge badge-danger margin-right-4 pull-right">' + state.unreadCount + '</span>';
                                    }

                                    let $state = $(
                                        unreadCountHtml + '<span>' + state.text + '</span>'
                                    );

                                    return $state;
                                }


                                let $campaignsDropdown = $('#select-campaign');

                                $campaignsDropdown.select2({
                                    placeholder: "Campaigns",
                                    templateResult: formatState,
                                    width: '100%',
                                    language: {
                                        noResults: function() {
                                            return `No campaigns found`;
                                        },
                                        searching: function() {
                                            return `Searching...`;
                                        }
                                    },
                                    ajax: {
                                        url: "/messages/list-campaigns",
                                        dataType: 'json',
                                        delay: 250,
                                        data: function(params) {
                                            var queryParameters = {
                                                term: params.term
                                            };

                                            if ($("#hashtag-select").val()) {
                                                queryParameters.selected = $campaignsDropdown.val().toString()
                                            }
                                            return queryParameters;
                                        },
                                        processResults: function(data, params) {
                                            return {
                                                results: data
                                            };
                                        },
                                        cache: true
                                    },
                                    escapeMarkup: function(markup) {
                                        return markup;
                                    }, // let our custom formatter work
                                    minimumInputLength: 0
                                });


                                $campaignsDropdown.on('select2:select', function(e) {
                                    let internId = e.params.data.id;

                                    if (internId === 'all') {
                                        window.top.location.href = "https://www.reachhero.de/messages";
                                    } else {
                                        window.top.location.href = "https://www.reachhero.de/messages?campaign=" + internId;
                                    }
                                });
                            });
                        </script>
                    </div>
                    <div class="col-md-10">
                        <div class="inbox-body">
                            <div class="inbox-header">
                                                        <h1 class="pull-left">Alle</h1>
                                                </div>
                            <div class="inbox-content">
                                                        <p>No messages available</p>
                                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
