@extends('layouts.influencer')

@section('styles')
<style>
    .face-container{
        display:flex;
        flex-wrap:wrap;
    }
    .face-card{
        flex:0 1 200px;
        height: 200px;
        border:3px solid #ACB1B4;
        margin:10px;
    }
    .face-head{
        background-color:#3B5997;
        color:white;
        height: 20%;
        width:100;
        padding:6px;
    }
    .face-head h3{
        font-size:20px;
    }
    .face-body{
        height: 80%;
        display:flex;
        justify-content: center;
        align-items: center;
        flex-direction:column;
    }
    .face-link{
        text-decoration:none;
        color:white;
        padding:5px 25px;
        margin-top:20px;
        background-color:rgb(76, 117, 201);
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 4px;
    }
    .face-link:hover{
        color:white;
    }
</style>
@endsection

@section('content')
@if(isset($facebook))
<div class="face-container">
    @foreach($facebook->pages as $page)
        <div class="face-card">
            <div class="face-head">
                <h3>Facebook Page</h3>
            </div>
            <div class="face-body">
                <h4>{{$page->name}}</h4>
                <a href="{{$page->permalink}}" class="face-link" target="_blank">Visit</a>
            </div>
        </div>
        @endforeach
    </div>
@endif
@endsection


@section('scripts')
@endsection