@extends('layouts.influencer')

@section('styles')
<link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />    
<style>
    .center_out{
        display:flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }
    .promo-socials{
        display: flex;
        flex-direction:row;
        justify-content: center;
        align-items: center;
        margin:5px 0px;
    }
    .lower-gray{
        background-color:#F5F5F5;
        padding:10px;
        margin:15px 0px;
        border-radius: 3%;
    }
    .lower-gray h5{
        font-weight: 550;
    }
    .coss-model-size{
        min-height: 300px;
    }
    .promoCard{
        display:flex;
        flex-direction:row;
        flex-wrap:wrap;
    }
    .promoCard .child1{
        flex:1 1 450px;
        display:flex;
        justify-content: flex-start;
        align-items: flex-start;
        flex-direction: column;
        padding:10px;
    }
    .promoCard .child1 h6{
        font-weight: 550;
        margin:10px 0px;
    }

    .promoCard .child2{
        flex:1 1 250px;
        display:flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }
    .promoCard .child2 .promo-img{
        width:250px;
        height: auto;
        
    }
    .promoCard .child2 .promo-img img{
        width:100%;
        height:100%;
        border-radius:3%;
    }
    .promoCard .child2 h5{
        font-weight: 550;
        margin:15px 0px;
    }
    .promoCard .child2 a{
        margin:5px 0px;
    }
    
    .promo-left{
        display:flex;
        justify-content: flex-start;
        align-items: flex-start;
        flex-direction: row;
        margin-bottom:20px;
    }
    .gray-head{
        display:flex;
        justify-content: center;
        align-items: center;
        text-align:center;
        background-color:#F5F5F5;
        padding:10px 0px;
        margin:10px 3px;
    }
    .form-parent{
        display:flex;
        flex-direction: row;
        flex-wrap:wrap;
        margin:20px 10px;
    }
    .form-parent .child1{
        flex:1 1 200px;
        
    }
    
    .form-parent .child2{
        flex:1 1 400px;
    }

</style>
@endsection


@section('content')
<div class="container">
    <h1 class="campaign-head">Placement Campaigns</h1>
    <div class="campaigns-tabs">
        <a href="{{url('/influencer/cross/index')}}">
            <span class="active">Cross-Promo</span>
        </a>
        <a href="{{url('/influencer/cross/create')}}">
            <span>My Cross-Promo</span>
        </a>
    </div>

    <div class="campaign-select">
        <select class="form-control mt-2" onchange="getPromosocials()" id="socialSelect">
            <option value="all">
                All
            </option>                
            @foreach($socials as $social)
                <option value="{{$social->name}}">
                        {{$social->name}}
                </option>
            @endforeach
        </select>
    </div>
    <div class="flex-container" id="promo-data">

        @include('cardData.CrossPromotion')
    
    </div>        
    <div class="center_out mt-3 mb-5">
        <img src="/images/gifs/campaignsLoader.svg" id="loadmore-loader" class="loaderhider">
    </div>

</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="coss-model-size">                
                    <div class="btn-center" id='crossPromo-data' style="height:500px;overflow-y:scroll;">             
                    </div>
                </div>
                <div class="modal-footer">
                 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example2-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request cooperation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="coss-model-size" style="height:500px;overflow-y:scroll;"> 
                    <h4>Cross-promo collaboration</h4>               
                    <div class="gray-head">             
                        <span>Please describe in detail how you envisage the collaboration with "<b id="username-model"></b>".</span>
                    </div>
                    <div class="form-parent">
                        <div class="child1">
                            <P>How do you envisage the collaboration?</P>
                        </div>
                        <div class="child2">
                            <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>                        
                    </div>
                    <div class="form-parent">
                        <div class="child1">
                            <P>When would you like to start?</P>
                        </div>
                        <div class="child2">
                            <input type="text" class="form-control" >
                        </div>                        
                    </div>
                </div>
                <div class="center_out">
                    <button class="btn btn-danger btn-sm m-2">Send request</button>
                    <button class="btn btn-link btn-sm m-2" data-dismiss="modal">chancel</button>

                </div>
                <div class="modal-footer">
                 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
<script src="{{asset('/js/ajax/cross_promotion_index.js')}}"></script> 
@endsection