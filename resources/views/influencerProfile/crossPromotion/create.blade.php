@extends('layouts.influencer')
@section('styles')
<link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />    
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .flex-straight{
        display:flex;
        justify-content:flex-start;
        align-items: flex-start;
        width:100%;
    }
    .flex-straight h6{
        color:#2ab4c0;
        font-weight: 600;
    }
    .float-right{
        position: absolute;
        right: 10px;
    }
    .model-size{
        height: 425px;
        overflow-y:scroll;
    }
    .gray-card{
        background-color:#F5F5F5;
        display:flex;
        justify-content: flex-start;
        align-items: flex-start;
        flex-direction: column;
        padding:15px;
        margin:20px;
    }
    @media(max-width:780px){
        .gray-card{ 
            margin:4px;
        }

    }
    .div-lists{
        display:flex;
        flex-direction:row;
        margin:5px 10px;
        flex-wrap:wrap;
    }
    .div-lists .child{
        flex:1 1 315px;
    }
    .model-section1{
        display:flex;
        flex-direction: row;
        flex-wrap:wrap;
        margin:10px;
    }
    .model-section1 .child1{
        flex:1 1 170px;
        padding-right:10px;
    }
    .model-section1 .child2{
        flex:1 1 500px;
        display:flex;
        flex-direction: row;
        flex-wrap:wrap;
        justify-content: center;
        align-items: center;
    }
    .model-section1 .child2 .channel{
        flex:0 1 170px;
        height: 60px;
        display:flex;
        justify-content: space-around;
        align-items: center;
        flex-direction: row;
        border:solid 1px rgb(17, 17, 17);
        margin:10px 0px; 
        margin-right: 70px;
        position: relative;
        z-index: 10;
    }
    @media(max-width:780px){
        .model-section1 .child2 .channel{
            margin-right:10px;
        }

    }
    .model-section1 .child2 .channel:hover{
        cursor:pointer;
    }
    .display_social{
        opacity:0!important;
    }
    .model-section1 .child2 .channel .icon{
        width:40px;
        height:40px;
        display:flex;
        justify-content: center;
        align-items: center;
        background-color:rgb(14, 13, 13);
        color:white;
        border-radius:100%;
        font-size:20px;
    }
    .model-section1 .child2 .channel span{
        /* display: */
        /* transform:translateX(); */
        font-size:14px;
        width:20px;
        height:20px;
        border-radius:100%;
        display:flex;
        justify-content: center;
        align-items: center;
        background-color:rgb(3, 184, 3);
        color:white;
        transform:translate(-110px,7px);
    }
    .model-section1 .child2 .channel .div h6{
        font-weight:520;
    }
    .model-section2{
        display: flex;
        flex-direction: column;
        padding:5px 30px;
    }
    .model-section2 .form-group span{
        margin:5px 0px;
    } 
    .model-section2 .form-group input{
        margin:5px 0px;
    }

    .btn-center{
        display:flex;
        justify-content: center;
        align-items: center;
        flex-direction:column;
        margin:70px 0px;
    }
    .btn-center button{
        margin:5px 0px;
    }
</style>
@endsection


@section('content')
<div id='page-wrap'>
    <div class="container">
        <h1 class="campaign-head">Placement Campaigns</h1>
        <div class="campaigns-tabs">
            <a href="{{url('/influencer/cross/index')}}">
                <span >Cross-Promo</span>
            </a>
            <a href="{{url('/influencer/cross/create')}}">
                <span class="active">My Cross-Promo</span>
            </a>
        </div>
    </div>


<div class="card">
    <div class="card-header flex-straight">
        <h6>MY CROSS-PROMO</h6>
        <button type="button" data-toggle="modal" data-target=".bd-example-modal-lg" class="btn btn-outline-danger btn-sm float-right">
            Start Cross-Promo
        </button>
    </div>
    <div class="card-body">
        <p>No cross-promos created yet.</p>    
        <button class="btn btn-danger btn-sm">Create Cross-Promo now</button>
    </div>
</div>


<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create cross-promo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <div class="model-size">                
                    <div class="gray-card">
                        <p>Please describe in detail what kind of crosspromo you are looking for. That will help you to find the right partner for your crosspromo.</p>
                        <div class="div-lists">
                            <div class="child">
                                <h6>Do's</h6>
                                <ul>
                                    <li>Choose an appealing title so other Influencer are encouraged to contact you</li>
                                    <li>Set up text that us precise and clear for others</li>
                                    <li>Shoutout for shoutout</li>
                                </ul>
                            </div>
                            <div class="child">
                                <h6>Dont's</h6>
                                <ul>
                                    <li>Search for sponsors, brands, giveaways/product samples, paid shoutouts etc.</li>
                                    <li>Selling shoutouts</li>
                                    <li>Selling social media channels</li>
                                </ul>
                            </div>
                        </div>
                        <p><i>If you do not follow these instructions, we will delete your cross promo campaign</i></p>
                    </div>
                    <div class="model-section1">
                        <div class="child1">
                            <p>What channels are you looking for?</p>
                        </div>
                        <div class="child2">
                            <div class="channel" onclick="check_channel('youtube')" id="">
                                <div class="icon"><i class="fab fa-youtube"></i></div>
                                <div class="div"><h6>YOUTUBE</h6></div>
                                <span id="youtube_id" class="display_social"><i class="fas fa-check"></i></span>
                            </div>
                            <div class="channel" onclick="check_channel('instagram')">
                                <div class="icon"><i class="fab fa-instagram"></i></div>
                                <div class="div"><h6>INSTAGRAM</h6></div>
                                <span id="instagram_id" class="display_social"><i class="fas fa-check"></i></span>
                            </div>
                            <div class="channel" onclick="check_channel('facebook')">
                                <div class="icon"><i class="fab fa-facebook-f"></i></div>
                                <div class="div"><h6>FACEBOOK</h6></div>
                                <span id="facebook_id" class="display_social"><i class="fas fa-check"></i></span>
                            </div>
                            <div class="channel" onclick="check_channel('twitter')">
                                <div class="icon"><i class="fab fa-twitter"></i></div>
                                <div class="div"><h6>TWITTER</h6></div>
                                <span id="twitter_id" class="display_social"><i class="fas fa-check"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="model-section2">
                        <form id="cross_form" method="POST" action="{{url('/influencer/cross/store')}}">
                            <input type="hidden" id="social-feild" value="" name="social">
                            @csrf
                            <div class="form-group">
                                <span>Title</span>
                                <input type="text" class="form-control" placeholder="e.g." name="title" required>
                            </div>
                            <div class="form-group">
                                <span>What are you looking for?</span>
                                <textarea class="form-control" name="details" required></textarea>
                            </div>
                            <div class="form-group">
                                <span>When would you like to start?</span>
                                <input type="text" class="form-control" name="date_range" required>
                            </div>
                            <div class="form-group">
                                <div>Subscribers?<div>
                                <div>If you wish to, you can enter the minimum number of subscribers that the cross-promo channel should have here.</div>
                                <input type="text" class="form-control" placeholder="Subscribers" name="subscribers" required>
                            </div>
                            <button type="submit" id="cross_id" style="display:none"></button>
                        </form>
                    </div>

                    <div class="btn-center">
                        <div id="error_id"></div>
                        <button type="button" class="btn btn-danger btn-sm" onclick="crosspromo_submit()">Create cross-promo</button>    
                        <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                <div class="modal-footer">
                 
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{asset('/js/ajax/cross_promotion.js')}}"></script> 
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $('input[name="date_range"]').daterangepicker(

    );
</script>

@endsection