@extends('layouts.influencer')

@section('styles')
    <link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <h3>Settings</h3>
    <div class="row">
        <div class="col-md-4">
            @include('influencerProfile.settings.settingMenu')
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>PROFILE</h5>
                            <div class="address-section">
                                <h5>Fill out your profile</h5>
                                <p>When you fill out your profile you will be more interesting for sponsors and we can mediate your profile better!</p>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="/influencer/profile/settings/profile" method="POST">
                            @csrf
                            <table class="general-table">
                                <colgroup>
                                    <col span="1" style="width:25%;padding:20px">
                                    <col style="margin:20px;">
                                </colgroup>
                                <tr>
                                <th colspan="2"><h5>Billing Address ?</h5></th>
                                <tr>
                                    <td class="pro-head"><div>Multi-Channel Network</div></td>    
                                    <td class="">
                                        <div class="channel-radio">
                                            <span>
                                                <input type="radio" value="false" {{$profile->multichannel=='false' ? 'checked':'' }} name="multichannel"> I am not a member of a multi-channel network (MCN)
                                            </span>
                                            <span >
                                                <input type="radio" value="true" {{$profile->multichannel == 'true' ? 'checked': ''}} name="multichannel">  I am a member of a multi-channel network (MCN)
                                            </span>
                                        <div>
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td class="pro-head"><div class="channel-radio">Categories</div></td>    
                                    <td>
                                        
                                        @if(count($profile->channeltypes)==0)
                                            @foreach($types as $type)
                                                <div class="pro">
                                                    <input type="checkbox" value="{{$type->id}}" name="channel_types[]">  {{$type->name}}
                                                </div>
                                            @endforeach
                                        @else
                                            @foreach($types as $type)

                                                <div class="pro">
                                                    <input type="checkbox" value="{{$type->id}}" name="channel_types[]" {{in_array($type->name,$checked)?'checked':''}}>  {{$type->name}}
                                                </div>
                                            @endforeach
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                            </table>
                        </form>
                            <div class="about-channel">
                                <div class="pro-head mt-5">About Channel</div>    
                                <div class="textarea">
                                    <div>
                                        <textarea row="6" class="form-control" name="about_channel">{{$profile->about_channel}}</textarea>
                                        <button class="mt-5 btn btn-info">Save Changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body img-body">
                            <div class="touch-me">
                                <h5>Touch the Image below to edit your profile</h5>
                            </div>
                            <div class="profile-container">
                                <img src="{{$profile->image}}" id="profile-image" onclick="profileClicked()">
                            </div>
                            <div class="upload-preload">
                                <img src="{{asset('/images/gifs/uploadPreload.gif')}}" style="z-index:5" class="loaderhider" id="profile-loader">
                            </div>
                        </div>
                        <div class="upload-btn">
                            <form method="post" id='profile-form' enctype="multipart/form-data">
                                <input type="file" name="image" style="display:none" id="profile-feild" onchange="previewProfile(event)">
                                <button type="submit" class="btn btn-info"><i class="fas fa-cloud-upload-alt"></i> upload</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>


@endsection

@section('scripts')
<script>
    function profileClicked(){
        $("#profile-feild").click();
    }

    function previewProfile(event){
        console.log(event);
        var reader = new FileReader();
        reader.onload = function(){
        
        document.getElementById('profile-image').src=reader.result;

        }
        reader.readAsDataURL(event.target.files[0]);
    }

    $('#profile-form').on('submit',function(event){
    event.preventDefault();
    document.querySelector('#profile-loader').classList.remove('loaderhider');
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //alert(this);
    $.ajax({
        type: "POST",
        url: "{{url('/uploadImage')}}",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        datatype: "json",
        // beforeSend: function()
        // {
        // }

        success: function(data){
            document.getElementById('profile-image').src=data.data;
            document.getElementById('nav-profile').src=data.data;

            document.querySelector('#profile-loader').classList.add('loaderhider');
        },
        error:function(){
            document.querySelector('#profile-loader').classList.add('loaderhider');
        }
    });
});
</script>

@endsection