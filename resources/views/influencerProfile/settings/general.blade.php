@extends('layouts.influencer')

@section('styles')
    <link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <h3>Settings</h3>
    <div class="row">
        <div class="col-md-4">
            @include('influencerProfile.settings.settingMenu')
        </div>
        <div class="col-md-8">
            <div class="card">                
                <div class="card-body">
                    <form action="/general_store" method="POST">
                    @csrf
                    
                            <div class="setting-submit"><h6>THE FOLLOWING INFORMATION IS NOT PUBLIC AND INVISIBLE FOR EVERYBODY</h6></div>
                           
                            <div class="gender-radio">
                                <div class="child1"><h5>Gender</h5></div>
                                <div class="child2">
                                    <div>
                                        <input type="radio" class="" name="gender" value="Male" {{$profile->gender=='Male'?'checked':''}}><span>Male</span>
                                    </div>
                                    <div>
                                        <input type="radio" class="" name="gender" value="Female" {{$profile->gender=='Female'?'checked':''}}><span>Female</span>
                                    </div>
                                    <div>
                                        <input type="radio" class="" name="gender" value="Without Gender" {{$profile->gender=='Without Gender'?'checked':''}}><span>Without Gender</span>
                                    </div>
                                </div>
                            </div>
                        <tr>
                        <div class="gender-radio">
                            <div class="child1"><h5>Email address</h5></div>
                            <div class="child3">
                                <span>{{$profile->user->email}}</span>
                                <span >
                                   Please contact influencer@reachhero.de to change the email address.
                                </span>
                            </div>
                        </div>
                        <div class="gender-radio">
                            <div class="child1"><h5>Telephone</h5></div>
                            
                            <div class="child3">
                                <input type="number" class="form-control" value="{{$profile->telephone}}" name="telephone">
                            </div>
                        </div>
                        <div class="gender-radio">
                            <div class="child1"><h5>Birthday</h5></div>

                            <div class="child2">
                                @include('influencerProfile.settings.selectTag')
                            </div>
                        </div>
                    
                    <div class="setting-submit">
                        <button class="btn btn-info">Save Changes</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>  
</div>
@php
    $pro=explode(':',$profile->birthday);
    $day=$pro[0];
    $month=$pro[1];
    $year=$pro[2];

@endphp

@endsection


@section('scripts')
<script>
    $(document).ready(function(){
        var set_val = {!! json_encode($day) !!};
        if(!set_val){
            return
        }
        let day=document.getElementById('day');
        let month=document.getElementById('month');
        let year=document.getElementById('year');


        for(var i=0;i<day.options.length;i++){

        var set_val = {!! json_encode($day) !!};

            if(day.options[i].value==set_val){
                day.options[i].selected = true;
            }
        }

        for(var i=0;i<month.options.length;i++){

            var set_val = {!! json_encode($month) !!};

            if(month.options[i].value==set_val){
                month.options[i].selected = true;
            }
        }

        for(var i=0;i<year.options.length;i++){

            var set_val = {!! json_encode($year) !!};

            if(year.options[i].value==set_val){
                year.options[i].selected = true;
            }
        }
    })
</script>
@endsection