<div class="card">                
    <div class="setting-container">
        <span class="{{request()->is('influencer/profile/settings/general')? 'active':''}}">
            <a href="{{url('/influencer/profile/settings/general')}}"><i class="fas fa-globe-europe"></i> General Information</a>
        </span>
        <span class="{{request()->is('influencer/profile/settings/addresses')? 'active':''}}">
            <a href="{{url('/influencer/profile/settings/addresses')}}"><i class="fas fa-id-badge"></i> Addresses</a>
        </span>
        <span class="{{request()->is('influencer/profile/settings/profile')? 'active':''}}">
            <a href="{{url('/influencer/profile/settings/profile')}}"><i class="fas fa-user-alt"></i> Profile</a>
        </span>
        <span class="{{request()->is('influencer/profile/settings/payout_history')? 'active':''}}">
            <a href="{{url('/influencer/profile/settings/payout_history')}}"><i class="far fa-money-bill-alt"></i> Notifications</a>
        </span>
    </div>
    <div class="pr-3 pl-3"><hr></div>
    <div class="setting-container">
        <span class="{{request()->is('influencer/profile/settings/channels')? 'active':''}}">
            <a href="{{url('/influencer/profile/settings/channels')}}"><i class="fab fa-youtube"></i> Social Channels</a>
        </span>
        <span class="{{request()->is('influencer/profile/settings/interests')? 'active':''}}">
            <a href="{{url('/influencer/profile/settings/interests')}}"><i class="far fa-address-card"></i> Interests</a>
        </span>
        <span class="{{request()->is('influencer/profile/settings/payouts')? 'active':''}}">
            <a href="{{url('/influencer/profile/settings/payouts')}}"><i class="fas fa-euro-sign"></i> Payouts Information</a>
        </span>
        <span class="{{request()->is('influencer/profile/settings/password')? 'active':''}}">
            <a href="{{url('/influencer/profile/settings/password')}}"><i class="fas fa-key"></i> password</a>
        </span>
        <span class="{{request()->is('influencer/profile/settings/terminate_account')? 'active':''}}">
            <a href="{{url('/influencer/profile/settings/terminate_account')}}"><i class="fas fa-times"></i> Terminate Account</a>
        </span>
    </div>
</div>