@extends('layouts.influencer')

@section('styles')
    <link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <h3>Settings</h3>
    <div class="row">
        <div class="col-md-4">
            @include('influencerProfile.settings.settingMenu')
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5>ADDRESSES</h5>
                    <div class="address-section">
                        <h5>Fill in your addresses here</h5>
                        <p>The addresses, you enter here, will not be published and are not visible to anyone!</p>
                    </div>
                </div>
                <div class="card-body">
                    <form action="/influencer/profile/settings/addresses" method="POST">
                    @csrf
                    
                        <div ><h5>Billing Address ?</h5></div>
                        <div class="gender-radio">
                            <div class="child1">Firstname:</div>    
                            <div class="child3">
                                <input type="text" class="form-control" value="{{$profile->first_name?$profile->first_name:$profile->user->name}}" name="first_name">
                            </div>
                        </div>
                        <div class="gender-radio">
                            <div class="child1">Lastname:</div>    
                            <div class="child3">
                                <input type="text" class="form-control" value="{{$profile->last_name}}" name="last_name">
                            </div>
                        </div>
                        <div class="gender-radio">
                            <div class="child1">Street and house number:</div>    
                            <div class="child3">
                                <input type="text" class="form-control" value="{{$profile->street_house}}" name="street_house">
                            </div>
                        </div>
                        <div class="gender-radio">
                            <div class="child1">Zip code:</div>    
                            <div class="child3">
                                <input type="text" class="form-control" value="{{$profile->zip}}" name="zip">
                            </div>
                        </div>
                        <div class="gender-radio">
                            <div class="child1">City:</div>    
                            <div class="child3">
                                <input type="text" class="form-control" value="{{$profile->city}}" name="city">
                            </div>
                        </div>
                        <div class="gender-radio">
                            <div class="child1">Country:</div>    
                            <div class="child3">
                                <select class="form-control" name="country">
                                    @foreach($countries as $country)
                                        <option value="{{$country->name}}" {{$country->name==$profile->country? 'selected': ''}}>{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    <div class="setting-submit">
                        <button class="btn btn-info">Save Changes</button>
                    <div>
                </form>
                </div>
            </div>
        </div>
    </div>  
</div>


@endsection

@section('scripts')

@endsection