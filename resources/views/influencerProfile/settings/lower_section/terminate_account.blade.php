@extends('layouts.influencer')

@section('styles')
    <link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <h3>Settings</h3>
    <div class="row">
        <div class="col-md-4">
            @include('influencerProfile.settings.settingMenu')
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        
                        <div class="card-body center_out">
                            <h2 class="heading1">Want to terminate your account?</h2>
                            <p>Too bad that you want to delete your ReachHero account.</p>
                            <p>Have you checked out our Cross-Promo section already? There you can do a cross-promotion with other influencers and win many new followers for your social channels!</p>
                            <p>If you still want to delete your ReachHero account, click on the "delete Account" button.</p>
                            <button class="btn btn-outline-danger" style="border-radius:5%;">Delete Account Now</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>


@endsection

@section('scripts')
    

@endsection