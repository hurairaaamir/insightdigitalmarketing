@extends('layouts.influencer')

@section('styles')
    <link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <h3>Settings</h3>
    <div class="row">
        <div class="col-md-4">
            @include('influencerProfile.settings.settingMenu')
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5>MY CHANNELS</h5>
                    <div class="address-section">
                        <p>With other social channels your ReachHero profile <b>will become even more appealing to advertising clients </b>and <b>you can earn more money!</b></p>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header facebook">
                                            <i class="fab fa-facebook-f"></i> <span>FaceBook</span> 
                                        </div>
                                        {{-- <div class="card-header" id="face-head">
                                            @foreach($channels as $channel)
                                                @if($channel->name=='facebook')
                                                <a href="{{url('channel/facebook/pages')}}">
                                                    <div class="channel-div ml-2">
                                                        <span>
                                                            <img src="{{$channel->facebook->image}}" class="ml-1 img-circle">
                                                        </span>
                                                        <span>{{$channel->facebook->name}}</span>
                                                    </div>
                                                </a>
                                                @endif
                                            @endforeach
                                        </div> --}}
                                        <div>
                                            <div class="card-body">
                                                {{-- <a class="btn btn-primary" href="{{url('/influencer/channel/facebook')}}">Add</a> --}}
                                                <button id="btn-login" type="button" class="btn btn-md facebook btn-lg">
                                                    <span> Add</span>
                                                </button>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
            
                            
            
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header instagram">
                                            <i class="fab fa-instagram"></i> Instagram
                                        </div>
                                        {{-- <div class="card-header" id='insta-head'>
                                            @foreach($channels as $channel)
                                                @if($channel->name=='instagram')
                                                <a href="{{$channel->instagram->permalink}}">
                                                    <div class="channel-div ml-2">
                                                        <span>
                                                            <img src="{{$channel->instagram->profile_picture_url}}" class="ml-1 img-circle">
                                                        </span>
                                                        <span>{{$channel->instagram->username}}</span>
                                                    </div>
                                                </a>
                                                @endif
                                            @endforeach
                                        </div> --}}
                                        <div class="card-body channel-sec">
                                            <button id="insta-login" onclick="intagram_login()" type="button" class="instagram btn btn-md">
                                                Add
                                            </button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header bg-danger">
                                            <i class="fab fa-youtube"></i> YouTube
                                        </div>
                                        <div>
                                            <div class="card-body">
                                                <button class="btn btn-danger btn-md" onclick="authenticate().then(loadClient)">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header twitter">
                                            <i class="fab fa-youtube"></i> twitter
                                        </div>
                                        <div>
                                            <div class="card-body">
                                                <button class="btn twitter btn-md" onclick="authenticate().then(loadClient)">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header tumblr">
                                            <i class="fab fa-youtube"></i> Tumblr
                                        </div>
                                        <div>
                                            <div class="card-body">
                                                <button class="btn tumblr btn-md" onclick="authenticate().then(loadClient)">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header blog">
                                            <i class="fab fa-youtube"></i> Blog
                                        </div>
                                        <div>
                                            <div class="card-body">
                                                <button class="btn blog btn-md" onclick="authenticate().then(loadClient)">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header tictok">
                                            <i class="fab fa-youtube"></i> TicTok
                                        </div>
                                        <div>
                                            <div class="card-body">
                                                <button class="btn tictok btn-md" onclick="authenticate().then(loadClient)">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header snapchat">
                                            <i class="fab fa-youtube"></i> SnapChat
                                        </div>
                                        <div>
                                            <div class="card-body">
                                                <button class="btn snapchat btn-md" onclick="authenticate().then(loadClient)">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>


@endsection

@section('scripts')
    

@endsection