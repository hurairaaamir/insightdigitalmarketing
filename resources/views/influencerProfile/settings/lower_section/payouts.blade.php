@extends('layouts.influencer')

@section('styles')
    <link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <h3>Settings</h3>
    <div class="row">
        <div class="col-md-4">
            @include('influencerProfile.settings.settingMenu')
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>TAX INFO & PAYOUT INFORMATION</h5>
                        </div>
                        <div class="card-body">
                            <p>How would you like the amounts received to be paid out to you?</p>
                            <div class="radio-div m-2">
                                <input type="radio" onclick="radio_active('section_1')" name="payment_type"> <span>Payout via <b>PayPal</b> <span class="small">(there are <a href="https://www.paypal.com/de/webapps/mpp/paypal-fees" target="_blank">recipient fees</a>) (Service fee of 2% (up to max. 12€) will be charged from the payout amount.)</span></span>
                            </div>

                            <form action="{{url('/influencer/profile/settings/payment-method')}}" id="upper_form" method="POST">
                                @csrf
                                <input type="hidden" value="paypal" name="payment_type" required>
                                <div class="input-div section_1 {{$profile->payment_type == 'paypal'? '':'hider'}}">
                                    <div class="child1">PayPal email address</div>
                                    <div class="child2"><input type="text" name="paypal_email" class="form-control" value="{{$profile->paypal_email}}" required></div>
                                </div>
                            </form>
                            <div class="radio-div m-2">
                                <input type="radio" onclick="radio_active('section_2')" name="payment_type"> <span> Payout via <b>bank transfer</b></span>
                            </div>

                            <form action="{{url('/influencer/profile/settings/payment-method')}}"  id="lower_form" method="POST">
                                @csrf
                                <input type="hidden" value="bank_transfer" name="payment_type" required>
                                <div class="input-div section_2 {{$profile->payment_type == 'bank_transfer'? '':'hider'}}">
                                    <div class="child1">Account holder</div>
                                    <div class="child2"><input type="text" class="form-control" name="Account_holder" value="{{$profile->Account_holder}}" required></div>
                                </div>
                                <div class="input-div section_2 {{$profile->payment_type == 'bank_transfer'? '':'hider'}}">
                                    <div class="child1">IBAN</div>
                                    <div class="child2"><input type="text" class="form-control" name="IBAN" value="{{$profile->IBAN}}" required></div>
                                </div>
                                <div class="input-div section_2 {{$profile->payment_type == 'bank_transfer'? '':'hider'}}">
                                    <div class="child1">BIC</div>
                                    <div class="child2"><input type="text" class="form-control" name="BIC" value="{{$profile->BIC}}" required></div>
                                </div>
                            </form>
                            
                            <div>
                                <button class="btn btn-info" onclick="submitPaymentType()">save</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>


@endsection

@section('scripts')
<script src="{{asset('/js/ajax/settings.js')}}"></script> 

@endsection