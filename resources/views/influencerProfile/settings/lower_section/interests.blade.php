@extends('layouts.influencer')

@section('styles')
    <link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <h3>Settings</h3>
    <div class="row">
        <div class="col-md-4">
            @include('influencerProfile.settings.settingMenu')
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>INTERESTS</h5>
                            <div class="address-section">
                                <h5>Fill out your profile</h5>
                                <p>When you fill out your interests <b>you will be more interesting for sponsors </b>and we can mediate your profile better!</p>
                            </div>
                        </div>
                        <div class="card-body checkbox-lists">
                            <div class="list">
                                <h5>Meine Haustiere</h5>
                                <div>
                                    <input type="checkbox"> <span> no pets</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span>dog</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span>cat</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span> rabbit</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span> bird</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span> other pets</span>
                                </div>  
                            </div>
                            <div class="list">
                                <h5>Meine Ernährung</h5>
                                <div>
                                    <input type="checkbox" class=> <span>  vegan</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span>vegetarian</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span> I pay attention to healthy nutrition</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span>  I eat meat</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span> other</span>
                                </div>
                                  
                            </div>
                            <div class="list">
                                <h5>Allgemeinese</h5>
                                <div>
                                    <input type="checkbox" class=> <span> I am single</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span>I am married</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span>I have kids</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span>  I drink alcohol</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span> I have a driver license</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span>  I have a valid passport</span>
                                </div> 
                                <div>
                                    <input type="checkbox"> <span>  I have a smartphone (Apple/iOS)</span>
                                </div> 
                                <div>
                                    <input type="checkbox"> <span>  I have a smartphone (Android)</span>
                                </div> 
                                <div>
                                    <input type="checkbox"> <span>  I have a tablet (Apple/iOS)</span>
                                </div>
                                <div>
                                    <input type="checkbox"> <span>  I have a tablet (Android)</span>
                                </div> 
                            </div>
                        </div>
                        <div><button class="btn btn-sm btn-success ml-4 mb-5" >Save</button></div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>  
</div>


@endsection

@section('scripts')
    

@endsection