@extends('layouts.influencer')

@section('styles')
    <link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <h3>Settings</h3>
    <div class="row">
        <div class="col-md-4">
            @include('influencerProfile.settings.settingMenu')
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @if(\Session::has('success'))
                                <div class="alert alert-info">{!! \Session::get('success') !!}</div>
                            @endif

                            
                            <form action="{{url('/influencer/profile/settings/change-password')}}" method="POST">
                                @csrf
                                <div class="input-div">
                                    <div class="child1">Current password</div>
                                    <div class="child2">
                                        <input type="password" class="form-control" placeholder="Current password" name="current_password" value="{{ old('current_password') }}">
                                        @if ($errors->has('current_password'))
                                            <span style="color:red">
                                                {{ $errors->first('current_password') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
    
                                <div class="input-div ">
                                    <div class="child1">New password</div>
                                    <div class="child2">
                                        <input type="password" class="form-control" placeholder="New password" name="password" value="{{ old('new_password') }}">
                                        @if ($errors->has('password'))
                                            <span style="color:red">
                                                {{ $errors->first('password') }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="input-div">
                                    <div class="child1">Repeat password</div>
                                    <div class="child2"><input type="password" class="form-control" placeholder="Repeat password" name="password_confirmation" value="{{ old('password_confirmation') }}"></div>
                                </div>
                                <div class="center_out mt-5 mb-4">
                                    <button class="btn btn-info btn-sm">save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>


@endsection

@section('scripts')
    

@endsection