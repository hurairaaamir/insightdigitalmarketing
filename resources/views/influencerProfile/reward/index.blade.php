@extends('layouts.influencer')

@section('styles')
<style>
.note.note-warning {
    background-color: #faeaa9;
    border-left:solid 8px #f3cc31;
    color: black;
    border-radius: 3px;
}

.page-title {
    padding: 0px;
    font-size: 26px;
    letter-spacing: -1px;
    line-height: 26px;
    display: block;
    color: #666;
    margin: 0px 0px 20px 0px;
    font-weight: 300;
    font-family: "Open Sans", sans-serif;
}
 .light {
    padding: 12px 20px 15px 20px;
    background-color: #fff;
}
.cursor-pointer {
    cursor: pointer !important;
}
.reward-container{
    display:flex;
    justify-content: flex-start;
    align-items: flex-start;
    padding:10px;
    flex-wrap:wrap;
}
@media(max-width:780px){
    .reward-container{
        justify-content: center;
        align-items: center;
    }
}
.reward-container .card{
    flex:0 1 302px;
    margin:10px;
}
.reward-container .card .card-body {
    display:flex;
    flex-direction: column;
}

.reward-container .card .card-body .reward-img{
    width:100%;
    height: 100%;
}
.reward-container .card .card-body .reward-img img{
    width:100%; 
    max-height:500px;
}
.reward-container .card .card-body .upper{
    margin:15px 0px;
}
.reward-container .card .card-body .upper h6{
    font-weight: 700;
    font-size:20px;
}
.reward-container .card .card-body .upper .points{
    color:#009465;
    font-size:16px;
}
.center_out{
    display:flex;
    justify-content: center;
    align-items: center;
    text-align:center;
}
.center-loader{
    display: flex;
    width: 100%;
    justify-content: flex-start;
    flex-direction: column;
}
.body-height{
    min-width:700px;
    min-height:80vh;
    display:flex;
    overflow-y:scroll;
    flex-direction: row;
    flex-wrap:wrap;
    padding:10px 5px;
}
@media(max-width:780px){
    .body-height{
        min-width:300px;
    }
}
.body-height .child1{
    flex:0 1 346px;
    display:flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
}
.body-height .child1 .img-container{
    width:300px;
    height: auto;
}
.body-height .child1 .img-container img{
    width:100%;
    height: 100%;
}

.body-height .child2{
    flex:0 1 400px;
    display:flex;
    padding:10px;
    flex-direction: column;
}
.body-height .child2 h6{
    font-weight: 600;
}
.body-height .child2 .points{
    color:#009465;
    font-size:23px;
}
.body-height .child2 span{
    font-size:13px;
    margin-top:10px;

}

</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
    <div class="col-md-12">
    <h1 class="page-title">Recruit influencer friends</h1>
    <div class="row note note-warning">
        <div class="col-sm-7" style="margin:16px;">
            <div>You currently have <a href="#" id="pointsBalance"><b>0 points</b></a>. You can recruit influencer friends (YouTube, instagram, facebook or twitter) to get points. You will receive <b>100 points</b> for each influencer that registers via your advertising link. You can exchange these points for great prizes (see below). Payments cannot be made in cash.</div>
    
            <div style="margin-top: 10px;">
                <b>Your personal advertising link is</b>
                <br>
                <div style="font-size: 1em;">
                    <a href="http://www.reachhero.de/r/4oE0" target="_blank">http://www.reachhero.de/r/4oE0</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-sm-1-offset" style="text-align: left; padding-left: 30px; margin: 16px 0;">
            <div style="display: inline-block; 28px; padding-left:28px; padding-top: 4px; height: 32px; margin-bottom: 18px; margin: 0 auto; background: url(/images/general/warning.svg) top left no-repeat; background-size: 24px; margin-left: -6px; color: #008000; font-weight: bold;">
                You receive points for this		</div>
            <div>
                <div>
                    <b>Recruit influencer friends*</b> (100 points for each one)			</div>
                <div style="font-size: 11px;">
                    *at least 2500 subscribers on YouTube, Instagram, Facebook, Twitter or Tumblr			</div>
            </div>
        </div>
    </div>
</div>
</div>
    <div class="reward-container">
        @foreach($rewards as $reward)
        @php
            $selected_reward=$reward;
        @endphp
        <div class="card">
            <div class="card-body light">
                <div class="reward-img">
                    <img src="{{$reward->image}}" />
                </div>
                <div class="upper">
                    <h6>{{$reward->title}}</h6>
                    <div class="points">Points: {{$reward->points}}</div>
                </div>
                @php
                    $description=substr($reward->description,0,150);
                @endphp
                <p>{{$description}}</p>
                <div class="center_out">
                    <button type="button" data-toggle="modal" data-target=".bd-example-modal-lg" class="btn-secondary btn-sm btn" onclick="getcompleteReward('{{$reward->id}}')">Details</button>
                </div>
            </div>
        </div>
        @endforeach
    </div>





    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body body-height" id="reward-id">
                    
                    
                </div>
                <div class="modal-footer">
                    
                </div>
            </div>
        </div>
    </div>
    
</div>


@endsection

@section('scripts')
<script>
    function getcompleteReward(id){
        document.getElementById('reward-id').innerHTML='<div class="center-loader"><img src="/images/gifs/bluePreloader.svg">';
        $.ajax({
            type: "GET",
            url: '/influencer/reward/show/'+id,
            success: function(response){
                document.getElementById('reward-id').innerHTML=response.data;
            }
        });
    }
</script>
@endsection
