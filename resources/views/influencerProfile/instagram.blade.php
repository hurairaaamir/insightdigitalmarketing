@extends('layouts.influencer')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection





@section('content')

@foreach($instagrams as $instagram)
    <blockquote class="embedly-card">
        <h4>
            <a href="{{$instagram->permalink}}">Login * Instagram</a>
        </h4>
        <p>Welcome back to Instagram. Sign in to check out what your friends, family & interests have been capturing & sharing around the world.</p>
    </blockquote>
@endforeach

@endsection


@section('scripts')
<script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
@endsection