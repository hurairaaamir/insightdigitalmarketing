@extends('layouts.influencer')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
<div id='page-wrap'>
    <div class="container">
        <h1 class="campaign-head">Placement Campaigns</h1>
        <div class="campaigns-tabs">
            <a href="#">
                <span class="active">Available</span>
            </a>
            <a href="{{url('/influencer/offers')}}">
                <span>My Offers</span>
            </a>
        </div>
        <div class="campaign-select">
            <select class="form-control mt-2" onchange="getbysocials()" id="socialSelect">
                <option value="all">
                            All
                    </option>                
                @foreach($socials as $social)
                    <option value="{{$social->name}}">
                            {{$social->name}}
                         {{-- <span class="select-image">
                            <img src="{{$social->image}}">
                        </span> --}}
                    </option>
                @endforeach
            </select>
            <select class="form-control mt-2" onchange="getbybudget()" id="budgetSelect">
                    <option value="1">
                            Filter by budget
                    </option>                
                {{-- @foreach($socials as $social) --}}
                    <option value="100">
                        Ab 100
                    </option>
                    <option value="1000">
                        Ab 1000
                    </option>
                    <option value="5000">
                        Ab 5000
                    </option>
                    <option value="10000">
                        Ab 10000
                    </option>
                {{-- @endforeach --}}
            </select>
            <select class="form-control mt-2" onchange="getbysocials()" id="socialSelect">
                <option value="all">
                            All
                    </option>                
                @foreach($socials as $social)
                    <option value="{{$social->name}}">
                            {{$social->name}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="flex-container" id="card-data" id="campaign-data">
            
            @include('cardData.data')
        
        </div>
        <div class="campaignsLoader loaderhider">
            <img src="/images/gifs/line.svg">
        </div>    
    </div>
</div>






<div class="model-outer">
    <div class="com-model">
        <div class="model-body p-3">
            <div class="model-cross1" onclick="showModel()"><i class="far fa-times-circle"></i></div>
            <div class="campaignsLoader loaderhider">
                <img src="/images/gifs/line.svg">
            </div> 
            <div class="model-inner p-1">
                <div class="complete-card" >
                    <h3 id="show-title">

                    </h3>
                    <div class="cost-badge mb-2">
                        <i class="far fa-money-bill-alt"></i> Budget 
                        <span id="show-upper-cost">
                            {{-- {{$campaign->lower_cost}} --}}
                        </span>
                                -
                        <span id="show-lower-cost">
                               {{-- {{$campaign->upper_cost}} --}}
                        </span>
                        <i class="fas fa-euro-sign"></i>
                    </div>
                </div>
                <div class="card-image">
                    <div class="owl-carousel owl-theme" id="carousel-images">
                        {{-- <div class="full-container">    
                            <img src="{{$campaign->image}}" >
                        </div> --}}
                    </div>
                </div>
                
                <div class="m-1">
                    <h4>Description</h4>
                    <p class="mt-3" id="show-description"> 

                    </p>
                    <div class="social-container m-1" id="show-socials">

                    </div>
                </div>
            </div>
            
            <h4 class="ml-3">Send Offer</h4>
            <div class="offer-section" id="offer-data">
                <div class="offer">
                    <div class="of1">
                        <h3>Content</h3>
                        <p>This content is required for the campaign</p>
                    </div>
                    <div class="of1">
                        <h3>Channel</h3>
                        <p>Where should the placement be posted? (Channels with insufficient followers are unavailable)</p>
                    </div>
                    <div class="of1">
                        <h3>Important Dates</h3>
                        <p>The date to submit the final content to the brand and the date range when the content should be published on your channel.</p>
                    </div>
                    <div class="of1">
                        <h3>Your price</h3>
                        <p>What does the brand have to pay for your content?</p>
                    </div>
                </div>

            </div>
            <form id="offer-submit" method="POST">
                <div class="earning">
                    <table>
                        <tr>
                            <th>Gross Earnings:</th>
                            <td class="gross-earning">
                                <input type="number" min="0" name="budget" class="form-control gross-input" placeholder="Set Price" onkeyup="changeTotalBudget()" onchange="changeTotalBudget()" id="total_budget"> 
                                <i class="fas fa-euro-sign m-1"></i>
                            </td>
                        </tr>
                        <tr class="border-line">
                            <th>Service Fee (15%):</th>
                            <td><span id='fee'></span> <i class="fas fa-euro-sign"></i></td>
                            <input type="hidden" name="fee" id="fee_id">
                        </tr>
                        <tr>
                            <th>Amount Disbursed:</th>
                            <td><span id="amount_disbursed"></span> <i class="fas fa-euro-sign"></i></td>
                        </tr>
                    </table>
                </div>
                <div class="offer-details">
                    <div class="inner">
                        <h2>
                            Your Offer Details
                        </h2>
                        <p>Please describe what you would like to do in as much detail as possible. Your description can determine whether your offer is accepted or not.</p>
                        <div>
                            <input type="hidden" name="campaign_id" id="campaign_id">
                            <span><img src="{{auth()->user()->profile ? auth()->user()->profile->image :'/images/general/default.jpg'}}" style="width:80px;height:80px;" alt=""></span>
                            <textarea class="form-control" rows="11" id="textarea_detail" name="details"></textarea>
                        </div>
                    </div>
                </div>
                <div id="offer-errors">
                </div>
                <div class="campaignsLoader mt-5 loaderhider" id="circle-loader">
                    <img src="/images/gifs/circleLoader.svg">
                </div>
                <div class="offer-details mt-4 mb-5">
                    <div>
                        <button class="btn btn-danger">Send Offer</button>
                        <p>By sending this proposal you agree that you will produce the content and publish it to your selected channel, when the sponsor accept your proposal.</p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection


@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>
@endsection