@extends('layouts.influencer')

@section('styles')
<link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
<style>
    .campaigns-tabs{
      display:flex;
      border-bottom:1px solid gainsboro;
      margin-bottom:10px;
      padding:10px 15px;

    }
    .campaigns-tabs a span{
        padding:10px 15px;
    }
    .campaigns-tabs a{
      text-decoration: none;
      color:rgb(27, 27, 27);
    }
    .campaigns-tabs a span:hover{
      border-bottom:4px solid rgb(252, 66, 66);
    } 
    .campaigns-tabs .active{
        background-color: transparent!important;
        border-bottom:4px solid red;
      cursor: default;
    }
    .campaign-head{
        font-weight:100;
        font-size: 2em;
    }
    .facebook{
        background-color:#4966A2;
        color:white;
        font-size:20px;
    }
    .instagram{
        background-color:#E66A5E;
        color:white;
        font-size:20px;
    }
    .channel-div{
        display:flex;
        flex-direction: row;
        justify-content: flex-start;
        align-items: center;
    }
    .channel-div span{
        margin:4px;
    }        
    .channel-div span img{
        width:40px;
        height:auto;
    }
    .channel-sec{
        display:flex;
        justify-content: flex-start;
        align-items: center;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="campaigns-tabs">
      <a href="#">
          <span class="active">Channel</span>
      </a>
      <a href="{{url('/influencer/content/index/youtube')}}">
          <span>Content</span>
      </a>
    </div>
    <h3>CHANNEL</h3>
        
    <div class="row">
        <div class="col-md-8">
            <div class="card" id="form-append">
                <div class="card-header">
                    {{-- <h5>Connect more social channels</h5> --}}
                    <div class="address-section">
                        <h5>Connect more social channels</h5>
                        <p>With more social channels, your ReachHero profile will be even more exciting for advertisers and you can earn more money!</p>
                    </div>
                </div>
                <div class="card-body">
                    <h6>MY CHANNELS</h6>


                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header facebook">
                                <i class="fab fa-facebook-f"></i> FaceBook
                            </div>
                            <div class="card-header" id="face-head">
                                @foreach($channels as $channel)
                                    @if($channel->name=='facebook')
                                    <a href="{{url('/influencer/channel/facebook/pages')}}">
                                        <div class="channel-div ml-2">
                                            <span>
                                                <img src="{{$channel->facebook->image ?? ''}}" class="ml-1 img-circle">
                                            </span>
                                            <span>{{$channel->facebook->name ?? ''}}</span>
                                        </div>
                                    </a>
                                    @endif
                                @endforeach
                            </div>
                            <div>
                                <div class="card-body">
                                    {{-- <a class="btn btn-primary" href="{{url('/influencer/channel/facebook')}}">Add</a> --}}
                                    <button id="btn-login" type="button" class="btn facebook btn-lg">
                                        <span> Add</span>
                                    </button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                   

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header instagram">
                                <i class="fab fa-instagram"></i> Instagram
                            </div>
                            <div class="card-header" id='insta-head'>
                                @foreach($channels as $channel)
                                    @if($channel->name=='instagram')
                                    <a href="{{$channel->instagram->permalink}}" target="_blank">
                                        <div class="channel-div ml-2">
                                            <span>
                                                <img src="{{$channel->instagram->profile_picture_url}}" class="ml-1 img-circle">
                                            </span>
                                            <span>{{$channel->instagram->username}}</span>
                                        </div>
                                    </a>
                                    @endif
                                @endforeach
                            </div>
                            <div class="card-body channel-sec">
                                <button id="insta-login" onclick="intagram_login()" type="button" class="instagram btn">
                                    Add
                                </button>
                                
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header bg-danger">
                                <i class="fab fa-youtube"></i> Youtube
                            </div>
                            <div class="card-header" id="youtube-head">
                                @foreach($channels as $channel)
                                    @if($channel->name=='youtube')
                                    <a href="{{$channel->youtube->channel_url}}" target="_blank">
                                        <div class="channel-div ml-2">
                                            <span>
                                                <img src="{{$channel->youtube->image_url}}" class="ml-1 img-circle">
                                            </span>
                                            <span>{{$channel->youtube->title}}</span>
                                        </div>
                                    </a>
                                    @endif
                                @endforeach
                            </div>
                            <div>
                                <div class="card-body">
                                    <button class="btn btn-danger" id="authorize-button" onclick="handleClientLoad()">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <form id="ajaxInstagram" style="display:none">
                    <input type="hidden" name="uid" id="uid_id">
                    <input type="hidden" name="access_token" id="access_token_id">
                    <input type="hidden" name="permissions" id="permissions_id">
                    <button id="intagram-submit"></button>
                </form>

                <form id="ajaxFacebook" style="display:none">
                    <input type="hidden" name="uid" id="Facebook_uid_id">
                    <input type="hidden" name="access_token" id="Facebook_access_token_id">
                    <input type="hidden" name="permissions" id="Facebook_permissions_id">
                    <button id="facebook-submit"></button>
                </form>
                
            </div>
        </div>
    </div>  
</div>

@endsection





@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script async defer src="https://apis.google.com/js/api.js"  ></script>

<script src="{{asset('/js/ajax/channels.js')}}"></script> 
<script src="{{asset('/js/ajax/youtube.js')}}"></script>

{{-- <script>

  const CLIENT_ID =
    '812304486538-1q7qb3ocluf11atmmtamdialv1rvj5v2.apps.googleusercontent.com';
  const DISCOVERY_DOCS = [
    'https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest',
  ];
  const SCOPES = 'https://www.googleapis.com/auth/youtube.readonly';
  
  const authorizeButton = document.getElementById('authorize-button');
  const videoContainer=document.getElementById('video-container');
  
  // Load auth2 library
  function handleClientLoad() {
    gapi.load('client:auth2', initClient);
  }
  
  // Init API client library and set up sign in listeners
  function initClient() {
    gapi.client
      .init({
        discoveryDocs: DISCOVERY_DOCS,
        clientId: CLIENT_ID,
        scope: SCOPES,
      })
      .then(() => {
        // Listen for sign in state changes
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
        // Handle initial sign in state
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        authorizeButton.onclick = handleAuthClick;
        
      });
  }
  
  // Update UI sign in state changes
  function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
      getChannel();
    } 
  }
  
  // Handle login
  function handleAuthClick() {
    gapi.auth2.getAuthInstance().signIn();
  }
  
  // Get channel from API
  function getChannel() {
  
    gapi.client.youtube.channels
      .list({
        part: 'snippet,contentDetails,statistics',
        mine: true,
      })
      .then((response) => {
        
        console.log(response);
        const channel = response.result.items[0];
        youtubeStore(channel);
        console.log(channel.snippet);
        console.log("url:"+channel.snippet.customUrl);
        const playlistId = channel.contentDetails.relatedPlaylists.uploads;
        requestVideoPlaylist(playlistId);
      })
      .catch((err) => alert('No Channel By That Name'));
  }
  
  // Add commas to number
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  
  function youtubeStore(channel){
      let title=channel.snippet.title;
      let youtube_channel_id=channel.id;
      let subscribers=numberWithCommas(channel.statistics.subscriberCount);
      let views=numberWithCommas(channel.statistics.viewCount);
      let videos=numberWithCommas(channel.statistics.videoCount);
      let comments=numberWithCommas(channel.statistics.commentCount);
      let channel_url=`https://youtube.com/${channel.snippet.customUrl}`;
      let image_url=channel.snippet.thumbnails.default.url;
  
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          url: 'influencer/channel/youtube/login',
          type:'POST',
          data:{title,youtube_channel_id,subscribers,views,videos,comments,channel_url,image_url},
      })
      .done(function(response)
      {   
          let data=response.data;
          document.getElementById('youtube-head').innerHTML=`
          <a href="${data.channel_url}" target="_blank">
              <div class="channel-div ml-2">
                  <span>
                      <img src="${data.image_url}" class="ml-1 img-circle">
                  </span>
                  <span>${data.title}</span>
              </div>
          </a>`
          console.log(data);
      });
  }
  function requestVideoPlaylist(playlistId) {
    const requestOptions = {
      playlistId: playlistId,
      part: 'snippet',
      maxResults: 10,
    };
  
    const request = gapi.client.youtube.playlistItems.list(requestOptions);
  
    request.execute((response) => {
      console.log(response);
      const playListItems = response.result.items;
      if (playListItems) {
        let output = '<br><h4 class="center-align">Latest Videos</h4>';
  
        // Loop through videos and append output
        playListItems.forEach((item) => {
          const videoId = item.snippet.resourceId.videoId;
  
          output += `
            <div class="col s3">
              <iframe width="100%" height="auto" src="https://www.youtube.com/embed/${videoId}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
          `;
        });
  
        // Output videos
        videoContainer.innerHTML = output;
      } else {
        videoContainer.innerHTML = 'No Uploaded Videos';
      }
    });
  }
  
  </script> --}}



{{-- <script>
// Options
const CLIENT_ID =
  '812304486538-1q7qb3ocluf11atmmtamdialv1rvj5v2.apps.googleusercontent.com';
const DISCOVERY_DOCS = [
  'https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest',
];
const SCOPES = 'https://www.googleapis.com/auth/youtube.readonly';

const authorizeButton = document.getElementById('authorize-button');
const videoContainer=document.getElementById('video-container');

// Load auth2 library
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

// Init API client library and set up sign in listeners
function initClient() {
  gapi.client
    .init({
      discoveryDocs: DISCOVERY_DOCS,
      clientId: CLIENT_ID,
      scope: SCOPES,
    })
    .then(() => {
      // Listen for sign in state changes
      gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
      // Handle initial sign in state
      updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      authorizeButton.onclick = handleAuthClick;
      
    });
}

// Update UI sign in state changes
function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    getChannel();
  } 
}

// Handle login
function handleAuthClick() {
  gapi.auth2.getAuthInstance().signIn();
}

// Get channel from API
function getChannel() {

  gapi.client.youtube.channels
    .list({
      part: 'snippet,contentDetails,statistics',
      mine: true,
    })
    .then((response) => {
      
      console.log(response);
      const channel = response.result.items[0];
      youtubeStore(channel);
      console.log(channel.snippet);
      console.log("url:"+channel.snippet.customUrl);
      const playlistId = channel.contentDetails.relatedPlaylists.uploads;
      requestVideoPlaylist(playlistId);
    })
    .catch((err) => alert('No Channel By That Name'));
}

// Add commas to number
function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function youtubeStore(channel){
    let title=channel.snippet.title;
    let youtube_channel_id=channel.id;
    let subscribers=numberWithCommas(channel.statistics.subscriberCount);
    let views=numberWithCommas(channel.statistics.viewCount);
    let videos=numberWithCommas(channel.statistics.videoCount);
    let comments=numberWithCommas(channel.statistics.commentCount);
    let channel_url=`https://youtube.com/${channel.snippet.customUrl}`;
    let image_url=channel.snippet.thumbnails.default.url;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/channel/youtube/login',
        type:'POST',
        data:{title,youtube_channel_id,subscribers,views,videos,comments,channel_url,image_url},
    })
    .done(function(response)
    {   
        let data=response.data;
        document.getElementById('youtube-head').innerHTML=`
        <a href="${data.channel_url}" target="_blank">
            <div class="channel-div ml-2">
                <span>
                    <img src="${data.image_url}" class="ml-1 img-circle">
                </span>
                <span>${data.title}</span>
            </div>
        </a>`
        console.log(data);
    });
}
function requestVideoPlaylist(playlistId) {
  const requestOptions = {
    playlistId: playlistId,
    part: 'snippet',
    maxResults: 10,
  };

  const request = gapi.client.youtube.playlistItems.list(requestOptions);

  request.execute((response) => {
    console.log(response);
    const playListItems = response.result.items;
    if (playListItems) {
      let output = '<br><h4 class="center-align">Latest Videos</h4>';

      // Loop through videos and append output
      playListItems.forEach((item) => {
        const videoId = item.snippet.resourceId.videoId;

        output += `
          <div class="col s3">
            <iframe width="100%" height="auto" src="https://www.youtube.com/embed/${videoId}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        `;
      });

      // Output videos
      videoContainer.innerHTML = output;
    } else {
      videoContainer.innerHTML = 'No Uploaded Videos';
    }
  });
}

</script> --}}

@endsection










































































{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
<script async defer src="https://apis.google.com/js/api.js" 
    onload="this.onload=function(){};handleClientLoad()"
      onreadystatechange="if (this.readyState === 'complete') this.onload()"
    ></script>

<script>
// Options
const CLIENT_ID =
  '812304486538-1q7qb3ocluf11atmmtamdialv1rvj5v2.apps.googleusercontent.com';
const DISCOVERY_DOCS = [
  'https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest',
];
const SCOPES = 'https://www.googleapis.com/auth/youtube.readonly';

const authorizeButton = document.getElementById('authorize-button');
const signoutButton = document.getElementById('signout-button');
const content = document.getElementById('content');
const videoContainer = document.getElementById('video-container');

// Load auth2 library
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

// Init API client library and set up sign in listeners
function initClient() {
  gapi.client
    .init({
      discoveryDocs: DISCOVERY_DOCS,
      clientId: CLIENT_ID,
      scope: SCOPES,
    })
    .then(() => {
      // Listen for sign in state changes
      gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
      // Handle initial sign in state
      updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      authorizeButton.onclick = handleAuthClick;
      signoutButton.onclick = handleSignoutClick;
    });
}

// Update UI sign in state changes
function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    authorizeButton.style.display = 'none';
    signoutButton.style.display = 'block';
    content.style.display = 'block';
    videoContainer.style.display = 'block';
    getChannel();
  } else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
    content.style.display = 'none';
    videoContainer.style.display = 'none';
  }
}

// Handle login
function handleAuthClick() {
  gapi.auth2.getAuthInstance().signIn();
}

// Handle logout
function handleSignoutClick() {
  gapi.auth2.getAuthInstance().signOut();
}

// Display channel data
function showChannelData(data) {
  const channelData = document.getElementById('channel-data');
  channelData.innerHTML = data;
}

// Get channel from API
function getChannel() {
  gapi.client.youtube.channels
    .list({
      part: 'snippet,contentDetails,statistics',
      mine: true,
    })
    .then((response) => {
      console.log(response);
      const channel = response.result.items[0];
      console.log('Subscribers: ', channel.statistics.subscriberCount);
        console.log(channel.snippet);

      const output = `
        <ul class="collection">
          <li class="collection-item">Title: ${channel.snippet.title}</li>
          <li class="collection-item">ID: ${channel.id}</li>
          <li class="collection-item">Subscribers: ${numberWithCommas(
            channel.statistics.subscriberCount
          )}</li>
          <li class="collection-item">Views: ${numberWithCommas(
            channel.statistics.viewCount
          )}</li>
          <li class="collection-item">Videos: ${numberWithCommas(
            channel.statistics.videoCount
          )}</li><li class="collection-item">Comments: ${numberWithCommas(
        channel.statistics.commentCount
      )}</li>
        </ul>
        <p>${channel.snippet.description}</p>
        <hr>
        <a class="btn grey darken-2" target="_blank" href="https://youtube.com/${
          channel.snippet.customUrl
        }">Visit Channel</a>
      `;
      showChannelData(output);

      const playlistId = channel.contentDetails.relatedPlaylists.uploads;
      requestVideoPlaylist(playlistId);
    })
    .catch((err) => alert('No Channel By That Name'));
}

// Add commas to number
function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function requestVideoPlaylist(playlistId) {
  const requestOptions = {
    playlistId: playlistId,
    part: 'snippet',
    maxResults: 10,
  };

  const request = gapi.client.youtube.playlistItems.list(requestOptions);

  request.execute((response) => {
    console.log(response);
    const playListItems = response.result.items;
    if (playListItems) {
      let output = '<br><h4 class="center-align">Latest Videos</h4>';

      // Loop through videos and append output
      playListItems.forEach((item) => {
        const videoId = item.snippet.resourceId.videoId;

        output += `
          <div class="col s3">
          <iframe width="100%" height="auto" src="https://www.youtube.com/embed/${videoId}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
        `;
      });

      // Output videos
      videoContainer.innerHTML = output;
    } else {
      videoContainer.innerHTML = 'No Uploaded Videos';
    }
  });
}
</script> --}}


