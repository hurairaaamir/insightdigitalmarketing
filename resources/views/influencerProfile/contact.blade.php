@extends('layouts.influencer')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/dashboard.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/campaigns/tips.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/influencer/list.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/contact.css') }}" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
    <!-- MAIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content" style="min-height: 889px;">
            <div>
                <h1 class="page-title">
                    Contact us        <br>
                    <small>Do you have questions or suggestions? Do not hesitate to contact us. Just choose one of the following options:</small>
                </h1>

                <div class="text-center">
                    <br><br>
                    <div class="row">
                            <div class="col-xs-12 col-sm-3 col-lg-3 channel noselect" style="max-width: 600px; text-align:center">
                                <div class="type" data-href="https://reachhero.freshdesk.com/support/solutions">
                                    <div class="activeSlope">
                                        <i class="fa fa-check" style=""></i>
                                    </div>
                                    <div>
                                        <i class="fa fa-question font-red-sunglo"></i>
                                    </div>
                                    <div class="uppercase headline">
                                        FAQ                        </div>
                                    <div class="description">
                                        In our FAQ you will find our <b>knowledge base with frequently asked questions</b>. This is the first point of contact for you to answer your questions quickly and easily.                        </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-lg-3 channel noselect" style="max-width: 600px; text-align:center">
                                <div class="type" data-href="mailto:brand@reachhero.de">
                                    <div class="activeSlope">
                                        <i class="fa fa-check" style=""></i>
                                    </div>
                                    <div>
                                        <i class="fa fa-envelope font-red-sunglo"></i>
                                    </div>
                                    <div class="uppercase headline">
                                        E-Mail                        </div>
                                    <div class="description">
                                        If you want to reach us by e-mail, just write us an e-mail to the address <b>brand@reachhero.de</b> and we will contact you within 24h.                        </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-lg-3 channel noselect" style="max-width: 600px; text-align:center">
                                <div class="type" data-href="https://reachhero.freshdesk.com/support/tickets/new">
                                    <div class="activeSlope">
                                        <i class="fa fa-check" style=""></i>
                                    </div>
                                    <div>
                                        <i class="fa fa-life-ring font-red-sunglo"></i>
                                    </div>
                                    <div class="uppercase headline">
                                        Support Ticket
                                    </div>
                                    <div class="description">
                                        You can send us a request quickly and easily via our ticket system. <b>Just create a new ticket</b> and we will contact you within 24h.
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-3 col-lg-3 channel noselect" style="max-width: 600px; text-align:center">
                                <div class="type">
                                    <div>
                                        <i class="fa fa-phone font-red-sunglo"></i>
                                    </div>
                                    <div class="uppercase headline">
                                        Hotline
                                    </div>
                                    <div class="description">
                                        You can reach us via our hotline from <b>Monday - Friday between 9am and 15pm</b>. Simply call our hotline for brand customers at <b>030-208498988</b>.
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
@endsection


@section('scripts')
    <script src="{{asset('js/ajax/compaigns.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
@endsection
