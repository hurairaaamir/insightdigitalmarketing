@extends('layouts.influencer')
@section('styles')
    <link href='{{ asset('css/fullscreenCalendar/daygrid.css') }}' rel='stylesheet' />
    <link href='{{ asset('css/fullscreenCalendar/core.css') }}' rel='stylesheet' />    
    <style>


    .card-tabs .active{
      background-color:transparent!important;
    }
    .tabs-box{
      margin-top:20px;
      min-height:210px; 
    }
    .tab-div{
      display:flex;
      justify-content: center;
      align-items: center;
      flex-wrap:wrap;
      margin-bottom:20px;
    }
    .tab-div .tab1{
      flex:1 1 100px;
      display:flex;
      flex-direction:column;
      justify-content: center;
      align-items: center;
      padding:30px;
    }
    .tab-div .tab1 span{
      font-size:1em;
      margin-top:10px;
    }
    .tab-div .tab1  img{
      width:150px;
      height:auto;
      border-radius: 15px;

    }
    .tab-div .tab2{
      flex:1 1 190px;
    }
    .tab-div .tab2 .social{
      display:flex;
      justify-content: flex-end;
      align-items: center;
      padding:3px;
    }
    .tab-div .tab2 .social span div{
      font-size:25px;
      width:40px;
      height: 40px;
      border-radius:100%;
      display:flex;
      margin-right:4px;
      justify-content: center;
      align-items:center;
    }
    .youtube{
    background-color: #BC1C0E;
    color:white;
  }
  .instagram{
      background-color: #E66A5E;
      color:white;
  }
  .twitter{
      background-color: #70B8F2;
      color:white;
  }
  .facebook{
      background-color: #4966A2;
      color:white;
  }
  .blog{
      background-color: #EF7438;
      color:white;
  }
  .in_active div{
      background-color: gainsboro!important;
  }
  .fc-day{
    width:20px!important;
    height:20px!important;
    font-size:10px!important;
  }
  .fc-dayGrid-view .fc-body .fc-row {
    min-height: 50px;
    max-height: 80px!important;

  }
  .fc-button-primary {
    color: #fff!important;
    background-color: #1e7edd!important;
    border-color: #1975d1!important;
    padding:5px!important;
    font-size:16px!important;
  }
  .fc-toolbar h2 {
    font-size: 1.7em;
    margin: 0;
  }
  .fc-content{
    padding:1.9px;
    transform: translateY(-2px)
  }
  .fc-day-grid-event .fc-content {
    white-space: normal;
    font-size: 12px;
    margin-bottom: 5px;
    height: 26px;
    transition: translateY(-3px);
  }
  
    </style>
@endsection


@section('content')
<div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p>Account balance</p>
              </div>
              <div class="icon">
                <i class="fas fa-euro-sign"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>

                <p>Reach</p>
              </div>
              <div class="icon">
                <i class="fas fa-globe-europe"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>44</h3>

                <p>Completed Content</p>
              </div>
              <div class="icon">
                <i class="fab fa-youtube"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>

                <p>Points collected</p>
              </div>
              <div class="icon">
                <i class="fas fa-star"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-7 ">
                <!-- Default box -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><i class="fas fa-bullhorn"></i> URGENT</h3>

                            <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button> --}}
                            </div>
                        </div>
                        <div class="card-body">
                          @if(count($offers)!=0)
                            @foreach($offers as $offer)
                              @if($offer->status=='Accepted')
                                <a href="{{url('/influencer/offers')}}">
                                  A counter offer for campaign "{{$offer->campaign->title}}" is awaiting for
                                  your response. Respond now
                                </a><br>
                              @endif
                            @endforeach
                          @else
                                <p><a href="{{url('/influencer/compaigns')}}">No offer has been made yet by you.</a></p>
                          @endif

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            
                            </div>
                        <!-- /.card-footer-->
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title"><i class="fas fa-globe-europe"></i> CHANNELS</h3>
                            </div>
                            <div class="card-body socials-icons">
                              @foreach($socials as $social)
                                @if(in_array($social->name,$channels))
                                <a href="{{url('/influencer/channels')}}">
                                  <div class="icon">
                                    {!!$social->image!!}
                                  </div>
                                </a>
                                @else
                                <a href="{{url('/influencer/channels')}}">
                                  <div class="icon in_active">
                                    {!!$social->image!!}
                                  </div>
                                </a>
                                @endif
                              @endforeach

                                 {{-- 
                                    <i class="fab fa-facebook-f"></i>
                                </div>
                                <div class="icon">
                                    <i class="fab fa-instagram"></i>
                                </div>
                                <div class="icon">
                                    <i class="fab fa-youtube"></i>
                                </div>
                                <div class="icon">
                                    <i class="fab fa-snapchat-ghost"></i>
                                </div>
                                <div class="icon">
                                    <i class="fab fa-tumblr"></i>
                                </div>
                                <div class="icon">
                                    <i class="fas fa-music"></i>
                                </div>  --}}
                            </div>
                        </div>
                    </div>
                    @php
                      $count=0;
                    @endphp
                    <div class="col-md-12">
                      <div>
                        
                        <div class="card card-secondary card-outline card-tabs">
                        <div class="card-header">
                            <h4 class="card-title"><i class="far fa-paper-plane"></i>  PROPOSALS</h4>
                        </div>
                          <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link p-3 active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="false">
                                  In Process <span class="badge badge-secondary">
                                    {{$inProgress}}
                                  </span>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link p-3" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">
                                  Accepted <span class="badge badge-primary">
                                    {{$Accepted}}
                                  </span>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link p-3" id="custom-tabs-two-messages-tab" data-toggle="pill" href="#custom-tabs-two-messages" role="tab" aria-controls="custom-tabs-two-messages" aria-selected="false">
                                  Rejected <span class="badge badge-danger">
                                    {{$rejected}}
                                  </span>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link p-3 " id="custom-tabs-two-settings-tab" data-toggle="pill" href="#custom-tabs-two-settings" role="tab" aria-controls="custom-tabs-two-settings" aria-selected="true">
                                  Completed <span class="badge badge-success">
                                    {{$completed}}
                                  </span>
                                </a>
                              </li> 
                            </ul>
                          </div>
                          <div class="card-body">
                            <div class="tab-content" id="custom-tabs-two-tabContent">
                              <div class="tab-pane fade active show" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                                <div class="tabs-box">
                                  @if($inProgress!=0)
                                  @foreach($offers as $offer)                                
                                    @if($offer->status=='In Process')
                                    <div class="tab-div">
                                      <div class="tab1">
                                        <img src="{{$offer->campaign->image}}">
                                        <span class="badge badge-secondary" >{{$offer->budget}} <i class="fas fa-euro-sign"></i></span>
                                      </div>
                                      <div class="tab2">
                                        <p>{{substr($offer->details,0,100).'...'}}</p>
                                        <div class="social">
                                          @foreach($offer->campaign->social_categories as $social)
                                            <span>{!!$social->image!!}</span>
                                          @endforeach
                                        </div>
                                        <div style="color:rgb(172, 187, 201)"><i class="fas fa-cogs"></i> This Offer is In Process</div>

                                        <a href="{{url('/influencer/offers')}}" class="btn btn-primary btn-sm mt-4">See Details</a>
                                        
                                      </div>
                                    </div>
                                    @endif
                                  @endforeach
                                  @else
                                    <p>No sent proposals available from your side</p>
                                    <a href="{{url('/influencer/compaigns')}}" class="btn btn-danger btn-sm "><i class="far fa-paper-plane"></i> Find Campaigns</a>
                                  @endif
                                </div>
                              </div>
                              <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
                                <div class="tabs-box">
                                  @if($Accepted!=0)
                                  @foreach($offers as $offer)                                
                                    @if($offer->status=='Accepted')
                                    <div class="tab-div">
                                      <div class="tab1">
                                        <img src="{{$offer->campaign->image}}">
                                        <span class="badge badge-secondary" >{{$offer->budget}} <i class="fas fa-euro-sign"></i></span>
                                      </div>
                                      <div class="tab2">
                                        <p>{{substr($offer->details,0,100).'...'}}</p>
                                        <div class="social">
                                          @foreach($offer->campaign->social_categories as $social)
                                            <span>{!!$social->image!!}</span>
                                          @endforeach
                                        </div>
                                        <div style="color:rgb(63, 63, 252)"><i class="fas fa-check"></i> This Offer is Accepted</div>

                                        <a href="{{url('/influencer/offers')}}" class="btn btn-primary btn-sm mt-4 ">See Details</a>

                                      </div>
                                    </div>
                                    @endif
                                  @endforeach
                                  @else
                                    <p>No sent proposals available from your side</p>
                                    <a href="{{url('/influencer/compaigns')}}" class="btn btn-danger btn-sm"><i class="far fa-paper-plane"></i> Find Campaigns</a>
                                  @endif
                                </div>
                              </div>
                              <div class="tab-pane fade" id="custom-tabs-two-messages" role="tabpanel" aria-labelledby="custom-tabs-two-messages-tab">
                                <div class="tabs-box">
                                  @if($rejected!=0)
                                  @foreach($offers as $offer)                                
                                    @if($offer->status=='Rejected')
                                      <div class="tab-div">
                                        <div class="tab1">
                                          <img src="{{$offer->campaign->image}}">
                                          <span class="badge badge-secondary" >{{$offer->budget}} <i class="fas fa-euro-sign"></i></span>
                                        </div>
                                        <div class="tab2">
                                          <p>{{substr($offer->details,0,100).'...'}}</p>
                                          <div class="social">
                                            @foreach($offer->campaign->social_categories as $social)
                                              <span>{!!$social->image!!}</span>
                                            @endforeach
                                          </div>
                                          <div style="color:red">X Your offer was declined</div>

                                          <a href="{{url('/influencer/offers')}}" class="btn btn-primary btn-sm mt-4">See Details</a>

                                        </div>
                                      </div>
                                    @endif
                                  @endforeach
                                  @else
                                    <p>No sent proposals available from your side</p>
                                    <a href="{{url('/influencer/compaigns')}}" class="btn btn-danger btn-sm"><i class="far fa-paper-plane"></i> Find Campaigns</a>
                                  @endif
                                </div>
                              </div>
                              <div class="tab-pane fade" id="custom-tabs-two-settings" role="tabpanel" aria-labelledby="custom-tabs-two-settings-tab">
                                <div class="tabs-box">
                                  @if($completed!=0)
                                  @foreach($offers as $offer)                                
                                    @if($offer->status=='completed')
                                    <div class="tab-div">
                                      <div class="tab1">
                                        <img src="{{$offer->campaign->image}}">
                                        <span class="badge badge-secondary" >{{$offer->budget}} <i class="fas fa-euro-sign"></i></span>
                                      </div>
                                      <div class="tab2">
                                        <p>{{substr($offer->details,0,100).'...'}}</p>
                                        <div class="social">
                                          @foreach($offer->campaign->social_categories as $social)
                                            <span>{!!$social->image!!}</span>
                                          @endforeach
                                        </div>
                                        <div style="color:green"><i class="fas fa-check-double"></i> This Offer is Completed</div>

                                        <a href="{{url('/influencer/offers')}}" class="btn btn-primary btn-sm mt-4">See Details</a>

                                      </div>
                                    </div>
                                    @endif
                                  @endforeach
                                  @else
                                    <p>No sent proposals available from your side</p>
                                    <a href="{{url('/influencer/compaigns')}}" class="btn btn-danger btn-sm"><i class="far fa-paper-plane"></i> Find Campaigns</a>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- /.card -->
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        


            <div class="col-md-5">
                <div class="card bg-gradient-success">
                    <div class="card-header border-0 ui-sortable-handle" >

                    <h3 class="card-title">
                        <i class="far fa-calendar-alt"></i>
                        CAMPAIGN'S DEADLINE
                    </h3>
                    
                    <div id="calendar" ></div>
                    <div class="card-body pt-0" style="display: block;">
                        <!--The calendar -->
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
      
    </section>
    
    
@endsection

@section('scripts')
<script src='{{ asset('js/fullscreenCalendar/core.js') }}'></script>
<script src='{{ asset('js/fullscreenCalendar/daygrid.js') }}'></script>

{{-- - 05/16/2020 --}}
    <script>
    $(document).ready(function(){

      // [
        
      //   {
      //     title  : 'event1',
      //     start  : '2020-07-04'
      //   },
      //   {
      //     title  : 'event2',
      //     start  : '2020-07-05',
      //     end    : '2020-07-07'
      //   },
      //   {
      //     title  : 'event3',
      //     start  : '2020-07-09T12:30:00',
      //     allDay : false // will make the time show
      //   }
      // ]
      $.ajax({
            type: "GET",
            url:'/calendar',
            success: function(response){
              var e=response.data;
              var currentDate=response.current;
              var calendarEl = document.getElementById('calendar');
              // '2020-05-16'
              var calendar = new FullCalendar.Calendar(calendarEl, {
      
                defaultDate: currentDate,
                plugins: ['dayGrid'],

                headerToolbar: {
                  left: 'prev,next today',
                  center: 'title',
                  right: 'dayGridMonth,timeGridWeek'
                },
                eventDidMount: function(info) {
                  var tooltip = new Tooltip(info.el, {
                    title: info.event.extendedProps.description,
                    placement: 'top',
                    trigger: 'hover',
                    container: 'body'
                  });
                },
                contentHeight: 390,
                events:e,
              });
                      
              calendar.render();

              
            },
            error:function(data){
                // 
            }
        });
      
        
      
              
  })
    </script>
@endsection

