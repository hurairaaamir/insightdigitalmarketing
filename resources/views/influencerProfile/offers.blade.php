@extends('layouts.influencer')

@section('styles')
    <link href="{{ asset('css/campaign.css') }}" rel="stylesheet" />    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet"/>
@endsection

@section('content')
<div id='page-wrap'>
    <h1 class="campaign-head">Offers</h1>
    <div class="campaigns-tabs">
        <a href="{{url('/influencer/compaigns')}}">
            <span>Available</span>
        </a>
        <a href="#">
            <span class="active">My Offers</span>
        </a>
    </div>
    <div class="container mt-3">
        <div class="card table-style">
            @php
                $counter=1;
            @endphp
            <table class="table table-bordered table-striped">
                <thead>

                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Influencer</th>
                    <th scope="col">budget</th>
                    <th scope="col">details</th>
                    <th scope="col">status</th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                @if(count($offers)!=0)        
                    @foreach($offers as $offer)
                    @php
                        $details=substr($offer->details,0,60).'...';
                        $selected_offer=$offer;
                    @endphp
                    <tr>
                        <th style="min-width:50px;" scope="row">{{$counter}}</th>
                        <td><img style="width:40px;height:40px;border-radius:100%;" src="{{$offer->campaign->image}}"> {{$offer->campaign->title}}</td>
                        <td><span class="badge badge-secondary">{{$offer->budget}} <i class="fas fa-euro-sign"></i></span></td>
                        <td style="min-width:250px">{{$details}}</td>
                        <td><span id='status_{{$offer->id}}' class="{{$selected_offer->status=='In Process' ? 'badge badge-secondary':''}} {{$selected_offer->status=='Accepted' ? ' badge badge-primary':''}} {{$selected_offer->status=='completed' ? ' badge badge-success':''}} {{$selected_offer->status=='Rejected' ? 'badge badge-danger':''}}">{{$offer->status}}</span></td>
                        <td>
                            <button class="btn btn-xs btn-info" onclick="offerCard('{{$offer->campaign->id}}','{{$offer->id}}')">See Details</button>
                        </td>
                    </tr>
                    @php
                        $counter++;
                    @endphp
                    @endforeach
                @else
                    <tr>
                        <th colspan="5">
                        <p style="padding:10px;color:gray;display:flex;justify-content:center">No offers are made yet.</p>
                        <th>
                    <tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- <div class="cost-badge mb-2">
                    <i class="far fa-money-bill-alt"></i> Total Budget:
                    <span id="show-upper-cost">
                        {{$selected_offer->budget}}
                    </span>
                    <i class="fas fa-euro-sign"></i>
                </div> --}}
<div class="model-outer">
    <div class="com-model">
        <div class="model-body" >
            <div class="model-padding">
                <div class="model-cross2" onclick="offerModel()"><i class="far fa-times-circle"></i></div>
                <div class="campaignsLoader loaderhider">
                    <img src="/images/gifs/line.svg">
                </div> 
                <div class="model-inner">
                    <div class="complete-card" >
                        <h3 id="show-title">

                        </h3>
                        <div class="cost-badge mb-2">
                            <i class="far fa-money-bill-alt"></i> Budget 
                            <span id="show-upper-cost">

                            </span>
                                    -
                            <span id="show-lower-cost">

                            </span>
                            <i class="fas fa-euro-sign"></i>
                        </div>
                    </div>
                    <div class="card-image">
                        <div class="owl-carousel owl-theme" id="carousel-images">
                            {{-- <div class="full-container">    
                                <img src="{{$campaign->image}}" >
                            </div> --}}
                        </div>
                    </div>

                    <div class="m-1">
                        <h4>Description</h4>
                        <p class="mt-3" id="show-description"> 

                        </p>
                        <div class="social-container m-1" id="show-socials">

                        </div>
                    </div>
                </div>
                <div class="lower-detail">
                    {{-- @include('cardData.offerCard') --}}
                </div>
            </div>
        </div>
    </div>
</div>



@endsection


@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="{{asset('js/ajax/offer_campaign.js')}}"></script>
@endsection