@extends('layouts.influencer')

@section('styles')
<link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
<link href="{{asset('/css/influencer/content.css')}}" rel='stylesheet' />
<style>
    .media_outer{
        width:500px;
        margin:20px;
    }
    @media(max-width:520px){
        .media_outer{
            width:320px;
            margin:10px 5px;
        }
    }
    .youtube-active{
        border: solid 10px rgb(230, 1, 1);
        padding: 9px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="campaigns-tabs">
        <a href="{{url('/influencer/channel/index')}}">
            <span>channels</span>
        </a>
        <a href="#">
            <span class="active">Content</span>
        </a>
    </div>
    <h3 class="m-3">CONTENT</h3>
    
    @include('influencerProfile/content/menu')
    @php
        $empty=true;
    @endphp
    <div class="container">
        <div class="card center_out p-5 mt-3" id="content_container">           
            <div class="tab-div">
                <div class="tab1">
                    <a href="{{url('/influencer/offers')}}"><img src="{{$offer->campaign->image}}"></a>
                    <span class="badge badge-secondary" >{{$offer->budget}} <i class="fas fa-euro-sign"></i></span>
                </div>
                <div class="tab2">
                    <p>{!!substr($offer->details,0,250).'...'!!}</p>
                    <div class="social">
                        @foreach($offer->offer_channels as $social)
                        @if($social->channel=='instagram')
                            <span><div class="instagram"><i class="fab fa-instagram"></i></div></span>
                        @endif
                        @if($social->channel=='facebook')
                            <span><div class="facebook"><i class="fab fa-facebook-f"></i></div></span>
                        @endif
                        @if($social->channel=='youtube')
                            <span><div class="youtube"><i class="fab fa-youtube"></i></div></span>
                        @endif
                        @endforeach
                    </div>
                    <span style="color:rgb(63, 63, 252);float:right;margin:10px 0px;"><i class="fas fa-check"></i> This Offer is Accepted if you have completed the content for this offer you can then submit your content</span>
                
                    <button class="btn btn-sm btn-danger choose-content mt-5 mb-3" onclick="intagram_login('video-container{{$offer->id}}','{{$offer->id}}')" id="btn-{{$offer->id}}" disabled="true">
                        Choose your content now
                    </button>
                    
                    <hr>
                </div>
            </div>
            <br><h4 class="center-align">Latest Videos</h4>
            <p>Select your content that you have made for this content and submit it</p>

            @foreach($instagrams as $key=>$permalink)
            <div class="center_out" id="insta-{{$key}}">
                    <input type="checkbox" class="instagram-select" onclick="addInsta('insta-{{$key}}','{{$permalink}}')" name="InstaContent[]" value="">
                    <blockquote class="instagram-media media_outer" data-instgrm-version="2" >
                        <p> 
                            <a href="{{$permalink}}" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; text-decoration:none;" target="_top"> View on Instagram</a>
                        </p>
                    </blockquote>
                </div>
                <script async defer type="text/javascript" src="//platform.instagram.com/en_US/embeds.js"></script>
            @endforeach

            <button class="btn btn-danger btn-sm" style="margin-top:100px;margin-bottom:20px;" onclick="instagramSubmit('{{$offer->id}}')">Submit Content</button>
        </div>

    </div>
</div>

@endsection
{{-- KHAWAJA_RigRex
khawaja@rigrex.com --}}




@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

<script async defer src="https://apis.google.com/js/api.js"  ></script>

<script src="{{asset('/js/ajax/channels.js')}}"></script> 

<script src="{{asset('/js/ajax/instagram.js')}}"></script> 

<script src="{{asset('/js/ajax/select_content.js')}}"></script> 
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
@endsection





