
<div class="content-tabs ml-1 mr-1 mt-3">
    <a href="{{url('/influencer/content/index/youtube')}}"><span class="{{ $type=='youtube' ?'active' : ''}}"><i class="fab fa-youtube"></i>Youtube</span></a>
    <a href="{{url('/influencer/content/index/facebook')}}"><span class="{{ $type=='facebook'? 'active':''}}" ><i class="fab fa-facebook-f"></i> FaceBook</span></a>
    <a href="{{url('/influencer/content/index/instagram')}}"><span class="{{ $type=='instagram' ?'active':''}}" ><i class="fab fa-instagram"></i> Instagram</span></a>
    <div class="float-right text-right " style="float:right">
    <select id="campaignsListing" class="form-control inline-block ml-2" onchange="selectCampaign('{{$type}}')" style="max-width: 300px;">
          <option disabled="disabled" selected="">-- Select campaign --</option>
            @foreach($offers as $offer)
              @if($offer->status=='Accepted')
                  <option value="{{$offer->campaign->id}}">{{$offer->campaign->title}}</option>
              @endif
            @endforeach
      </select>
    </div>
  </div>