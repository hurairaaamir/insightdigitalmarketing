@extends('layouts.influencer')

@section('styles')
<link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
<link href="{{asset('/css/influencer/content.css')}}" rel='stylesheet' />
@endsection

@section('content')
<div class="container">
    <div class="campaigns-tabs">
        <a href="{{url('/influencer/channel/index')}}">
            <span>channels</span>
        </a>
        <a href="#">
            <span class="active">Content</span>
        </a>
    </div>
    <h3 class="m-3">CONTENT</h3>
    
    @include('influencerProfile/content/menu')
    @php
      $empty=true;
    @endphp
    <div class="container">
      <div class="card center_out p-5 mt-3" id="content_container">
        @if(count(auth()->user()->youtubeContent)!=0)
          @foreach(auth()->user()->youtubeContent as $content)
            <div class="youtube-hover">
              <div class="youtube-container">
                <h3 class="m-3">{{$content->campaign_name}}</h3>
                <iframe  width="768" height="480" src="https://www.youtube.com/embed/{{$content->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div> 
            <br>
          @endforeach
        @else
        <div class="center_out" style="width:400px;">
          <h2>No Youtube Video</h2>
          <p>Your finished Instagram posts that you have created for companies appear here.</p>
          <a href="{{url('/influencer/compaigns')}}" class="btn btn-sm btn-danger " >
              Find a Campaign Now
          </a>
        </div>
        @endif
      </div>
</div>

@endsection
{{-- KHAWAJA_RigRex
khawaja@rigrex.com --}}

@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

  <script async defer src="https://apis.google.com/js/api.js" ></script>

  <script src="{{asset('/js/ajax/channels.js')}}"></script> 
  <script src="{{asset('/js/ajax/youtube.js')}}"></script>
  <script src="{{asset('/js/ajax/select_content.js')}}"></script> 
@endsection





