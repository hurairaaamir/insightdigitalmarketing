@extends('layouts.influencer')

@section('styles')
<link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
<link href="{{asset('/css/influencer/content.css')}}" rel='stylesheet' />
@endsection

@section('content')
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v7.0&appId=574933150068731&autoLogAppEvents=1" nonce="bnXA9tQZ"></script>
<div class="container">
    <div class="campaigns-tabs">
        <a href="{{url('/influencer/channel/index')}}">
            <span>channels</span>
        </a>
        <a href="#">
            <span class="active">Content</span>
        </a>
    </div>
    <h3 class="m-3">CONTENT</h3>
    
    @include('influencerProfile/content/menu')

    @php
        $empty=true;
    @endphp
    <div class="container">
      <div class="card center_out p-5 mt-3" id="content_container">
        @if(count(auth()->user()->faceBookContent)!=0)
            @foreach(auth()->user()->facebookContent as $content)
            <div class="center_out">
                <h3>{{$content->campaign_name}}</h3>
                <div class="fb-post" "
                    data-href="{{$content->content_url}}"
                    data-width="500">
                </div>
              </div> 
              <br><br>
            @endforeach
        @else
          <div class="center_out" style="width:400px;">
              <h2>No FaceBook Post</h2>
              <p>Your finished FaceBook posts that you have created for companies appear here.</p>
              <a href="{{url('/influencer/compaigns')}}" class="btn btn-sm btn-danger " >
                  Find a Campaign Now
              </a>
          </div>
        @endif
        </div>
      <div id="form-append"></div>
</div>

@endsection
{{-- KHAWAJA_RigRex
khawaja@rigrex.com --}}




@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

<script async defer src="https://apis.google.com/js/api.js"  ></script>

<script src="{{asset('/js/ajax/facebook.js')}}"></script> 

<script src="{{asset('/js/ajax/select_content.js')}}"></script> 


@endsection





