@extends('layouts.influencer')

@section('styles')
<link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
<link href="{{asset('/css/influencer/content.css')}}" rel='stylesheet' />
@endsection

@section('content')
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v7.0&appId=574933150068731&autoLogAppEvents=1" nonce="bnXA9tQZ"></script>
<div class="container">
    <div class="campaigns-tabs">
        <a href="{{url('/influencer/channel/index')}}">
            <span>channels</span>
        </a>
        <a href="#">
            <span class="active">Content</span>
        </a>
    </div>
    <h3 class="m-3">CONTENT</h3>
    
    @include('influencerProfile/content/menu')

    @php
        $empty=true;
    @endphp
    <div class="container">
      <div class="card center_out p-5 mt-3" id="content_container">
        <div class="tab-div">
            <div class="tab1">
                <a href="{{url('/influencer/offers')}}"><img src="{{$offer->campaign->image}}"></a>
              <span class="badge badge-secondary" >{{$offer->budget}} <i class="fas fa-euro-sign"></i></span>
            </div>
            <div class="tab2">
              <p>{!!substr($offer->details,0,250).'...'!!}</p>
              <div class="social">
                @foreach($offer->offer_channels as $social)
                  @if($social->channel=='instagram')
                    <span><div class="instagram"><i class="fab fa-instagram"></i></div></span>
                  @endif
                  @if($social->channel=='facebook')
                    <span><div class="facebook"><i class="fab fa-facebook-f"></i></div></span>
                  @endif
                  @if($social->channel=='youtube')
                    <span><div class="youtube"><i class="fab fa-youtube"></i></div></span>
                  @endif
                @endforeach
              </div>
              <span style="color:rgb(63, 63, 252);float:right;margin:10px 0px;"><i class="fas fa-check"></i> This Offer is Accepted if you have completed the content for this offer you can then submit your content</span>
              {{-- <a href="{{url('/influencer/offers')}}" class="btn btn-primary btn-sm mt-4 "></a> --}}
              @if($type=='youtube')
                <button class="btn btn-sm btn-danger choose-content mt-5 mb-3" onclick="getContent('video-container{{$offer->id}}','{{$offer->id}}')" id="btn-{{$offer->id}}">
                  Choose your content now
                </button>
              @endif
              @if($type=='instagram')
                <button class="btn btn-sm btn-danger choose-content mt-5 mb-3" onclick="intagram_login('video-container{{$offer->id}}','{{$offer->id}}')" id="btn-{{$offer->id}}">
                  Choose your content now
                </button>
              @endif
              @if($type=='facebook')
                <button class="btn btn-sm btn-danger choose-content mt-5 mb-3" onclick="showPages('video-container{{$offer->id}}','{{$offer->id}}')" id="btn-{{$offer->id}}">
                  Choose your content now
                </button>
              @endif
              <hr>
            </div>
          </div>
          <div class="face-container disapper pages_con{{$offer->id}}">
            @foreach(auth()->user()->facebook->pages as $page)
              <div class="face-card" id="face-{{$page->id}}">
                  <div class="face-head">
                      <h3>Facebook Page</h3>
                  </div>
                  <div class="face-body">
                    <h4>{{$page->name}}</h4>
                    <a onclick="addPage('{{$page->page_id}}','face-{{$page->id}}')" class="face-link" target="_blank">select</a>
                  </div>
              </div>
            @endforeach
              <div class="center_out pages_con{{$offer->id}}" style="width:100%;">
                <p>if you have compelted content for this offer Than</p>
                <button class="btn-sm btn-primary btn" disabled="true" onclick="facebookLogin()">Select Content Now</button>
              </div>
            </div>
            @foreach($posts as $key=>$permalink_url)
                <div class="center_out" >
                    <input type="checkbox"  class="facebook-select" onclick="addfacebook('face-{{$key}}','{{$permalink_url}}')" name="faceContent[]" value="">
                    <div class="fb-post" id="face-{{$key}}"
                        data-href="{{$permalink_url}}"
                        data-width="500">
                    </div>
                </div>
                <br><br>
            @endforeach

            <button class="btn btn-danger btn-sm" style="margin-top:100px;margin-bottom:20px;" onclick="facebookSubmit('{{$offer->id}}')">Submit Content</button>

            {{-- face-active --}}
      </div>

      {{-- <div class="fb-post" 
      data-href=""
      data-width="500">
  </div> --}}
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v7.0&appId=574933150068731&autoLogAppEvents=1" nonce="bnXA9tQZ"></script>
      {{-- <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>   --}}
    
</div>

@endsection
{{-- KHAWAJA_RigRex
khawaja@rigrex.com --}}




@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v7.0&appId=574933150068731&autoLogAppEvents=1" nonce="bnXA9tQZ"></script>

<script async defer src="https://apis.google.com/js/api.js"  ></script>

<script src="{{asset('/js/ajax/facebook.js')}}"></script> 

<script src="{{asset('/js/ajax/select_content.js')}}"></script> 


@endsection





