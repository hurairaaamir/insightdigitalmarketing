@extends('layouts.influencer')

@section('styles')
<link href="{{asset('/css/influencer/settings.css')}}" rel='stylesheet' />
<link href="{{asset('/css/influencer/content.css')}}" rel='stylesheet' />
<style>
    .media_outer{
        width:500px;
        margin:20px;
    }
    @media(max-width:520px){
        .media_outer{
            width:320px;
            margin:10px 5px;
        }
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="campaigns-tabs">
        <a href="{{url('/influencer/channel/index')}}">
            <span>channels</span>
        </a>
        <a href="#">
            <span class="active">Content</span>
        </a>
    </div>
    <h3 class="m-3">CONTENT</h3>
    
    @include('influencerProfile/content/menu')
    @php
        $empty=true;
    @endphp
    <div class="container">
      <div class="card center_out p-5 mt-3" id="content_container">
        @if(count(auth()->user()->instagramContent)!=0)
            @foreach(auth()->user()->instagramContent as $key=> $content)
            <div class="center_out" id="insta-{{$key}}">
                <h2>{{$content->campaign_name}}</h2>
                <blockquote class="instagram-media media_outer" data-instgrm-version="2" >
                    <p> 
                        <a href="{{$content->content_url}}" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; text-decoration:none;" target="_top"> View on Instagram</a>
                    </p>
                </blockquote>
            </div>
            <br>
            @endforeach
        @else
        <div class="center_out " style="width:400px;">
            <h2>No Instagram Content</h2>
            <p>Your finished Instagram content that you have created for companies appear here.</p>
            <a href="{{url('/influencer/compaigns')}}" class="btn btn-sm btn-danger " >
                Find a Campaign Now
            </a>
        </div>
      @endif
      <br><br>
      </div>
    </div>
    <div id="form-append"></div>
</div>

@endsection
{{-- KHAWAJA_RigRex
khawaja@rigrex.com --}}




@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>


<script async defer src="https://apis.google.com/js/api.js"  ></script>

<script src="{{asset('/js/ajax/channels.js')}}"></script> 

<script src="{{asset('/js/ajax/instagram.js')}}"></script> 

<script src="{{asset('/js/ajax/select_content.js')}}"></script> 

<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
@endsection





