<aside class="main-sidebar sidebar-dark-primary elevation-2 influ-sidebar" style="min-height: 917px;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" >
        {{-- <span class="brand-text font-weight-light">Project</span> --}}
        <img src="/images/general/logo.png" class="logo-default" alt="logo" style="width: 120px; height: inherit !important;" id="reachHero">
    </a>
    <!-- Sidebar -->
        <div class="sidebar foo sidebar-flex" >

            <!-- Sidebar user (optional) -->
            <!-- Sidebar Menu -->
            <nav class="mt-2" style="width:100%">

                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="{{ url('/brand/dashboard') }}" class="nav-link {{ request()->is('brand/dashboard') || request()->is('brand/dashboard/*') ? 'active' : '' }} influ-item">
                            <i class="fas fa-home"></i>
                            <p>
                                <span>{{ trans('global.dashboard') }}</span>
                            </p>
                        </a>

                    </li>
                    <li class="nav-item has-sub-menu">
                        <a class="nav-link {{ request()->is('brand/campaigns/') || request()->is('brand/campaigns/*') ? 'active' : '' }} nav-dropdown-toggle influ-item cursor-pointer">
                            <i class="far fa-paper-plane"></i>
                            <p>
                                <span>MarketPlace</span>
                            </p>
                        </a>
                        <ul class="sub-menu">
                            <div class="arrow-div"></div>
                            <li id="sidebar-campaign-create" class="nav-item">
                                <a href="/brand/campaigns/select-social" class="nav-link">
                                    <span class="title">Create Campaign</span>
                                </a>
                            </li>
                            <li id="sidebar-my-campaigns" class="nav-item">
                                <a href="/brand/campaigns/index" class="nav-link">
                                    <span class="title">My Campaigns</span>
                                </a>
                            </li>
                            <li id="sidebar-my-content" class="nav-item">
                                <a href="/brand/content" class="nav-link">
                                    <span class="title">My Content</span>
                                </a>
                            </li>
                            <li id="sidebar-marketplace-help" class="nav-item">
                                <a href="/brand/campaigns/tips" class="nav-link">
                                    <span class="title">Help / Tips</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-sub-menu">
                        <a  class="nav-link {{ request()->is('brand/influencer/') || request()->is('brand/influencer/*') ? 'active' : '' }} nav-dropdown-toggle influ-item cursor-pointer">
                            <i class="fas fa-users"></i>
                            <p>
                                <span>Influencer Research</span>
                            </p>
                        </a>
                        <ul class="sub-menu">
                        <div class="arrow-div"></div>

                            <li id="sidebar-influencer-search" class="nav-item">
                                <a href="/brand/influencer/index" class="nav-link">
                                    <i class="icon-magnifier"></i>
                                    <span class="title">Search</span>
                                </a>
                            </li>
                            <li id="sidebar-influencer-list" class="nav-item">
                                <a href="/brand/influencer/list" class="nav-link ">
                                    <i class="icon-list"></i>
                                    <span class="title">Lists</span>
                                </a>
                            </li>
                            <li id="sidebar-search-premium-packages" class="nav-item">
                                <a href="/brand/influencer/subscription" class="nav-link">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Premium packages</span>
                                </a>
                            </li>
                            <li id="sidebar-search-premium-packages" class="nav-item">
                                <a href="/brand/influencer/tutorial" class="nav-link">
                                    <i class="icon-question"></i>
                                    <span class="title">Tutorial</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/brand/messages') }}" class="nav-link {{ request()->is('brand/messages') || request()->is('brand/messages/*') ? 'active' : '' }} influ-item">
                            <i class="fas fa-comments"></i>
                            <p>
                                <span>Messages</span>
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{url('/brand/content/index/youtube')}}" class="nav-link {{ request()->is('brand/content/index') || request()->is('brand/content/*') ? 'active' : '' }} influ-item">
                            <i class="fab fa-youtube"></i>
                            <p>
                                <span>Content</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('/brand/settings') }}" class="nav-link {{ request()->is('brand/settings') || request()->is('brand/settings/*') ? 'active' : '' }} influ-item">
                            <i class="fas fa-cog"></i>
                            <p>
                                <span>Settings</span>
                            </p>
                        </a>
                    </li>

                </ul>
            </nav>
            <nav class="mt-2 md-inline-table bottom-sidebar">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="https://reachhero.freshdesk.com/support/home" class="nav-link" target="_blank">
                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                            <span class="title">Help</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/brand/contact" class="nav-link">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <span class="title">Contact us</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/brand/influencer/subscription" class="nav-link">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <span class="title">Pricing</span>
                        </a>
                    </li>

                    {{-- <li class="nav-item">
                        <a href="#" data-toggle="modal" data-target="#exampleModalCenter" class="nav-link">
                            <i class="fa fa-id-card" aria-hidden="true"></i>
                            <span class="title">Individual offer</span>
                        </a>
                    </li> --}}


                    <!-- INDIVIDUAL OFFER MODAL -->
                    <div style="padding-left: 15px;" class="modal fade modal-scroll in" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" style="color: #000 !important;">INDIVIDUELLES ANGEBOT ANFRAGEN</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>

                                </div>

                                <div class="modal-body">
                                    <iframe id="frame" src="/brand/individualProposal" style="zoom:0.60" width="99.6%" height="650" frameborder="0"></iframe>
                                </div>

                                <div class="modal-footer fc_login_footer">
                                    <div class="navigation_links">
                                        <span>Brauchst du Hilfe?</span>
                                        <a class="link_register" style="color: #007bff !important;" href="/downloads/Reachhero_Leitfaden_Unternehmen.pdf" target="_blank">Lies dir
                                            unsere Anleitung durch
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <li class="nav-item ">
                        <a href="#" data-toggle="modal" data-target="#requestDemoModal" id="button-request-demo" class="nav-link">
                            <i class="fa fa-laptop" aria-hidden="true"></i>
                            <span class="title">Request demo</span>
                        </a>
                    </li>
                    <li  class="nav-item has-sub-menu lang-nav">
                        <ul class="dropdown-menu lang-sub-menu" role="menu" >
                            <li role="presentation">
                                <a class="changeLanguage" data-lang="de" role="menuitem" tabindex="-1" href="#">
                                    <img class="flag-svg" src="{{ asset('images/flags/fr.svg') }}" alt="">
                                    <span class="language-code">DE</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a class="changeLanguage" data-lang="en" role="menuitem" tabindex="-1" href="#">
                                    <img class="flag-svg" src="{{ asset('images/flags/en.svg') }}" alt="">
                                    <span class="language-code">EN</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a class="changeLanguage" data-lang="pl" role="menuitem" tabindex="-1" href="#">
                                    <img class="flag-svg" src="{{ asset('images/flags/fr.svg') }}" alt="">
                                    <span class="language-code">PL</span>
                                </a>
                            </li>
                        </ul>
                        <a href="#" class="nav-link ">
                            <img class="flag-svg" src="{{ asset('images/flags/en.svg') }}" alt="">
                            <span class="language-code">EN</span>
                            <i class="fa fa-angle-up" aria-hidden="true" style="float: right; margin-top: 3px;"></i>

                        </a>

                    </li>

                    <!-- REQUEST DEMO MODAL -->
                    <div class="modal fade modal-scroll in" id="requestDemoModal" role="dialog" style="padding-left: 15px;">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" style="color: #000 !important;">DEMO ANFRAGEN</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>

                                </div>

                                <div class="modal-body">
                                    <iframe id="frame" src="/brand/requestDemo" style="zoom:0.60" width="99.6%" height="650" frameborder="0"></iframe>
                                </div>

                                                <div class="modal-footer fc_login_footer">
                                        <div class="navigation_links">
                                            <span>Brauchst du Hilfe?</span>
                                            <a class="link_register" style="color: #007bff !important;" href="/downloads/Reachhero_Leitfaden_Unternehmen.pdf" target="_blank">Lies dir
                                                unsere Anleitung durch
                                            </a>
                                        </div>
                                    </div>
                                        </div>
                        </div>
                    </div>
                </ul>
            </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
