<aside class="main-sidebar sidebar-dark-primary elevation-4" style="min-height: 917px; background-color:rgb(38,52,75)!important;opacity: 5;">
    <!-- Brand Logo -->
    {{-- {
        background-color: rgb(38,52,75)!important;
        opacity: 5;
    } --}}
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">Admin Panel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url("admin") }}" class="nav-link">
                        <p>
                            <i class="fas fa-tachometer-alt">

                            </i>
                            <span>{{ trans('global.dashboard') }}</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview hover {{ request()->is('admin/permissions*') ? 'menu-open' : '' }} {{ request()->is('admin/roles*') ? 'menu-open' : '' }} {{ request()->is('admin/users*') ? 'menu-open' : '' }}" >
                    <a class="nav-link nav-dropdown-toggle">
                        <i class="fas fa-users">

                        </i>
                        <p>
                            <span>{{ trans('global.userManagement.title') }}</span>
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                            {{-- <li class="nav-item">
                                <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                    <i class="fas fa-unlock-alt">

                                    </i>
                                    <p>
                                        <span>{{ trans('global.permission.title') }}</span>
                                    </p>
                                </a>
                            </li> --}}
                            {{-- <li class="nav-item">
                                <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                    <i class="fas fa-briefcase">

                                    </i>
                                    <p>
                                        <span>{{ trans('global.role.title') }}</span>
                                    </p>
                                </a>
                            </li> --}}
                            <li class="nav-item">
                                <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                    <i class="fas fa-user">

                                    </i>
                                    <p>
                                        <span>{{ trans('global.user.title') }}</span>
                                    </p>
                                </a>
                            </li>
                    </ul>
                </li>
                {{-- <li class="nav-item has-treeview hover {{ request()->is('notification/*') ? 'menu-open' : '' }} " >
                    <a class="nav-link nav-dropdown-toggle">
                        <i class="fas fa-users">

                        </i>
                        <p>
                            <span>Notifications</span>
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ url("notification.admin_all_notification") }}" class="nav-link {{ request()->is('notification.admin_all_notification') || request()->is('notification.admin_all_notificatio/*') ? 'active' : '' }}">
                                    <i class="fas fa-unlock-alt">

                                    </i>
                                    <p>
                                        <span>All Notifications</span>
                                    </p>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a href="{{ url("notification.admin_payment_notification") }}" class="nav-link {{  request()->is('notification.admin_payment_notification') ? 'active' : '' }}">
                                    <i class="fas fa-user">

                                    </i>
                                    <p>
                                        <span>Payment Notifications</span>
                                    </p>
                                </a>
                            </li>
                    </ul>
                </li> --}}
                <li class="nav-item">
                    <a href="{{ url("notification/admin_all_notification") }}" class="nav-link {{ request()->is('notification.admin_all_notification') || request()->is('notification.admin_all_notificatio/*') ? 'active' : '' }}">
                        <i class="fas fa-unlock-alt">

                        </i>
                        <p>
                            <span>All Notifications</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/paypal/index') }}" class="nav-link {{ request()->is('admin/paypal/index') || request()->is('/admin/paypal/*') ? 'active' : '' }}">
                        <i class="fas fa-comments"></i>
                        <p>
                            Paypal
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/campaigns/index') }}" class="nav-link {{ request()->is('admin/campaigns/index') || request()->is('admin/campaigns/*') ? 'active' : '' }}">
                        <i class="far fa-paper-plane"></i>
                        <p>
                            Campaigns
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/reward/index') }}" class="nav-link {{ request()->is('admin/reward/index') || request()->is('admin/reward/*') ? 'active' : '' }}">
                        <i class="fas fa-award"></i>
                        <p>
                            Rewards
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/chat/index') }}" class="nav-link {{ request()->is('admin/chat/index') || request()->is('/admin/chat/*') ? 'active' : '' }}">
                        <i class="fas fa-comments"></i>
                        <p>
                            Chats       
                        </p>
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a href="{{ route("admin.products.index") }}" class="nav-link {{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}">
                        <i class="fas fa-cogs">

                        </i>
                        <p>
                            <span>{{ trans('global.product.title') }}</span>
                        </p>
                    </a>
                </li> --}}
                <li class="nav-item">
                    <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <p>
                            <i class="fas fa-sign-out-alt">

                            </i>
                            <span>{{ trans('global.logout') }}</span>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>