<aside class="main-sidebar sidebar-dark-primary elevation-2 influ-sidebar side-hider" style="min-height:917px;transition: all 1s ease 1s;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" >
        {{-- <span class="brand-text font-weight-light">Project</span> --}}
        <img src="/images/general/logo.png" class="logo-default" alt="logo" style="width: 120px; height: inherit !important;" id="reachHero">
    </a>
    <!-- Sidebar -->
    <div class="sidebar foo">
        <!-- Sidebar user (optional) -->
        <!-- Sidebar Menu -->
        <nav class="mt-2" style="min-height:1100px;!important">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" style="position:relative!important;">
                <li class="nav-item">
                    <a href="{{ url('/influencer/dashboard') }}" class="nav-link {{ request()->is('influencer/dashboard') || request()->is('influencer/dashboard/*') ? 'active' : '' }} influ-item">
                        <i class="fas fa-home"></i>
                        <p>
                            <span>{{ trans('global.dashboard') }}</span>
                        </p>
                    </a>
                </li>
                    <li class="nav-item">
                        <a href="{{ url('/influencer/compaigns') }}" class="nav-link {{ request()->is('influencer/compaigns')||request()->is('influencer/offers') || request()->is('influencer/compaigns/*') ? 'active' : '' }} influ-item">
                            <i class="far fa-paper-plane"></i> 
                            <p>
                                <span>Campaigns</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('influencer/cross/index')}}" class="nav-link {{ request()->is('influencer/cross/index') || request()->is('influencer/cross/*') ? 'active' : '' }} influ-item">
                            <i class="fas fa-random"></i>
                            <p>
                                <span>Cross promotions</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('influencer/messages/index')}}" class="nav-link {{ request()->is('influencer/messages/index') || request()->is('admin/products/*') ? 'active' : '' }} influ-item">
                            <i class="far fa-comments"></i>
                            <p>
                                <span>Messages</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('influencer/channel/index') }}" class="nav-link {{ request()->is('influencer/content/*') || request()->is('influencer/channel/*') ? 'active' : '' }} influ-item">
                            <i class="fab fa-youtube"></i>
                            <p>
                                <span>Channels</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('influencer/reward/index')}}" class="nav-link {{ request()->is('influencer/reward/index') || request()->is('influencer/reward/index/*') ? 'active' : '' }} influ-item">
                            <i class="fas fa-award"></i>
                            <p>
                                <span>Invite friends</span>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('influencer/profile/settings/general') }}" class="nav-link {{ request()->is('influencer/profile/settings/') || request()->is('influencer/profile/settings/*') ? 'active' : '' }} influ-item">
                            <i class="fas fa-cogs">

                            </i>
                            <p>
                                <span>Settings</span>
                            </p>
                        </a>
                    </li>  
                    <li>
                        <div class="bottom-tabs">
                            <div><a href="https://reachhero.freshdesk.com/support/home" target="_blank"><i class="fas fa-question-circle"></i> Help</a></div>
                            <div><a href="/influencer/contact" target="_blank"><i class="far fa-envelope"></i> Contact us</a></div>
                        </div>
                    </li>                 
                </ul>
                
            
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<style>
    .bottom-tabs{
        margin-top:20px;
        position: relative;
        bottom:0px;
        left:0px;
        display:flex;
        width:100%; 
        flex-direction:column;
        justify-content: center;
        align-items: center;
    }
    .bottom-tabs div{
        padding:10px 10px;
        background-color:#0D182E;
        color:rgb(241, 238, 238);
        width:100%;
        font-size:14px;
    }
</style>