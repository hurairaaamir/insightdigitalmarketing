<div id="leftMenu" class="portlet light">
    <a href="/brand/settings">
        <div class="entry {{ request()->is('brand/settings') ? 'bg-red bg-font-red' : '' }}"><i class="fa fa-globe" aria-hidden="true"></i> General information            </div>
    </a>
    <a href="/brand/settings/billing-address">
        <div class="entry {{ request()->is('brand/settings/billing-address') ? 'bg-red bg-font-red' : '' }} "><i class="fa fa-home" aria-hidden="true"></i> Billing address            </div>
    </a>
    <a href="/brand/settings/profile">
        <div class="entry {{ request()->is('brand/settings/profile') ? 'bg-red bg-font-red' : '' }} "><i class="fa fa-user" aria-hidden="true"></i> Profil picture            </div>
    </a>
    <a href="/brand/settings/invoices">
        <div class="entry {{ request()->is('brand/settings/invoices') ? 'bg-red bg-font-red' : '' }} "><i class="fa fa-clipboard" aria-hidden="true"></i> Invoices            </div>
    </a>
    <hr>
    {{-- <a href="/brand/settings/created-brands">
        <div class="entry {{ request()->is('brand/settings/created-brands') ? 'bg-red bg-font-red' : '' }} "><i class="fa fa-address-book" aria-hidden="true"></i> User Management            </div>
    </a> --}}
    <a href="/brand/settings/notifications">
        <div class="entry {{ request()->is('brand/settings/notifications') ? 'bg-red bg-font-red' : '' }} ">
            <i class="fa fa-envelope" aria-hidden="true"></i>
            Notifications
        </div>
    </a>
    <a href="/brand/settings/email-notifications">
        <div class="entry {{ request()->is('brand/settings/email-notifications') ? 'bg-red bg-font-red' : '' }} "><i class="fa fa-envelope" aria-hidden="true"></i> Email Notifications        </div>
    </a>
    <a href="/brand/settings/password">
        <div class="entry {{ request()->is('brand/settings/password') ? 'bg-red bg-font-red' : '' }} "><i class="fa fa-key" aria-hidden="true"></i> Password        </div>
    </a>
    <a href="/brand/settings/remove-account">
        <div class="entry {{ request()->is('brand/settings/remove-account') ? 'bg-red bg-font-red' : '' }} "><i class="fa fa-times" aria-hidden="true"></i> Terminate Account        </div>
    </a>
</div>
