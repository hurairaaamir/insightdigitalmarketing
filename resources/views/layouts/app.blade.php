<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title></title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/css/adminltev3.css" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet" />
    <style>
        .page-loader{
            display:flex;
            justify-content: center;
            align-items: center;
            background-color:white;
            width:100%;
            height:100%;
            position:fixed;
            z-index: 10000000000000000000000000;
            transition: all 0.3s ease-out 0.1s;
        }
        .load-hidden{
            opacity:0;
            z-index:-10;
        }
        .red{
            color:red;
        }
    </style>
</head>

<body class="header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden login-page">
    <div class="page-loader">
        <img src="/images/gifs/campaignsLoader.svg" >
    </div>
    @yield('content') 
    <script>
        window.addEventListener("load", () => {
            const preload = document.querySelector(".page-loader");
            preload.classList.add("load-hidden");
        });
    </script>   
</body>


</html>