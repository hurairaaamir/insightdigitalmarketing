<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Influencer Marketing Plattform und Agentur ★ ReachHero</title>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="shortcut icon" href="/FrontEndImages/Icon-dark.png">

    <link rel="stylesheet" href="/css/AfzaalCss/vendor/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/AfzaalCss/vendor/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/AfzaalCss/style.css">
    <link rel="stylesheet" href="/css/AfzaalCss/responsive.css">
    <link rel="stylesheet" href="/css/AfzaalCss/chat.css">


    <style>
        .page-loader{
            display:flex;
            justify-content: center;
            align-items: center;
            background-color:white;
            width:100%;
            height:100%;
            position:fixed;
            z-index: 10000000000000000000000000;
            transition: all 0.3s ease-out 0.1s;
        }
        .load-hidden{
            opacity:0;
            z-index:-10;
        }
        .red{
            color:red;
        }
        .skiptranslate{
            display:none;
            opacity: 0;
            z-index:-10;
            background:none!important;
            position: absolute;
            top:-100%;
        }
        /* ZgYWQOY-1594150232942 */
        #Ti7AezT-1594149927143{
            display:none!important;
        }
    </style>
</head>

<body class="header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden login-page">
    <div class="page-loader">
        <img src="/FrontEndImages/loading-circle.gif" >
    </div>
    @include('frontend.partials.nav')
    
    
    
    @yield('content')
 
    {{-- <button onclick="language('en')" class="btn btn-primary">french</button>
    <div id="google_translate_element"></div> --}}
    

    {{-- @include('frontend.partials.chats') --}}

    @include('frontend.partials.footer')

    <script
      src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="/js/AfzaalJs/vendors/owl.carousel.min.js"></script>
    <script src="/js/AfzaalJs/app.js"></script>
    <script src="/js/chat/chat.js"></script>

    <script src="https://kit.fontawesome.com/ba93c8b340.js" crossorigin="anonymous"></script>

@yield('scripts')

<script>

    window.addEventListener("load", () => {
        const preload = document.querySelector(".page-loader");
        preload.classList.add("load-hidden");
    });
</script>


<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5f04b708760b2b560e6feb98/1eclbfe9e';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
</body>

</html>

