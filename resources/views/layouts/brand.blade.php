<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Brand</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" />

    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/adminlte/OverlayScrollbars.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/brand/brandMenu.css') }}" rel="stylesheet" />


    <link href="{{ asset('css/influencer.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="/css/AfzaalCss/chat.css">
    @yield('styles')
    <style>
        .btn-home{
            border:1px solid #e7505a;
            background:none;
            color:#e7505a;
            border-radius:25px;
            justify-content: center;
            align-items: center;
            display:flex;
            text-transform:uppercase;
            transition:all 0.4s ease;
            margin:3px;
            }
            .btn-home a{
            text-decoration: none;
            color:#e7505a;
            padding:6px 18px;
            font-size:15px;
            }
            @media(max-width:780px){
            .btn-home{
                display:none!important;
            }
            }
            .btn-home a:hover{
            color:white;
            }
            .btn-home:hover{
            background-color:#e7505a;
            color:white!important;
            }
        .page-loader{
            display:flex;
            justify-content: center;
            align-items: center;
            background-color:white;
            width:100%;
            height:100%;
            position:fixed;
            z-index: 10000000000000000000000000;
            transition: all 0.3s ease-out 0.1s;
        }
        .load-hidden{
            opacity:0;
            z-index:-10;
        }
        .nav-sidebar .active i{
        color: #E60000!important;
        }
        .nav-sidebar .active{
            background-color:#232F44!important;
        }
        .campaigns-tabs{
            display:flex;
            border-bottom:1px solid gainsboro;
            margin-bottom:10px;
            padding:10px 15px;

        }
        .campaigns-tabs a span{
            padding:10px 15px;
        }
        .campaigns-tabs a{
            text-decoration: none;
            color:rgb(27, 27, 27);
        }
        .campaigns-tabs a span:hover{
            border-bottom:4px solid rgb(252, 66, 66);
        }
        .campaigns-tabs .active{
            background-color: transparent!important;
            border-bottom:4px solid red;
            cursor: default;
        }
        .main-header{
            z-index: 10!important;


        }
        .center_out{
            display:flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;

        }
        .dropdown-item:hover{
            cursor:pointer;
        }
        .red-notification{
            margin-bottom:4px;
            background-color:#f0f0f0;
        }
        .red-notification:hover{
            background-color:#f0f0f0!important;
        }

    </style>
</head>

<body class="sidebar-mini " cz-shortcut-listen="true" style="height: auto;" id="influencer-body">
    <div class="page-loader">
        <img src="/images/gifs/campaignsLoader.svg" >
    </div>
    <div class="wrapper">
        <nav class=" main-header navbar navbar-expand bg-white navbar-light border-bottom">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item" onclick="brandSidebarClicked()">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
                </li>
                {{-- <li class="nav-item">
                    <a href="{{ url('/') }}" class="nav-link {{ request()->is('influencer/dashboard') || request()->is('influencer/dashboard/*') ? 'active' : '' }}">
                        <p>
                            <span>home</span>
                        </p>
                    </a>
                </li> --}}
                <li class="nav-item">
                    <div class="btn-home">
                      <a href="{{ url('/') }}" >
                        home
                      </a>
                    </div>
                  </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                      <i class="far fa-bell" style="font-size:20px;"></i>
                        <h4 style="font-size:13px" class="badge badge-danger navbar-badge" id="count_notification">{{count(auth()->user()->unreadNotifications)}}</h4>
                    </a>

                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" >
                      <div style="left:inherit; right: 0px;z-index:1000000;max-height:400px;overflow-y:scroll;">
                        @foreach(auth()->user()->unreadNotifications as $notification)
                        <a href="#" class="dropdown-item">
                          <!-- Message Start -->
                          <div class="media">
                            {{-- <i class="far fa-paper-plane mr-3" style="font-size:40px;"></i>  --}}
                          <div class="mr-3"><img style="width:50px;height:50px;border-radius:100%;" src="{{$notification->data['image']}}" alt=""></div>
                            <div class="media-body">
                              <h3 class="dropdown-item-title">
                                {{ $notification->data['title'] }}
                              </h3>
                              <p class="text-sm">{{ $notification->data['description'] }}</p>
                              <p class="text-sm text-muted mt-3"><i class="far fa-clock mr-1"></i> {{ $notification->created_at }}</p>
                            </div>
                          </div>
                          <!-- Message End -->
                        </a>
                        @endforeach
                        @foreach(auth()->user()->readNotifications as $notification)
                        <a href="#" class="dropdown-item red-notification">
                          <!-- Message Start -->
                          <div class="media">
                            {{-- <i class="far fa-paper-plane mr-3" style="font-size:40px;"></i>  --}}
                          <div class="mr-3"><img style="width:50px;height:50px;border-radius:100%;" src="{{$notification->data['image']}}" alt=""></div>
                            <div class="media-body">
                              <h3 class="dropdown-item-title">
                                {{ $notification->data['title'] }}
                              </h3>
                              <p class="text-sm">{{ $notification->data['description'] }}</p>
                              <p class="text-sm text-muted mt-3"><i class="far fa-clock mr-1"></i> {{ $notification->created_at }}</p>
                            </div>
                          </div>
                          <!-- Message End -->
                        </a>
                        @endforeach

                        @if(!count(auth()->user()->unreadNotifications))
                        <div style="min-width:50px;min-height:200px" class="center_out">
                           <p> No New Notifications</p>
                        </div>

                        @endif
                        <div class="dropdown-divider"></div>

                        <div class="dropdown-divider"></div>
                      </div>

                        <span onclick="markasread('{{auth()->user()->id}}')" class="dropdown-item dropdown-footer">mark as read</span>
                    </div>
                  </li>
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                  <a class="nav-link" data-toggle="dropdown" href="#">
                  <img src="{{auth()->user()->profile->image ? auth()->user()->profile->image :'/images/general/default.jpg'}}" alt="User Avatar" class="mr-3 img-circle" id="nav-profile" style="width:40px;height:40px;transform:translateY(-15px)">
                    {{-- <span class="badge badge-warning navbar-badge">15</span> --}}
                  </a>
                  {{-- <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">15 Notifications</span>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                      <i class="fas fa-envelope mr-2"></i> 4 new messages
                      <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                  </div> --}}
                </li>
              </ul>

            <!-- Right navbar links -->
            @if(count(config('panel.available_languages', [])) > 1)
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#">
                            {{ strtoupper(app()->getLocale()) }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @foreach(config('panel.available_languages') as $langLocale => $langName)
                                <a class="dropdown-item" href="{{ url()->current() }}?change_language={{ $langLocale }}">{{ strtoupper($langLocale) }} ({{ $langName }})</a>
                            @endforeach
                        </div>
                    </li>
                </ul>
            @endif

        </nav>

        @include('partials.brandMenu')
        {{-- @include('partials.brandSideBar') --}}
        <div class="content-wrapper" style="min-height: 917px;">
            {{-- <div class="campaigns-tabs">
                <a href="{{url('/brand/campaigns/index')}}">
                    <span class="{{ request()->is('brand/campaigns/index') || request()->is('brand/campaigns/edit/*') || request()->is('brand/campaigns/show/*') ? 'active' : '' }}" >My Campaigns</span>
                </a>
                <a href="{{url('/brand/campaigns/select-social')}}">
                    <span class="{{ request()->is('brand/campaigns/select-social') || request()->is('brand/campaigns/select-social/*') ? 'active' : '' }}">Create Campaigns</span>
                </a>
            </div> --}}
            <section  style="padding-top: 20px;">
                @yield('content')
            </section>
            <!-- /.content -->
        </div>
        {{-- @include('frontend.partials.chats') --}}

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 3.0.0-alpha
            </div>
            <strong> &copy;</strong> {{ trans('global.allRightsReserved') }}
        </footer>
        <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
    <script src="{{ asset('js/adminlte/jquery.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/adminlte/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/adminlte/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <script src="{{ asset('js/adminlte/jquery.overlayScrollbars.min.js') }}"></script>

    <script src="{{ asset('js/my.js') }}"></script>
    <script src="{{ asset('js/adminlte/adminlte.min.js') }}"></script>

    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>


    @yield('scripts')
    <script src="/js/chat/chat.js"></script>
    <script>
        /*!
     * AdminLTE v3.0.0-alpha.2 (https://adminlte.io)
     * Copyright 2014-2018 Abdullah Almsaeed <abdullah@almsaeedstudio.com>
     * Licensed under MIT (https://github.com/almasaeed2010/AdminLTE/blob/master/LICENSE)
     */
    !function(e,t){"object"==typeof exports&&"undefined"!=typeof module?t(exports):"function"==typeof define&&define.amd?define(["exports"],t):t(e.adminlte={})}(this,function(e){"use strict";var i,t,o,n,r,a,s,c,f,l,u,d,h,p,_,g,y,m,v,C,D,E,A,O,w,b,L,S,j,T,I,Q,R,P,x,B,M,k,H,N,Y,U,V,G,W,X,z,F,q,J,K,Z,$,ee,te,ne="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},ie=function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")},oe=(i=jQuery,t="ControlSidebar",o="lte.control.sidebar",n=i.fn[t],r=".control-sidebar",a='[data-widget="control-sidebar"]',s=".main-header",c="control-sidebar-open",f="control-sidebar-slide-open",l={slide:!0},u=function(){function n(e,t){ie(this,n),this._element=e,this._config=this._getConfig(t)}return n.prototype.show=function(){this._config.slide?i("body").removeClass(f):i("body").removeClass(c)},n.prototype.collapse=function(){this._config.slide?i("body").addClass(f):i("body").addClass(c)},n.prototype.toggle=function(){this._setMargin(),i("body").hasClass(c)||i("body").hasClass(f)?this.show():this.collapse()},n.prototype._getConfig=function(e){return i.extend({},l,e)},n.prototype._setMargin=function(){i(r).css({top:i(s).outerHeight()})},n._jQueryInterface=function(t){return this.each(function(){var e=i(this).data(o);if(e||(e=new n(this,i(this).data()),i(this).data(o,e)),"undefined"===e[t])throw new Error(t+" is not a function");e[t]()})},n}(),i(document).on("click",a,function(e){e.preventDefault(),u._jQueryInterface.call(i(this),"toggle")}),i.fn[t]=u._jQueryInterface,i.fn[t].Constructor=u,i.fn[t].noConflict=function(){return i.fn[t]=n,u._jQueryInterface},u),re=(d=jQuery,h="Layout",p="lte.layout",_=d.fn[h],g=".main-sidebar",y=".main-header",m=".content-wrapper",v=".main-footer",C="hold-transition",D=function(){function n(e){ie(this,n),this._element=e,this._init()}return n.prototype.fixLayoutHeight=function(){var e={window:d(window).height(),header:d(y).outerHeight(),footer:d(v).outerHeight(),sidebar:d(g).height()},t=this._max(e);d(m).css("min-height",t-e.header),d(g).css("min-height",t-e.header)},n.prototype._init=function(){var e=this;d("body").removeClass(C),this.fixLayoutHeight(),d(g).on("collapsed.lte.treeview expanded.lte.treeview collapsed.lte.pushmenu expanded.lte.pushmenu",function(){e.fixLayoutHeight()}),d(window).resize(function(){e.fixLayoutHeight()}),d("body, html").css("height","auto")},n.prototype._max=function(t){var n=0;return Object.keys(t).forEach(function(e){t[e]>n&&(n=t[e])}),n},n._jQueryInterface=function(t){return this.each(function(){var e=d(this).data(p);e||(e=new n(this),d(this).data(p,e)),t&&e[t]()})},n}(),d(window).on("load",function(){D._jQueryInterface.call(d("body"))}),d.fn[h]=D._jQueryInterface,d.fn[h].Constructor=D,d.fn[h].noConflict=function(){return d.fn[h]=_,D._jQueryInterface},D),ae=(E=jQuery,A="PushMenu",w="."+(O="lte.pushmenu"),b=E.fn[A],L={COLLAPSED:"collapsed"+w,SHOWN:"shown"+w},S={screenCollapseSize:768},j={TOGGLE_BUTTON:'[data-widget="pushmenu"]',SIDEBAR_MINI:".sidebar-mini",SIDEBAR_COLLAPSED:".sidebar-collapse",BODY:"body",OVERLAY:"#sidebar-overlay",WRAPPER:".wrapper"},T="sidebar-collapse",I="sidebar-open",Q=function(){function n(e,t){ie(this,n),this._element=e,this._options=E.extend({},S,t),E(j.OVERLAY).length||this._addOverlay()}return n.prototype.show=function(){E(j.BODY).addClass(I).removeClass(T);var e=E.Event(L.SHOWN);E(this._element).trigger(e)},n.prototype.collapse=function(){E(j.BODY).removeClass(I).addClass(T);var e=E.Event(L.COLLAPSED);E(this._element).trigger(e)},n.prototype.toggle=function(){(E(window).width()>=this._options.screenCollapseSize?!E(j.BODY).hasClass(T):E(j.BODY).hasClass(I))?this.collapse():this.show()},n.prototype._addOverlay=function(){var e=this,t=E("<div />",{id:"sidebar-overlay"});t.on("click",function(){e.collapse()}),E(j.WRAPPER).append(t)},n._jQueryInterface=function(t){return this.each(function(){var e=E(this).data(O);e||(e=new n(this),E(this).data(O,e)),t&&e[t]()})},n}(),E(document).on("click",j.TOGGLE_BUTTON,function(e){e.preventDefault();var t=e.currentTarget;"pushmenu"!==E(t).data("widget")&&(t=E(t).closest(j.TOGGLE_BUTTON)),Q._jQueryInterface.call(E(t),"toggle")}),E.fn[A]=Q._jQueryInterface,E.fn[A].Constructor=Q,E.fn[A].noConflict=function(){return E.fn[A]=b,Q._jQueryInterface},Q),se=(R=jQuery,P="Treeview",B="."+(x="lte.treeview"),M=R.fn[P],k={SELECTED:"selected"+B,EXPANDED:"expanded"+B,COLLAPSED:"collapsed"+B,LOAD_DATA_API:"load"+B},H=".nav-item",N=".nav-treeview",Y=".menu-open",V="menu-open",G={trigger:(U='[data-widget="treeview"]')+" "+".nav-link",animationSpeed:300,accordion:!0},W=function(){function i(e,t){ie(this,i),this._config=t,this._element=e}return i.prototype.init=function(){this._setupListeners()},i.prototype.expand=function(e,t){var n=this,i=R.Event(k.EXPANDED);if(this._config.accordion){var o=t.siblings(Y).first(),r=o.find(N).first();this.collapse(r,o)}e.slideDown(this._config.animationSpeed,function(){t.addClass(V),R(n._element).trigger(i)})},i.prototype.collapse=function(e,t){var n=this,i=R.Event(k.COLLAPSED);e.slideUp(this._config.animationSpeed,function(){t.removeClass(V),R(n._element).trigger(i),e.find(Y+" > "+N).slideUp(),e.find(Y).removeClass(V)})},i.prototype.toggle=function(e){var t=R(e.currentTarget),n=t.next();if(n.is(N)){e.preventDefault();var i=t.parents(H).first();i.hasClass(V)?this.collapse(R(n),i):this.expand(R(n),i)}},i.prototype._setupListeners=function(){var t=this;R(document).on("click",this._config.trigger,function(e){t.toggle(e)})},i._jQueryInterface=function(n){return this.each(function(){var e=R(this).data(x),t=R.extend({},G,R(this).data());e||(e=new i(R(this),t),R(this).data(x,e)),"init"===n&&e[n]()})},i}(),R(window).on(k.LOAD_DATA_API,function(){R(U).each(function(){W._jQueryInterface.call(R(this),"init")})}),R.fn[P]=W._jQueryInterface,R.fn[P].Constructor=W,R.fn[P].noConflict=function(){return R.fn[P]=M,W._jQueryInterface},W),ce=(X=jQuery,z="Widget",q="."+(F="lte.widget"),J=X.fn[z],K={EXPANDED:"expanded"+q,COLLAPSED:"collapsed"+q,REMOVED:"removed"+q},$="collapsed-card",ee={animationSpeed:"normal",collapseTrigger:(Z={DATA_REMOVE:'[data-widget="remove"]',DATA_COLLAPSE:'[data-widget="collapse"]',CARD:".card",CARD_HEADER:".card-header",CARD_BODY:".card-body",CARD_FOOTER:".card-footer",COLLAPSED:".collapsed-card"}).DATA_COLLAPSE,removeTrigger:Z.DATA_REMOVE},te=function(){function n(e,t){ie(this,n),this._element=e,this._parent=e.parents(Z.CARD).first(),this._settings=X.extend({},ee,t)}return n.prototype.collapse=function(){var e=this;this._parent.children(Z.CARD_BODY+", "+Z.CARD_FOOTER).slideUp(this._settings.animationSpeed,function(){e._parent.addClass($)});var t=X.Event(K.COLLAPSED);this._element.trigger(t,this._parent)},n.prototype.expand=function(){var e=this;this._parent.children(Z.CARD_BODY+", "+Z.CARD_FOOTER).slideDown(this._settings.animationSpeed,function(){e._parent.removeClass($)});var t=X.Event(K.EXPANDED);this._element.trigger(t,this._parent)},n.prototype.remove=function(){this._parent.slideUp();var e=X.Event(K.REMOVED);this._element.trigger(e,this._parent)},n.prototype.toggle=function(){this._parent.hasClass($)?this.expand():this.collapse()},n.prototype._init=function(e){var t=this;this._parent=e,X(this).find(this._settings.collapseTrigger).click(function(){t.toggle()}),X(this).find(this._settings.removeTrigger).click(function(){t.remove()})},n._jQueryInterface=function(t){return this.each(function(){var e=X(this).data(F);e||(e=new n(X(this),e),X(this).data(F,"string"==typeof t?e:t)),"string"==typeof t&&t.match(/remove|toggle/)?e[t]():"object"===("undefined"==typeof t?"undefined":ne(t))&&e._init(X(this))})},n}(),X(document).on("click",Z.DATA_COLLAPSE,function(e){e&&e.preventDefault(),te._jQueryInterface.call(X(this),"toggle")}),X(document).on("click",Z.DATA_REMOVE,function(e){e&&e.preventDefault(),te._jQueryInterface.call(X(this),"remove")}),X.fn[z]=te._jQueryInterface,X.fn[z].Constructor=te,X.fn[z].noConflict=function(){return X.fn[z]=J,te._jQueryInterface},te);e.ControlSidebar=oe,e.Layout=re,e.PushMenu=ae,e.Treeview=se,e.Widget=ce,Object.defineProperty(e,"__esModule",{value:!0})});
    //# sourceMappingURL=adminlte.min.js.map
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5f04b708760b2b560e6feb98/1eclbfe9e';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    </script>
    <script>

        function brandSidebarClicked(){
            document.querySelector('body').classList.remove('sidebar-closed');
            document.querySelector('body').classList.remove('sidebar-collapse');
            document.querySelector('body').classList.add('sidebar-open');
        }


        window.addEventListener("load", () => {
            const preload = document.querySelector(".page-loader");
            preload.classList.add("load-hidden");
        });
        function markasread(id){

            $.ajax({
                type:'GET',
                url:'/notification/mark_as_read/'+id,
                success:function(data){
                    document.getElementById('count_notification').innerHTML='0';
                }
            });
        }
    </script>
</body>

</html>
