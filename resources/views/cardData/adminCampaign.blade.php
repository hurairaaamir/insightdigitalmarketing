@if(count($campaigns)!=0)
    @foreach($campaigns as $campaign)
        <div class="post" id="campaign_{{$campaign->id}}">
            <div class="user-block">
                <img style="width:100px;height:auto;margin-right:10px;" src="{{$campaign->image}}" alt="user image">
                <span class="username">
                    <a href="#">{{$campaign->title}}</a>
                    <a  class="float-right btn btn-danger btn-xs m-3" id="del_btn_{{$campaign->id}}" style="color:white" onclick="deleteCampaign('{{$campaign->id}}')"> delete  <i class="fas fa-trash-alt"></i></a>
                </span>
                @php
                    $description=substr($campaign->description,0,200).'.......'
                @endphp
                <span class="" id="des_{{$campaign->id}}">
                    {!!$description!!}
                    <div class=" centerOut sm-svg loaderhider" id="sm_blue_{{$campaign->id}}">
                        <img src="/images/gifs/bluePreloader.svg">
                    </div>
                    <button class="btn btn-secondary btn-xs m-2"  onclick="readmore('{{$campaign->id}}')">read more</button>
                </span>
            </div>
            <span class="social-icon">
                @foreach($campaign->social_categories as $social)
                    {!!$social->image!!}
                @endforeach
            </span>
            <p>
                <a class="link-black text-sm mr-2">{{$campaign->lower_cost}} - {{$campaign->upper_cost}} <i class="fas fa-euro-sign"></i></a>
                
                <span class="float-right">    
                    <a class="text-md mt-2" style="cursor:pointer" onclick="getAllOffers('{{$campaign->offers()->count() != 0 ? $campaign->id : ''}}')">
                        <i class="fas fa-caret-down"></i> <i class="far fa-comments mr-1"></i>view all offers {{$campaign->offers()->count()}}
                    </a>    
                </span>
                <span style="display:block;" class="mt-2">
                    <a class="link-black text-sm mr-2"><i class="fas fa-angle-double-right"></i> published at : {{$campaign->created_at}}</a>
                </span>
            </p>
        </div>
        <div class="ml-5" id="offer_data_{{$campaign->id}}">
            <div class="centerOut mt-2 mb-2 loaderhider" id="blue_sm_loader_{{$campaign->id}}">
                <img src="/images/gifs/bluePreloader.svg">
            </div>
        </div>
    @endforeach
{{-- @else

    <i class="fas fa-caret-up"></i>
    <i class="fas fa-caret-down"></i>

<div>
    <h1>No result found !</h1>
</div> --}}
@endif