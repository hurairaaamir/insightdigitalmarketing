<h5>offers</h5>
@if($offers->count()!=0)
    @foreach($offers as $index => $offer)
        <div class="post clearfix">
            <div class="user-block">
                <img class="img-circle img-bordered-sm" src="{{$offer->user->profile->image}}" alt="User Image">
                <span class="username">
                    <a href="#">{{$offer->user->name}}</a>
                    <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                </span>
                <span class="description">sent at -{{$offer->created_at}}</span>
                
                <table class="table table-striped table-bordered mt-2">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Channels</th>
                            <th scope="col">Budget</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($offer->offer_channels as $key => $channel)
                        @if($channel->channel=='facebook')
                            <tr>
                                <td scope="col">{{$key+1}}</td>
                                <td>{{$channel->channel}}</td>
                                <td>{{$channel->budget}} <i class="fas fa-euro-sign"></i></td>
                                <td>
                                    <span class="social-icon">
                                        <div class="facebook"><i class="fab fa-facebook-f"></i></div>
                                    </span>
                                </td>
                            </tr>
                            {{-- <h3>{{$channel->channel}}</h3>
                            --}}
                        @endif
                        @if($channel->channel=='instagram')
                            <tr>
                                <td scope="col">{{$key+1}}</td>
                                <td>{{$channel->channel}}</td>
                                <td>{{$channel->budget}} <i class="fas fa-euro-sign"></i></td>
                                <td>
                                    <span class="social-icon">
                                        <div class="instagram"><i class="fab fa-instagram"></i></div>
                                    </span>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.user-block -->
            <p>
            {{$offer->details}}
            </p>  
        </div>
    @endforeach
    <hr>
@endif