<div class="child1">
    <div class="img-container">
        <img src="{{$selected_reward->image}}" alt="">
    </div>
    <h4>{{$selected_reward->title}}</h4>
</div>
<div class="child2">
    <h6>Details</h6>

    <p>{{$selected_reward->description}}</p>

    <p class="points">Points: {{$selected_reward->points}}</p>

    <div class="mt-3 mb-1">
        <button class="btn btn-secondary">Redeem reward</button>
    </div>

    <span>Unfortunately, you <b>currently have too few points</b> to redeem this reward.</span>
</div>