<div class="child">
    <h2 style="padding:5px;font-size:20px;" class="{{$selected_offer->status=='In Process' ? 'badge badge-secondary':''}} {{$selected_offer->status=='Accepted' ? ' badge badge-primary':''}} {{$selected_offer->status=='completed' ? ' badge badge-success':''}} {{$selected_offer->status=='Rejected' ? 'badge badge-danger':''}}">Status: {{$selected_offer->status}}</h2>
    <span class="btn-ac-rej" onclick="OfferUpdate('{{$selected_offer->id}}','Rejected')"><i class="fas fa-times mr-1"></i>  Reject</span> 
    {{-- <span class="btn-ac-rej" onclick="OfferUpdate('{{$selected_offer->id}}','Accept')"><i class="fas fa-check mr-1"></i>  Accept</span> --}}
    @php
        $payment=$selected_offer->budget + $selected_offer->fee;
        $offer_id=$selected_offer->id;
        $campaign_id=$selected_offer->campaign->id;
    @endphp
    <a href="{{url('/brand/campaigns/payment/'.$offer_id)}}" style="text-decoration:none;color:white;" class="btn-ac-rej"><i class="fas fa-check mr-1"></i>  Accept</a>

</div>
<div class="profile">    
    <div class="child1">
        <h1>{{$selected_offer->user->name}}</h1>
        <div class="profile-img">
            <img src="{{$selected_offer->user->profile->image}}">
        </div>
        <div class="ch">
            <h5 class="mt-2">Budget: {{$selected_offer->budget + $selected_offer->fee}} <i class="fas fa-euro-sign"></i></h5>
            <h5>Accepted Offers:</h5>
            <div>
                @foreach($selected_offer->offer_channels as $channel)
                    @if($channel->channel=='instagram')
                        <div class="social-image">
                            <div class="instagram"><i class="fab fa-instagram"></i></div>
                        </div>
                    @endif
                    @if($channel->channel=='facebook')
                        <div class="social-image">
                            <div class="facebook"><i class="fab fa-facebook-f"></i></div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="child2">    
        <h3>Details:</h3> 
        <p>
            {!!$selected_offer->details!!}
        </p>
    </div>
</div>
<div class="pull-up">
    <h2>Channels Details</h2>
    <div class="socials-sec">
        @foreach($selected_offer->user->channels as $channel)
            @foreach($selected_offer->offer_channels as $offer_channel)
                @if($channel->name == $offer_channel->channel && $offer_channel->channel=='facebook')
                    <div class="card facebook-border">
                        <div class="channel-img">
                            <img src="{{$channel->facebook->image}}" >
                        </div>
                        <span>Facebook</span>
                        <table class="table table-bordered">
                            <tr>
                                <th><i class="far fa-file"></i> Pages</th>
                                <th><i class="far fa-thumbs-up"></i> Likes</th>
                                <th><i class="fab fa-telegram-plane"></i> Posts</th>
                            </tr>
                            @foreach($channel->facebook->pages as $page)
                            <tr>
                                <td><a href="{{$page->permalink}}" target="_blank" >{{$page->name}}</a></td>
                                <td>{{$page->fan_count}}</td>
                                <td>{{$page->posts}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                @endif
                @if($channel->name == $offer_channel->channel && $offer_channel->channel=='instagram')
                    <div class="card instagram-border">
                        <a href="{{$channel->instagram->permalink}}" target="_blank">
                            <div class="channel-img">
                                <img src="{{$channel->instagram->profile_picture_url}}" >
                            </div>
                        </a>
                        <span>Instagram</span>
                        <a href="{{$channel->instagram->permalink}}" target="_blank">
                            <h5>{{$channel->instagram->username}}</h5>
                        </a>
                        <table class="table table-bordered">
                            <tr>
                                <th><i class="far fa-user"></i> Followers</th>
                                <th><i class="fab fa-medium"></i> Total Media</th>
                            </tr>
                            <tr>
                                <td>{{$channel->instagram->followers_count}}</td>
                                <td>{{$channel->instagram->media_count}}</td>
                            </tr>
                        </table>
                    </div>
                @endif
                @if($channel->name == $offer_channel->channel && $offer_channel->channel=='youtube')
                    <div class="card instagram-border">
                        <a href="{{$channel->youtube->channel_url}}" target="_blank">
                            <div class="channel-img">
                                <img src="{{$channel->youtube->image_url}}" >
                            </div>
                        </a>
                        <span>Youtube</span>
                        <a href="{{$channel->youtube->channel_url}}" target="_blank">
                            <h5>{{$channel->youtube->title}}</h5>
                        </a>
                        <table class="table table-bordered">
                            <tr>
                                <th><i class="far fa-user"></i> Subscribers</th>
                                <th><i class="far fa-eye"></i> Views</th>
                                <th><i class="fas fa-video"></i> Videos</th>
                                <th><i class="far fa-comments"></i> Comments</th>
                            </tr>
                            <tr>
                                <td>{{$channel->youtube->subscribers}}</td>
                                <td>{{$channel->youtube->views}}</td>
                                <td>{{$channel->youtube->videos}}</td>
                                <td>{{$channel->youtube->comments}}</td>
                            </tr>
                        </table>
                    </div>
                @endif
            @endforeach
        @endforeach
    </div>
</div>