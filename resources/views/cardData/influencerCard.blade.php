@php
    $last_id=0;
@endphp
    @foreach($users as $user)
    <div class="col-md-4">
        <div class="card fade-in">
            <div class="card-header">
                <img src="{{$user->profile->image}}">

                    <p>{{$user->name}}</p>
                    <add-to-list-button :id="{{ $user->id }}"></add-to-list-button>
                    <div class="socials">
                        @if(count($user->channels)==2)
                            @foreach($user->channels as $channel)
                                @if($channel->name=='instagram')
                                    <div class="mb-2">{{$channel->instagram->followers_count}} Followers</div>
                                @endif
                            @endforeach
                        @endif
                        @foreach($user->channels as $channel)
                            @if(count($user->channels)==1 && $channel->name=='facebook')
                                <div class="mb-2">{{$channel->facebook->pages[0]->fan_count}} likes</div>
                            @elseif(count($user->channels)==1 &&  $channel->name=='instagram')
                                 <div class="mb-2">{{$channel->instagram->followers_count}} Followers</div>
                            @endif
                        @endforeach
                            <div class="social">
                                @foreach($user->channels as $channel)
                                    @if($channel->name=='instagram')
                                        <div class="instagram soc"><i class="fab fa-instagram"></i></div>
                                    @endif
                                    @if($channel->name=='facebook')
                                        <div class="facebook soc"><i class="fab fa-facebook-f"></i></div>
                                    @endif
                                @endforeach
                            </div>
                        {{-- <span>{{$channel->facebook->pages[0]->fan_count}} likes</span> --}}
                    </div>
            </div>
        </div>
    </div>
    @php
        $last_id=$user->id;
    @endphp
    @endforeach
<div class="center-out mt-5 mb-5" id="loadmore-btn">
    <button class="btn btn-primary btn-lg" id="load-btn" onclick="loadMoreData('{{$last_id}}')">Load More</button>
</div>
