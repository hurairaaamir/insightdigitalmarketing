@foreach($campaigns as $campaign)
            <div class="card card-size anim" >
                <div class="card-image">

                    <div class="img-container">
                        <div class="card-price cost-badge">
                            {{$campaign->lower_cost}}-{{$campaign->upper_cost}} <i class="fas fa-euro-sign"></i>
                        </div>
                        <img src="{{$campaign->image}}" >
                    </div>
                    
                </div>
                
                <div class="card-body m-1">
                    <h2>{{$campaign->title}}</h2>
                    <div class="social-container m-1">
                        @foreach($campaign->social_categories as $social)
                        <div class="social-image">
                            {!!$social->image!!}
                        </div>
                    @endforeach
                    </div>
                    @php
                        $description=substr($campaign->description,0,200);
                    @endphp
                    <p class="mt-3"> 
                        {!!$description!!}...
                    </p>
                    <div class="card-btn">
                        <button class="btn btn-info" onclick="CompleteCard({{$campaign->id}})">Show Details</button>
                    </div>
                </div>
            </div>
    @endforeach
    