<div class="tabs-box">
  @php
      $empty=true;
      $social_contents;
  @endphp
    @if(count($offers)!=0)
    @foreach($offers as $offer)   
      @foreach($offer->offer_channels as $social)
        @if($social->channel==$type)
            @php
                $empty=false;
            @endphp
          <div class="tab-div">
            <div class="tab1">
                <a href="{{url('/influencer/offers')}}"><img src="{{$offer->campaign->image}}"></a>
              <span class="badge badge-secondary" >{{$offer->budget}} <i class="fas fa-euro-sign"></i></span>
            </div>
            <div class="tab2">
              <p>{!!substr($offer->details,0,250).'...'!!}</p>
              <div class="social">
                @foreach($offer->offer_channels as $social)
                  @if($social->channel=='instagram')
                    <span><div class="instagram"><i class="fab fa-instagram"></i></div></span>
                  @endif
                  @if($social->channel=='facebook')
                    <span><div class="facebook"><i class="fab fa-facebook-f"></i></div></span>
                  @endif
                  @if($social->channel=='youtube')
                    <span><div class="youtube"><i class="fab fa-youtube"></i></div></span>
                  @endif
                @endforeach
              </div>
              <span style="color:rgb(63, 63, 252);float:right;margin:10px 0px;"><i class="fas fa-check"></i> This Offer is Accepted if you have completed the content for this offer you can then submit your content</span>
              {{-- <a href="{{url('/influencer/offers')}}" class="btn btn-primary btn-sm mt-4 "></a> --}}
              @if($type=='youtube')
                <button class="btn btn-sm btn-danger choose-content mt-5 mb-3" onclick="getContent('video-container{{$offer->id}}','{{$offer->id}}')" id="btn-{{$offer->id}}">
                  Choose your content now
                </button>
              @endif
              @if($type=='instagram')
                <button class="btn btn-sm btn-danger choose-content mt-5 mb-3" onclick="intagram_login('video-container{{$offer->id}}','{{$offer->id}}')" id="btn-{{$offer->id}}">
                  Choose your content now
                </button>
              @endif
              @if($type=='facebook')
                <button class="btn btn-sm btn-danger choose-content mt-5 mb-3" onclick="showPages('video-container{{$offer->id}}','{{$offer->id}}')" id="btn-{{$offer->id}}">
                  Choose your content now
                </button>
              @endif
              <hr>
            </div>
          </div>
          @if($type=='youtube')
            @foreach($offer->youtube_content as $content)
            <div style="margin:26px;padding:26px;">
              <div class="youtube-container">
                  <h3 class="p-4">{{$content->campaign_name}}</h3>
                  <iframe  width="768" height="480" src="https://www.youtube.com/embed/{{$content->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            @endforeach
            
          @endif
          @if($type=='instagram')
              @if(count($offer->instagram_content)!=0)
                @foreach($offer->instagram_content as $key=> $content)
                  <div class="center_out" id="insta-{{$key}}">
                      <h2>{{$content->campaign_name}}</h2>
                      <blockquote class="instagram-media media_outer" data-instgrm-version="2" >
                          <p> 
                              <a href="{{$content->content_url}}" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; text-decoration:none;" target="_top"> View on Instagram</a>
                          </p>
                      </blockquote>
                  </div>
                  <script async defer type="text/javascript" src="//platform.instagram.com/en_US/embeds.js"></script>
                @endforeach
              @endif
          @endif
          @if($type=='facebook')
              @if(count(auth()->user()->facebook->pages)!=0)
              <div class="face-container disapper pages_con{{$offer->id}}">
                @foreach(auth()->user()->facebook->pages as $page)
                  <div class="face-card face_tag" id="face-{{$page->id}}">
                      <div class="face-head">
                          <h3>Facebook Page</h3>
                      </div>
                      <div class="face-body">
                        <h4>{{$page->name}}</h4>
                        <a onclick="addPage('{{$page->page_id}}','face-{{$page->id}}')" class="face-link" target="_blank">select</a>
                      </div>
                  </div>
                @endforeach
                  <div class="center_out pages_con{{$offer->id}}" style="width:100%;margin-top:40px;">
                    <p>if you have compelted content for this offer Than</p>
                    <button class="btn-sm btn-primary btn" onclick="facebookLogin()">Select Content Now</button>
                  </div>
                </div>
              @endif
          @endif
        @endif
      @endforeach
      <div class="center_out">
        <div id="video-container{{$offer->id}}">
          
        </div>
      </div>
    @endforeach
    @endif
    @if($empty)
        <div class="center_out" style="width:400px;">
          <h2>No <span>{{$type}}</span> Required</h2>
          <p>This campaign offer don't require content related to {{$type}}</p>
        </div>
    @endif
</div>