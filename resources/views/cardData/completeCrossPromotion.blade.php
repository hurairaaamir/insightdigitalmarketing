<div>
    <div>
        <h3>{{$selected_promotion->title}}</h3>
    </div>
    <div class="promoCard">
        <div class="child1">
            <div class="promo-left">
                @foreach($selected_promotion->user->channels->pluck('name')->toArray() as $channel)

                    @if($channel=='instagram')
                        <div class="social-image">
                            <div class="instagram"><i class="fab fa-instagram"></i></div>
                        </div>
                    @endif
                    @if($channel=='facebook')
                        <div class="social-image">
                            <div class="facebook"><i class="fab fa-facebook-f"></i></div>
                        </div>
                    @endif 

                    
                @endforeach
            </div>
            <h6>Details</h6>
            <p>{{$selected_promotion->details}}</p>
        </div>
        <div class="child2">
            <div class="promo-img">
                <img src="{{$selected_promotion->user->profile->image}}" alt="">
            </div>
            <h5>{{$selected_promotion->user->name}}</h5>
            @foreach($selected_promotion->user->channels->pluck('name')->toArray() as $channel)
                @if($channel=='instagram')
                    <a href="{{$selected_promotion->user->instagram->permalink}}">
                        <i class="fab fa-instagram"></i> instagram
                    </a>
                @endif
                @if($channel=='facebook')
                    <a href="{{$selected_promotion->user->facebook->pages[0]->permalink}}">
                        <i class="fab fa-facebook-f"></i> facebook
                    </a>
                @endif
            @endforeach
            <div class="center_out"><button class="btn btn-danger btn-sm mt-2"><i class="far fa-comments"></i> Contact</button></div>
        </div>
    </div>
</div>
<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
{{-- <div class="social-image">
                    <div class="facebook"><i class="fab fa-facebook-f"></i></div>
                </div>
                <div class="social-image">
                    <div class="youtube"><i class="fab fa-youtube"></i></div>
                </div>
                <div class="social-image">
                    <div class="twitter"><i class="fab fa-twitter"></i></div>
                </div>  --}}