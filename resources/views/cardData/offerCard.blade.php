<div class="cost-badge mb-2" style="max-width:290px">
    <i class="far fa-money-bill-alt"></i> Total Budget:
    <span id="show-upper-cost" >
        {{$selected_offer->budget}}
    </span>
    <i class="fas fa-euro-sign"></i>
</div>
<h3>Details</h3>
<p>{{$selected_offer->details}}</p>

<div class="table-style">
    <h4>
        <b>Status:</b>
        <span class="{{$selected_offer->status=='In Process' ? 'badge badge-secondary':''}} {{$selected_offer->status=='Accepted' ? ' badge badge-primary':''}} {{$selected_offer->status=='Reject' ? 'badge badge-danger':''}}">
            {{$selected_offer->status}}
        </span>
    </h4>
    <table class="table table-striped table-bordered ">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Channels</th>
                <th scope="col">Budget</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach($selected_offer->offer_channels as $key => $channel)
                @if($channel->channel=='facebook')
                    <tr>
                        <td scope="col">{{$key+1}}</td>
                        <td>{{$channel->channel}}</td>
                        <td>{{$channel->budget}} <i class="fas fa-euro-sign"></i></td>
                        <td>
                            <div class="social-image">
                                <div class="facebook"><i class="fab fa-facebook-f"></i></div>
                            </div>
                        </td>
                    </tr>
                    {{-- <h3>{{$channel->channel}}</h3>
                     --}}
                @endif
                @if($channel->channel=='instagram')
                    <tr>
                        <td scope="col">{{$key+1}}</td>
                        <td>{{$channel->channel}}</td>
                        <td>{{$channel->budget}} <i class="fas fa-euro-sign"></i></td>
                        <td>
                            <div class="social-image">
                                <div class="instagram"><i class="fab fa-instagram"></i></div>
                            </div>
                        </td>
                    </tr>
                @endif
            @endforeach
            
        </tbody>
    </table>
</div>