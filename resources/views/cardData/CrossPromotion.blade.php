@foreach($promotions as $promotion)
            <div class="card card-size anim" >
                <div class="card-image">

                    <div class="img-container">
                        <img src="{{$promotion->user->profile->image}}" >
                    </div>
                    
                </div>
                {{-- @php
                    $promo_channels=$promotion->channels->pluck('name');
                    $promo=in_array('facebook',$promo_channels);
                @endphp
                {{$promo}}                 --}}
                {{-- in_array($mychannel->name,$promotion->channels->pluck('name')->toArray()) --}}
                <div class="card-body">
                    <div class="center_out">
                        <h2>{{$promotion->user->name}}</h2>
                        <div class="promo-socials">
                        @foreach($promotion->user->channels as $mychannel)

                            @if(in_array($mychannel->name,$channels)&&in_array($mychannel->name,$promotion->channels->pluck('name')->toArray())&&$mychannel->name=='instagram')
                                <div class="social-image mr-3">
                                    <div class="instagram"><i class="fab fa-instagram"></i></div>
                                    {{$promotion->user->instagram->followers_count}} followers
                                </div>
                            @endif
                            @if(in_array($mychannel->name,$channels)&&in_array($mychannel->name,$promotion->channels->pluck('name')->toArray())&&$mychannel->name=='facebook')
                                <div class="social-image mr-3">
                                    <div class="facebook"><i class="fab fa-facebook-f"></i></div>
                                    {{$promotion->user->facebook->pages[0]->fan_count}} likes
                                </div>
                            @endif

                            {{-- <div class="social-image">
                                <div class="facebook"><i class="fab fa-facebook-f"></i></div>
                            </div>
                            <div class="social-image">
                                <div class="youtube"><i class="fab fa-youtube"></i></div>
                            </div>
                            <div class="social-image">
                                <div class="twitter"><i class="fab fa-twitter"></i></div>
                            </div> --}}
                            @endforeach
                        </div>
                    </div>
                    <div class="lower-gray">
                        <h5>{{$promotion->title}}</h5>
                        @php
                            $details=substr($promotion->details,0,120).'....'
                        @endphp
                        <p>{{$details}}</p>

                        <p><b>at least</b> {{$promotion->subscribers}} subscribers</p>
                    </div>
                    <button type="button" data-toggle="modal" data-target=".bd-example-modal-lg" class="btn btn-secondary mr-5 btn-sm" onclick="complete_details('{{$promotion->id}}')">
                        Details
                    </button>
                    <button type="button" data-toggle="modal" data-target=".bd-example2-modal-lg" class="btn btn-danger btn-sm" onclick="select_form('{{$promotion->id}}','{{$promotion->user->name}}')">
                        Contact
                    </button>

                </div>
            </div>
    @endforeach
    