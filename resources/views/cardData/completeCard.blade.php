<div class="complete-card" >
    <div class="cost-badge mb-2">
           <i class="far fa-money-bill-alt"></i> Budget {{$campaign->lower_cost}}-{{$campaign->upper_cost}} <i class="fas fa-euro-sign"></i>
    </div>
    <div class="card-image">
        <div class="full-container">    
            <img src="{{$campaign->image}}" >
        </div>
        
    </div>
    
    <div class="m-1">
        <h2>{{$campaign->title}}</h2>
        <p class="mt-3"> 
            {!!$campaign->description!!}
        </p>
        <div class="social-container m-1">
            @foreach($campaign->social_categories as $social)
                <div class="social-image">
                    {!!$social->image!!}
                </div>
            @endforeach
        </div>
    </div>
</div>
    