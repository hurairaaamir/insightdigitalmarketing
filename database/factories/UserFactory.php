<?php

use App\User;
use App\Campaign;
use App\CampaignsSocials;
use App\SocialCategory;
use App\Channeltype;
use App\ChanneltypeProfile;
use App\InfluencerReward;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'role' => $faker->randomElement(['influencer', 'brand']),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Campaign::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'upper_cost' => $faker->numberBetween(200, 10000),
        'lower_cost' => $faker->numberBetween(200, 10000),
        'user_id' => $faker->numberBetween(1, 10),
        'image' => $faker->randomElement(['/images/compaigns/compaign1.jpg', '/images/compaigns/compaign2.png', '/images/compaigns/compaign3.jpg', '/images/compaigns/compaign4.png', '/images/compaigns/compaign5.png', '/images/compaigns/compaign6.jpg', '/images/compaigns/compaign7.png', '/images/compaigns/compaign8.jpg']),
        'description' => $faker->paragraph(10),
        'daterange' => '05/16/2020 - 05/16/2020',
    ];
});

$factory->define(InfluencerReward::class, function (Faker $faker) {
    $i = rand(1, 6);
    while ($i > 7) {
        $i = rand(1, 6);
    }
    return [
        'title' => $faker->name,
        'points' => $faker->numberBetween(200, 10000),
        'image' => '/images/rewards/' . $i . '.jpg',
        'description' => $faker->paragraph(10),
    ];
});



$factory->define(Channeltype::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
