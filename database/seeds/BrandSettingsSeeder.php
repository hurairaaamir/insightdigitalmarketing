<?php

use Illuminate\Database\Seeder;
use App\User;
use App\EmailNotificationsSetting;
use App\Setting;
use App\BillingAddress;

class BrandSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('role', '=', 'admin')->get();

        foreach ($users as $user) {
            EmailNotificationsSetting::create([
                'user_id' => $user->id
            ]);
            Setting::create([
                'user_id' => $user->id
            ]);
            BillingAddress::create([
                'user_id' => $user->id
            ]);
        }
    }
}
