<?php

use Illuminate\Database\Seeder;
use App\User;
use App\CrossPromotion;
use App\CrossPromotionChannel;
class CrossPromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users=User::where('role','=','influencer')->get();

        foreach($users as $user){
            $cross_promotion=CrossPromotion::create([
                'subscribers' => '1100',
                'date_range' => '05/16/2020 - 05/16/2020',
                'title' => 'Unser Quad Channel sucht kooperationspartner',
                'details' => 'Wir suchen Partner auf instagram um unseren Kanal wachsen zu lassen, zurzeit haben wir die 1000 Follower marke geknackt. Wir benötigen Personen die sich mit Insta auskennen und uns helfen können und uns erklären wie man auf Insta wachst.',
                'user_id'=>$user->id
            ]);
           
            CrossPromotionChannel::create([
                "name"=>'facebook',
                "cross_promotion_id"=>$cross_promotion->id
            ]);

            CrossPromotionChannel::create([
                "name"=>'instagram',
                "cross_promotion_id"=>$cross_promotion->id
            ]);
        }
    }
}
