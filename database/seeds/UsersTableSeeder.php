<?php

use App\User;
use App\Campaign;
use App\Offer;
use App\SocialCategory;
use App\Channeltype;
use App\ChanneltypeProfile;
use App\Profile;
use App\Channel;
use App\Fakebook;
use App\Facebookpage;
use App\Instagram;
use App\CrossPromotion;
use App\InfluencerReward;
use App\CrossPromotionChannel;
use App\Youtube;
use App\YoutubeContent;
use App\FacebookContent;
use App\InstagramContent;
use App\OfferChannel;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $admin = User::create([
            'id'             => 1,
            'name'           => 'Admin',
            'role'           => 'admin',
            'email'          => 'admin@admin.com',
            'password'       => '$2y$10$imU.Hdz7VauIT3LIMCMbsOXvaaTQg6luVqkhfkBcsUd.SJW2XSRKO',
            'remember_token' => null,
            'created_at'     => '2019-04-15 19:13:32',
            'updated_at'     => '2019-04-15 19:13:32',
            'deleted_at'     => null,
        ]);





        // Profile::create([
        //     "id"=>1,
        //     'first_name'=>'admin',
        //     'last_name'=>'admin',
        //     'gender'=>'Male',
        //     'user_id'=>1,
        // ]);
        factory(User::class, 60)->create();
        factory(Channeltype::class, 15)->create();
        factory(InfluencerReward::class, 6)->create();



        $campaigns = factory(Campaign::class, 30)->create();

        $socialCategories = [
            [
                'id'         => '1',
                'name' => 'youtube',
                'image' => '<div class="youtube"><i class="fab fa-youtube"></i></div>',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '2',
                'name' => 'instagram',
                'image' => '<div class="instagram"><i class="fab fa-instagram"></i></div>',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '3',
                'name' => 'facebook',
                'image' => '<div class="facebook"><i class="fab fa-facebook-f"></i></div>',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '4',
                'name' => 'twitter',
                'image' => '<div class="twitter"><i class="fab fa-twitter"></i></div>',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '5',
                'name' => 'blog',
                'image' => '<div class="blog"><i class="fas fa-rss"></i></div>',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ]
        ];

        $socialCategories = SocialCategory::insert($socialCategories);

        // $campaigns->first()->social_categories()->sync($socialCategories);

        foreach ($campaigns as $campaign) {
            $id1 = rand(1, 5);
            $id2 = rand(1, 5);
            $campaign->social_categories()->attach([$id1, $id2]);
        }
        // $profile->channeltypes()->attach($request->channel_types);

        $channel = Channel::create([
            "name" => 'facebook',
            'user_id' => $admin->id,
            'channeltype_id' => rand(1, 15)
        ]);

        $face = Fakebook::create([
            "facebook_id" => "263157344872115",
            "name" => "Muhammad Huraira Aamir",
            "image" => "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=263157344872115&height=50&width=50&ext=1593158509&hash=AeSfj-xZiC0LZh-Q",
            "user_id" => $admin->id,
            "channel_id" => $channel->id,
            "updated_at" => "2020-05-27 08:01:49",
            "created_at" => "2020-05-27 08:01:49",
            "id" => 1,
        ]);

        Facebookpage::create([
            "name" => "TEST page",
            "page_id" => "106788421053782",
            "category" => "Personal blog",
            "permalink" => "https://www.facebook.com/106788421053782",
            "fan_count" => 0,
            "fakebook_id" => $face->id,
            "posts" => 6,
            "updated_at" => "2020-05-27 08:01:49",
            "created_at" => "2020-05-27 08:01:49",
            "id" => 1,
        ]);

        Facebookpage::create([
            "name" => "Test 2 page",
            "page_id" => "112836070443241",
            "category" => "Personal blog",
            "permalink" => "https://www.facebook.com/112836070443241",
            "fan_count" => 0,
            "fakebook_id" => $face->id,
            "posts" => 3,
            "updated_at" => "2020-05-27 08:01:50",
            "created_at" => "2020-05-27 08:01:50",
            "id" => 2,
        ]);

        $channel = Channel::create([
            "name" => 'instagram',
            'user_id' => $admin->id,
            'channeltype_id' => rand(1, 15)
        ]);

        Instagram::create([
            "biography" => "this is come bio c019",
            "media_count" => 3,
            "username" => "cosmos09090",
            "profile_picture_url" => "https://scontent-frx5-1.xx.fbcdn.net/v/t51.2885-15/100951084_250632922687171_3114862900427096064_n.jpg?_nc_cat=100&_nc_sid=86c713&_nc_eui2=AeF3gV4KcSFug7glcNirb3ix1LNx55FsikDUs3HnkWyKQMPA_-62YpXscZ5yzYBGXrgq6mNzShJG8auD88qwfAEg&_nc_ohc=pckS1ml0j48AX-zwf1F&_nc_ht=scontent-frx5-1.xx&oh=d0ae5e91ed69397cc69f6187d94b7d1c&oe=5EF31028",
            "followers_count" => 0,
            "instagram_id" => "17841434789241030",
            "permalink" => "https://www.instagram.com/cosmos09090",
            "channel_id" => $channel->id,
            'user_id' => $admin->id
        ]);
        $channel = Channel::create([
            "name" => 'youtube',
            'user_id' => $admin->id,
            'channeltype_id' => rand(1, 15)
        ]);
        Youtube::create([
            'title' => "0909 Cosmos",
            'youtube_channel_id' => "UCElPAbo5FU_ZvHDoLaApi7w",
            'subscribers' => "1",
            "views" => "5",
            "videos" => "2",
            "comments" => "0",
            "channel_url" => "https://www.youtube.com/channel/UCElPAbo5FU_ZvHDoLaApi7w",
            "image_url" => "https://yt3.ggpht.com/a/AATXAJz-jDTu-G5k-9T4mGC0cK1Df7nsR3UowgSzlTa-=s88-c-k-c0xffffffff-no-rj-mo",
            "user_id" => $admin->id,
            "channel_id" => $channel->id,
        ]);

        $users = User::all();

        foreach ($users as $user) {
            $num = rand(1, 13);
            $token = rand(1, 3);
            if ($user->role == 'influencer') {
                Profile::create([
                    "user_id" => $user->id,
                    "gender" => Arr::random(['Male', 'Female', 'Without Gender']),
                    "birthday" => "1:1: ",
                    "image" => "/images/profiles/{$num}.jpg"
                ]);
                if ($token === 1 || $token === 3) {
                    $channel = Channel::create([
                        "name" => 'facebook',
                        'user_id' => $user->id,
                        'channeltype_id' => rand(1, 15)
                    ]);

                    $face = Fakebook::create([
                        "facebook_id" => "263157344872115",
                        "name" => "Muhammad Huraira Aamir",
                        "image" => "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=263157344872115&height=50&width=50&ext=1593158509&hash=AeSfj-xZiC0LZh-Q",
                        "user_id" => $user->id,
                        "channel_id" => $channel->id,
                    ]);

                    Facebookpage::create([
                        "name" => "TEST page",
                        "page_id" => "106788421053782",
                        "category" => "Personal blog",
                        "permalink" => "https://www.facebook.com/106788421053782",
                        "fan_count" => 0,
                        "fakebook_id" => $face->id,
                        "posts" => 6,
                    ]);

                    Facebookpage::create([
                        "name" => "Test 2 page",
                        "page_id" => "112836070443241",
                        "category" => "Personal blog",
                        "permalink" => "https://www.facebook.com/112836070443241",
                        "fan_count" => 0,
                        "fakebook_id" => $face->id,
                        "posts" => 6,
                    ]);
                }

                if ($token == 2 || $token == 3) {
                    $channel = Channel::create([
                        "name" => 'instagram',
                        'user_id' => $user->id,
                        'channeltype_id' => rand(1, 15)
                    ]);
                    Instagram::create([
                        "biography" => "this is come bio c019",
                        "media_count" => 3,
                        "username" => "cosmos09090",
                        "profile_picture_url" => "https://scontent-frx5-1.xx.fbcdn.net/v/t51.2885-15/100951084_250632922687171_3114862900427096064_n.jpg?_nc_cat=100&_nc_sid=86c713&_nc_eui2=AeF3gV4KcSFug7glcNirb3ix1LNx55FsikDUs3HnkWyKQMPA_-62YpXscZ5yzYBGXrgq6mNzShJG8auD88qwfAEg&_nc_ohc=pckS1ml0j48AX-zwf1F&_nc_ht=scontent-frx5-1.xx&oh=d0ae5e91ed69397cc69f6187d94b7d1c&oe=5EF31028",
                        "followers_count" => 0,
                        "instagram_id" => "17841434789241030",
                        "permalink" => "https://www.instagram.com/cosmos09090",
                        "channel_id" => $channel->id,
                        'user_id' => $user->id
                    ]);
                }
            } else {
                Profile::create([
                    "user_id" => $user->id,
                    "birthday" => "1:1: ",
                    "image" => "/images/profiles/brand.png"
                ]);
            }
        }


        $users = User::where('role', '=', 'influencer')->get();

        foreach ($users as $user) {
            $cross_promotion = CrossPromotion::create([
                'subscribers' => '1100',
                'date_range' => '05/16/2020 - 05/16/2020',
                'title' => 'Unser Quad Channel sucht kooperationspartner',
                'details' => 'Wir suchen Partner auf instagram um unseren Kanal wachsen zu lassen, zurzeit haben wir die 1000 Follower marke geknackt. Wir benötigen Personen die sich mit Insta auskennen und uns helfen können und uns erklären wie man auf Insta wachst.',
                'user_id' => $user->id
            ]);

            CrossPromotionChannel::create([
                "name" => 'facebook',
                "cross_promotion_id" => $cross_promotion->id
            ]);

            CrossPromotionChannel::create([
                "name" => 'instagram',
                "cross_promotion_id" => $cross_promotion->id
            ]);
        }

        $campaign = Campaign::create([
            'image' => '/images/compaigns/in.png',
            'title' => 'Testing',
            'upper_cost' => '90.23',
            'lower_cost' => '40.2',
            'user_id' => $admin->id,
            'description' => "Etiam rhoncus. Ut non enim eleifend felis pretium feugiat. Fusce ac felis sit amet ligula pharetra condimentum. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.

            In dui magna, posuere eget, vestibulum et, tempor auctor, justo. Praesent congue erat at massa. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Aenean massa. Morbi mollis tellus ac sapien.

            Vivamus aliquet elit ac nisl. Vivamus aliquet elit ac nisl. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Cras ultricies mi eu turpis hendrerit fringilla. Ut id nisl quis enim dignissim sagittis.",

            'daterange' => "05/16/2020 - 05/16/2020"
        ]);

        $campaign->social_categories()->attach([1, 2, 3]);

        $offer = Offer::create([
            'user_id' => $admin->id,
            'campaign_id' => $campaign->id,
            'details' => "Maecenas nec odio et ante tincidunt tempus. Vivamus elementum semper nisi. Praesent vestibulum dapibus nibh. Aliquam eu nunc. Vivamus euismod mauris.

            Curabitur nisi. Proin magna. Praesent venenatis metus at tortor pulvinar varius. Sed aliquam ultrices mauris. Maecenas egestas arcu quis ligula mattis placerat.",
            'budget' => "20.0",
            'fee' => "39.00",
            'status' => "Accepted"
        ]);

        OfferChannel::create([
            'channel' => "youtube",
            'budget' => "13",
            'offer_id' => $offer->id
        ]);
        OfferChannel::create([
            'channel' => "facebook",
            'budget' => "13",
            'offer_id' => $offer->id
        ]);
        OfferChannel::create([
            'channel' => "instagram",
            'budget' => "13",
            'offer_id' => $offer->id
        ]);

        YoutubeContent::create([
            'video_url' => "https://www.youtube.com/embed/8weBxA1Vpq0",
            'campaign_name' => $campaign->title,
            'offer_id' => $offer->id,
            'user_id' => $admin->id,
            'campaign_id' => $campaign->id
        ]);
        YoutubeContent::create([
            'video_url' => "https://www.youtube.com/embed/IbJBmNHsEYs",
            'campaign_name' => $campaign->title,
            'offer_id' => $offer->id,
            'user_id' => $admin->id,
            'campaign_id' => $campaign->id
        ]);

        FacebookContent::create([
            'content_url' => "https://www.facebook.com/106788421053782/posts/106821727717118/",
            'campaign_name' => $campaign->title,
            'offer_id' => $offer->id,
            'user_id' => $admin->id,
            'campaign_id' => $campaign->id
        ]);

        FacebookContent::create([
            'content_url' => "https://www.facebook.com/106788421053782/posts/106816914384266/",
            'campaign_name' => $campaign->title,
            'offer_id' => $offer->id,
            'user_id' => $admin->id,
            'campaign_id' => $campaign->id
        ]);

        InstagramContent::create([
            'content_url' => "https://www.instagram.com/p/CAo0mrqFC6r/",
            'campaign_name' => $campaign->title,
            'offer_id' => $offer->id,
            'user_id' => $admin->id,
            'campaign_id' => $campaign->id
        ]);

        InstagramContent::create([
            'content_url' => "https://www.instagram.com/p/CAcs9F2luLH/",
            'campaign_name' => $campaign->title,
            'offer_id' => $offer->id,
            'user_id' => $admin->id,
            'campaign_id' => $campaign->id
        ]);
    }
}
