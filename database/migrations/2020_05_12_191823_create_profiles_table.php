<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('gender')->nullable();
            $table->string('channel_type')->nullable();
            $table->string('telephone')->nullable();
            $table->string('birthday')->nullable('::');
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('street_house')->nullable();
            $table->string('zip')->nullable();

            $table->enum('payment_type',['paypal','bank_transfer'])->nullable();
            $table->string('paypal_email')->nullable();
            $table->string('Account_holder')->nullable();
            $table->string('IBAN')->nullable();
            $table->string('BIC')->nullable();
            
            $table->text('about_channel')->nullable();

            $table->string('image')->default('/images/general/default.jpg');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
