<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaypalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paypals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trans_id');
            $table->string('status');
            $table->string('payer_email');
            $table->string('payer_id');
            $table->string('payer_address');
            $table->string('payer_firstname');
            $table->string('payer_surname');
            $table->string('amount');
            $table->string('currency_code');

            $table->unsignedBigInteger('campaign_id');
            $table->foreign('campaign_id')->references('id')->on('campaigns')
                ->onDelete('cascade');

            $table->unsignedBigInteger('offer_id');
            $table->foreign('offer_id')->references('id')->on('offers')
                ->onDelete('cascade');  
                  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypals');
    }
}
