<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebookpagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facebookpages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('page_id');
            $table->string('category');
            $table->string('permalink');
            $table->string('fan_count');
            $table->string('posts');

            $table->unsignedBigInteger('fakebook_id');
            $table->foreign('fakebook_id')->references('id')
                ->on('channels')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facebookpages');
    }
}
