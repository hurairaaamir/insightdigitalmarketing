<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailNotificationsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void "summary"
     */
    public function up()
    {
        Schema::create('email_notifications_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('summary')->nullable()->default('1');
            $table->boolean('newProposalForPlacementCampaign')->nullable()->default(false);
            $table->boolean('newParticipantForPerformanceCampaign')->nullable()->default(false);
            $table->boolean('newRequestForGiveawayCampaign')->nullable()->default(false);
            $table->boolean('campaignContentPublished')->nullable()->default(false);
            $table->boolean('newMessage')->nullable()->default(false);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('brand_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_notifications_settings');
    }
}
