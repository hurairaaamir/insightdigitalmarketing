function chat_show(){
    if(document.getElementById('chat-btn-id').classList.contains('chat-hider')){
        document.getElementById('chat-x').classList.add('chat-hider');
        document.getElementById('chat-btn-id').classList.remove('chat-hider');
        document.getElementById('chat-id').classList.add('chat-hider');
    }else{
        document.getElementById('chat-x').classList.remove('chat-hider');
        document.getElementById('chat-btn-id').classList.add('chat-hider');
        document.getElementById('chat-id').classList.remove('chat-hider');
    }
}

$('#chat_submit').on('submit',function(event){
    event.preventDefault();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: '/chat/store',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        datatype: "json",
        success: function(response){
            let data=response.data;
            console.log(data);
            let html='';
            html=`<div class="text-section float-right">`;
            if(data.user){
                html+=`<div class="float-right"><span class="float-right">${data.user.name}</span></div>`;
            }
            html+=`                
            <div class="text-inner right_align">
                <div class="part2 text-radius2">
                    <p>${data.message}</p>
                </div>`;
            if(data.user){
                html+=`<div class="part1">
                            <img src="${data.user.profile.image}" height="24px" width="24px">
                        </div>
                    </div>
                </div>`
            }else{
                html+=`<div class="part1">
                            <img src="/images/general/default.jpg" height="24px" width="24px">
                        </div>
                    </div>
                </div>`;
            }

            document.querySelector('#chat_inner_id').innerHTML+=html;
            document.getElementById('chat_submit').reset();

        }
    });
});
