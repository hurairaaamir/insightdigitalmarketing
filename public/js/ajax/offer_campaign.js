
// +++++++++++++++++++++++++++++++++++++++++++++++ OFFERS ++++++++++++++++++++++++++++++++++++++++++++
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



function offerModel()
{
    // modelReset();
    model=document.querySelector('.com-model');
    model.classList.toggle('enter');
    document.querySelector('#page-wrap').classList.toggle('page-wrap-class'); 
}

function offerCard(id,offer_id)
{
    console.log(offer_id);
    console.log(id);
    offerModel();

    
    $.ajax({
        url: '/influencer/compaigns/'+id,
        type:'GET',
        beforeSend: function()
        {
            document.querySelector('.campaignsLoader').classList.remove('loaderhider');
        }
    })
    .done(function(data)
    {
        document.querySelector('.campaignsLoader').classList.add('loaderhider');
        campaign=data.campaign;
        channels=data.channels;
        console.log(channels);
        let otherImages=campaign.other_img;

        document.querySelector('#show-upper-cost').innerHTML='';
        document.querySelector('#show-lower-cost').innerHTML='';
        document.querySelector('#carousel-images').innerHTML="";
        document.querySelector('#show-title').innerHTML="";
        document.querySelector('#show-description').innerHTML="";
        document.querySelector('#show-socials').innerHTML="";

        document.querySelector('#show-upper-cost').innerHTML=campaign.upper_cost;
        document.querySelector('#show-lower-cost').innerHTML=campaign.lower_cost;

        let img='';
        
        img+=`<div class="full-container">    
                    <img src='${campaign.image}' >
                </div>
                `;
        if(otherImages){
            for(var i=0;i<otherImages.length;i++){
                img+=`<div class="full-container">    
                    <img src='${otherImages[i].image}' >
                </div>
                `;
            }
        }

        document.querySelector('#carousel-images').innerHTML=img;

        $('.owl-carousel').owlCarousel('destroy');

        document.querySelector('#show-title').innerHTML=campaign.title;
        document.querySelector('#show-description').innerHTML=campaign.description;
        

        var social_div=document.querySelector('#show-socials');
        var socials=campaign.campaigns_socials;
        if(socials){
            for(var i=0;i<socials.length;i++){
                social_div.innerHTML+=`
                <div class="social-image">
                    <img src='${socials[i].image}'>
                </div>
                `;
            }
        }

        $('.owl-carousel').owlCarousel({
            margin:10,
            responsive:{
                0:{
                    items:1
                },
                700:{
                    items:2
                },
                1200:{
                    items:2
                }
            }
        });
        // document.querySelector('.model-inner').innerHTML=data.campaign;
    })

    $.ajax({
        url: '/influencer/offer/'+offer_id,
        type:'GET',
    })
    .done(function(response)
    {  
       document.querySelector('.lower-detail').innerHTML=response.data;
    });

    document.querySelector('.campaignsLoader').classList.remove('loaderhider');
}