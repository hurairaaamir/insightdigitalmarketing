let model=false;
function OfferUpdate(id,status){
    console.log(id,status);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    if(confirm(`Are you Sure you want to ${status} this Offer. You will not be able to revert this action`)){
        $.ajax({
            type: "GET",
            url: '/influencer/offer/update/'+id+'/'+status,
            success: function(data){
                console.log('status_'+id);
                document.getElementById('status_'+id).innerHTML=status;
            },
            error:function(data){
                // 
            }
        });
    }
}
$('#show-owl-carousel').owlCarousel({
    margin:5,
    responsive:{
        0:{
            items:1
        },
        700:{
            items:1 
        },
        1200:{
            items:1
        }
    }
});

function showModel(id){
    model=!model;
    model=document.querySelector('.com-model');
    model.classList.toggle('enter');
    document.querySelector('#page-wrap').classList.toggle('page-wrap-class');
    if(model){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "GET",
            url: '/influencer/campaign/offer/'+id,
            success: function(response){
                console.log(response.data);
                document.querySelector('.profile-card').innerHTML=response.data;
            },
            error:function(data){
                // 
            }
        });
    }

}