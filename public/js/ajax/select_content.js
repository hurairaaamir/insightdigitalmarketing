
function selectCampaign(type) {
    var campaignsSelect = document.getElementById("campaignsListing");
    var id = campaignsSelect.options[campaignsSelect.selectedIndex].value;
    let container=document.getElementById('content_container');
    container.innerHTML=`<div class="center_out">
        <img src="/images/gifs/campaignsLoader.svg">
    </div>`;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/influencer/content/campaign/'+id+'/'+type,
        type:'GET',
    })
    .done(function(response)
    {   
        container.innerHTML=response.data;
        window.instgrm.Embeds.process();
        if(type=='instagram'){
            window.instgrm.Embeds.process();
        }
    });
}
