let page=0;
let social='all';
let repeat=0;

$(window).scroll(function() 
{
    clearTimeout($.data(this,'scrollCheck'));
    $.data(this,'scrollCheck',setTimeout(function(){

        if($(window).scrollTop() + $(window).height() + 100 >= $(document).height()) {
    
            page++;
            loadMorePromotion(page,social);
        }
    },350)) 
});

function loadMorePromotion(page,social)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/influencer/cross/loadmore?page='+page,
        type:'POST',
        data:{social:social},
        beforeSend: function()
        {
            document.querySelector('#loadmore-loader').classList.remove('loaderhider');
        }
    })
    .done(function(response)
    {
        if(response.data == ""){
            if(repeat<3){
                loadMorePromotion(page,social);
            }
            repeat++;
            return;
        }
        let tag=document.getElementById('promo-data');  
        tag.innerHTML+=response.data;      
        document.querySelector('#loadmore-loader').classList.add('loaderhider');
    })
}

function complete_details(id)
{
    console.log(id);
    document.getElementById('crossPromo-data').innerHTML='<div class="center_out"><img src="/images/gifs/bluePreloader.svg">';


    $.ajax({
        type: "GET",
        url: '/influencer/cross/show/'+id,
        contentType: false,
        cache: false,
        processData: false,

        success: function(response){
            document.getElementById('crossPromo-data').innerHTML=response.data;
        },
        error:function(data){
            
        }
    });

}
function getPromosocials(){
    var select= document.getElementById("socialSelect");
    social = select.options[select.selectedIndex].value;   
    page=0;

    loadMorePromotion(page,social);
}

let crossPromotionId='';

function select_form(id,username){
    console.log(id);
    crossPromotionId=id;
    document.getElementById('username-model').innerHTML=username;

}