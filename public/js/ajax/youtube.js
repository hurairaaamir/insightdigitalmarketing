let containerId;
let selectedVideos=[];
let gettingchannels=false;
let offerId;
  function getContent(container,id){
    gettingchannels=true;
    offerId=id;
    document.getElementById(container).innerHTML=`
        <div class="center_out">
            <p>Please wait ..</p>
            <img src="/images/gifs/line.svg">
        </div>
    `;
    let btns=document.querySelectorAll('.choose-content');
    btns.forEach(element=>{
      if(element.id!='btn-'+id){
        element.setAttribute('disabled',true);
      }
    })
    containerId=container;
    console.log(containerId);
    handleClientLoad()
  }
function addVideo(id,video){
    let element=document.getElementById(id);
    if(element.classList.contains("youtube-active")){
      element.classList.remove("youtube-active");
      let index=selectedVideos.indexOf(video);
      selectedVideos.splice(index,1);
      console.log(selectedVideos);
      return;
    }else{
      console.log(selectedVideos);
      element.classList.add("youtube-active");
      selectedVideos.push(video);
    }
  }
  function youtubeSubmit(){
    if(selectedVideos.length==0){
      return ;
    }
    if(confirm('Are you sure ? Be sure to selected the Right content this actions can not be changed')){
        console.log(selectedVideos);
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    
        $.ajax({
            type: "POST",
            url: '/influencer/content/youtube',
            data: {selectedVideos,offer_id:offerId},
    
            success: function(response){
               console.log(response.data);
               location.reload();
            },
            error:function(response){
            }
        })
    }
  }

  const CLIENT_ID =
    '812304486538-1q7qb3ocluf11atmmtamdialv1rvj5v2.apps.googleusercontent.com';
  const DISCOVERY_DOCS = [
    'https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest',
  ];
  const SCOPES = 'https://www.googleapis.com/auth/youtube.readonly';
  
  
  
  
  // Load auth2 library
  function handleClientLoad() {
    gapi.load('client:auth2', initClient);
  }
  
  // Init API client library and set up sign in listeners
  function initClient() {
    gapi.client
      .init({
        discoveryDocs: DISCOVERY_DOCS,
        clientId: CLIENT_ID,
        scope: SCOPES,
      })
      .then(() => {
        // Listen for sign in state changes
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
        // Handle initial sign in state
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());

        handleAuthClick();
      });
  }
  
  // Update UI sign in state changes
  function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
      getChannel();
    } 
  }
  
  // Handle login
  function handleAuthClick() {
    gapi.auth2.getAuthInstance().signIn();
  }
  
  // Get channel from API
  function getChannel() {
  
    gapi.client.youtube.channels
      .list({
        part: 'snippet,contentDetails,statistics',
        mine: true,
      })
      .then((response) => {
        
        console.log(response);
        const channel = response.result.items[0];
        if(!gettingchannels){
            youtubeStore(channel);
        }else{
            console.log(channel.snippet);
            console.log("url:"+channel.snippet.customUrl);
            const playlistId = channel.contentDetails.relatedPlaylists.uploads;
            requestVideoPlaylist(playlistId);
        }
      })
      .catch((err) => alert('No Channel By That Name'));
  }
  
  // Add commas to number
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  
  function youtubeStore(channel){

      let title=channel.snippet.title;
      let youtube_channel_id=channel.id;
      let subscribers=numberWithCommas(channel.statistics.subscriberCount);
      let views=numberWithCommas(channel.statistics.viewCount);
      let videos=numberWithCommas(channel.statistics.videoCount);
      let comments=numberWithCommas(channel.statistics.commentCount);
      let channel_url=`https://youtube.com/${channel.snippet.customUrl}`;
      let image_url=channel.snippet.thumbnails.default.url;
    
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
          url: '/influencer/channel/youtube/login',
          type:'POST',
          data:{title,youtube_channel_id,subscribers,views,videos,comments,channel_url,image_url},
      })
      .done(function(response)
      {   
          let data=response.data;
          let element=document.getElementById('youtube-head');
          if(element){
            element.innerHTML=`
            <a href="${data.channel_url}" target="_blank">
                <div class="channel-div ml-2">
                    <span>
                        <img src="${data.image_url}" class="ml-1 img-circle">
                    </span>
                    <span>${data.title}</span>
                </div>
            </a>`
          }
          console.log(data);
      });
  }
  function requestVideoPlaylist(playlistId) {
    const requestOptions = {
      playlistId: playlistId,
      part: 'snippet',
      maxResults: 10,
    };
  
    const request = gapi.client.youtube.playlistItems.list(requestOptions);
  
    request.execute((response) => {
      console.log(response);
      const playListItems = response.result.items;
      if (playListItems) {
        let output = `<br><h4 class="center-align">Latest Videos</h4>
                      <p>Select your content that you have made for this content and submit it</p>`;    
  
        // Loop through videos and append output
        playListItems.forEach((item) => {
          const videoId = item.snippet.resourceId.videoId;
  
          output +=`
            <div class="youtube-hover">
              <div class="youtube-container">
                <input type="checkbox" class="youtube-select" onclick="addVideo('myYoutube${videoId}','https://www.youtube.com/embed/${videoId}')" name="youtubeContent[]" value="https://www.youtube.com/embed/${videoId}">
                <iframe id="myYoutube${videoId}" width="768" height="480" src="https://www.youtube.com/embed/${videoId}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
            </div>
          `;
        });
        output+=`<button class="btn btn-danger btn-sm" style="margin-top:100px;margin-bottom:20px;" onclick="youtubeSubmit()">Submit Content</button>`
        // Output videos
        document.getElementById(containerId).innerHTML = output;
      } else {
        videoContainer.innerHTML = 'No Uploaded Videos';
      }
    });
  }
  