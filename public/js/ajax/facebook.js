var container_id;
var selected_page='';

var offer_id;
var page_id='';
function showPages(conId,offerId){
    container_id=conId;
    offer_id=offerId;
    let divs=document.querySelectorAll('.pages_con'+offerId);
    
    divs.forEach(element=>{
        element.classList.remove('disapper');
    });
}

function addPage(pg_id,face_id){
    let cons=document.querySelectorAll('.face_tag');

    let face_div=document.getElementById(face_id);

    if(selected_page==face_id){
        face_div.classList.add('page-opacity');
        selected_page='';
        page_id='';
        return ;
    }
    cons.forEach(element=>{
        if(element.id!=face_id){
            element.classList.add('page-opacity');
            console.log("add opacity");
        }else{
            element.classList.remove('page-opacity');
            page_id=pg_id;
            selected_page=face_id;
        }
    });

    console.log('');
    console.log(page_id);

}


















window.fbAsyncInit = function() {
    FB.init({
        appId      : '228877818409800',
        cookie     : true,
        xfbml      : true,
        version    : 'v7.0'
    });
    
    FB.AppEvents.logPageView();   
};

(function(d, s, id){
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) {return;}
 js = d.createElement(s); js.id = id;
 js.src = "https://connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));





var uid ;
var access_token;
var permissions ;

// attach login click event handler
function facebookLogin(){
    if(page_id){
        FB.login(
            processLoginClick, {
                scope:'public_profile,pages_manage_ads,pages_manage_metadata, pages_read_engagement,pages_read_user_content,pages_manage_posts,pages_manage_engagement',
                return_scopes: true
            });  
    }else{
        document.getElementById(container_id).innerHTML='<p style="margin-top:20px;color:red">you have not selected any page.</p>'    
    }
}


// function to send uid and access_token back to server
// actual permissions granted by user are also included just as an addition
function processLoginClick (response) {    
    uid = response.authResponse.userID;
    access_token = response.authResponse.accessToken;
    permissions = response.authResponse.grantedScopes;
    facebook_submit();
}


function facebook_submit(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: '/influencer/content/facebook',
        data:{uid,access_token,page_id},
        success: function(response){
            console.log(response.data);
            // let permalinks=response.data;
            
            // console.log(permalinks);
            // let container=document.getElementById(container_id);
            // for(var i=0;i<permalinks.length;i++){
            //     console.log(permalinks[i].permalink_url);
            //     container.innerHTML+=`  
                
            //     `;

            //     $("#"+container_id).load(window.location.href + " #"+container_id );
            // }
            let posts=response.data;

            var method = "post";
            var url="/influencer/content/facebookRedirect";
            var form = document.createElement("form");

            form.setAttribute("method", method);
            form.setAttribute("action", url);
            form.setAttribute('id','ajaxInstagram')

            var token = document.createElement("input");
            token.setAttribute("type", "hidden");
            token.setAttribute("name", `_token`);
            token.setAttribute("value", $('meta[name="csrf-token"]').attr('content'));
            form.appendChild(token);

            var offer = document.createElement("input");
            offer.setAttribute("type", "hidden");
            offer.setAttribute("name", `offer_id`);
            offer.setAttribute("value", offer_id);
            form.appendChild(offer);
            for(let i=0;i<posts.length;i++) {   

                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", `data[${i}]`);
                hiddenField.setAttribute("value", posts[i].permalink_url);
                form.appendChild(hiddenField);

            }
            var btn=document.createElement("button");
            btn.setAttribute('type','submit');
            form.appendChild(btn);
            document.getElementById('form-append').appendChild(form);
    
            btn.click();
        },
        error:function(response){
        
        }
    })

}
// const inlineScript = document.createElement('script')
// script.innerHTML = 'alert("Inline script loaded!")'
// document.head.append(script);
let facePost=[];
function addfacebook(id,face){
    let element=document.getElementById(id);
    if(element.classList.contains("face-active")){
      element.classList.remove("face-active");
      let index=facePost.indexOf(face);
      facePost.splice(index,1);
      console.log(facePost);
      return;
    }else{
      console.log(facePost);
      element.classList.add("face-active");
      facePost.push(face);
    }
  }

  function facebookSubmit(offer_id){
    if(facePost.length==0){
      return ;
    }
    if(confirm('Are you sure ? Be sure to selected the Right content this actions can not be changed')){
        console.log(facePost);
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
        $.ajax({
            type: "POST",
            url: '/influencer/content/facebook/store',
            data: {contents:facePost,offer_id:offer_id},
    
            success: function(response){
               console.log(response.data);
               window.location.replace('/influencer/content/index/facebook');
            },
            error:function(response){
            }
        })
    }
  }