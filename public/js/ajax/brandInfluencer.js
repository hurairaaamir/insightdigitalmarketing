//-------------------------------------------------------------- feilds
let social = "all";
let gender;
let category;
let followers;
let last_id;

//------------------------------------------------------------- setters

function set_category(value) {
    console.log(value);
    id = "id_" + value;
    console.log(id);
    socials = document.querySelectorAll(".social-hover");

    socials.forEach(element => {
        console.log(element.classList.contains("in-active"));
        if (element.id == id) {
            document.getElementById(id).classList.remove("in-active");
            social = value;
            console.log("selected:" + social);
        } else if (!element.classList.contains("in-active")) {
            element.classList.add("in-active");
        }
    });
}
function reset() {
    socials = document.querySelectorAll(".social-hover");
    socials.forEach(element => {
        element.classList.add("in-active");
    });
    social = "all";
    gender = "";
    category = "";
    followers = "";
    loadMoreData(0);
}

// -------------------------------------------------------------Ajax call

function loadMoreData(id) {
    last_id = id;
    btn = document.getElementById("load-btn");
    if (btn) {
        btn.innerHTML =
            'loading <img src="/images/gifs/circleLoader.svg" style="width:30px;height:30px;">';
    }
    $.ajax({
        type: "GET",
        url: "/brand/influencer/load_more_data/" + last_id,

        success: function(response) {
            let btn = document.getElementById("loadmore-btn");
            let btn_error = document.querySelector(".no-data");
            if (btn) {
                btn.remove();
            }
            if (btn_error) {
                btn_error.remove();
            }
            let data = response.data;
            document.getElementById("influencer-cards").innerHTML += data;
        },
        error: function(response) {}
    });
}
function execute() {
    document.getElementById("execute-btn").click();
}
$("#execute_search").on("submit", function(event) {
    event.preventDefault();
    let searchFormData = $("form").serializeArray();

    console.log(social);
    console.log(gender);
    btn = document.getElementById("load-btn");
    if (btn) {
        btn.innerHTML =
            'loading <img src="/images/gifs/circleLoader.svg" style="width:30px;height:30px;">';
    }
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });
    // document.querySelector('#circle-loader').classList.remove('loaderhider');

    $.ajax({
        type: "POST",
        url: "/brand/influencer/execute_search/" + social,
        data: new FormData(document.querySelector("form")),
        contentType: false,
        cache: false,
        processData: false,
        datatype: "json",

        success: function(response) {
            console.log(response);
            btn = document.getElementById("loadmore-btn");
            if (btn) {
                btn.remove();
            }

            document.getElementById("influencer-cards").innerHTML =
                response.data;
        },
        error: function(data) {}
    });
});
