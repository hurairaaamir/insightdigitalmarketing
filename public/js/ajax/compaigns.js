
var social='all';
var page = 1;
var budget=1;
var type='all';

// ------------------------------------------------------------------------------------------------
//---------------------------------------------SETTERS--------------------------------------

function setsocial(){
    var select= document.getElementById("socialSelect");
    document.getElementById('card-data').innerHTML='';
    social = select.options[select.selectedIndex].value;
}
function setbudget(){
    var select= document.getElementById("budgetSelect");
    document.getElementById('card-data').innerHTML='';
    budget = select.options[select.selectedIndex].value;
}

function resetpage(value){
    page=0;
}

// ------------------------------------------------------------------------------------------------
// -----------------------------------------EVENTS--------------------------------------------

$(window).scroll(function() {
    clearTimeout($.data(this,'scrollCheck'));
    $.data(this,'scrollCheck',setTimeout(function(){

        if($(window).scrollTop() + $(window).height() + 100 >= $(document).height()) {
    
            page++;
            loadMoreData(page,social,budget,type);
        }
    },350)) 
});

function getbysocials(){
    resetpage();
    setsocial()
    loadMoreData(page,social,budget,type);
}
function getbybudget(){
    resetpage();
    setbudget()
    loadMoreData(page,social,budget,type);
}

// ------------------------------------------------------------------------------------------------
// -------------------------------------------------FUNCTIONS--------------------------------------

function loadMoreData(page,social,budget,type){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/pagination?page=' + page,
        type:'POST',
        data:{social:social,budget:budget,type:type},
        beforeSend: function()
        {
            document.querySelector('.campaignsLoader').classList.remove('loaderhider');
        }
    })
    .done(function(data)
    {
        // console.log(data.campaigns);
        if(data.campaigns == ""){
            resetpage();
            loadMoreData(page,social,budget,type);
            return;
        }
        
        document.getElementById('card-data').innerHTML+=data.campaigns;
        document.querySelector('.campaignsLoader').classList.add('loaderhider');
    })
}






























// +++++++++++++++++++++++++++++++++++++++MODEL FUNCTION++++++++++++++++++++++++++++++++++++++++++=

let campaign;

function showModel(){
    modelReset();
    model=document.querySelector('.com-model');
    model.classList.toggle('enter');
    document.querySelector('#page-wrap').classList.toggle('page-wrap-class')
}
function CompleteCard(id){
    document.querySelector('.campaignsLoader').classList.remove('loaderhider');
    showModel();

   
    $.ajax({
        url: '/influencer/compaigns/'+id,
        type:'GET',
        beforeSend: function()
        {
            document.querySelector('.campaignsLoader').classList.remove('loaderhider');
        }
    })
    .done(function(data)
    {
        campaign=data.campaign;
        channels=data.channels;
        console.log(channels);
        let otherImages=campaign.other_img;

        document.querySelector('#show-upper-cost').innerHTML='';
        document.querySelector('#show-lower-cost').innerHTML='';
        document.querySelector('#carousel-images').innerHTML="";
        document.querySelector('#show-title').innerHTML="";
        document.querySelector('#show-description').innerHTML="";
        document.querySelector('#show-socials').innerHTML="";

        document.querySelector('#show-upper-cost').innerHTML=campaign.upper_cost;
        document.querySelector('#show-lower-cost').innerHTML=campaign.lower_cost;

        let img='';
        
        img+=`<div class="full-container">    
                    <img src='${campaign.image}' >
                </div>
                `;
        if(otherImages){
            for(var i=0;i<otherImages.length;i++){
                img+=`<div class="full-container">    
                    <img src='${otherImages[i].image}' >
                </div>
                `;
            }
        }

        document.querySelector('#carousel-images').innerHTML=img;

        $('.owl-carousel').owlCarousel('destroy');

        document.querySelector('#show-title').innerHTML=campaign.title;
        document.querySelector('#show-description').innerHTML=campaign.description;
        

        var social_div=document.querySelector('#show-socials');
        var socials=campaign.campaigns_socials;
        if(socials){
            for(var i=0;i<socials.length;i++){
                social_div.innerHTML+=`
                <div class="social-image">
                    <img src='${socials[i].image}'>
                </div>
                `;
            }
        }

        $('.owl-carousel').owlCarousel({
            margin:10,
            responsive:{
                0:{
                    items:1
                },
                700:{
                    items:2
                },
                1200:{
                    items:2
                }
            }
        });

        
        lowerOfferSection(campaign,channels);

        // document.querySelector('.model-inner').innerHTML=data.campaign;
        // document.querySelector('.campaignsLoader').classList.add('loaderhider');
    })
}

let budgetIds=[];
let totalbudget=0;

function lowerOfferSection(campaign,channels){

    var socials=campaign.social_categories;
    document.getElementById('campaign_id').value=campaign.id;
    let offer=document.getElementById('offer-data');

    socials.forEach(social => {
        let status=false;
        let active_state='comp_in_active';
        channels.forEach(channel=>{
            if(channel.name==social.name){
                status=true;
                active_state='';
            }
        })
        var html=`<div class="offer reset_div" id='offer_${social.id}'>
                        <div class="of1">
                            <div class="social-image">
                                ${social.image} <span>${social.name}</span>
                            </div>
                        </div>
                        <div class="of2">
                            <select class="form-control" class="feild_${social.id}">
                                <option>${social.name}</option>
                            </select>
                        </div>
                        <div class="of2">
                            <h3>Publish Date Range:</h3>
                            <p>${social.created_at}</p>
                            <h3>Submission deadline:</h3>
                            <p>${campaign.daterange}</p>
                        </div>
                        <div class="of3">
                            <input type="number" class="feild_${social.id} form-control" min="0" onkeyup="EnteringBudget('budget_${social.id}')" onchange="EnteringBudget('budget_${social.id}')" class="form-control" value="0" id='budget_${social.id}' data="${social.name}"> <i class="fas fa-euro-sign m-2"></i>
                                                            
                            <button class="btn btn-danger" id="submit_${social.id}" onclick="inactivate('offer_${social.id}','budget_${social.id}','feild_${social.id}','submit_${social.id}')"> <i class="fas fa-trash-alt" id="fa-icon"></i> </button>
                        </div>
                    </div>`
            offer.innerHTML+=html;
            if(status){
                let id=`budget_${social.id}`;
                budgetIds.push(id);
            }else{
                let btn=document.getElementById('submit_'+social.id);
                btn.click();
                btn.remove();

            }
    });
}










// +++++++++++++++++++++++++++++++++++++++++++++++++++BUDGET+++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function EnteringBudget(id){
    totalbudget=0;
    // console.log(budgetIds);
    budgetIds.forEach(id=>{
        
        let budget=parseInt(document.getElementById(id).value);
        totalbudget=parseInt(totalbudget)+parseInt(budget);
    });
    // console.log('total budget='+totalbudget);
    document.getElementById('total_budget').value=totalbudget;
    
    DistributeBuget(totalbudget);
}


function changeTotalBudget(){
    totalbudget=parseInt(document.getElementById('total_budget').value);
    let half=totalbudget/budgetIds.length;
    budgetIds.forEach(id=>{
        document.getElementById(id).value=half;
    });
    
    DistributeBuget(totalbudget);
}

function DistributeBuget(totalbudget){
    var fee=(15*totalbudget)/100;
    var earning=totalbudget-fee;

    document.getElementById('fee').innerHTML=fee;
    document.getElementById('fee_id').value=fee;
    document.getElementById('amount_disbursed').innerHTML=earning;
}

function inactivate(offer_id,budget_id,feild_class,submit_button){
    let element=document.getElementById(offer_id);
    element.classList.toggle('comp_in_active');
    if(element.classList.contains('comp_in_active')){

        document.getElementById(submit_button).classList.add('btn-success');
        document.getElementById(submit_button).classList.remove('btn-danger');
        let btn=document.getElementById(submit_button);
        btn.innerHTML='';
        btn.innerHTML='<i class="fas fa-plus"></i>';

        // .classList.add('fa-plus');
        // document.getElementById('fa-icon').classList.remove('fa-trash-alt');

        let id=budgetIds.indexOf(budget_id);
        let value=document.getElementById(budget_id).value;

        if(id!=-1){
            budgetIds.splice(id,1);
            totalbudget=totalbudget-parseInt(value);
        }

        feilds=document.querySelectorAll('.'+feild_class);
        // console.log(feilds);
        feilds.forEach(feild=>{
            feild.disabled = true;
        });


    }else{
        document.getElementById(submit_button).classList.remove('btn-success');
        document.getElementById(submit_button).classList.add('btn-danger');
        let btn=document.getElementById(submit_button);
        btn.innerHTML='';
        btn.innerHTML='<i class="fas fa-trash-alt" ></i>';

        // document.getElementById('fa-icon').classList.remove('fa-plus');

        // document.getElementById('fa-icon').classList.add('fa-trash-alt');

        budgetIds.push(budget_id);
        let value=document.getElementById(budget_id).value;

        totalbudget=totalbudget+parseInt(value);

        feilds=document.querySelectorAll('.'+feild_class);

        feilds.forEach(feild=>{
            feild.disabled = false;
        });

    }

    DistributeBuget(totalbudget);

    document.getElementById('total_budget').value=totalbudget;
}

function modelReset(){
    budgetIds=[];
    totalbudget=0;
    resetsDivs=document.querySelectorAll('.reset_div');
    resetsDivs.forEach(div=>{
        div.remove();
    });
    document.getElementById('total_budget').value=0;
    document.getElementById('fee').innerHTML='';
    document.getElementById('amount_disbursed').innerHTML='';
}

























// ************************************************** offer submit ************************************
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


$('#offer-submit').on('submit',function(event){
    event.preventDefault();
    
    if(budgetIds.length==0 || totalbudget==0){
        if(budgetIds.length==0){
            document.getElementById('offer-errors').innerHTML='<p>all offers are inactivated</p>';
            return;
        }
        if(totalbudget==0){
            document.getElementById('offer-errors').innerHTML='<p>you have not set your budget</p>'
            return;
        }
    }
    

    document.querySelector('#circle-loader').classList.remove('loaderhider');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: '/influencer/offer',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        datatype: "json",

        success: function(data){
            submit_offer_channels(data.data.id);
            document.querySelector('#circle-loader').classList.add('loaderhider');
            document.getElementById('offer-errors').innerHTML=`<p style="color:green;">Offer sent successfully</p>`;
            console.log(document.getElementById('textarea_detail'));
            // $('#textarea_detail');
            document.getElementById('offer-submit').reset();
        },
        error:function(data){
            document.querySelector('#circle-loader').classList.add('loaderhider');
            document.getElementById('offer-errors').innerHTML=`<p style="color:red;">${data.responseJSON.errors.budget[0]}</p>`;
            document.getElementById('offer-errors').innerHTML=`<p style="color:red;">${data.responseJSON.errors.details[0]}</p>`;
            
        }
    });
});


function submit_offer_channels(offer_id)
{
    budgetIds.forEach(id=>{
        let data=document.getElementById(id);
        // offer_id=id.substr(7,id.length);
        console.log(offer_id);
        budget=data.value;
        channel=data.getAttribute('data');
        console.log(channel);
        console.log(budget);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        $.ajax({
            type: "POST",
            url: '/influencer/offer/channel',
            data: {budget,channel,offer_id},
    
            success: function(data){
                console.log(data.data);
            },
            error:function(data){
        
            }
        });
    })
}
