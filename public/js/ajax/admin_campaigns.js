var page = 1;
var type = '';


$(window).scroll(function() {
    clearTimeout($.data(this,'scrollCheck'));
    $.data(this,'scrollCheck',setTimeout(function(){

        if($(window).scrollTop() + $(window).height() + 100 >= $(document).height()) {
    
            page++;
            loadMoreCampaign(page,type);
        }
    },350)) 
});

function loadMoreCampaign(page,type){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/admin/campaigns/loadmore?page=' + page,
        type:'POST',
        data:{type:type},
        beforeSend: function()
        {
            document.querySelector('#blue-loader').classList.remove('loaderhider');
        }
    })
    .done(function(data)
    {
        if(data.campaigns == ""){
                     
            resetpage();
            loadMoreData(page,type);
            return;
        }
        document.querySelector('#activity').innerHTML+=data.campaigns;
        document.querySelector('#blue-loader').classList.add('loaderhider');

        
    })
}


function getCampaigns(value){
    type=value;
    page=1;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '/admin/campaigns/loadmore?page=' + page,
        type:'POST',
        data:{type:type},
        beforeSend: function()
        {
            document.querySelector('#blue-loader').classList.remove('loaderhider');
        }
    })
    .done(function(data)
    {
        if(data.campaigns == ""){
                     
            resetpage();
            loadMoreData(page,type);
            return;
        }
        document.querySelector('#activity').innerHTML=data.campaigns;
        document.querySelector('#blue-loader').classList.add('loaderhider');
        
    })       
}

function readmore(id){
    console.log(id);
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });
    $.ajax({
        url: '/admin/campaigns/readmore/'+id,
        type:'GET',
        beforeSend: function()
        {
            document.querySelector('#sm_blue_'+id).classList.remove('loaderhider');
        }
    })
    .done(function(data)
    {
        
        console.log(data.description);
        document.querySelector('#des_'+id).innerHTML=data.description;
        
    })
}
function deleteCampaign(id){

    if(confirm('Are you sure you want to perform this action, This is a highly critical action')){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/admin/campaigns/delete/'+id,
            type:'DELETE',
            beforeSend: function()
            {
                // document.getElementById('del_btn_'+id).innerHTML="deleting....";
            }
        })
        .done(function(data)
        {
            document.getElementById('campaign_'+id).remove();
            // console.log(data.description);
            // document.querySelector('#des_'+id).innerHTML=data.description;
            
        })
    }
}
function getAllOffers(id){

    if(id!=''){
        $.ajax({
            url: '/admin/campaigns/offers/'+id,
            type:'GET',
            beforeSend: function()
            {
                document.getElementById('blue_sm_loader_'+id).classList.remove('loaderhider');
            }
        })
        .done(function(data)
        {
            console.log('offer_data_'+id);
            document.getElementById('offer_data_'+id).innerHTML=data.data;
            
        })
    }

}