window.fbAsyncInit = function() {
    FB.init({
        appId      : '228877818409800',
        cookie     : true,
        xfbml      : true,
        version    : 'v7.0'
    });
    
    FB.AppEvents.logPageView();   
};

(function(d, s, id){
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) {return;}
 js = d.createElement(s); js.id = id;
 js.src = "https://connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


$(document).ready(function() {
    // attach login click event handler
    $("#btn-login").click(function(){
        FB.login(
            processLoginClick, {
                scope:'public_profile,pages_manage_ads,pages_manage_metadata, pages_read_engagement,pages_read_user_content,pages_manage_posts,pages_manage_engagement',
                return_scopes: true
            });  
    });
});

// function to send uid and access_token back to server
// actual permissions granted by user are also included just as an addition
function processLoginClick (response) {    
    var uid = response.authResponse.userID;
    var access_token = response.authResponse.accessToken;
    var permissions = response.authResponse.grantedScopes;
    document.getElementById('Facebook_uid_id').value=uid;
    document.getElementById('Facebook_access_token_id').value=access_token;
    document.getElementById('Facebook_permissions_id').value=permissions;

    document.getElementById('facebook-submit').click();
}


$('#ajaxFacebook').on('submit',function(event){
    event.preventDefault();

    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $.ajax({
        type: "POST",
        url: '/influencer/channel/facebook/login',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,

        success: function(response){
            console.log('here');
            if(response.data){
                let facebook=response.data;
                document.getElementById('face-head').innerHTML=`
                    <a href="/influencer/channel/facebook/pages">
                        <div class="channel-div ml-2">
                            <span>
                                <img src="${facebook.image}" class="ml-1 img-circle">
                            </span>
                            <span>${facebook.name}</span>
                        </div>
                    </a>
                `;
            }
        },
        error:function(response){
            if(response.error){
                let error=response.error;
                document.getElementById('face-head').innerHTML=`
                <p style="color:red">${error}</p>`;
            }
        }
    })

});


// function to post any data to server

function intagram_login(){
    console.log("here");
    FB.login(
    processInsatalogin, {
        scope:'public_profile,instagram_basic,pages_show_list,business_management,instagram_manage_insights,pages_show_list,pages_manage_metadata,pages_read_engagement,pages_manage_ads',
        return_scopes: true
    });  
}
function processInsatalogin(response){
    var uid = response.authResponse.userID;
    var access_token = response.authResponse.accessToken;
    var permissions = response.authResponse.grantedScopes;

    document.getElementById('uid_id').value=uid;
    document.getElementById('access_token_id').value=access_token;
    document.getElementById('permissions_id').value=permissions;

    document.getElementById('intagram-submit').click();
   
        
}


$('#ajaxInstagram').on('submit',function(event){
    event.preventDefault();
    console.log('submitting');
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    $.ajax({
        type: "POST",
        url: '/influencer/channel/instagram/login',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,

        success: function(response){
            console.log('here');
            console.log(response.data);
            let div=document.getElementById('insta-head').innerHTML;
            if(response.data || !div){
                let instagram=response.data;
                document.getElementById('insta-head').innerHTML=`
                <a href="${instagram.permalink}">
                    <div class="channel-div ml-2">
                        <span>
                            <img src="${instagram.profile_picture_url}" class="ml-1 img-circle">
                        </span>
                        <span>${instagram.username}</span>
                    </div>
                </a>`;
            }
            if(response.error){
                let error=response.error;
                document.getElementById('insta-head').innerHTML=`
                <p style="color:red">${error}</p>`;
            }
            
        },
        error:function(response){
            document.getElementById('insta-head').innerHTML+=`<pstyle="color:red">${response.error[0]}</p>`;
        }
    })

});




 // var data = { uid:uid, 
    //              access_token:access_token, 
    //              permissions:permissions 
    //            };
    // console.log('user_id'+uid);
    // console.log('access_token'+access_token);
    // console.log('permissions'+access_token);   
    // console.log('data'+data);
    // ajaxInstagram(data);

    // var method = "post";
    // var url="/channel/instagram/login"
    // var form = document.createElement("form");
    // form.setAttribute("method", method);
    // form.setAttribute("action", url);
    // form.setAttribute('id','ajaxInstagram')
    // for(var key in data) {
    //     if(data.hasOwnProperty(key)) 
    //     {
    //         var hiddenField = document.createElement("input");
    //         hiddenField.setAttribute("type", "hidden");
    //         hiddenField.setAttribute("name", key);
    //         hiddenField.setAttribute("value", data[key]);
    //         form.appendChild(hiddenField);
    //     }
    // }
    // var btn=document.createElement("button");

    // btn.setAttribute('type','submit');
    // btn.innerHTML='send';
    // form.appendChild(btn);
    
    // document.getElementById('form-append').appendChild(form);
    
    // btn.click();

    // document.getElementById('ajaxInstagram').submit();


