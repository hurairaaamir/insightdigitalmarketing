var uid;
var access_token;
var container_id;
var offer_id;
function intagram_login(conId,offerId){
    container_id=conId;
    offer_id=offerId;
    
    FB.login(
    processInsatalogin, {
        scope:'public_profile,instagram_basic,pages_show_list,business_management,instagram_manage_insights,pages_show_list,pages_manage_metadata,pages_read_engagement,pages_manage_ads',
        return_scopes: true
    });  
}
function processInsatalogin(response){
    uid = response.authResponse.userID;
    access_token = response.authResponse.accessToken;
    instagramContent();       
}


function instagramContent(){
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
    $.ajax({
        type: "POST",
        url: '/influencer/content/instagram',
        data: {uid,access_token},
        success: function(response){

            let medias=response.data;

            var method = "post";
            var url="/influencer/content/instagramRedirect";
            var form = document.createElement("form");

            form.setAttribute("method", method);
            form.setAttribute("action", url);
            form.setAttribute('id','ajaxInstagram')

            var token = document.createElement("input");
            token.setAttribute("type", "hidden");
            token.setAttribute("name", `_token`);
            token.setAttribute("value", $('meta[name="csrf-token"]').attr('content'));
            form.appendChild(token);

            var offer = document.createElement("input");
            offer.setAttribute("type", "hidden");
            offer.setAttribute("name", `offer_id`);
            offer.setAttribute("value", offer_id);
            form.appendChild(offer);
            
            for(let i=0;i<medias.length;i++) {

                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", `data[${i}]`);
                hiddenField.setAttribute("value", medias[i].permalink);
                form.appendChild(hiddenField);

            }
            var btn=document.createElement("button");
            btn.setAttribute('type','submit');
            form.appendChild(btn);
            document.getElementById('form-append').appendChild(form);
    
            btn.click();
        },
        error:function(response){
    
        }
    });
}    
let instaPost=[];
function addInsta(id,insta){
    let element=document.getElementById(id);
    if(element.classList.contains("insta-active")){
      element.classList.remove("insta-active");
      let index=instaPost.indexOf(insta);
      instaPost.splice(index,1);
      console.log(instaPost);
      return;
    }else{
      console.log(instaPost);
      element.classList.add("insta-active");
      instaPost.push(insta);
    }
  }

  function instagramSubmit(offer_id){
    if(instaPost.length==0){
      return ;
    }
    if(confirm('Are you sure ? Be sure to selected the Right content this actions can not be changed')){
        console.log(instaPost);
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
        $.ajax({
            type: "POST",
            url: '/influencer/content/instagram/store',
            data: {contents:instaPost,offer_id:offer_id},
    
            success: function(response){
               console.log(response.data);
               window.location.replace('/influencer/content/index/instagram');
            },
            error:function(response){
            }
        })
    }
  }
