<?php

Route::get('/', function () {
    return view('frontend/home');
});

Route::get('/home', function () {
    return view('frontend/home');
});

Route::get('/admin', function () {
    return view('layouts.admin');
});

Route::get('/logout', function () {

    Auth::logout(auth()->user());
    return redirect('/');
})->name('logout');


Route::get('/company', function () {
    return view('frontend.company');
});

Route::get('/influencer', function () {
    return view('frontend.influencer');
});
Auth::routes();

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin',
    'middleware' => ['auth']
], function () {

    Route::get('/admin', 'HomeController@index')->name('home');
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');
    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');
    Route::resource('products', 'ProductsController');

});
Route::get('/sdk', function () {
    return view('sdk');
});


Route::group(['prefix' => 'admin', 'middleware' => ['adminWare']], function () {

    Route::group(['prefix' => 'campaigns'], function () {

        Route::get('/index', 'CampaignController@admin_index');
        Route::post('/loadmore', 'CampaignController@admin_loadmore');
        Route::get('/readmore/{id}', 'CampaignController@readmore');
        Route::delete('/delete/{id}', 'CampaignController@destroy');
        Route::get('/offers/{id}', 'OfferController@all_campaign_offer');

    });

    Route::group(['prefix' => 'reward'], function () {
        
        Route::get('index', 'InfluencerRewardController@admin');
        Route::post('store', 'InfluencerRewardController@store');
        Route::get('edit/{id}', 'InfluencerRewardController@edit');
        Route::post('destroy/{id}', 'InfluencerRewardController@destroy');

    });

    Route::group(['prefix' => 'paypal'], function () {
        
        Route::get('/index', 'PaypalController@index');
        Route::delete('/delete/{id}', 'PaypalController@destroy');

    });

    Route::group(['prefix' => 'chat'], function () {

        Route::get('/index', 'ChatController@index');    
        Route::delete('/destroy/{id}', 'ChatController@destroy');
        Route::delete('/mass_destroy', 'ChatController@mass_destroy');

    });
    
});












Route::get('/calendar', 'OfferController@calendar');


Route::post('/getCampaignData', 'CampaignController@getCampaignData');

Route::post('/pagination', 'CampaignController@index');



Route::group(['prefix' => 'notification', 'middleware' => ['auth']], function () {

    Route::get('mark_as_read/{id}', 'NotificationController@mark_as_read');
    Route::get('admin_all_notification', 'NotificationController@admin_notification_all');
    Route::delete('admin/delete/{id}', 'NotificationController@admin_destroy');

});

// Route::resource('/brand/campaigns','BrandCampaignsController');

Route::get('brand/register','RegisterController@brand_index');
Route::post('/brand/register','RegisterController@store');



Route::group(['prefix' => 'brand', 'middleware' => ['brandWare']], function () {

    // Route::get('/index','BrandInfluencerController@index');
    Route::view('/messages', 'brandProfile.messages');
    Route::view('/contact', 'brandProfile.contact');
    Route::view('/create', 'brandProfile.create');
    Route::post('/create', 'BrandController@create');
    Route::view('/individualProposal', 'brandProfile.individualProposal');
    Route::view('/requestDemo', 'brandProfile.requestDemo');


    Route::group(['prefix' => 'settings'], function () {

        Route::get('/', 'SettingController@viewGeneralSettings');
        Route::get('/getSettingsAndCountries', 'SettingController@getSettingsAndCountries');
        Route::post('/', 'SettingController@postGeneral');
        Route::get('/billing-address', 'SettingController@viewBillingAddress');
        Route::get('/getBillingAddressSettingsDataAndCountries', 'SettingController@getBillingAddressSettingsDataAndCountries');
        Route::post('/billing-address', 'SettingController@postBillingAddress');
        Route::get('/profile', 'SettingController@viewProfileImage');
        Route::get('/getProfileImage', 'SettingController@getProfileImage');
        Route::post('/profile', 'SettingController@postProfile');
        Route::view('/invoices', 'brandProfile.settings.invoices');
        Route::get('/created-brands', 'BrandController@view');
        Route::get('/activate-brand/{id}', 'BrandController@activateBrand');
        Route::get('/get-allowed-users/{id}', 'BrandController@getAllowedUsers');
        Route::post('/add-allowed-user/{id}', 'BrandController@addAllowedUser');
        Route::post('/delete-allowed-user/{id}', 'BrandController@deleteAllowedUser');
        Route::get('/notifications', 'ProfileController@payout_history');
        Route::get('/email-notifications', 'SettingController@viewEmailNotificationsSettings');
        Route::get('/getEmailNotificationsSettings', 'SettingController@getEmailNotificationsSettings');
        Route::post('/postEmailNotificationsSettings', 'SettingController@postEmailNotificationsSettings');
        Route::view('/password', 'brandProfile.settings.password');
        Route::post('/changePassword', 'SettingController@changePassword');
        Route::view('/remove-account', 'brandProfile.settings.removeAccount');

    });



    Route::group(['prefix' => 'content'], function () {
        
        Route::get('/index', 'ContentController@brand');
        Route::get('/index/{type}/{campaign_id?}', 'BrandContentController@index');


    });

    Route::group(['prefix' => 'campaigns'], function () {

        Route::view('/tips', 'brandProfile.campaigns.tips');
        Route::get('/index', 'BrandCampaignsController@index');
        Route::get('/select-social', 'BrandCampaignsController@select_social');
        Route::post('/select-social', 'BrandCampaignsController@select_social_store');
        Route::get('/create', 'BrandCampaignsController@create');
        Route::post('/store', 'BrandCampaignsController@store');
        Route::get('/show/{id}', 'BrandCampaignsController@show');
        Route::get('/edit/{id}', 'BrandCampaignsController@edit');
        Route::put('/update/{id}', 'BrandCampaignsController@update');
        Route::get('/success', 'BrandCampaignsController@success');
        Route::delete('/destroy/{id}', 'BrandCampaignsController@destroy');
        Route::get('/payment/{offer_id}', 'BrandCampaignsController@payment');

    });

    Route::group(['prefix' => 'influencer'], function () {

        Route::view('/tutorial', 'brandProfile.influencer.tutorial');
        Route::view('/subscription', 'brandProfile.influencer.subscription');
        Route::view('/subscription/99/subscribe', 'brandProfile.influencer.subscription.subscribe');
        Route::view('/list', 'brandProfile.influencer.list.list');
        Route::view('/list/show-all', 'brandProfile.influencer.list.show-all');
        Route::get('/list/getAllLists', 'ResearchListController@getAllLists');
        Route::post('/list/createList', 'ResearchListController@createList');
        Route::post('/list/updateList', 'ResearchListController@updateList');
        Route::post('/list/addInfluencerToList', 'ResearchListController@addInfluencerToList');
        Route::delete('/list/deleteList/{id}', 'ResearchListController@deleteList');
        Route::get('/list/{id}', 'ResearchListController@showSingleList');
        Route::get('/index', 'BrandInfluencerController@index');
        Route::get('/load_more_data/{id}', 'BrandInfluencerController@load_more_data');
        Route::post('/execute_search/{social}', 'BrandInfluencerController@execute_search');

    });

    Route::group(['prefix' => 'paypal'], function () {

        Route::post('/store', 'PaypalController@store');
        Route::get('test/{campaign_id}/{offer_id}', 'PaypalController@send_notification');

    });

    Route::get('/dashboard', 'BrandController@index');
});








Route::get('/info', 'ProfileController@info');

Route::post('/general_store', 'ProfileController@general_store');

Route::post('/uploadImage', 'ProfileController@uploadImage');






Route::group(['prefix' => 'influencer', 'middleware' => ['influencerWare']], function () {

    Route::get('/dashboard', 'HomeController@influencer');
    Route::resource('/compaigns', 'CampaignController');
    Route::resource('offer', 'OfferController');
    Route::get('offers', 'OfferController@index');
    Route::get('/campaign/offer/{id}', 'OfferController@campaign_offer');
    Route::get('/profile', 'ProfileController@index');


    Route::group(['prefix' => 'offer' ], function () {

        Route::get('/update/{id}/{status}', 'OfferController@updateOffer');
        Route::post('/channel', 'OfferController@channel');
        Route::get('/{id}', 'OfferController@show');

    });

    Route::group(['prefix' => 'cross'], function () {

        Route::get('index', 'CrossPromotionController@index');
        Route::get('create', 'CrossPromotionController@create');
        Route::post('store', 'CrossPromotionController@store');
        Route::get('show/{id}', 'CrossPromotionController@show');
        Route::post('loadmore', 'CrossPromotionController@loadmore');

    });

    Route::group(['prefix' => 'messages'], function () {

        Route::get('/index', 'InfluencerMessageController@index');

    });

    Route::group(['prefix' => 'reward'], function () {

        Route::get('/index', 'InfluencerRewardController@index');
        Route::get('/show/{id}', 'InfluencerRewardController@show');

    });

    Route::get('contact', function () {

        return view('influencerProfile.contact');
    });

    Route::group(['prefix' => 'content'], function () {

        Route::get('/index/{type}', 'ContentController@index');
        Route::post('/youtube', 'ContentController@youtube');
        Route::post('/instagram', 'ContentController@instagram');
        Route::post('/instagram/store', 'ContentController@instagram_store');
        Route::get('/campaign/{id}/{type}', 'ContentController@campaign');
        Route::post('instagramRedirect', 'ContentController@instagram_redirect');
        Route::post('/facebook/store','ContentController@facebook_store');
        Route::post('/facebook','ContentController@facebook');
        Route::get('/campaign/{id}/{type}','ContentController@campaign');
        Route::post('instagramRedirect','ContentController@instagram_redirect');
        Route::post('facebookRedirect','ContentController@facebook_redirect');
    });

    Route::group(['prefix' => 'channel'], function () {

        Route::get('/index', 'ChannelController@index');
        Route::post('/instagram/login', 'ChannelController@instagram_login');
        Route::post('/facebook/login', 'ChannelController@facebook_login');
        Route::get('/facebook/pages', 'ChannelController@facebook_pages');
        Route::post('/youtube/login', 'ChannelController@youtube_login');

    });

    Route::group(['prefix' => 'profile'], function () {

        Route::group(['prefix' => 'settings'], function () {

            Route::get('/profile', 'ProfileController@settings_profile');
            Route::post('/profile', 'ProfileController@profile_store');
            Route::get('/payout_history', 'ProfileController@payout_history');
            Route::get('/general', 'ProfileController@settings_general');
            Route::get('/addresses', 'ProfileController@settings_addresses');
            Route::post('/addresses', 'ProfileController@addresses_store');
            Route::get('/channels', 'ProfileController@channels');
            Route::get('/interests', 'ProfileController@interests');
            Route::get('/addresses', 'ProfileController@settings_addresses');
            Route::get('/payouts', 'ProfileController@payouts');
            Route::get('/password', 'ProfileController@password');
            Route::get('/terminate_account', 'ProfileController@terminate_account');
            Route::post('payment-method','ProfileController@payment_method');
            Route::post('change-password','ProfileController@change_password');

        });
    });
    
    Route::get('/profile', 'ProfileController@index');
    Route::resource('/profile', 'ProfileController');

});



Route::group(['prefix' => 'chat'], function () {

    Route::post('/store', 'ChatController@store');

});








Route::get('login/instagram', 'ChannelController@redirectToInstagramProvider')->name('instagram.login');

Route::get('login/instagram/callback', 'ChannelController@instagramProviderCallback')->name('login/instagram/callback');

Route::get('/instagram', 'ChannelController@instagram');

Route::get('/index_instagram', 'ChannelController@index_instagram');
